<?php
// 优化字体
if (!function_exists('admin_area_beautifier')) :

function admin_area_beautifier() { ?>
	<style type="text/css">
		#dashboard_right_now p.sub,
		#dashboard_recent_comments .comment-meta .approve,
		#plugin-information #section-screenshots li p,
		.wrap h2,
		#quicktags #ed_em,
		#ed_reply_toolbar #ed_reply_em,
		.tablenav .displaying-num,
		#footer,
		#footer a,
		p.help,
		p.description,
		span.description,
		.form-wrap p,
		#side-sortables .comments-box thead th,
		#normal-sortables .comments-box thead th,
		.howto,
		.inline-edit-row fieldset span.title,
		.inline-edit-row fieldset span.checkbox-title,
		#utc-time, #local-time,
		form.upgrade .hint,
		p.install-help
		
		{
			font-style: normal !important;
		}
		
		#adminmenu .wp-submenu a,
		.postbox p,
		.postbox ul,
		.postbox ol,
		.postbox blockquote,
		#wp-version-message,
		#dashboard_recent_comments .comment-meta .approve,
		#the-comment-list .comment-item p.row-actions,
		#dashboard_quick_press #media-buttons,
		#dashboard_recent_drafts h4 abbr,
		#plugin-information .fyi h2,
		h3,
		h4,
		h5,
		h6,
		.subsubsub,
		.widefat td,
		.widefat th,
		.widefat td p,
		.widefat td ol,
		.widefat td ul,
		kbd,
		code,
		.side-info,
		.submit input,
		.button,
		.button-primary,
		.button-secondary,
		.button-highlighted,
		#postcustomstuff .submit input,
		#wpcontent select,
		#howto,
		.tablenav .tablenav-pages,
		#wphead #site-visit-button,
		#adminmenu #awaiting-mod,
		#adminmenu span.update-plugins,
		#sidemenu li a span.update-plugins,
		.post-com-count span,
		.form-table td,
		#wpbody-content .describe td,
		.form-wrap p,
		.form-wrap label,
		.col-wrap h3,
		#post-status-info td,
		.tagchecklist span,
		.hndle a,
		#poststuff .inside,
		#poststuff .inside p,
		#poststuff .inside-submitbox,
		#side-sortables .inside-submitbox,
		#pending,
		.submitbox .submit,
		#replyrow,
		#edithead .inside,
		#edithead .inside input,
		#screen-meta a.show-settings,
		.inline-edit-row fieldset ul.cat-checklist label,
		.inline-edit-row .catshow,
		.inline-edit-row .cathide,
		.inline-edit-row #bulk-titles div,
		#favorite-actions a,
		form.upgrade .hint,
		.subtitle
		
		{
			font-size:12px !important;
		}
		

	</style>
<?php
}
add_action('admin_head', 'admin_area_beautifier');

endif;

function login_area_beautifier() {
?>
<style type="text/css">
	body,
	form .forgetmenot label
 {
	font-style: normal !important;
	font-size:12px !important;
}
</style>
<?php
}
add_action('login_head', 'login_area_beautifier');

?>