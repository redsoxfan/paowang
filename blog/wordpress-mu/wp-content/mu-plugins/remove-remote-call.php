﻿<?php
/*
Plugin Name: Remove WPMU remote calls
Plugin URI: http://blog.clearskys.net/
Description: If you are running WordPress MU on a local installation, without an internet connection, then it can be slowed down by repeated calls to external sites. This plugin removes all those calls.
Author: Barry at clearskys.net
Version: 0.3
Author URI: http://blog.clearskys.net/
*/

function wpmu_remove_nags() {
	// Added to remove update nag
	remove_action( 'admin_notices', 'update_nag', 3);
	remove_filter( 'update_footer', 'core_update_footer' );
}

function wpmu_remove_remotes() {
	
	remove_action( 'init', 'wp_version_check' );
	remove_action( 'load-plugins.php', 'wp_update_plugins' );
	remove_action( 'load-update.php', 'wp_update_plugins' );
	remove_action( 'admin_init', '_maybe_update_plugins' );
	remove_action( 'wp_update_plugins', 'wp_update_plugins' );

	remove_action( 'admin_init', '_maybe_update_themes' );
	remove_action( 'wp_update_themes', 'wp_update_themes' );
	// 0.3 - New action below
	add_action('admin_notices', 'wpmu_remove_nags', 1);

	if ( wp_next_scheduled('wp_update_plugins')) {
		wp_clear_scheduled_hook('wp_update_plugins');
	}
		
	if ( wp_next_scheduled('wp_update_themes') ) {
		wp_clear_scheduled_hook('wp_update_themes');
	}
	
}

add_action('init', 'wpmu_remove_remotes', 1);
?>
