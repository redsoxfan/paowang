<?php
/*
Plugin Name: Update Paowang On New Post
Plugin URI: http://redsox.blog.paowang.net/
Description: Update Paowang when a new post is published, so that Paowang main page and blog page can display the latest posts.
Version: 0.1
Author: Fan Wu
Author URI: http://redsox.blog.paowang.net/
*/

function curl_post_async($url, $params) {
    $post_params = array();		
    foreach ($params as $key => &$val) {
      if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);

    $parts=parse_url($url);

    $fp = fsockopen($parts['host'],
        isset($parts['port'])?$parts['port']:80,
        $errno, $errstr, 30);

    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out.= "Content-Length: ".strlen($post_string)."\r\n";
    $out.= "Connection: Close\r\n\r\n";
    if (isset($post_string)) $out.= $post_string;

    fwrite($fp, $out);
    fclose($fp);
}


// update paowang blog when a new post is published
function update_paowang() {
//    set_time_limit(12000);
    curl_post_async('http://dev.uupage.com/feed/fetchfeeds', array());
}

add_action('publish_post', 'update_paowang');
?>
