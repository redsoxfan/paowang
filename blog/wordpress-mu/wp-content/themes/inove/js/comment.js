/*
Author: mg12
Update: 2008/05/05
Author URI: http://www.neoease.com/
*/
(function() {

function reply(authorId, commentId, commentBox) {
	var author = MGJS.$(authorId).innerHTML;
	var insertStr = '<a href="#' + commentId + '">@' + author.replace(/\t|\n|\r\n/g, "") + ' </a> \n';
    window['MGJS_CMT']['prependText'] = insertStr;

    var field;
    if(MGJS.$(commentBox) && MGJS.$(commentBox).type == 'textarea') {
        field = MGJS.$(commentBox);

    } else {
        alert("The comment box does not exist!");
        return false;
    }
    field.focus();
	//appendReply(insertStr, commentBox);
}

function quote(authorId, commentId, commentBodyId, commentBox) {
	var author = MGJS.$(authorId).innerHTML;
	var comment = MGJS.$(commentBodyId).innerHTML;

	var insertStr = '<blockquote cite="#' + commentBodyId + '">';
	insertStr += '\n<strong><a href="#' + commentId + '">' + author.replace(/\t|\n|\r\n/g, "") + '</a> :</strong>';
	insertStr += comment.replace(/\t/g, "");
	insertStr += '</blockquote>\n';
    
    window['MGJS_CMT']['prependText'] = insertStr;

    var field;
    if(MGJS.$(commentBox) && MGJS.$(commentBox).type == 'textarea') {
        field = MGJS.$(commentBox);

    } else {
        alert("The comment box does not exist!");
        return false;
    }
    field.focus();    

	//insertQuote(insertStr, commentBox);
}

function appendReply(insertStr, commentBox) {
	if(MGJS.$(commentBox) && MGJS.$(commentBox).type == 'textarea') {
		field = MGJS.$(commentBox);

	} else {
		alert("The comment box does not exist!");
		return false;
	}

	if (field.value.indexOf(insertStr) > -1) {
		alert("You've already appended this reply!");
		return false;
	}

	if (field.value.replace(/\s|\t|\n/g, "") == '') {
		field.value = insertStr;
	} else {
		field.value = field.value.replace(/[\n]*$/g, "") + '\n\n' + insertStr;
	}
	field.focus();
}

function insertQuote(insertStr, commentBox) {
	if(MGJS.$(commentBox) && MGJS.$(commentBox).type == 'textarea') {
		field = MGJS.$(commentBox);

	} else {
		alert("The comment box does not exist!");
		return false;
	}

	if(document.selection) {
		field.focus();
		sel = document.selection.createRange();
		sel.text = insertStr;
		field.focus();

	} else if (field.selectionStart || field.selectionStart == '0') {
		var startPos = field.selectionStart;
		var endPos = field.selectionEnd;
		var cursorPos = startPos;
		field.value = field.value.substring(0, startPos)
					+ insertStr
					+ field.value.substring(endPos, field.value.length);
		cursorPos += insertStr.length;
		field.focus();
		field.selectionStart = cursorPos;
		field.selectionEnd = cursorPos;

	} else {
		field.value += insertStr;
		field.focus();
	}
}

window['MGJS_CMT'] = {};
window['MGJS_CMT']['reply'] = reply;
window['MGJS_CMT']['quote'] = quote;
window['MGJS_CMT']['prepareMgjsCommentText'] = prepareMgjsCommentText;
window['MGJS_CMT']['prependText'] = '';

function prepareMgjsCommentText(commentBox) {
    if (MGJS_CMT.prependText.replace(/\s|\t|\n/g, "") == '') {
        // no text to prepend
        return true;
    }
    var field;
    if(MGJS.$(commentBox) && MGJS.$(commentBox).type == 'textarea') {
        field = MGJS.$(commentBox);
    } else {
        alert("The comment box does not exist!");
        return false;
    }
    if (field.value.replace(/\s|\t|\n/g, "") == '') {
        return true;
    }
     
    field.value = window['MGJS_CMT']['prependText'] + '\n\n' + field.value.replace(/[\n]*$/g, "");
    return true; 
}

})();