	<div id="sidebar">
	<div id="sidebar-left">
		
			<div class="sidebar-box s-menu">
			<h2>Menu</h2>
			<ul  id="menu">
				<li <?php if(is_home()){echo 'class="current_page_item"';}?>><a href="<?php echo get_option('home'); ?>/" title="Home">Home</a></li>
	   	 		<?php wp_list_pages('title_li=&depth=1&sort_column=menu_order');?>
			</ul>
			
			<div class="s-menub"></div>
			</div>
			
			<div class="sidebar-box s-category">
						<h2>Categories</h2>
			<ul>
			<?php wp_list_categories('orderby=count&show_count=0&title_li=0&hierarchical=0&order=DESC'); ?>
			</ul>
			<div class="s-categoryb"></div>
			</div>
			
			<div class="sidebar-box s-ark">
			<h2>Archives</h2>
				<ul>
				<?php wp_get_archives('type=monthly'); ?>
				</ul>
			
			<div class="s-arkb"></div>
			</div>
			<div class="sidebar-box s-menu">
			<div class="s-menub"></div>
			</div>
			<div class="sidebar-box s-category">			
			<div class="s-categoryb"></div>
			</div>



			<div class="sidebar-box s-ark">
<?php /* If this is the frontpage */ if ( is_home()) { ?>
				<?php wp_list_bookmarks('title_li=&category_before=&category_after='); ?>
			<?php } ?>
			
			<div class="s-arkb"></div>
			</div>
			
	</div>

	<div id="sidebar-right">
			<div id="subfeed" >	
				<a href="<?php bloginfo('rss2_url'); ?>"><img height="50" border="0" width="215" alt="Sub via feed" title="Sub via feed" src="<?php bloginfo('template_url'); ?>/img/feed.gif"/></a>
			</div>
				<?php 	/* Widgetized sidebar, if you have the plugin installed. */
					if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>
										
					<div class="sidebar-box s-right">	
					<h2>Recent Posts</h2>
					<?php query_posts('showposts=6'); ?>
					<ul>
					<?php while (have_posts()) : the_post(); ?>
						<li>
							<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a>
						</li>
					<?php endwhile; ?>
					</ul>
			<div class="s-rightb"></div>
			</div>					
					
								<div class="sidebar-box s-right">	
			<h2>Meta</h2>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<li><a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional">Valid <abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a></li>
					<li><a href="http://gmpg.org/xfn/"><abbr title="XHTML Friends Network">XFN</abbr></a></li>
					<li><a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress</a></li>
					<?php wp_meta(); ?>
				</ul>
			<div class="s-rightb"></div>
			</div>
				<?php endif; ?>
				
			<div class="sidebar-box s-right">	
					<?php include (TEMPLATEPATH . "/searchform.php"); ?>
			<div class="s-rightb"></div>
			</div>

	</div>

	</div>

