<?php
function is_current_user_senior_editor() {
    global $current_user;
    get_currentuserinfo();
    if(array_key_exists('user_level', $current_user) && intval($current_user->user_level) == 7) {
        return true;
    }
    return false;
}
?>