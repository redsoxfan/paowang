<?php

class ApiController extends CommonController{

	protected $_redirector = null;

	public public function init()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_redirector = $this->_helper->getHelper('Redirector');
	}

	public function forumAction(){
		$theaction = $this->_getParam('method');
		if($theaction == null){
			$theaction = 'list';
		}
		Logging::debug("action is $theaction");
		if($theaction == 'list'){
			header('Content-type: text/xml; charset: utf-8');
			echo $this->forumGetList();
		}
	}

	private function forumGetList(){
		$xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><data />");
		$forums = $xml->addChild("forums");
		foreach(PaowangData::$orderedDisplayForumsById as $forumId =>$forumName){
			$singleForum = $forums->addChild("forum");
			$singleForum->addChild('id', $forumId);
			$this->addCDataChild($singleForum, 'name', $forumName);

		}
		return $xml->asXML();
	}

	public function postAction(){
		$theaction = $this->_getParam('method');
		if($theaction == null){
			$theaction = 'list';
		}
		$useCData = $this->_getParam('cdata');
		if($useCData === 'false'){
			$useCData = false;
		}
		else{
			$useCData = true;
		}
		header('Content-type: text/xml; charset: utf-8');
		if($theaction == 'list'){
			header('Content-type: text/xml; charset: utf-8');
			$forumId = intval($this->_getParam('forumId'));
			if($forumId == 0){
				$forumId = 1;
			}
			$page = intval($this->_getParam('page'));
			if($page == 0){
				$page = 1;
			}
			echo $this->postGetList($forumId, $page, $useCData);
		}
		else if($theaction == 'get'){
			$topId = intval($this->_getParam('topId'));
			if($topId == 0){
				echo "Invalid topId";
				return;
			}
			$topOnly = $this->_getParam('topOnly');
			if($topOnly === 'true'){
				$topOnly = true;
			}
			else{
				$topOnly = false;
			}
			echo $this->postGet($topId, $topOnly, $useCData);
		}
		else if($theaction == 'add'){
			echo $this->handlePostAdd();
		}
	}

	private function handlePostAdd(){

		$userName = $this->_getParam('username');
		$password = $this->_getParam('password');
		$replyId = intval($this->_getParam('replyId'));
		$forumId = intval($this->_getParam('forumId'));
		$title = $this->_getParam('title');
		$content = $this->_getParam('content');
		$tags = $this->_getParam('tags');
		if($userName == null || $password == null){
			echo $this->getResultXml(false, "请输入用户名和密码");
			return;
		}
		if($title == null){
			echo $this->getResultXml(false, "请输入标题");
			return;
		}
		$loginResult = ControllerUtils::login($userName, $password, 0);
		if(!$loginResult){
			echo $this->getResultXml(false, "您输入的用户名或密码不正确。");
			return;
		}
		if($forumId == 0){
			echo $this->getResultXml(false, "API错误：没有论坛ID!");
		}
		$userId = $loginResult;
		$isUserOpen = Rbm_RbmService::getInstance()->isOpenUser($userId);
		if(!$isUserOpen){
			echo $this->getResultXml(false, "此用户已被关闭。");
			return;
		}
		$canAddPost = Rbm_RbmService::getInstance()->hasPermission($userId, $forumId, Rbm_RbmService::USER_ADD_POST);
		if(!$canAddPost){
			echo $this->getResultXml(false, '只有剑的会员才可以在剑里加帖。请参考使用说明。');
			return;
		}

		$parameters = array();
		$parameters['forumId'] = $forumId;
		$parameters['userId'] = $userId;
		$parameters['userName'] = $userName;
		$parameters['title'] = $title;
		$parameters['text'] = "<div>$content</div>";
		$parameters['tags'] = $tags;
		// for replying a post
		$parameters['parentId'] = $replyId;
		$replyPost = false;
		if($replyId != null){
			$replyPost = true;
		}
		$postId = ControllerUtils::simpleAddPost($parameters, $replyPost);
		$postInfo = Post_PostDao::getInstance()->get($postId);

		$xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><data />");
		$posts = $xml->addChild("topic");
		$singlePost = $posts->addChild('post');
		$singlePost->addChild('postId', $postId);
		$this->addCDataChild($singlePost, 'title', $postInfo->getTitle());
		$this->addCDataChild($singlePost, 'author', $postInfo->getUserName());
		$this->addCDataChild($singlePost, 'content', $postInfo->getHtml());
		$singlePost->addChild('authorId', $postInfo->getUserId());
		$singlePost->addChild('time', $postInfo->getCreateAt());
		$singlePost->addChild('timeText', Util::date2string16($postInfo->getCreateAt()));
		$singlePost->addChild('parentId', $postInfo->getParentId());
		$singlePost->addChild('depth', $postInfo->getDepth());
		$singlePost->addChild('length', $postInfo->getTextLength());
		$singlePost->addChild('click', $postInfo->getClick());
		return $xml->asXML();
	}

	private function postGet($topId, $topOnly, $useCData){
		$postDao = Post_PostDao::getInstance();
		$postArray = $postDao->getPostListByTopId($topId);
		$postpicDao = Postpic_PostpicDao::getInstance();
		$picArray =  $postpicDao->getListByPostId($topId);
		$xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><data />");
		$posts = $xml->addChild("topic");
		foreach ($postArray as $row){
			$postInfo = $row["postInfo"];
			$parentId = $postInfo->getParentId();
			if($topOnly && intval($parentId) != 0){
				continue;
			}

			$postId = $postInfo->getPostId();
			$title = $postInfo->getTitle();
			$author = $postInfo->getUserName();
			$authorId = $postInfo->getUserId();
			$time = $postInfo->getCreateAt();
			$timeText = Util::date2string16($postInfo->getCreateAt());
			$content = $postInfo->getHtml();
			if($topId == $postId){
				$pictureContent = $this->getPictureContent($picArray);
				if($pictureContent != ''){
					$content = "<div>$pictureContent $content</div>";
				}
			}
			$depth = $postInfo->getDepth();

			$singlePost = $posts->addChild('post');
			$singlePost->addChild('postId', $postId);
			if($useCData){
				$this->addCDataChild($singlePost, 'title', $title);
				$this->addCDataChild($singlePost, 'author', $author);
				$this->addCDataChild($singlePost, 'content', $content);
			}
			else{
				$singlePost->addChild('title', $this->encode($title));
				$singlePost->addChild('author', $this->encode($author));
				$singlePost->addChild('content', $this->encode($content));
			}
			$singlePost->addChild('authorId', $authorId);
			$singlePost->addChild('time', $time);
			$singlePost->addChild('timeText', $timeText);
			$singlePost->addChild('parentId', $parentId);
			$singlePost->addChild('depth', $depth);
			$singlePost->addChild('length', $postInfo->getTextLength());
			$singlePost->addChild('click', $postInfo->getClick());
		}
		$xmlString = $xml->asXML();
		return $xmlString;
	}

	private function getPictureContent($pictureArray){
		$content = '';
		$config = Zend_Registry::get('config');
		foreach($pictureArray as $postpicInfo){
			$content = $content . '<p>';
			$content = $content . '<img src="' . $config->general->picture->url . $postpicInfo->getPicPath() . '" title="' . $postpicInfo->getPicDesc() . ' "/>';
			$content = $content . '<br />';
			$content = $content . '<div>' . $postpicInfo->getPicDesc() . '</div>';
			$content = $content . '<br /></p>';
		}
		if($content == ''){
			return '';
		}
		return "<div>$content</div>";
	}

	private function postGetList($forumId, $page, $useCData){
		$postDao = Post_PostDao::getInstance();
		$perpage = PaowangData::FORUM_POSTS_PER_PAGE;
		$postList = $postDao->getListByForumId($forumId, $perpage, $page, PaowangData::FORUM_TAB_ALL, false, true);
		$xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><data />");
		$posts = $xml->addChild("posts");
		foreach ($postList as $row) {
			$postInfo = $row["postInfo"];
			$singlePost = $posts->addChild('post');

			if($useCData){
				$this->addCDataChild($singlePost, 'title', $postInfo->getTitle());
				$this->addCDataChild($singlePost, 'author', $postInfo->getUserName());
			}
			else{
				$singlePost->addChild('title', $this->encode($postInfo->getTitle()));
				$singlePost->addChild('author', $this->encode($postInfo->getUserName()));
			}
			$singlePost->addChild('postId', $postInfo->getPostId());
			$singlePost->addChild('authorId', $postInfo->getUserId());
			$singlePost->addChild('time', $postInfo->getCreateAt());
			$singlePost->addChild('timeText', Util::date2string16($postInfo->getCreateAt()));
			$singlePost->addChild('length', $postInfo->getTextLength());
			$singlePost->addChild('click', $postInfo->getClick());
		}
		$xmlString = $xml->asXML();
		return $xmlString;
	}

	private function addCDataChild($xmlNode, $nodeName, $cdataText){
		$cdataNode = $xmlNode->addChild($nodeName);
		$node= dom_import_simplexml($cdataNode);
		$no = $node->ownerDocument;
		$node->appendChild($no->createCDATASection($cdataText));
	}

	private function encode($text){
		$rtn = htmlspecialchars($text);
		return  $rtn;
	}

	private function getResultXml($result, $message = null){
		$xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><data />");
		$resultText = "fail";
		if($result){
			$resultText = "success";
		}
		$xml->addChild("result", $resultText);
		if($message != null){
			$xml->addChild("message", $message);
		}
		return $xml->asXML();
	}

	public function loginAction(){
		$username = $this->_getParam('username');
		$password = $this->_getParam('password');
		$flag = intval($this->_getParam('rememberpass'));
		//    	$this->_redirector->gotoSimple('loginpro',
		//            'user',
		//            null,
		//            array(  'name' => $username,
		//                    'password' => $password,
		//                    'flag' => $flag
		//                    ));
		//    	$this->_redirect('/user/loginpro');
		$result = ControllerUtils::login($username, $password, $flag);
		header('Content-type: text/xml; charset: utf-8');
		echo $this->getResultXml($result);
	}
}