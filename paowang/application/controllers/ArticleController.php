<?php
    /**
     * Title  ArticleController
     */

    class ArticleController extends CommonController{ 
	    
	    function  indexAction(){
	        $this->_redirect('/article/list');
	    }
	    
	    function  listAction(){
	    	
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
	
			$perpage = 5;

            $articleDao = Article_ArticleDao::getInstance();
            $this->view->lists = $articleDao->getList($perpage,$page);
			$this->view->total= $articleDao->getListAllCount();
			$this->view->perpage= $perpage;
			$this->view->page= $page;
	    }
	    
	    function  newAction(){
            $articleInfo = new Article_ArticleInfo();
            $this->view->articleInfo = $articleInfo;
	    }
	    
	    function  addAction(){
            //request
            $forumId = $this->_getParam('forumId');
            $userId = $this->_getParam('userId');
            $userName = $this->_getParam('userName');
            $status = $this->_getParam('status');
            $createAt = $this->_getParam('createAt');
            $userIp = $this->_getParam('userIp');
            $updateAt = $this->_getParam('updateAt');
            $updateIp = $this->_getParam('updateIp');
            $title = $this->_getParam('title');
            $text = $this->_getParam('text');
            $userPostTypeId = $this->_getParam('userPostTypeId');
            
            //info
            $articleInfo = new Article_ArticleInfo();
            $articleInfo->setForumId($forumId);
            $articleInfo->setUserId($userId);
            $articleInfo->setUserName($userName);
            $articleInfo->setStatus($status);
            $articleInfo->setCreateAt($createAt);
            $articleInfo->setUserIp($userIp);
            $articleInfo->setUpdateAt($updateAt);
            $articleInfo->setUpdateIp($updateIp);
            $articleInfo->setTitle($title);
            $articleInfo->setText($text);
            $articleInfo->setUserPostTypeId($userPostTypeId);
            
            //dao
            $articleDao = Article_ArticleDao::getInstance();
            $articleDao->add($articleInfo);
			$this->_redirect('/article/list');
	    }
	    
	    function  editAction(){
			 $id = intval($this->_getParam('id'));
			 if(empty($id)){
				$this->_redirect('/article/list/');
			 }else{
	            $articleDao = Article_ArticleDao::getInstance();
	            $this->view->articleInfo = $articleDao->get($id);
			 }
	    }
	    	    
	    function  modifyAction(){
            //request
			$id = intval($this->_getParam('id'));
            $forumId = $this->_getParam('forumId');
            $userId = $this->_getParam('userId');
            $userName = $this->_getParam('userName');
            $status = $this->_getParam('status');
            $createAt = $this->_getParam('createAt');
            $userIp = $this->_getParam('userIp');
            $updateAt = $this->_getParam('updateAt');
            $updateIp = $this->_getParam('updateIp');
            $title = $this->_getParam('title');
            $text = $this->_getParam('text');
            $userPostTypeId = $this->_getParam('userPostTypeId');
            
            //dao,info
	        $articleDao = Article_ArticleDao::getInstance();
            $articleInfo = $articleDao->get($id);
            $articleInfo->setForumId($forumId);
            $articleInfo->setUserId($userId);
            $articleInfo->setUserName($userName);
            $articleInfo->setStatus($status);
            $articleInfo->setCreateAt($createAt);
            $articleInfo->setUserIp($userIp);
            $articleInfo->setUpdateAt($updateAt);
            $articleInfo->setUpdateIp($updateIp);
            $articleInfo->setTitle($title);
            $articleInfo->setText($text);
            $articleInfo->setUserPostTypeId($userPostTypeId);
            $articleDao->modify($articleInfo);
			$this->_redirect('/article/list');
	    	
	    }

	    function  viewAction(){
			$id = intval($this->_getParam('id'));
			if(empty($id)){
				$this->_redirect('/article/list/');
			}else{
	            $articleDao = Article_ArticleDao::getInstance();
	            $this->view->articleInfo = $articleDao->get($id);
			}
	    }	    	   
	    
	    function  delAction(){
			$articleDao = Article_ArticleDao::getInstance();
	    	$idArray = $this->_getParam('id');
	        if(is_array($idArray)){	    	
		    	foreach( $idArray as $id){
			    	$id = intval($id);
			    	if (!$id) {
			    	  //null
			    	}else {
			    		$articleDao->delById($id);
			    	}
		    	}
	        }else{
		    	$id = intval($idArray);
		    	if (!$id) {
		    		//null
		    	}else {
		    		$articleDao->delById($id);
		    	}
	        }
	    	$this->_redirect('/article/list');
	    }

	    function  ajaxlistAction(){
	    }

	    function  jsonlistAction(){
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
			$perpage = 5;
            $articleDao = Article_ArticleDao::getInstance();
	        $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $total = $articleDao->getListAllCount();
 		    $ajaxpage = new PageUtil(array('total'=>$total,'perpage'=>$perpage,'ajax'=>'ajax_page','nowindex'=>$page,'page_name'=>'page','url'=>"/article/jsonlist/"));
	        $data = array('list' => $articleDao->getList($perpage,$page) ,
	                      'total' => $total,
	                      'perpage' => $perpage,
	                      'page' => $page,
	                      'pagebar' => $ajaxpage->show()
	                );
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    
	    function  jsongetAction(){
		   $id = intval($this->_getParam('id'));
           $articleDao = Article_ArticleDao::getInstance();
           $articleInfo = $articleDao->get($id);
 	      
 	       $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
           $data = array('articleInfo' => $articleInfo);
	       $json = Zend_Json::encode($data);
	       echo $json;
	    }
	    	 
	    function  jsonsaveAction(){
	    	$id = intval($this->_getParam('id'));
            $forumId = $this->_getParam('forumId');
            $userId = $this->_getParam('userId');
            $userName = $this->_getParam('userName');
            $status = $this->_getParam('status');
            $createAt = $this->_getParam('createAt');
            $userIp = $this->_getParam('userIp');
            $updateAt = $this->_getParam('updateAt');
            $updateIp = $this->_getParam('updateIp');
            $title = $this->_getParam('title');
            $text = $this->_getParam('text');
            $userPostTypeId = $this->_getParam('userPostTypeId');
            
	        $articleDao = Article_ArticleDao::getInstance();
            $cmd ="";
            if($id>0){
            	//modify
            	$cmd = "modify";
		        $articleInfo = $articleDao->get($id);
                $articleInfo->setForumId($forumId);
                $articleInfo->setUserId($userId);
                $articleInfo->setUserName($userName);
                $articleInfo->setStatus($status);
                $articleInfo->setCreateAt($createAt);
                $articleInfo->setUserIp($userIp);
                $articleInfo->setUpdateAt($updateAt);
                $articleInfo->setUpdateIp($updateIp);
                $articleInfo->setTitle($title);
                $articleInfo->setText($text);
                $articleInfo->setUserPostTypeId($userPostTypeId);
		        $articleDao->modify($articleInfo);
            	
            }else{
                //add
                $cmd = "add";
	            //info
	            $articleInfo = new Article_ArticleInfo();
                $articleInfo->setForumId($forumId);
                $articleInfo->setUserId($userId);
                $articleInfo->setUserName($userName);
                $articleInfo->setStatus($status);
                $articleInfo->setCreateAt($createAt);
                $articleInfo->setUserIp($userIp);
                $articleInfo->setUpdateAt($updateAt);
                $articleInfo->setUpdateIp($updateIp);
                $articleInfo->setTitle($title);
                $articleInfo->setText($text);
                $articleInfo->setUserPostTypeId($userPostTypeId);
	            //dao
	            $articleDao->add($articleInfo);                
            }
		    $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $data = array('cmd'=>$cmd,'articleInfo' => $articleInfo);
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    	    
	    function  jsondelAction(){
	    	$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
			$articleDao = Article_ArticleDao::getInstance();
	    	$idArray = $this->_getParam('id');
	        if(is_array($idArray)){	    	
		    	foreach( $idArray as $id){
			    	$id = intval($id);
			    	if (!$id) {
			    	  //null
			    	}else {
			    		$articleDao->delById($id);
			    	}
		    	}
	        }else{
		    	$id = intval($idArray);
		    	if (!$id) {
		    		//null
		    	}else {
		    		$articleDao->delById($id);
		    	}
	        }
	        $json = Zend_Json::encode($idArray);
	        echo $json;
	    }
	    function __call($action, $arguments)
	    {
	        //return $this->defaultAction();
	        //throw new Zend_Controller_Exception('Invalid method called');
	        //echo "Action = " . $action . "<br />";
	        //echo "Arguments = " . $arguments;
	    }
	}
?>