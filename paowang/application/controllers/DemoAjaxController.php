<?php


    /**
     * Title  DemoController
     */

    class DemoAjaxController extends CommonController{ 
	    
	    function  indexAction(){
	        $this->_redirect('/demoajax/list');
	    }
	    
	    function  listAction(){
	    	
		/*	$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
	        $log = Zend_Registry::get('log');
            $log->debug( "################### page:".$page);
			$perpage = 5;

            $demoDao = Demo_DemoDao::getInstance();
            $this->view->lists = $demoDao->getList($perpage,$page);
			$this->view->total= $demoDao->getListAllCount();
			$this->view->perpage= $perpage;
			$this->view->page= $page;
			*/
	    }
	    
	    function  newAction(){
            $demoInfo = new Demo_DemoInfo();
            $this->view->demoInfo = $demoInfo;
	    }
	    
	    function  jsonlistAction(){
	    	
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
	        $log = Zend_Registry::get('log');
            $log->debug( "################### page:".$page);
			$perpage = 5;

            $demoDao = Demo_DemoDao::getInstance();
		    // disable autorendering for this action only:
	        $this->_helper->viewRenderer->setNoRender();
	        $total = $demoDao->getListAllCount();
 		    $ajaxpage = new PageUtil(array('total'=>$total,'perpage'=>$perpage,'ajax'=>'ajax_page','nowindex'=>$page,'page_name'=>'page','url'=>"/demo-ajax/jsonlist/"));
	        $data = array('list' => $demoDao->getList($perpage,$page) ,
	                      'total' => $total,
	                      'perpage' => $perpage,
	                      'page' => $page,
	                      'pagebar' => $ajaxpage->show()
	                );
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    
	    function  jsongetAction(){
			 $demoId = intval($this->_getParam('demoId'));
             $log = Zend_Registry::get('log');
             $log->debug( "################### demoId:".$demoId);				 
             $demoDao = Demo_DemoDao::getInstance();
             $demoInfo = $demoDao->get($demoId);

		    $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:

	        $data = array('demoInfo' => $demoInfo);
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    	 
	    	 
	    function  jsonsaveAction(){
	    	$demoId = intval($this->_getParam('demoId'));
            $demoVarchar = $this->_getParam('demoVarchar');
            $demoDate = $this->_getParam('demoDate');
            $demoLong = $this->_getParam('demoLong');
            $demoDouble = $this->_getParam('demoDouble');
            
	        $demoDao = Demo_DemoDao::getInstance();
            $cmd ="";
            if($demoId>0){
            	//modify
            	$cmd = "modify";
		        $demoInfo = $demoDao->get($demoId);
		        $demoInfo->setDemoVarchar($demoVarchar);
		        $demoInfo->setDemoDate($demoDate);
		        $demoInfo->setDemoLong($demoLong);
		        $demoInfo->setDemoDouble($demoDouble);
		        $demoDao->modify($demoInfo);
            	
            }else{
                //add
                $cmd = "add";
	            //info
	            $demoInfo = new Demo_DemoInfo();
	            $demoInfo->setDemoVarchar($demoVarchar);
	            $demoInfo->setDemoDate($demoDate);
	            $demoInfo->setDemoLong($demoLong);
	            $demoInfo->setDemoDouble($demoDouble);
	            //dao
	            $demoDao->add($demoInfo);                
            }
		    $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $data = array('cmd'=>$cmd,'demoInfo' => $demoInfo);
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    	    
	    function  jsondelAction(){
	    	$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
			$demoDao = Demo_DemoDao::getInstance();
	    	$demoIdArray = $this->_getParam('demoId');
	        if(is_array($demoIdArray)){	    	
		    	foreach( $demoIdArray as $demoId){
			    	$demoId = intval($demoId);
			    	if (!$demoId) {
			    	  //null
			    	}else {
			    		$demoDao->delById($demoId);
			    	}
		    	}
	        }else{
		    	$demoId = intval($demoIdArray);
		    	if (!$demoId) {
		    		//null
		    	}else {
		    		$demoDao->delById($demoId);
		    	}
	        }
	        $json = Zend_Json::encode($demoIdArray);
	        echo $json;
	    }

	    function __call($action, $arguments)
	    {
	        //return $this->defaultAction();
	        //throw new Zend_Controller_Exception('Invalid method called');
	        //echo "Action = " . $action . "<br />";
	        //echo "Arguments = " . $arguments;
	    }
	}
?>
