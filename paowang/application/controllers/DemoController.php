<?php

    /**
     * Title  DemoController
     */

    class DemoController extends CommonController{ 
	    
	    function  indexAction(){
	        $this->_redirect('/demo/list');
	    }
	    
	    function  listAction(){
	    	
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
	
			$perpage = 5;

            $demoDao = Demo_DemoDao::getInstance();
            $this->view->lists = $demoDao->getList($perpage,$page);
			$this->view->total= $demoDao->getListAllCount();
			$this->view->perpage= $perpage;
			$this->view->page= $page;
	    }
	    
	    function  newAction(){
            $demoInfo = new Demo_DemoInfo();
            $this->view->demoInfo = $demoInfo;
	    }
	    
	    function  addAction(){
            //request
            $demoVarchar = $this->_getParam('demoVarchar');
            $demoDate = $this->_getParam('demoDate');
            $demoLong = $this->_getParam('demoLong');
            $demoDouble = $this->_getParam('demoDouble');
            
            //info
            $demoInfo = new Demo_DemoInfo();
            $demoInfo->setDemoVarchar($demoVarchar);
            $demoInfo->setDemoDate($demoDate);
            $demoInfo->setDemoLong($demoLong);
            $demoInfo->setDemoDouble($demoDouble);
            
            //dao
            $demoDao = Demo_DemoDao::getInstance();
            $demoDao->add($demoInfo);
     	    $log = Zend_Registry::get('log');
            $log->debug( "### demo_id".$demoInfo->getDemoId());	            
			$this->_redirect('/demo/list');
	    }
	    
	    function  editAction(){
			 $demoId = intval($this->_getParam('demoId'));
             $log = Zend_Registry::get('log');
             $log->debug( "################### demoId:".$demoId);				 
			 if(empty($demoId))
			 {
				 $this->_redirect('/demo/list/');
			 }
			 else
			 {
	            $demoDao = Demo_DemoDao::getInstance();
	            $this->view->demoInfo = $demoDao->get($demoId);
			 }
	    }
	    	    
	    function  modifyAction(){
	    	

            //request
			$demoId = intval($this->_getParam('demoId'));
            $demoVarchar = $this->_getParam('demoVarchar');
            $demoDate = $this->_getParam('demoDate');
            $demoLong = $this->_getParam('demoLong');
            $demoDouble = $this->_getParam('demoDouble');
            
            //dao,info
	        $demoDao = Demo_DemoDao::getInstance();
            $demoInfo = $demoDao->get($demoId);
            $demoInfo->setDemoVarchar($demoVarchar);
            $demoInfo->setDemoDate($demoDate);
            $demoInfo->setDemoLong($demoLong);
            $demoInfo->setDemoDouble($demoDouble);
            $demoDao->modify($demoInfo);
			$this->_redirect('/demo/list');
	    	
	    }

	    function  viewAction(){
			$demoId = intval($this->_getParam('demoId'));
			if(empty($demoId))
			{
				$this->_redirect('/demo/list/');
			}else{
	            $demoDao = Demo_DemoDao::getInstance();
	            $this->view->demoInfo = $demoDao->get($demoId);
//	            $demoInfo = new Demo_DemoInfo();
//	            $demoInfo->setDemoId($demoId);
//	            $demoDao->load($demoInfo);
//	            $this->view->demoInfo = $demoInfo;
			}
	    }	    	   
	    
	    function  delAction(){
			$demoDao = Demo_DemoDao::getInstance();
	    	$demoIdArray = $this->_getParam('demoId');
	        if(is_array($demoIdArray)){	    	
		    	foreach( $demoIdArray as $demoId){
			    	$demoId = intval($demoId);
			    	if (!$demoId) {
			    	  //null
			    	}else {
			    		$demoDao->delById($demoId);
			    	}
		    	}
	        }else{
		    	$demoId = intval($demoIdArray);
		    	if (!$demoId) {
		    		//null
		    	}else {
		    		$demoDao->delById($demoId);
		    	}
	        }
	    	$this->_redirect('/demo/list');
	    }

	    function  ajaxlistAction(){
	    }


	    
	    function  jsongetAction(){
		   $demoId = intval($this->_getParam('demoId'));
           $demoDao = Demo_DemoDao::getInstance();
           $demoInfo = $demoDao->get($demoId);
 	      
 	       $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
           $data = array('demoInfo' => $demoInfo);
	       $json = Zend_Json::encode($data);
	       echo $json;
	    }
	    	 
	    function  jsonsaveAction(){
	    	$demoId = intval($this->_getParam('demoId'));
            $demoVarchar = $this->_getParam('demoVarchar');
            $demoDate = $this->_getParam('demoDate');
            $demoLong = $this->_getParam('demoLong');
            $demoDouble = $this->_getParam('demoDouble');
            
	        $demoDao = Demo_DemoDao::getInstance();
            $cmd ="";
            if($demoId>0){
            	//modify
            	$cmd = "modify";
		        $demoInfo = $demoDao->get($demoId);
		        $demoInfo->setDemoVarchar($demoVarchar);
		        $demoInfo->setDemoDate($demoDate);
		        $demoInfo->setDemoLong($demoLong);
		        $demoInfo->setDemoDouble($demoDouble);
		        $demoDao->modify($demoInfo);
            	
            }else{
                //add
                $cmd = "add";
	            //info
	            $demoInfo = new Demo_DemoInfo();
	            $demoInfo->setDemoVarchar($demoVarchar);
	            $demoInfo->setDemoDate($demoDate);
	            $demoInfo->setDemoLong($demoLong);
	            $demoInfo->setDemoDouble($demoDouble);
	            //dao
	            $demoDao->add($demoInfo);                
            }
		    $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $data = array('cmd'=>$cmd,'demoInfo' => $demoInfo);
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    	    
	    function  jsondelAction(){
	    	$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
			$demoDao = Demo_DemoDao::getInstance();
	    	$demoIdArray = $this->_getParam('demoId');
	        if(is_array($demoIdArray)){	    	
		    	foreach( $demoIdArray as $demoId){
			    	$demoId = intval($demoId);
			    	if (!$demoId) {
			    	  //null
			    	}else {
			    		$demoDao->delById($demoId);
			    	}
		    	}
	        }else{
		    	$demoId = intval($demoIdArray);
		    	if (!$demoId) {
		    		//null
		    	}else {
		    		$demoDao->delById($demoId);
		    	}
	        }
	        $json = Zend_Json::encode($demoIdArray);
	        echo $json;
	    }
	    
	    function __call($action, $arguments)
	    {
	        //return $this->defaultAction();
	        //throw new Zend_Controller_Exception('Invalid method called');
	        //echo "Action = " . $action . "<br />";
	        //echo "Arguments = " . $arguments;
	    }
	}
?>
