<?php
class FeedController extends CommonController
{
	public function init()
	{
		// Local to this controller only; affects all actions, as loaded in init:
		$this->_helper->viewRenderer->setNoRender(true);
	}

	public function preDispatch()
	{
		//$this->_helper->layout()->disableLayout();
		 
		//Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);
	}

	function noforumAction()

	{
		$forumId = intval($this->_getParam('id'));
		 
		$feed = new Feed_ForumFeed($forumId);
		//        header ("Pragma: no-cache");
		//        header('Content-Type: xml');
		$feed->send();
		//$feed->saveXml();
	}

	function paowangfeedAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$cacheId = Cache_Engine::generateId(Array('FeedController', 'paowangfeedAction'));
		$feeds = Cache_Engine::load($cacheId);
		if(!$feeds){
	        $feeds = Feed_BlogManager::getFeedsForMainPage();
            Cache_Engine::save($feeds, $cacheId, array(Cache_Engine::TAG_FEED_BLOG), 14400);
		}
        $json = Zend_Json::encode($feeds);
		echo $json;
	}

	function getentriesAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$feeds = Feed_BlogManager::getFeedsToManage();
		echo Zend_Json::encode($feeds);
//		echo $this->_helper->json($feeds);
	}
    
    function getblogsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $cacheId = Cache_Engine::generateId(Array('FeedController', 'getblogsAction'));
        $blogs = Cache_Engine::load($cacheId);
        if(!$blogs){
            $blogs = Blog_BlogDao::getInstance()->getBlogs();    
            Cache_Engine::save($blogs, $cacheId, array(Cache_Engine::TAG_FEED_BLOG), 14400);
        }
        echo Zend_Json::encode($blogs);
//        echo $this->_helper->json($feeds);
    }
    
    function getallblogsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $cacheId = Cache_Engine::generateId(Array('FeedController', 'getallblogsAction'));
        $blogs = Cache_Engine::load($cacheId);
        if(!$blogs){
            $blogs = Blog_BlogDao::getInstance()->getAll();    
            Cache_Engine::save($blogs, $cacheId, array(Cache_Engine::TAG_FEED_BLOG), 14400);
        }
        echo Zend_Json::encode($blogs);
//        echo $this->_helper->json($feeds);
    }    
	
    function fetchfeedsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $feeds = Feed_BlogManager::fetchFeeds();
        Cache_Engine::clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array(Cache_Engine::TAG_FEED_BLOG));
        echo "successful";
    }
    

	function updatetypesAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$typeDataString = $this->_getParam('typeData');
		$typeData = json_decode($typeDataString, true);
		Feed_BlogManager::updateType($typeData);
		Cache_Engine::clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array(Cache_Engine::TAG_FEED_BLOG));
		echo "{success: true, messages: { message: '更新成功！'}}";
	}

    function updateblogsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $blogDataString = $this->_getParam('blogData');
        $blogData = json_decode($blogDataString, true);
        foreach($blogData as $map){
        	$blogInfo = new Blog_BlogInfo();
        	$blogInfo->setAuthorId($map['authorId']);
        	$blogInfo->setAuthorName($map['authorName']);
        	$blogInfo->setBlogTitle($map['blogTitle']);
        	$blogInfo->setBlogUrl($map['blogUrl']);
        	$blogInfo->setStatus($map['status']);
        	Blog_BlogDao::getInstance()->update($blogInfo);
        }
        Cache_Engine::clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array(Cache_Engine::TAG_FEED_BLOG));
        echo "{success: true, messages: { message: '更新成功！'}}";
    }
    
    function fetchtitlesAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $blogs = Feed_BlogManager::fetchBlogTitles();	
        Cache_Engine::clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array(Cache_Engine::TAG_FEED_BLOG));
        echo Zend_Json::encode($blogs);
    }
	
	function blogbytypeAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$cacheId = Cache_Engine::generateId(Array('FeedController', 'blogbytypeAction'));
		$feeds = Cache_Engine::load($cacheId);
        if($feeds){
            echo $feeds;
            return;
        }
	   
	   $data = Feed_BlogManager::getFeedsByType();
	   $feeds = Zend_Json::encode($data);	 
	   Cache_Engine::save($feeds, $cacheId, array(Cache_Engine::TAG_FEED_BLOG), 14400);
	   echo $feeds;
	}

}
