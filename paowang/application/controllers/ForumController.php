<?php

class ForumController extends CommonController{ 
	
	/**
	 * init of ForumController
	 *
	 */
    public function init()
    {
    	parent::init();
    	// set the hightlight to be 1 so that FORUM tab is highlighted 
    	$this->view->highlight= PaowangData::NAVIGATION_BAR_FORUM;
        //tag 显示 
        $tagDao = Tag_TagDao::getInstance();
        $this->view->tagsArray = $tagDao ->getTagTop(24);     
    }	
	
	function indexAction(){
		$forumId = 0;
		$forumUrlName = $this->_getParam('forumUrlName');
		if($forumUrlName != null){
			$forumId = PaowangData::$forumsByUrl[$forumUrlName];
		}
        if($forumId==0){
        	$forumId = intval($this->_getParam('forumId'));
        }
        if($forumId==0){
            $forumId =  Util::getForumId();
        }else{
		    $expire = time()+60*60*24*30;//设定 cookie 保存30天
        	setcookie("forumId", $forumId, $expire, "/");
        }            
        if($forumId==0){
            $forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖绝色页面 
            setcookie("forumId", $forumId, $expire, "/");
        }     
        
        if(empty(PaowangData::$orderedForumsById[$forumId])){
        	 $this->_redirect("/index/notfound");
        }
        
        // 剑和客栈改成登录才能看到
        $this->view->internalForum = false;
        if(!empty(PaowangData::$forumIdRequiredLogin[$forumId])){
        	$loginUserId = Util::getUserId();
        	if($loginUserId <= 0){
        		$this->view->internalForum = true;
        		return;
        	}
        }
        
        
        
        $type = $this->_getParam('type');
        if(empty($type)){
            $type = PaowangData::FORUM_TAB_ALL;
        }
        
      
        
        //查看如果是江湖论剑，而用户没有权限，设置参数提示用户不能加帖
        $currentLoginUserId = Util::getUserId();
        $canAddPost = true;
        if($forumId == PaowangData::FORUM_ID_JIAN){
            $canAddPost = Rbm_RbmService::getInstance()->hasPermission($currentLoginUserId, $forumId, Rbm_RbmService::USER_ADD_POST);
        }
        $this->view->canAddPost = $canAddPost? 'true' : 'false';
        
        	         
        $listType = $this->_getParam('listType');
        if(empty($listType)){
            $listType =  Util::getListType();
        }else{
		    $expire = time()+60*60*24*30;//设定 cookie 保存30天
        	setcookie("listType", $listType, $expire, "/");
        } 	
        
        
        //显示风格
        $style = $this->_getParam('style');
        if(empty($style) || $style == ''){
            $style =  Util::getStyle();
        }else{
		    $expire = time()+60*60*24*30;//设定 cookie 保存30天
        	setcookie("style", $style, $expire, "/");
        }
        // Log the style stats
        $ipAddress = Util::getUserIp();
        Logging::error("user selects style: $ipAddress $style");
                        
        
        $isManage = intval($this->_getParam('isManage'));
        if($isManage>0){
           $isManage = 1;	
        }
        
            
        $this->view->forumId = $forumId;
        
        $forumName = Forum_ForumDao::getInstance()->getForumName($forumId);
        $this->view->forumName = $forumName;
            
        $page = intval($this->_getParam('page'));
        if(empty($page)) $page = 1;
        
        if($type == PaowangData::FORUM_TAB_ALL){
        	$this->handleIndexTypeAll($forumId, $page);
        }
        
        $perpage = PaowangData::FORUM_POSTS_PER_PAGE;    
        
        //如果是图片论坛，以列表方式展示的时候，每页显示20条
        if(PaowangData::isPhotoForum($forumId)){
        	$perpage = 20;       	
        } 
        
               
        if($listType=='pic' && PaowangData::isPhotoForum($forumId)){
           $perpage = PaowangData::FORUM_IMAGE_PER_PAGE;
           if($style=='classic'){
              $perpage = PaowangData::FORUM_IMAGE_PER_PAGE_CLASSIC;
           }
	       $postpicDao = Postpic_PostpicDao::getInstance();
           $this->view->lists = $postpicDao->getListByForumId($forumId,$perpage,$page);
		   $this->view->total= $postpicDao->getListAllCountByForumId($forumId);           
        }else{
           $postDao = Post_PostDao::getInstance();
           $this->view->lists = $postDao->getListByForumId($forumId, $perpage, $page, $type,$isManage);
           $this->view->total= $postDao->getListAllCountByForumId($forumId, $type,$isManage);
        }
        

        
        $this->view->perpage= $perpage;
        $this->view->page= $page;
        $this->view->type= $type;
        $this->view->listType= $listType;
        $this->view->isManage= $isManage;
        $this->view->style= $style;
	}


	/**
	 * 如果type==all，需要显示置顶贴和热门贴，这些贴子只在第一页显示
	 */
	private function handleIndexTypeAll($forumId, $page){
		if($page != 1){
			return;
		}
		$postDao = Post_PostDao::getInstance();
        $this->view->topLists = $postDao->getCeilPosts($forumId);
        $this->view->hotLists = $postDao->getHotPosts($forumId);
	}
	
	function mypostAction(){
	    $forumId = 0;
        $forumUrlName = $this->_getParam('forumUrlName');
        if($forumUrlName != null){
            $forumId = PaowangData::$forumsByUrl[$forumUrlName];
        }
        if($forumId==0){
            $forumId = intval($this->_getParam('forumId'));
        }
        if($forumId==0){ 
            $forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖论剑页面
        }
        
    	//显示风格
    	$style =  Util::getStyle();
		
        $this->view->forumId = $forumId;
        
        $forumName = Forum_ForumDao::getInstance()->getForumName($forumId);
        $this->view->forumName = $forumName;
            
        $page = intval($this->_getParam('page'));
        if(empty($page)) $page = 1;
        
        $userId = Util::getUserId();
        
        // Define type to highlight the tab
        $this->view->type= PaowangData::FORUM_TAB_MYPOST;
        
        $perpage = PaowangData::FORUM_MYPOSTS_PER_PAGE;           
        $postDao = Post_PostDao::getInstance();
        
        $this->view->myPostList = $postDao->getUserPostsByForumId($forumId, $userId, $perpage, $page, PaowangData::FORUM_TAB_MY_MAINPOST);
        $this->view->myPostTotal = $postDao->getListAllCountByForumId($forumId, PaowangData::FORUM_TAB_MY_MAINPOST,0,$userId);
        $this->view->myPostPerPage = $perpage;
        $this->view->myPostPage = $page;

        $this->view->myReplyList = $postDao->getUserPostsByForumId($forumId, $userId, $perpage, $page, PaowangData::FORUM_TAB_MY_REPLY);
        $this->view->myReplyTotal = $postDao->getListAllCountByForumId($forumId, PaowangData::FORUM_TAB_MY_REPLY,0,$userId);
        $this->view->myReplyPerPage = $perpage;
        $this->view->myReplyPage = $page;  
        $this->view->style= $style;     
	}

    function myvoteAction(){
        $forumId = 0;
        $forumUrlName = $this->_getParam('forumUrlName');
        if($forumUrlName != null){
            $forumId = PaowangData::$forumsByUrl[$forumUrlName];
        }
        if($forumId==0){
            $forumId = intval($this->_getParam('forumId'));
        }
        if($forumId==0){ 
            $forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖论剑页面
        }
        $this->view->forumId = $forumId;
        
        $forumName = Forum_ForumDao::getInstance()->getForumName($forumId);
        $this->view->forumName = $forumName;
            
        $page = intval($this->_getParam('page'));
        if(empty($page)) $page = 1;
        
        //显示风格
        $style =  Util::getStyle();
        
        $userId = Util::getUserId();
        
        // Define type to highlight the tab
        $this->view->type= PaowangData::FORUM_TAB_MYVOTE;
        
        //$perpage = PaowangData::FORUM_POSTS_PER_PAGE;           
        $perpage = PaowangData::FORUM_MYPOSTS_PER_PAGE;
        $postDao = Post_PostDao::getInstance();
        
        $this->view->yesList = $postDao->getUserPostsByForumId($forumId, $userId, $perpage, $page, PaowangData::FORUM_TAB_MY_VOTE_YES);
        $this->view->yesTotal = $postDao->getListAllCountByForumId($forumId, PaowangData::FORUM_TAB_MY_VOTE_YES,0,$userId);
        $this->view->yesPerPage = $perpage;
        $this->view->yesPage = $page;

        $this->view->noList = $postDao->getUserPostsByForumId($forumId, $userId, $perpage, $page, PaowangData::FORUM_TAB_MY_VOTE_NO);
        $this->view->noTotal = $postDao->getListAllCountByForumId($forumId, PaowangData::FORUM_TAB_MY_VOTE_NO,0,$userId);
        $this->view->noPerPage = $perpage;
        $this->view->noPage = $page;        
        $this->view->style= $style;     
    }	
    
    
	    function  myvoteyesjsonAction(){
	        $forumId = intval($this->_getParam('forumId'));
	        if($forumId==0){
	            $forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖论剑页面
	        } 			
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
			$perpage = PaowangData::FORUM_MYPOSTS_PER_PAGE;
			
			$style =  Util::getStyle();
			
			$userId = Util::getUserId();
			
            $postDao = Post_PostDao::getInstance();
	        $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $list = $postDao->getUserPostsByForumId($forumId, $userId, $perpage, $page, PaowangData::FORUM_TAB_MY_VOTE_YES);
	        $total = $postDao->getListAllCountByForumId($forumId, PaowangData::FORUM_TAB_MY_VOTE_YES,0,$userId);
 		    $ajaxpage = new PageUtil(array('total'=>$total,'perpage'=>$perpage,'ajax'=>'yes_ajax_page','nowindex'=>$page,'page_name'=>'page','url'=>"/forum/myvoteyesjson/forumId/$forumId/"));
		 	$html = "";
		    foreach ($list as $row) { 
		        $postInfo = $row["postInfo"];
		        $html .= Util::singleMainPostWithStyle($postInfo, $style);
		        
		        //不显示跟贴
		        /*
		        if($row["childs"]!=null){
		        	foreach ($row["childs"] as $postInfoChild) { 
		        		$html .= Util::forumChildPost($postInfoChild);
		            } //end foreach循环子帖
		        }// end if 子贴非空
		        */
		    }//endforeach 循环主贴
            //$html = "adfaga".$page;
            //'list' => $list ,
	        $data = array('html' => $html ,   
	                      'total' => $total,
	                      'perpage' => $perpage,
	                      'page' => $page,
	                      'pagebar' => $ajaxpage->show($this->getAjaxPageShowNumber($style))
	                );
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    
	    function  myvotenojsonAction(){
	        $forumId = intval($this->_getParam('forumId'));
	        if($forumId==0){
	            $forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖论剑页面
	        } 			
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
			$perpage = PaowangData::FORUM_MYPOSTS_PER_PAGE;
			$style =  Util::getStyle();
			
			$userId = Util::getUserId();
			
            $postDao = Post_PostDao::getInstance();
	        $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $list = $postDao->getUserPostsByForumId($forumId, $userId, $perpage, $page, PaowangData::FORUM_TAB_MY_VOTE_NO);
	        $total = $postDao->getListAllCountByForumId($forumId, PaowangData::FORUM_TAB_MY_VOTE_NO,0,$userId);
 		    $ajaxpage = new PageUtil(array('total'=>$total,'perpage'=>$perpage,'ajax'=>'no_ajax_page','nowindex'=>$page,'page_name'=>'page','url'=>"/forum/myvotenojson/forumId/$forumId/"));
		 	$html = "";
		    foreach ($list as $row) { 
		        $postInfo = $row["postInfo"];
		        $html .= Util::singleMainPostWithStyle($postInfo, $style);
		        //不显示跟贴
		        /*
		        if($row["childs"]!=null){
		        	foreach ($row["childs"] as $postInfoChild) { 
		        		$html .= Util::forumChildPost($postInfoChild);
		            } //end foreach循环子帖
		        }// end if 子贴非空
		        */
		    }//endforeach 循环主贴
            //$html = "adfaga".$page;
            //'list' => $list ,
	        $data = array('html' => $html ,   
	                      'total' => $total,
	                      'perpage' => $perpage,
	                      'page' => $page,
	                      'pagebar' => $ajaxpage->show($this->getAjaxPageShowNumber($style))
	                );
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }	    
	        
	        
	    function  mypostmainjsonAction(){
	        $forumId = intval($this->_getParam('forumId'));
	        if($forumId==0){
	            $forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖论剑页面
	        } 			
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
			$perpage = PaowangData::FORUM_MYPOSTS_PER_PAGE;
			$style =  Util::getStyle();
			$userId = Util::getUserId();
			
            $postDao = Post_PostDao::getInstance();
	        $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $list = $postDao->getUserPostsByForumId($forumId, $userId, $perpage, $page, PaowangData::FORUM_TAB_MY_MAINPOST);
            $total = $postDao->getListAllCountByForumId($forumId, PaowangData::FORUM_TAB_MY_MAINPOST,0,$userId);

 		    $ajaxpage = new PageUtil(array('total'=>$total,'perpage'=>$perpage,'ajax'=>'main_ajax_page','nowindex'=>$page,'page_name'=>'page','url'=>"/forum/mypostmainjson/forumId/$forumId/"));
		 	$html = "";
		    foreach ($list as $row) { 
		        $postInfo = $row["postInfo"];
		        $html .= Util::singleMainPostWithStyle($postInfo, $style);
		        if($row["childs"]!=null){
		        	if($style == 'classic'){
		        		$html .= "<div class=\"post_replay\">\n";
		        	}
		        	foreach ($row["childs"] as $postInfoChild) { 
		        		$html .= Util::singleChildPostWithStyle($postInfoChild, $style);
		            } //end foreach循环子帖
		            if($style == 'classic'){
                        $html .= "</div>\n";
                    }
		        }// end if 子贴非空
		    }//endforeach 循环主贴
            //$html = "adfaga".$page;
            //'list' => $list ,
	        $data = array('html' => $html ,   
	                      'total' => $total,
	                      'perpage' => $perpage,
	                      'page' => $page,
	                      'pagebar' => $ajaxpage->show($this->getAjaxPageShowNumber($style))
	                );
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    
	    function  mypostchildjsonAction(){
	        $forumId = intval($this->_getParam('forumId'));
	        if($forumId==0){
	            $forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖论剑页面
	        } 			
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
			$perpage = PaowangData::FORUM_MYPOSTS_PER_PAGE;
			$style =  Util::getStyle();
			$userId = Util::getUserId();
			
            $postDao = Post_PostDao::getInstance();
	        $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
            $list = $postDao->getUserPostsByForumId($forumId, $userId, $perpage, $page, PaowangData::FORUM_TAB_MY_REPLY);
            $total = $postDao->getListAllCountByForumId($forumId, PaowangData::FORUM_TAB_MY_REPLY,0,$userId);
        
 		    $ajaxpage = new PageUtil(array('total'=>$total,'perpage'=>$perpage,'ajax'=>'child_ajax_page','nowindex'=>$page,'page_name'=>'page','url'=>"/forum/mypostchildjson/forumId/$forumId/"));
		 	$html = "";
		    foreach ($list as $row) { 
		        $postInfo = $row["postInfo"];
		        $html .= Util::singleMainPostWithStyle($postInfo, $style);
		        if($row["childs"]!=null){
		            if($style == 'classic'){
                        $html .= "<div class=\"post_replay\">\n";
                    }
		        	foreach ($row["childs"] as $postInfoChild) { 
		        		$html .= Util::singleChildPostWithStyle($postInfoChild, $style);
		            } //end foreach循环子帖
		            if($style == 'classic'){
                        $html .= "</div>\n";
                    }
		        }// end if 子贴非空
		    }//endforeach 循环主贴
            //$html = "adfaga".$page;
            //'list' => $list ,
	        $data = array('html' => $html ,   
	                      'total' => $total,
	                      'perpage' => $perpage,
	                      'page' => $page,
	                      'pagebar' => $ajaxpage->show($this->getAjaxPageShowNumber($style))
	                );
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    
	    private static function getAjaxPageShowNumber($style = ""){
	    	if($style == 'classic'){
	    		return 8;
	    	}
	    	return 7;
	    }
	            
    function addpostAction(){
        $forumId = intval($this->_getParam('forumId'));
        if($forumId==0){
            $forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖论剑页面
        } 
        $this->view->forumId = $forumId;
        $forumName = Forum_ForumDao::getInstance()->getForumName($forumId);
        $this->view->forumName = $forumName;
        
        //request
        $userId = Util::getUserId();
        $userName = Util::getUserName();
        $parentId = 0;
        $topId = 0;//?
        $isDigest = "0";
        $title = $this->_getParam('title');
        $depth = 1;
        $showOrder = 1;
        $listOrder = 1;
        $canHaschild = 1;
        $isTail = 1;
        $status = 1;
        $createAt = time();
        $userIp = Util::getUserIp();
        $click = 0;
        $tags = $this->_getParam('tags');
        $updateAt = time();
        $updateIp = $userIp;
        $isCeil = 0;
        $isRelive = 0;
        $reliveTime = 0;
        $userPostTypeId = 0;
        $childType = 0;
        $isPic = 0;
        $childPosts = 1;
        
        //text and length
        $html = $this->_getParam('text');
        $html = str_replace("\\\"", "\"", $html);//替换一下\"

        
        $text = Util::stripText($html);
        $textLength = Util::strLenUtf8($text);
        
        //info
        $postInfo = new Post_PostInfo();
        $postInfo->setForumId($forumId);
        $postInfo->setUserId($userId);
        $postInfo->setUserName($userName);
        $postInfo->setParentId($parentId);
        $postInfo->setTopId($topId);
        $postInfo->setIsDigest($isDigest);
        $postInfo->setTitle($title);
        $postInfo->setDepth($depth);
        $postInfo->setShowOrder($showOrder);
        $postInfo->setListOrder($listOrder);
        $postInfo->setCanHaschild($canHaschild);
        $postInfo->setIsTail($isTail);
        $postInfo->setStatus($status);
        $postInfo->setCreateAt($createAt);
        $postInfo->setUserIp($userIp);
        $postInfo->setClick($click);
        $postInfo->setTags($tags);
        
        $postInfo->setTextLength($textLength);
        $postInfo->setUpdateAt($updateAt);
        $postInfo->setUpdateIp($updateIp);
        $postInfo->setIsCeil($isCeil);
        $postInfo->setIsRelive($isRelive);
        $postInfo->setReliveTime($reliveTime);
        $postInfo->setUserPostTypeId($userPostTypeId);
        $postInfo->setChildType($childType);
        $postInfo->setIsPic($isPic);
        $postInfo->setChildPosts($childPosts);
        
        //dao
        $postDao = Post_PostDao::getInstance();
        $postDao->add($postInfo);
        $postId = $postInfo->getPostId();
        $postInfo->setTopId($postId);
        $postDao->modify($postInfo);
        
        //info
        $posttextInfo = new Posttext_PosttextInfo();
        $posttextInfo->setPostId($postId);
        $posttextInfo->setText($text);
        $posttextInfo->setHtml($html);
        
        
        $tagArray =   preg_split ('/([\s,;]+)/', $tags);
        $tagDao = Tag_TagDao::getInstance();
        foreach($tagArray as $tagName){
            $tagInfo = new Tag_TagInfo();
            $tagInfo->setPostId($postId);
            $tagInfo->setTagName($tagName);
            $tagInfo->setStatus(1);
            $tagInfo->setUserId($userId);
            $tagInfo->setCreateAt($createAt);
            $tagInfo->setUserIp($userIp);
            $tagInfo->setTagType(1);//帖子
            $tagInfo->setTagUserId($userId);//帖子发布者的ID
            
            if($tagDao->getListAllCountByUserIdAndCreateUserIdAddTagNameAndTopId($userId,$postInfo->getUserId(),$tagName,$postInfo->getTopId())<1){
              $tagDao->add($tagInfo);    
            }           
        }
        
        //dao
        $posttextDao = Posttext_PosttextDao::getInstance();
        $posttextDao->add($posttextInfo);
        
        //图片上传 开始
        //$postDao->uploadPictures($postId,"pic_files");
        $postDao->uploadPictures($postId,$_FILES,"pic_files");
        
        //图片上传 结束
        //$log = Zend_Registry::get('log');
        //$log->debug("postAddAction end");
        //$this->_redirect("/post/postaddpicpreview/postId/$postId");           	
    }
    
    /**
     * display the page to add new post
     *
     */
    function postAction(){
    	$forumId = intval($this->_getParam('forumId'));
        if($forumId==0){
            $forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖论剑页面
        } 
        $this->view->forumId = $forumId;
    	$forumName = Forum_ForumDao::getInstance()->getForumName($forumId);
        $this->view->forumName = $forumName;
    	
    }
    
    /*
     * 
     */ 
    function listbytagnameAction(){
    	$tagName = $this->_getParam('tagName');
        
        $page = intval($this->_getParam('page'));
        if(empty($page)) $page = 1;
        
        $perpage = PaowangData::TAG_POSTS_PER_PAGE;   
        
        $postDao = Post_PostDao::getInstance();
        
        
        
        $this->view->lists = $postDao->getListByTagName($tagName,$perpage,$page);
        $this->view->total=  $postDao->getListByTagNameAllCount($tagName,$perpage,$page);
        $this->view->perpage= $perpage;
        $this->view->page= $page;
        $this->view->tagName = $tagName;
        

    	
    }    
    
    
    function backendlefteditAction(){
    	//$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	    //$json = Zend_Json::encode($_SERVER);    	
	        	//$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	    //$json = Zend_Json::encode($_SERVER);    	
        $userId = Util::getUserId();
        $isAdministrator = Rbm_RbmService::getInstance()->isAdministrator($userId);
        
	    $fileName = $_SERVER['DOCUMENT_ROOT']."/../../picture/data/hottag.php";
	    $content = file_get_contents($fileName);
	    $this->view->content= $content;
	    $this->view->isAdministrator= $isAdministrator;
    }
    
    function backendlefteditsaveAction(){
    	$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
    	$content = $this->_getParam('content');
    	        $content = str_replace("\\\"", "\"", $content);//替换一下\"
    	        	
	    $fileName = $_SERVER['DOCUMENT_ROOT']."/../../picture/data/hottag.php";
	    $rtn = file_put_contents($fileName, $content);
        $userId = Util::getUserId();
        $isAdministrator = Rbm_RbmService::getInstance()->isAdministrator($userId);
        if(!$isAdministrator){
        	echo "{success: false, messages: { message: '您没有权限修改，请联系系统管理员!'}}";
        	return ;
        }
	    if($rtn>0){
	    	 echo "{success: true, messages: { message: '保存成功!'}}";
	    }else{
	    	 echo "{success: false, messages: { message: '保存失败，请检查!'}}";
	    }	
    }    


    function backendkeyeditAction(){
    	//$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	    //$json = Zend_Json::encode($_SERVER);    	
	        	//$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	    //$json = Zend_Json::encode($_SERVER);    	
        $userId = Util::getUserId();
        $isAdministrator = Rbm_RbmService::getInstance()->isAdministrator($userId);
        
	    $fileName = $_SERVER['DOCUMENT_ROOT']."/../../picture/data/key.txt";
	    $content = file_get_contents($fileName);
	    $this->view->content= $content;
	    $this->view->isAdministrator= $isAdministrator;
    }
    
    function backendkeyeditsaveAction(){
    	$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
    	$content = $this->_getParam('content');
    	$content = str_replace("\\\"", "\"", $content);//替换一下\"
    	        	
	    $fileName = $_SERVER['DOCUMENT_ROOT']."/../../picture/data/key.txt";
	    $rtn = file_put_contents($fileName, $content);
     	$canManageForum = Rbm_RbmService::getInstance()->hasPermission(
    		Util::getUserId(), PaowangData::FORUM_ID_PHOTO, Rbm_RbmService::ADMIN_MANAGE_FORUM);//江湖色的版主可以编辑
    	        
        if(!$canManageForum){
        	echo "{success: false, messages: { message: '您没有权限修改，请联系系统管理员!'}}";
        	return ;
        }
	    if($rtn>0){
	    	 echo "{success: true, messages: { message: '保存成功!'}}";
	    }else{
	    	 echo "{success: false, messages: { message: '保存失败，请检查!'}}";
	    }	
    }

    function backendindexpicturesgenAction(){
    	 $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
    	 $postDao = Post_PostDao::getInstance();
         $arrayPhoto =  $postDao->getIndexPics(PaowangData::FORUM_ID_PHOTO,3);
         $arrayChuangketie =  $postDao->getIndexPics(PaowangData::FORUM_ID_CHUANGKETIE,5);         
         $pics = $arrayPhoto['pics']."|".$arrayChuangketie['pics'];
         $links = $arrayPhoto['links']."|".$arrayChuangketie['links'];
         $texts = $arrayPhoto['texts']."|".$arrayChuangketie['texts'];
         $texts = str_replace("&",'',$texts);
         $texts = str_replace("'","\'",$texts);
         if(Util::endsWith($pics,"|")){
         	$pics= substr($pics,0,strlen($pics)-strlen("|"));
         }
         if(Util::endsWith($links,"|")){
         	$links= substr($links,0,strlen($links)-strlen("|"));
         }
         if(Util::endsWith($texts,"|")){
         	$texts= substr($texts,0,strlen($texts)-strlen("|"));
         }                  
    	 $html = "";
		 
		 //$html .="   var focus_width=480;\n";
		 //$html .="    var focus_height=300;\n";
		 //$html .="    var text_height=30;\n";
		 //$html .="    var swf_height = focus_height+text_height;\n";
		 //$html .="    \n";
		 //$html .="    var pics='".$pics."';\n";
		 //$html .="    var links='".$links."';\n";
		 //$html .="    var texts='".$texts."';\n";
		 //$html .="    \n";
		 //$html .="    document.write('<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0\" width=\"'+ focus_width +'\" height=\"'+ swf_height +'\">');\n";
		 //$html .="    document.write('<param name=\"allowScriptAccess\" value=\"sameDomain\"><param name=\"movie\" value=\"images/pixviewer.swf\"><param name=\"quality\" value=\"high\"><param name=\"bgcolor\" value=\"#E8EBF2\">');\n";
		 //$html .="    document.write('<param name=\"menu\" value=\"false\"><param name=wmode value=\"opaque\">');\n";
		 //$html .="    document.write('<param name=\"FlashVars\" value=\"pics='+pics+'&links='+links+'&texts='+texts+'&borderwidth='+focus_width+'&borderheight='+focus_height+'&textheight='+text_height+'\">');\n";
		 //$html .="    document.write('<embed src=\"/images/pixviewer.swf\" wmode=\"opaque\" FlashVars=\"pics='+pics+'&links='+links+'&texts='+texts+'&borderwidth='+focus_width+'&borderheight='+focus_height+'&textheight='+text_height+'\" menu=\"false\" bgcolor=\"#ffffff\" quality=\"high\" width=\"'+ focus_width +'\" height=\"'+ focus_height +'\" allowScriptAccess=\"sameDomain\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />');     document.write('</object>');\n";
		$html .="var swf_width=480;\n";
		$html .="var swf_height=330;\n";
		$html .="var config='10|0xffffff|0x0099ff|50|0xffffff|0x0099ff|0x000000ss';\n";
		$html .="var pics='$pics';\n";
		$html .="var links='$links';\n";
		$html .="var texts='$texts';\n";
		$html .="document.write('<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0\" width=\"'+ swf_width +'\" height=\"'+ swf_height +'\">');\n";
		$html .="document.write('<param name=\"movie\" value=\"<?php echo \$this->resourceDir; ?>/images/focusX.swf\" />');\n";
		$html .="document.write('<param name=\"quality\" value=\"high\" />');\n";
		$html .="document.write('<param name=\"menu\" value=\"false\" />');\n";
		$html .="document.write('<param name=wmode value=\"opaque\" />');\n";
		$html .="document.write('<param name=\"FlashVars\" value=\"config='+config+'&bcastr_flie='+pics+'&bcastr_link='+links+'&bcastr_title='+texts+'\" />');\n";
		$html .="document.write('<embed src=\"<?php echo \$this->resourceDir; ?>/images/focusX.swf\" wmode=\"opaque\" FlashVars=\"config='+config+'&bcastr_flie='+pics+'&bcastr_link='+links+'&bcastr_title='+texts+'& menu=\"false\" quality=\"high\" width=\"'+ swf_width +'\" height=\"'+ swf_height +'\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />');\n";
		$html .="document.write('</object>');\n\n";
		 
		 $config = Zend_Registry::get('config');
         $data_path = $config->data->path ;         
	     $fileName = $data_path . "/indexpics.php";
	     
	     $rtn = file_put_contents($fileName, $html);
	     if($rtn>0){
	    	 echo "{success: true, messages: { message: '生成成功!'}}";
	     }else{
	    	 echo "{success: false, messages: { message: '生成失败，请检查!'}}";
	     }		 
     }


    function piclistAction(){
		$forumId = intval($this->_getParam('forumId'));
        if($forumId==0){
            $forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖论剑页面
        } 
        
        $log = Zend_Registry::get('log');
        $log->debug("forumId".$forumId);
           
        $type = $this->_getParam('type');
        if(empty($type)){
            $type = PaowangData::FORUM_TAB_ALL;
        }
        
        
        $listType = $this->_getParam('listType');
        if(empty($listType)){
            $listType = "list";
        }
        
        
        $isManage = intval($this->_getParam('isManage'));
        if($isManage>0){
           $isManage = 1;	
        }
        
            
        $this->view->forumId = $forumId;
        
        $forumName = Forum_ForumDao::getInstance()->getForumName($forumId);
        $this->view->forumName = $forumName;
            
        $page = intval($this->_getParam('page'));
        if(empty($page)) $page = 1;
        
        if($type == PaowangData::FORUM_TAB_ALL){
        	$this->handleIndexTypeAll($forumId, $page);
        }
        
        $perpage = PaowangData::FORUM_POSTS_PER_PAGE;           
        
        if($listType=='pic'){
           $perpage = PaowangData::FORUM_IMAGE_PER_PAGE;
	       $postpicDao = Postpic_PostpicDao::getInstance();
           $this->view->lists = $postpicDao->getListByForumId($forumId,$perpage,$page);
		   $this->view->total= $postpicDao->getListAllCountByForumId($forumId);           
        }else{
           $postDao = Post_PostDao::getInstance();
           $this->view->lists = $postDao->getListByForumId($forumId, $perpage, $page, $type,$isManage);
           $this->view->total= $postDao->getListAllCountByForumId($forumId, $type,$isManage);
        }
        $this->view->perpage= $perpage;
        $this->view->page= $page;
        $this->view->type= $type;
        $this->view->listType= $listType;
        $this->view->isManage= $isManage;
    }
	
	function __call($action, $arguments){
	}
}
?>
