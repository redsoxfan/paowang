<?php
class IndexController extends CommonController
{
    /*
     * 首页
     */
    function indexoldAction(){
        $this->view->highlight= PaowangData::NAVIGATION_BAR_MAINPAGE;
    }
    
    function indexAction(){
        $this->view->highlight= PaowangData::NAVIGATION_BAR_MAINPAGE;
    }

    function forumdataAction(){
    	$forumId = intval($this->_getParam('forumId'));
    	
        $map = array();
        foreach(PaowangData::$orderedDisplayForumsById as $forumId =>$forumName){
            $forumDataArray = $this->getForumData($forumId);
            $map[$forumId] = array();
            $map[$forumId]['name'] = $forumName;
            $map[$forumId]['data'] = $forumDataArray; 
        }
        
        Zend_Json::encode($map);   
    }
    
    // Right now don't use this function
    private function getForumData($forumId){
        $postDao = Post_PostDao::getInstance();
        $type = "new";
        $page = 1;
        $perpage = 5;
        $posts = $postDao->getHotPosts($forumId, 5);
        
        $data = array();
        foreach($posts as $post){
            $postInfo = $post["postInfo"];
            array_push($data, array(
               'title'=>$postInfo->getTitle(),
               'note'=>$postInfo->getUserName(),
               'titleLink'=>'/post/' . $postInfo->getPostId(),
               'noteLink'=>''
            ));
        }
        $forumName = PaowangData::$orderedDisplayForumsById[$forumId];
        $forumUrl = PaowangData::$forumUrlById[$forumId];
        array_push($data, array(
            'title'=> "",
            'note'=> "=> 进入" . $forumName,
            'titleLink'=>'',
            'noteLink'=>'/' . $forumUrl
        ));
        return $data;
    }
    
    function generalforumAction(){
    	$this->_helper->viewRenderer->setNoRender();
    	$cacheId = Cache_Engine::generateId(Array('IndexController', 'generalforumAction'));
        $data = Cache_Engine::load($cacheId);
        if($data){
        	echo $data;
        	return;
        }
    	
    	$postDao = Post_PostDao::getInstance();
        $type = "new";
        $posts = array();
        foreach(array_keys(PaowangData::$generalForumHotPosts) as $forumId){
        	$numHot = PaowangData::$generalForumHotPosts[$forumId];
        	$forumPosts = $postDao->getHotPosts($forumId, $numHot);
        	$forumName = PaowangData::$orderedForumsById[$forumId];
        	$forumUrl = '/' . PaowangData::$forumUrlById[$forumId];
        	foreach($forumPosts as $post){
        		$postInfo = $post["postInfo"];
        		$posts[$postInfo->getPostId()] = array(
	               'title'=>$postInfo->getTitle(),
	               'note'=>$forumName,
	               'titleLink'=>'/post/' . $postInfo->getPostId(),
        		   'noteLink'=>$forumUrl
	            );
        	}
        }
        krsort($posts);
        $valueArray = array_values($posts);
        array_push($valueArray, array(
            'title'=> "",
            'note'=> "=> 进入论坛",
            'titleLink'=>'',
            'noteLink'=>'/forum'
        ));
        
        $data = Zend_Json::encode($valueArray);   
        Cache_Engine::save($data, $cacheId, array(Cache_Engine::TAG_INDEX), 14400);
        return $data;
    }
    
    function photoforumAction(){
        $this->_helper->viewRenderer->setNoRender();
        $cacheId = Cache_Engine::generateId(Array('IndexController', 'photoforumAction'));
        $data = Cache_Engine::load($cacheId);
        if($data){
            echo $data;
            return;
        }
        
    	$postDao = Post_PostDao::getInstance();
        $type = "new";
        $posts = array();
        foreach(array_keys(PaowangData::$photoForumHotPosts) as $forumId){
            $numHot = PaowangData::$photoForumHotPosts[$forumId];
            $forumPosts = $postDao->getHotPosts($forumId, $numHot);
            $forumName = PaowangData::$orderedForumsById[$forumId];
            $forumUrl = '/' . PaowangData::$forumUrlById[$forumId];
            foreach($forumPosts as $post){
                $postInfo = $post["postInfo"];
                $posts[$postInfo->getPostId()] = array(
                   'title'=>$postInfo->getTitle(),
                   'note'=>$forumName,
                   'titleLink'=>'/post/' . $postInfo->getPostId(),
                   'noteLink'=>$forumUrl
                );
            }
        }
        krsort($posts);
        $valueArray = array_values($posts);
        array_push($valueArray, array(
            'title'=> "",
            'note'=> "=> 进入江湖色",
            'titleLink'=>'',
            'noteLink'=>'/photo'
        ));
        $data = Zend_Json::encode($valueArray);
        Cache_Engine::save($data, $cacheId, array(Cache_Engine::TAG_INDEX), 14400);
        return $data;
    }
    
    function singleblogAction(){
    	$this->_helper->viewRenderer->setNoRender();
    	$feedUrl = $this->_getParam('feedUrl');
    	$blogUrl = $this->_getParam('blogUrl');
        $cacheId = Cache_Engine::generateId(Array('IndexController', 'singleblogAction', md5($feedUrl)));
        $data = Cache_Engine::load($cacheId);
        if($data){
            echo $data;
            return;
        }
    	
//    	$generalFeed = new Feed_GeneralFeed('http://movie.blog.paowang.net/feed/');
        $generalFeed = new Feed_GeneralFeed($feedUrl);
    	$feeds = $generalFeed->getFeeds();
    	$data = array();
    	foreach($feeds as $feed){
    		$title = $feed['title'];
    		$link = $feed['link'];
    		$user = $feed['user'];
    		$data[] = array(
                'title'=> $title,
                'note'=> $user,
                'titleLink'=> $link,
                'noteLink'=>''
            ); 
    	}
    	array_push($data, array(
            'title'=> "",
            'note'=> "=> 更多",
            'titleLink'=>'',
            'noteLink'=>$blogUrl
        ));
        $data = Zend_Json::encode($data);
        Cache_Engine::save($data, $cacheId, array(Cache_Engine::TAG_INDEX), 14400);
        echo $data;
    }
    
    function tagpostAction(){
    	$this->_helper->viewRenderer->setNoRender();
    	$tagName = $this->_getParam('tagName');
        $cacheId = Cache_Engine::generateId(Array('IndexController', 'tagpostAction', md5($tagName)));
        $data = Cache_Engine::load($cacheId);
        if($data){
            echo $data;
            return;
        }
    	
    	$postDao = Post_PostDao::getInstance();
        $list = $postDao->getListByTagName($tagName,10,1);
        $data = array();
        foreach($list as $post){
        	$postInfo = $post['postInfo'];
        	$title = $postInfo->getTitle();
        	$date = Util::date2string16($postInfo->getCreateAt());
        	$id = $postInfo->getPostId();
        	$data[] = array(
                'title'=> $title,
	            'note'=> $date,
	            'titleLink'=>'/post/' . $id,
        	    'noteLink'=>''
        	); 
        }
        array_push($data, array(
            'title'=> "",
            'note'=> "=> 更多",
            'titleLink'=>'',
            'noteLink'=>'/forum/listbytagname/tagName/' . $tagName
        ));
        $data = Zend_Json::encode($data);
        Cache_Engine::save($data, $cacheId, array(Cache_Engine::TAG_INDEX), 36000);
        echo $data;
    }
    
    function notfoundAction(){
    	
    }

    /*
     * 洋洋大观
     */
    function  newsAction(){
		$page = intval($this->_getParam('page'));
	    if(empty($page)) $page = 1;
        $url = "http://paowang.com/news/3/index".($page==1?"":$page-1).".html";
        $snoopy = new Snoopy();
        $snoopy->fetch($url);
        $content =  $snoopy->results;
        $content =  stristr($content,'<td align=middle bordercolor="#C7C7C7" height="109">');
        //preg_match_all('%<a(.*?)href=(.*?)(.*?)>(.*?)<\/a>%i', $content, $matches_all);
        preg_match_all('%href=([\w\W]*?\.html) target=_blank>([\w\W]*?)</a><font color=#666666 size=1>([\w\W]*?)</font>%',$content,$matches_all);
        $this->view->matches_all = $matches_all;
        $this->view->page = $page;
        $this->view->highlight= PaowangData::NAVIGATION_BAR_NEWS;
    }    

    /*
     * 博客
     */
    function  blogAction(){
        $this->view->highlight= PaowangData::NAVIGATION_BAR_BLOG;
    }

    function  manageblogAction(){
        $currentUserId = Rbm_RbmService::getInstance()->getCurrentLoginUserId();
        if($currentUserId == 0){
            $this->_redirect('/index/notfound');
            return;        
        }
            
        if(!Rbm_RbmService::getInstance()->isAdministrator($currentUserId) &&
            !Rbm_RbmService::getInstance()->isBanzhu($currentUserId)){
            $this->_redirect('/index/notfound');
            return;     
        }
        $this->view->highlight= PaowangData::NAVIGATION_BAR_BLOG;
    }        

    function  managebloggerAction(){
        $currentUserId = Rbm_RbmService::getInstance()->getCurrentLoginUserId();
        if($currentUserId == 0){
            $this->_redirect('/index/notfound');
            return;        
        }
            
        if(!Rbm_RbmService::getInstance()->isAdministrator($currentUserId) &&
            !Rbm_RbmService::getInstance()->isBanzhu($currentUserId)){
            $this->_redirect('/index/notfound');
            return;     
        }
        $this->view->highlight= PaowangData::NAVIGATION_BAR_BLOG;
    }       
    
    /*
     * 聊天
     */
    function  chatAction(){
        $this->view->highlight= PaowangData::NAVIGATION_BAR_CHAT;
    }            
    
	function yydgjsAction(){
		$this->_helper->viewRenderer->setNoRender();
		$ds = new DbModel("","");
        $dbchar = 'utf8';
        $ds->query('set character_set_connection='.$dbchar.',character_set_results='.$dbchar.',character_set_client=binary;');		
        $sql = "select id,title,newstime,newspath from empirecms.phome_ecms_news order by id desc limit 24";
        $list = $ds->fetchAll($sql);
        $a = array();
        foreach($list as $row){
        	$newstime = Util::date2string16($row['newstime']);
        	$newsurl = "http://yydg.paowang.net/".$row['newspath']."/".$row['id'].".html";
        	$title = $row['title'];
        	$a[] = "{user: '$newstime', title: '$title', link: '$newsurl'}";
        }	
        $js = "newsData = [".implode($a,",")."]";
	    $fileName = $_SERVER['DOCUMENT_ROOT']."/../../picture/data/yydgnews.js";
	    $rtn = file_put_contents($fileName, $js);        
        Zend_Debug::dump($js);
	}
	
}
