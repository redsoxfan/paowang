<?php
    /**
     * Title  PostController
     */

    class PeopleController extends CommonController{ 
	    
	    function  indexAction(){
	    	
	        $userId = 1;
	    	$userposttypeDao = Userposttype_UserposttypeDao::getInstance();
            $this->view->postTypeList = $userposttypeDao->getListByUserIdAndType($userId,0);//文章的分类
            $this->view->picTypeList = $userposttypeDao->getListByUserIdAndType($userId,1);//图片的分类
            
	    }
	    
	    function articlelistAction(){
	    	$userId = intval($this->_getParam('userId'));
	    	$typeId = intval($this->_getParam('typeId'));           
            
            if($userId==0 ){
            	$userId = Util::getUserId();
            }

	    	$userposttypeDao = Userposttype_UserposttypeDao::getInstance();
            $this->view->postTypeList = $userposttypeDao->getListByUserIdAndType($userId,0);//文章的分类
            $this->view->picTypeList = $userposttypeDao->getListByUserIdAndType($userId,1);//图片的分类
                        
            $log = Zend_Registry::get('log');
            $log->debug( "################### userId:".$userId);
             
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
			$perpage = 5;	        
	        $postDao = Post_PostDao::getInstance();
            $this->view->lists = $postDao->getPostListByUserIdAndTypeId($userId,$typeId,$perpage,$page);
			$this->view->total= $postDao->getPostListByUserIdAndTypeIdCount($userId,$typeId);
			$this->view->perpage= $perpage;
			$this->view->page= $page;
			$this->view->userId= $userId;
			$this->view->typeId= $typeId;
			
	    }
	    
	    function posteditAction(){
	    	
	    }

	   function postviewAction(){
			$postId = intval($this->_getParam('postId'));
			if(empty($postId)){
				$this->_redirect('/post/index/postId/'.$postId);
			}else{
			    //加载postInfo 和 postTextInfo			
	            $postDao = Post_PostDao::getInstance();
	            $postInfo = $postDao->get($postId);
	            $this->view->postInfo = $postInfo;
	            $posttextDao = Posttext_PosttextDao::getInstance();
	            $this->view->posttextInfo = $posttextDao->get($postId);
	            
	            //页面左侧的论坛列表显示
		    	$forumId = $postInfo->getForumId();
		    	if($forumId==0){
		    	   $forumId = 1;
		    	}
		        $forumDao = Forum_ForumDao::getInstance();
		        $this->view->forumlists = $forumDao->getListByOrder();
		        $this->view->forumId = $forumId;
		        $this->view->forumName = $forumDao->getForumNameById($forumId);
	 		    
	 		    $childList = null;
				$postDao->loadChilds($postInfo->getTopId(),$childList,true);
				$this->view->childList = $childList;
				
				$postpicDao = Postpic_PostpicDao::getInstance();
				$this->view->piclists =  $postpicDao->getListByPostId($postId);
				  
			}
	    }
	    

	    function posttypesaveAction(){
	    	$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	    	$userPostTypeId = intval($this->_getParam('userPostTypeId'));
	    	$postIdStr = $this->_getParam('postIdStr');

	    	$postDao = Post_PostDao::getInstance();
	    	$postDao->updateUserPostType($userPostTypeId,$postIdStr);
	    	
	    	echo "success";
	    }
	    
	    function __call($action, $arguments)
	    {
	        //return $this->defaultAction();
	        //throw new Zend_Controller_Exception('Invalid method called');
	        //echo "Action = " . $action . "<br />";
	        //echo "Arguments = " . $arguments;
	    }
	}
?>