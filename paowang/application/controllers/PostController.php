<?php
/**
 * PostController
 */

class PostController extends CommonController{ 

    public function init()
    {
        parent::init();
        // set the hightlight to be 1 so that FORUM tab is highlighted 
        $this->view->highlight= PaowangData::NAVIGATION_BAR_FORUM;
    }   	
	
	function __call($action, $arguments){
    	//return $this->defaultAction();
        //throw new Zend_Controller_Exception('Invalid method called');
        //echo "Action = " . $action . "<br />";
        //echo "Arguments = " . $arguments;
    }  

    function  viewAction(){
        $postId = intval($this->_getParam('postId'));
    	$oldForumUrlName = $this->_getParam('oldForumUrlName');
    	$oldPostId = intval($this->_getParam('oldForumPostId'));
    	if(!empty($oldForumUrlName) && !empty($oldPostId)){
    		$postId = (string) Rewrite_RewriteUtils::getNewPostId($oldForumUrlName, $oldPostId);
    	}
    	
        if(empty($postId)){
            $this->_redirect('/forum/');
            return ;
        }
        else{
        	//加载postInfo 和 postTextInfo         
            $postDao = Post_PostDao::getInstance();
            $postInfo = $postDao->get($postId);
            if($postInfo == null){
            	$this->_redirect('/post/viewerror');
                return ;
            }

            //查看如果是江湖论剑，而用户没有权限，设置参数提示用户不能回帖
            $currentLoginUserId = Util::getUserId();
            $forumId = $postInfo->getForumId();
            
            //剑和客栈成为内部论坛
            $this->view->internalForum = false;
            if(!empty(PaowangData::$forumIdRequiredLogin[$forumId])){
	            $loginUserId = Util::getUserId();
	            if($loginUserId <= 0){
	                $this->view->internalForum = true;
	                return;
	            }
            }
            
            $canReplyPost = true;
            if($forumId == PaowangData::FORUM_ID_JIAN){
                $canReplyPost = Rbm_RbmService::getInstance()->hasPermission($currentLoginUserId, $forumId, Rbm_RbmService::USER_ADD_POST);
            }
            $this->view->canReplyPost = $canReplyPost? 'true' : 'false';
            
            $this->view->postInfo = $postInfo;
            $posttextDao = Posttext_PosttextDao::getInstance();
            $this->view->posttextInfo = $posttextDao->get($postId);
                
            $canManageForum = Rbm_RbmService::getInstance()->hasPermission(Util::getUserId(), $postInfo->getForumId(), Rbm_RbmService::ADMIN_MANAGE_FORUM);    
            if($postInfo->getStatus()==0 && !$canManageForum){
            	$this->_redirect('/post/viewerror');
            	return ;
            }              
            //页面左侧的论坛列表显示
            $forumId = $postInfo->getForumId();
            if($forumId==0){
                $forumId = 1;
            }
            $forumDao = Forum_ForumDao::getInstance();
//            $this->view->forumlists = $forumDao->getListByOrder();
            $this->view->forumId = $forumId;
                
            $childList = null;
            $postDao->loadChilds($postInfo->getTopId(),$childList,true);
            $this->view->childList = $childList;
                
            $postpicDao = Postpic_PostpicDao::getInstance();
            $this->view->piclists =  $postpicDao->getListByPostId($postId);
                
            $postjudgeDao = Postjudge_PostjudgeDao::getInstance();
            $this->view->voteyes = $postjudgeDao->getListCountVoteYes($postId);
            $this->view->voteno = $postjudgeDao->getListCountVoteNo($postId);
                
//            $this->view->postInfoNext = $postDao->getNext($forumId,$postId);//下一篇
//            $this->view->postInfoPrevious = $postDao->getPrevious($forumId,$postId);//上一篇
                
            $postDao->addClicks($postId);//更新当前帖子的点击数
            
            
	        //显示风格
	        $style = $this->_getParam('style');
	        if(empty($style) || $style == ''){
	            $style =  Util::getStyle();
	        }else{
			    $expire = time()+60*60*24*30;//设定 cookie 保存30天
	        	setcookie("style", $style, $expire, "/");
	        } 

            //tag 显示 开始
            $tagDao = Tag_TagDao::getInstance();
//            $this->view->tagsArray = $tagDao ->getTagTop(24); //前24个标签
            
            //当前帖子的主贴对应的标签列表，所有标签都记录在主贴上
            $this->view->tagArray =  $tagDao->getTagArrayByTopId($postInfo->getTopId());
            
            
            //Dig
            $numDigYes = Postjudge_PostjudgeDao::getInstance()->getListCountVoteYes($postId);
            $numDigNo = Postjudge_PostjudgeDao::getInstance()->getListCountVoteNo($postId);
            $this->view->digYes = $numDigYes;
            $this->view->digNo = $numDigNo;
            $this->view->style = "";            
            

                    
        }    	
    }
    
    
    function  viewclassicAction(){
        $postId = intval($this->_getParam('postId'));
    	$oldForumUrlName = $this->_getParam('oldForumUrlName');
    	$oldPostId = intval($this->_getParam('oldForumPostId'));
    	if(!empty($oldForumUrlName) && !empty($oldPostId)){
    		$postId = (string) Rewrite_RewriteUtils::getNewPostId($oldForumUrlName, $oldPostId);
    	}
    	
        if(empty($postId)){
            $this->_redirect('/forum/');
            return ;
        }
        else{
        	//加载postInfo 和 postTextInfo         
            $postDao = Post_PostDao::getInstance();
            $postInfo = $postDao->get($postId);
            if($postInfo == null){
            	$this->_redirect('/post/viewerror');
                return ;
            }

            //查看如果是江湖论剑，而用户没有权限，设置参数提示用户不能回帖
            $currentLoginUserId = Util::getUserId();
            $forumId = $postInfo->getForumId();
            $canReplyPost = true;
            if($forumId == PaowangData::FORUM_ID_JIAN){
                $canReplyPost = Rbm_RbmService::getInstance()->hasPermission($currentLoginUserId, $forumId, Rbm_RbmService::USER_ADD_POST);
            }
            $this->view->canReplyPost = $canReplyPost? 'true' : 'false';
            
            $this->view->postInfo = $postInfo;
            $posttextDao = Posttext_PosttextDao::getInstance();
            $this->view->posttextInfo = $posttextDao->get($postId);
                
            $canManageForum = Rbm_RbmService::getInstance()->hasPermission(Util::getUserId(), $postInfo->getForumId(), Rbm_RbmService::ADMIN_MANAGE_FORUM);    
            if($postInfo->getStatus()==0 && !$canManageForum){
            	$this->_redirect('/post/viewerror');
            	return ;
            }              
            //页面左侧的论坛列表显示
            $forumId = $postInfo->getForumId();
            if($forumId==0){
                $forumId = 1;
            }
            $forumDao = Forum_ForumDao::getInstance();
//            $this->view->forumlists = $forumDao->getListByOrder();
            $this->view->forumId = $forumId;
                
            $childList = null;
            $postDao->loadChilds($postInfo->getTopId(),$childList,true);
            $this->view->childList = $childList;
                
            $postpicDao = Postpic_PostpicDao::getInstance();
            $this->view->piclists =  $postpicDao->getListByPostId($postId);
                
            $postjudgeDao = Postjudge_PostjudgeDao::getInstance();
            $this->view->voteyes = $postjudgeDao->getListCountVoteYes($postId);
            $this->view->voteno = $postjudgeDao->getListCountVoteNo($postId);
                
//            $this->view->postInfoNext = $postDao->getNext($forumId,$postId);//下一篇
//            $this->view->postInfoPrevious = $postDao->getPrevious($forumId,$postId);//上一篇
                
            $postDao->addClicks($postId);//更新当前帖子的点击数
            
            
            

            //tag 显示 开始
            $tagDao = Tag_TagDao::getInstance();
//            $this->view->tagsArray = $tagDao ->getTagTop(24); //前24个标签
            
            //当前帖子的主贴对应的标签列表，所有标签都记录在主贴上
            $this->view->tagArray =  $tagDao->getTagArrayByTopId($postInfo->getTopId());
            
            
            //Dig
            $numDigYes = Postjudge_PostjudgeDao::getInstance()->getListCountVoteYes($postId);
            $numDigNo = Postjudge_PostjudgeDao::getInstance()->getListCountVoteNo($postId);
            $this->view->digYes = $numDigYes;
            $this->view->digNo = $numDigNo;
        }    	
    }
        
    function clickAction() {
    	$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
    	$postId = intval($this->_getParam('postId'));
    	if(empty($postId)){
    		return;
    	}
    	Post_PostDao::getInstance()->addClicks($postId);
    }
    
    function replypostAction(){
    	$this->_helper->viewRenderer->setNoRender();
        $parameters = array();
    	$parameters['parentId'] = $this->_getParam('parentId');
		$parameters['title'] = $this->_getParam('replyTitle');
		$parameters['text'] = $this->_getParam('replyText');
		$parameters['tags'] = "";
		$userId = Util::getUserId();
        $parameters['userId'] = $userId;
		$parameters['userName'] = Util::getUserName();
		
		$isUserOpen = Rbm_RbmService::getInstance()->isOpenUser($userId);
        if(!$isUserOpen){
            echo "{success: false, messages: { message: '此用户已被关闭。'}}";
            return;
        }

  	    $keyfileName = $_SERVER['DOCUMENT_ROOT']."/../../picture/data/key.txt";
	    $keycontent = file_get_contents($keyfileName);
	    $keyArray =   preg_split ('/([\s,;]+)/', $keycontent);
	    
	    foreach($keyArray as $keyname){
	        if(preg_match("/".$keyname."/",$parameters['text']) || preg_match("/".$keyname."/",$parameters['title'])){
	            echo "{success: false, messages: { message: '回贴失败，请检查您的回帖内容!'}}";
	            return;
	        }
        }
		
	    $postId = ControllerUtils::simpleAddPost($parameters, true);
	    echo "{success: true, messages: { message: '回帖成功!',postId:'$postId'}}";
        //$this->_redirect("/post/$postId");            
    }    
    
    function  topAction(){
    	$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
    	//request
        $postId = $this->_getParam('postId');
        $userId = Util::getUserId();
        $flag = $this->_getParam('flag');
        //dao
        $postDao = Post_PostDao::getInstance();
        if($flag == "1"){
        	$postDao->addToTop($postId);
        }
        else {
        	$postDao->undoTop($postId);
        }
        $rtn = array();
        $rtn['msg'] = "置顶改变成功";
        $json = Zend_Json::encode($rtn);
        echo $json;
    }           
    
    
    
    

	    function  tagaddAction(){
		   $topId = intval($this->_getParam('topId'));
		   $tagInput = $this->_getParam('tagInput');
 	       $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
 	       $userId = Util::getUserId();
 	       $createAt = time();
 	       $userIp = Util::getUserIp();
 	       
			$isUserOpen = Rbm_RbmService::getInstance()->isOpenUser($userId);
	        if(!$isUserOpen){
	            //非法用户，返回false，客户端页面会检测，如果为false，则提示"此用户没有激活或已经被关闭,您不能再增加标签"
	            echo "false";
	            return;
	        }
         	       
	        $tagArray =   preg_split ('/([\s,;]+)/', $tagInput);
	        $tagDao = Tag_TagDao::getInstance();
	        foreach($tagArray as $tagName){
	            $tagInfo = new Tag_TagInfo();
	            $tagInfo->setPostId($topId);
	            $tagInfo->setTagName(trim($tagName));
	            $tagInfo->setStatus(1);
	            $tagInfo->setUserId($userId);
	            $tagInfo->setCreateAt($createAt);
	            $tagInfo->setUserIp($userIp);
	            $tagInfo->setTagType(1);//帖子
	            $tagInfo->setTagUserId($userId);//帖子发布者的ID
	            
	            if($tagDao->getListAllCountByUserIdAndCreateUserIdAddTagNameAndTopId($userId,0,$tagName,$topId)<1){
	              $tagDao->add($tagInfo);    
	            }           
	        } 	       
 	       
 	       
 	       $tagArray = $tagDao->getTagArrayByTopId($topId);
 	       $text = "";
		    foreach ($tagArray as $tag){ 
			  $text .=  "<a href=\"/forum/listbytagname/tagName/".$tag["tag_name"]."\">".$tag["tag_name"]."</a> ";
			}  	       
 	       
	       echo $text;
	    }

        function postaddpicpreviewAction(){
			$postId = intval($this->_getParam('postId'));
		    //加载postInfo 和 postTextInfo			
            $postDao = Temppost_TemppostDao::getInstance();
            $postInfo = $postDao->get($postId);
            $this->view->postInfo = $postInfo;
            $posttextDao = Tempposttext_TempposttextDao::getInstance();
            $this->view->posttextInfo = $posttextDao->get($postId);			
			$postpicDao = Temppostpic_TemppostpicDao::getInstance();
			$this->view->piclists =  $postpicDao->getListByPostId($postId);        	
        }
        
        function postaddpicpreviewbakAction(){
			$postId = intval($this->_getParam('postId'));
		    //加载postInfo 和 postTextInfo			
            $postDao = Post_PostDao::getInstance();
            $postInfo = $postDao->get($postId);
            $this->view->postInfo = $postInfo;
            $posttextDao = Posttext_PosttextDao::getInstance();
            $this->view->posttextInfo = $posttextDao->get($postId);			
			$postpicDao = Postpic_PostpicDao::getInstance();
			$this->view->piclists =  $postpicDao->getListByPostId($postId);        	
        }        

        function postaddpicpreviewdealAction(){
        	
	        $log = Zend_Registry::get('log');
			$postId = intval($this->_getParam('postId'));
	        //$postDao->uploadPictures($postId,"pic_files");
	        $picfiles =  $_FILES["pic_files"];

			//处理修改内容 开始
		    //加载postInfo 和 postTextInfo			
	        $temppostDao = Temppost_TemppostDao::getInstance();
	        $temppostInfo = $temppostDao->get($postId);
	        $tempposttextDao = Tempposttext_TempposttextDao::getInstance();
	        $tempposttextInfo = $tempposttextDao->get($postId);
	
	        //text and length
	        $html = $this->_getParam('text');
	        $html = preg_replace("/<a(((?!\starget).)+?)>/i","<a\\1 target=\"_blank\">",$html);
//	        $html = str_replace("\\\"", "\"", $html);//替换一下\"
//	        $html = preg_replace('/\s*(<br\s*\/?\s*>\s*){1,}/im',"",$html);  //把br给去掉
	        $text = Util::stripText($html);
	        $textLength = Util::strLenUtf8($text);
	        
	        //request
	        $title = $this->_getParam('title');
	        $tags = $this->_getParam('tags');
	        $userId = Util::getUserId();
	        $userIp = Util::getUserIp();
	        $updateIp = $userIp;
	        $createAt = time();
	        $updateAt = $createAt;
	        $updateUserId = Util::getUserId();
	        
	        $userDao = User_UserDao::getInstance();
	        $updateUserName =  $userDao->getUserNameByUserId($updateUserId);
	        
	                
	
	        //info
	        $temppostInfo->setTitle($title);
	        $temppostInfo->setTags($tags);
	        $temppostInfo->setUpdateUserId($updateUserId);
	        $temppostInfo->setUpdateUserName($updateUserName);
	        $temppostInfo->setTextLength($textLength);
	        $temppostInfo->setUpdateAt($updateAt);
	        $temppostInfo->setUpdateIp($updateIp);        
	        
	        $tempposttextInfo->setText($text);
	        $tempposttextInfo->setHtml($html);
	        
	
	        //dao
	        $temppostDao->modify($temppostInfo);
	        $tempposttextDao->modify($tempposttextInfo);
			
			//处理修改内容 结束
	        
	        //处理删除 开始
	        $postIdStr = $this->_getParam('postIdStr');
	        $postIdDelArray = split(",",$postIdStr);
	        $temppostpicDao = Temppostpic_TemppostpicDao::getInstance();
			$config = Zend_Registry::get('config');
			$file_path = $config->upload->root ;	        
	        if($postIdDelArray!=null){
	          foreach($postIdDelArray as $postIdDel){
			        if($postIdDel!=""){
				        $temppostpicInfo = $temppostpicDao->get($postIdDel);
		                unlink($file_path.$temppostpicInfo->getPicPath());
		                unlink($file_path.$temppostpicInfo->getPicThumb());
		                unlink($file_path.$temppostpicInfo->getPicThumb2());
		                unlink($file_path.$temppostpicInfo->getPicThumb3());
		                $temppostpicDao->del($temppostpicInfo);
			        }			        
	          }	
	        }
	        //处理删除 结束
	        
	        if($picfiles!=null && !empty($picfiles['name'][0])){
		        //图片上传 开始
				$temppostDao = Temppost_TemppostDao::getInstance();
				$temppostDao->uploadPictures($postId,$_FILES,"pic_files");
				
				$this->_redirect("/post/postaddpicpreview/postId/$postId"); 
		        //图片上传 结束
	        }else{
				//不再上传图片 开始

				$postpicDao = Temppostpic_TemppostpicDao::getInstance();
		    	$postPicIdArray = $this->_getParam('postPicId');
		    	$picDescArray = $this->_getParam('picDesc');
		    	//$tagsArray = $this->_getParam('tags');
		    	for($i=0;$i<count($postPicIdArray);$i++){
		    	    $postpicInfo = $postpicDao->get($postPicIdArray[$i]);
		    	    $postpicInfo->setPicDesc($picDescArray[$i]);
		    	    //$postpicInfo->setTags($tagsArray[$i]);
		    	    $postpicDao->modify($postpicInfo);
		    	}
		    	
				$latest_title = $this->_getParam('title');
	
		        //post 处理 开始  
		        $temppostDao = Temppost_TemppostDao::getInstance();
		        $postDao = Post_PostDao::getInstance();
	            $temppostInfo = $temppostDao->get($postId);
	            $postInfo = new Post_PostInfo();
	            $postInfo->temp2online($temppostInfo); 
	            $postInfo->setPostId(0);
				$postInfo->setTitle($latest_title);		    	
	            $postDao->add($postInfo);
		        $postInfo->setTopId($postInfo->getPostId());
		        $postDao->modify($postInfo);	            
	            //post 处理 结束
	            
		        //post text 处理 开始  
		        $tempposttextDao = Tempposttext_TempposttextDao::getInstance();
		        $posttextDao = Posttext_PosttextDao::getInstance();
	            $tempposttextInfo = $tempposttextDao->get($postId);
	            $posttextInfo = new Posttext_PosttextInfo();
	            $posttextInfo->temp2online($tempposttextInfo); 
	            $posttextInfo->setPostId($postInfo->getPostId());
	            $posttextDao->add($posttextInfo);
	            //post text 处理 结束
	            
		        //post pic 处理 开始  
		        $temppostpicDao = Temppostpic_TemppostpicDao::getInstance();
		        $temppiclists = $temppostpicDao->getListByPostId($postId);
				$postpicDao = Postpic_PostpicDao::getInstance();
				$config = Zend_Registry::get('config');
				$file_path = $config->upload->root ;
		        foreach ($temppiclists as $temppostpicInfo){
		            $postpicInfo = new Postpic_PostpicInfo();
		            rename($file_path.$temppostpicInfo->getPicPath(), str_replace('/temp','',$file_path.$temppostpicInfo->getPicPath()));
	                rename($file_path.$temppostpicInfo->getPicThumb(), str_replace('/temp','',$file_path.$temppostpicInfo->getPicThumb()));
	                rename($file_path.$temppostpicInfo->getPicThumb2(), str_replace('/temp','',$file_path.$temppostpicInfo->getPicThumb2()));
	                rename($file_path.$temppostpicInfo->getPicThumb3(), str_replace('/temp','',$file_path.$temppostpicInfo->getPicThumb3()));
		            $postpicInfo->temp2online($temppostpicInfo);
		            $postpicInfo->setPicPath(str_replace('/temp','',$postpicInfo->getPicPath())); 
		            $postpicInfo->setPicThumb(str_replace('/temp','',$postpicInfo->getPicThumb()));
		            $postpicInfo->setPicThumb2(str_replace('/temp','',$postpicInfo->getPicThumb2()));
	                $postpicInfo->setPicThumb3(str_replace('/temp','',$postpicInfo->getPicThumb3()));	            
	
		            
		            $postpicInfo->setPostId($postInfo->getPostId());
		            $postpicDao->add($postpicInfo);
		        }
	            //post pic 处理 结束
	            
	            
	            //标签 处理 开始
		        $tagArray =   preg_split ('/([\s,;]+)/', $postInfo->getTags());
		        $tagDao = Tag_TagDao::getInstance();
		        $tagDao->delAllByUserIdAndTopId($userId,$postInfo->getTopId());
		        foreach($tagArray as $tagName){
		            $tagInfo = new Tag_TagInfo();
		            $tagInfo->setPostId($postInfo->getPostId());
		            $tagInfo->setTagName($tagName);
		            $tagInfo->setStatus(1);
		            $tagInfo->setUserId($userId);
		            $tagInfo->setCreateAt($createAt);
		            $tagInfo->setUserIp($userIp);
		            $tagInfo->setTagType(1);//帖子
		            $tagInfo->setTagUserId($userId);//帖子发布者的ID
		            
		            if($tagDao->getListAllCountByUserIdAndCreateUserIdAddTagNameAndTopId($userId,$postInfo->getUserId(),$tagName,$postInfo->getTopId())<1){
		              $tagDao->add($tagInfo);    
		            }           
		        }	            
	            //标签 处理 结束
	            
	            //删除临时表里的数据
	            $temppostDao->delById($postId);
		    	$this->_redirect("/post/".$postInfo->getPostId());
		    	//不再上传图片 结束
            }    	
        }


        function postaddpicpreviewdealbakAction(){
			$postId = intval($this->_getParam('postId'));
			$postpicDao = Postpic_PostpicDao::getInstance();
	    	$postPicIdArray = $this->_getParam('postPicId');
	    	$picDescArray = $this->_getParam('picDesc');
	    	//$tagsArray = $this->_getParam('tags');
	    	for($i=0;$i<count($postPicIdArray);$i++){
	    	    $postpicInfo = $postpicDao->get($postPicIdArray[$i]);
	    	    $postpicInfo->setPicDesc($picDescArray[$i]);
	    	    //$postpicInfo->setTags($tagsArray[$i]);
	    	    $postpicDao->modify($postpicInfo);
	    	}
	
	    	$this->_redirect("/post/$postId");    	
        }
        
        
        function postaddpiccancelAction(){
            //删除临时表里的数据
            $this->_helper->viewRenderer->setNoRender();
            $postId = intval($this->_getParam('postId'));
	        $temppostDao = Temppost_TemppostDao::getInstance();
            $temppostInfo = null;
			if(empty($postId)){
				//$this->_redirect('/post/index/');
			}else{
                $temppostInfo = $temppostDao->get($postId);  
                $temppostDao->delById($postId);
			}
        }        

        function postnewAction(){
            $forumId = intval($this->_getParam('forumId'));        	
            $this->view->forumId = $forumId;
	    }
	                            
        function postviewAction(){
			$postId = intval($this->_getParam('postId'));
			if(empty($postId)){
				$this->_redirect('/post/index/');
			}else{
			    //加载postInfo 和 postTextInfo			
	            $postDao = Post_PostDao::getInstance();
	            $postInfo = $postDao->get($postId);
	            $this->view->postInfo = $postInfo;
	            $posttextDao = Posttext_PosttextDao::getInstance();
	            $this->view->posttextInfo = $posttextDao->get($postId);
	            
	            //页面左侧的论坛列表显示
		    	$forumId = $postInfo->getForumId();
		    	if($forumId==0){
		    	   $forumId = 1;
		    	}
		        $forumDao = Forum_ForumDao::getInstance();
//		        $this->view->forumlists = $forumDao->getListByOrder();
		        $this->view->forumId = $forumId;
		        $this->view->forumName = $forumDao->getForumNameById($forumId);
	 		    
	 		    $childList = null;
				$postDao->loadChilds($postInfo->getTopId(),$childList,true);
				$this->view->childList = $childList;
				
				$postpicDao = Postpic_PostpicDao::getInstance();
				$this->view->piclists =  $postpicDao->getListByPostId($postId);
				
				$postjudgeDao = Postjudge_PostjudgeDao::getInstance();
				$this->view->voteyes = $postjudgeDao->getListCountVoteYes($postId);
				$this->view->voteno = $postjudgeDao->getListCountVoteNo($postId);
				  
			}
	    }


	    function newpostAction(){
	    	$forumId = intval($this->_getParam('forumId'));
	    	if($forumId==0){
	    	   $forumId = 1;
	    	}
	        $forumDao = Forum_ForumDao::getInstance();
	        $this->view->forumId = $forumId;
	        $this->view->forumName = $forumDao->getForumNameById($forumId);	       
	       	
	    }
	    
    function addpostAction(){

        
        $log = Zend_Registry::get('log');
        //图片上传 开始
        //$postDao->uploadPictures($postId,"pic_files");
        $log->debug("start upload pic");
        $picfiles =  $_FILES["pic_files"];
        if($picfiles!=null && !empty($picfiles['name'][0])){
			$log->debug("have pic");
			$this->dealhavepicAction();
        }else{
        	$log->debug("dont have pic");
        	$this->dealnopicAction();
        }    
        //图片上传 结束
        //$log = Zend_Registry::get('log');
        //$log->debug("postAddAction end");
    }
    
    function dealhavepicAction(){
        $log = Zend_Registry::get('log');
        $log->debug("###################dealhavepicAction:");
        
        $forumId = intval($this->_getParam('forumId'));
        if($forumId==0){
            $forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖论剑页面
        } 
        $this->view->forumId = $forumId;
        $forumName = Forum_ForumDao::getInstance()->getForumName($forumId);
        $this->view->forumName = $forumName;
                
        //request
        $userId = Util::getUserId();
        $userName = Util::getUserName();
        $parentId = 0;
        $topId = 0;//?
        $isDigest = "0";
        $title = "";// $this->_getParam('title');

        $depth = 1;
        $showOrder = 1;
        $listOrder = 1;
        $canHaschild = 1;
        $isTail = 1;
        $status = 1;
        $createAt = time();
        $userIp = Util::getUserIp();
        $click = rand(0, 2);
        $tags = "";//$this->_getParam('tags');
        $updateAt = time();
        $updateIp = $userIp;
        $isCeil = 0;
        $isRelive = 0;
        $reliveTime = 0;
        $userPostTypeId = 0;
        $childType = 0;
        $isPic = 0;
        $childPosts = 1;
        
        //text and length
        /*
        $html = $this->_getParam('text');
        $log->debug("111:".$html);
        $html = preg_replace("/<a(((?!\starget).)+?)>/i","<a\\1 target=\"_blank\">",$html);
        $log->debug("222:".$html); 
        $text = Util::stripText($html);
        $textLength = Util::strLenUtf8($text);
        */
        
        $html = "";
        $text = "";
        $textLength = 0;
                
        //info
        $postInfo = new Temppost_TemppostInfo();
        $postInfo->setForumId($forumId);
        $postInfo->setUserId($userId);
        $postInfo->setUserName($userName);
        $postInfo->setParentId($parentId);
        $postInfo->setTopId($topId);
        $postInfo->setIsDigest($isDigest);
        $postInfo->setTitle($title);
        $postInfo->setDepth($depth);
        $postInfo->setShowOrder($showOrder);
        $postInfo->setListOrder($listOrder);
        $postInfo->setCanHaschild($canHaschild);
        $postInfo->setIsTail($isTail);
        $postInfo->setStatus($status);
        $postInfo->setCreateAt($createAt);
        $postInfo->setUserIp($userIp);
        $postInfo->setClick($click);
        $postInfo->setTags($tags);
        
        $postInfo->setTextLength($textLength);
        $postInfo->setUpdateAt($updateAt);
        $postInfo->setUpdateIp($updateIp);
        $postInfo->setIsCeil($isCeil);
        $postInfo->setIsRelive($isRelive);
        $postInfo->setReliveTime($reliveTime);
        $postInfo->setUserPostTypeId($userPostTypeId);
        $postInfo->setChildType($childType);
        $postInfo->setIsPic($isPic);
        $postInfo->setChildPosts($childPosts);
        
        //dao
        $postDao = Temppost_TemppostDao::getInstance();
        $postDao->add($postInfo);
        $postId = $postInfo->getPostId();
        $postInfo->setTopId($postId);
        $postDao->modify($postInfo);
        
        //info
        $posttextInfo = new Tempposttext_TempposttextInfo();
        $posttextInfo->setPostId($postId);
        $posttextInfo->setText($text);
        $posttextInfo->setHtml($html);
        
        
        //dao
        $posttextDao = Tempposttext_TempposttextDao::getInstance();
        $posttextDao->add($posttextInfo);
        
        //$postDao->uploadPictures($postId,$_FILES,"pic_files");
                
        //$this->_redirect("/post/postaddpicpreview/postId/$postId");
        return $postId;
    }    
        
    function dealnopicAction(){
        $log = Zend_Registry::get('log');
        
        $parameters = array();
        $parameters['userId'] = Util::getUserId();
        $parameters['userName'] = Util::getUserName();
        $forumId = intval($this->_getParam('forumId'));
        $parameters['forumId'] = $forumId;
        $parameters['title'] = $this->_getParam('title');
        $parameters['tags']  = trim($this->_getParam('tags'));
        $parameters['text']  = $this->_getParam('text');
    

               $postDao = Post_PostDao::getInstance();
$userId = Util::getUserId();
$registerDays = $postDao->getRegisterDays($userId);
if($registerDays<31){
	//发贴受用户id限制
                    //$maxPostCount = $postDao->getPostDayMaxCountByUserId($userId);
                    $maxPostCount = 6;
                    $nowPostCount = $postDao->getPostDayCountByUserId($userId);
                    if($nowPostCount>=$maxPostCount){
                          echo "{success: false, messages: { message: '发贴失败，您今天最多允许发帖数为$maxPostCount!'}}";
                          return;
                    }
   //发贴受用户ip限制
                   $nowPostCountByIp = $postDao->getPostDayCountByUserIp(Util::getUserIp());
                    if($nowPostCountByIp>=$maxPostCount){
                          echo "{success: false, messages: { message: '发贴失败，您今天最多允许发帖数为$maxPostCount!'}}";
                          return;
                    }                    

}

        ControllerUtils::simpleAddPost($parameters, false);
        echo "{success: true,forumId:$forumId }";
    }


    
    function editpostAction(){
        $postId = intval($this->_getParam('postId')); 
		if(empty($postId)){
			$this->_redirect('/post/index/');
		}else{
			//如果是图片论坛 ，确认删除图片 开始
	        $temppostDao = Temppost_TemppostDao::getInstance();
            $temppostDao->delById($postId);
			//如果是图片论坛 ，确认删除图片 结束
						
		    //加载postInfo 和 postTextInfo			
            $postDao = Post_PostDao::getInstance();
            $postInfo = $postDao->get($postId);
            $this->view->postInfo = $postInfo;
            $posttextDao = Posttext_PosttextDao::getInstance();
            $this->view->posttextInfo = $posttextDao->get($postId);
            
	    	$forumId = $postInfo->getForumId();
	    	if($forumId==0){
	    	   $forumId = 1;
	    	}
	        $forumDao = Forum_ForumDao::getInstance();
	        $this->view->forumId = $forumId;
	        $this->view->forumName = $forumDao->getForumNameById($forumId);
            $postpicDao = Postpic_PostpicDao::getInstance();
            $this->view->piclists =  $postpicDao->getListByPostId($postId);	        
		}

    }
    
   
    function modifypostAction(){
        $postId = intval($this->_getParam('postId')); 
        
	    //加载postInfo 和 postTextInfo			
        $postDao = Post_PostDao::getInstance();
        $postInfo = $postDao->get($postId);
        $posttextDao = Posttext_PosttextDao::getInstance();
        $posttextInfo = $posttextDao->get($postId);

        $forumId = intval($postInfo->getForumId());
        
        if( ! Rbm_RbmService::canEditPost(Util::getUserId(), $postInfo->getForumId(), $postInfo->getPostId())){
             $this->_redirect('/post/noright/');
             return ;      	
        }
        
        //text and length
        $html = $this->_getParam('text');
        $html = preg_replace("/<a(((?!\starget).)+?)>/i","<a\\1 target=\"_blank\">",$html);
//        $html = str_replace("\\\"", "\"", $html);//替换一下\"
//        $html = preg_replace('/\s*(<br\s*\/?\s*>\s*){1,}/im',"",$html);  //把br给去掉   
$html = preg_replace("/<a(((?!\starget).)+?)>/i","<a\\1 target=\"_blank\">",$html);
     	
        $text = Util::stripText($html);
        $textLength = Util::strLenUtf8($text);
        
        //request
        $title = $this->_getParam('title');
        $tags = $this->_getParam('tags');
        $userId = Util::getUserId();
        $userIp = Util::getUserIp();
        $updateIp = $userIp;
        $createAt = time();
        $updateAt = $createAt;
        $updateUserId = Util::getUserId();
        
        $userDao = User_UserDao::getInstance();
        $updateUserName =  $userDao->getUserNameByUserId($updateUserId);

        //info
        $postInfo->setTitle($title);
        $postInfo->setTags($tags);
        $postInfo->setUpdateUserId($updateUserId);
        $postInfo->setUpdateUserName($updateUserName);
        $postInfo->setTextLength($textLength);
        $postInfo->setUpdateAt($updateAt);
        $postInfo->setUpdateIp($updateIp);        
        
        $posttextInfo->setText($text);
        $posttextInfo->setHtml($html);
        
        //dao
        $postDao->modify($postInfo);
        $posttextDao->modify($posttextInfo);
        
        $tagArray =   preg_split ('/([\s,;]+)/', $tags);
        $tagDao = Tag_TagDao::getInstance();
//        $tagDao->delAllByUserIdAndTopId($userId,$postInfo->getTopId());
        foreach($tagArray as $tagName){
            $tagInfo = new Tag_TagInfo();
            $tagInfo->setPostId($postId);
            $tagInfo->setTagName(trim($tagName));
            $tagInfo->setStatus(1);
            $tagInfo->setUserId($userId);
            $tagInfo->setCreateAt($createAt);
            $tagInfo->setUserIp($userIp);
            $tagInfo->setTagType(1);//帖子
            $tagInfo->setTagUserId($userId);//帖子发布者的ID
            
            if($tagDao->getListAllCountByUserIdAndCreateUserIdAddTagNameAndTopId($userId,$postInfo->getUserId(),$tagName,$postInfo->getTopId())<1){
                $tagDao->add($tagInfo);    
            }           
        }
        
	     $ds = new DbModel("","");        
        //处理删除图片开始
	     $ds = new DbModel("","");
	     $del_post_pic_id_str = $this->_getParam('del_post_pic_id_str');
         $sql = "select * from pw_post_pic where post_pic_id in ($del_post_pic_id_str)";
         $list = $ds->fetchAll($sql);
		 $config = Zend_Registry::get('config');
		 $file_path = $config->upload->root;
         foreach($list as $row){
         	 $pic_path = $row['pic_path'];
         	 $pic_thumb = $row['pic_thumb'];
             unlink($file_path.$pic_path);
             unlink($file_path.$pic_thumb);
         }
         $sql = "delete from pw_post_pic where post_pic_id in ($del_post_pic_id_str)";
         $ds->query($sql);   
        //处理删除图片结束
        
         //处理图片描述 开始
         $sql = "select post_pic_id from pw_post_pic where post_id=$postId";
         $list = $ds->fetchAll($sql);
         foreach($list as $row){
         	 $picDesc = $this->_getParam('picDesc'.$row['post_pic_id']);
	         $sql = "update pw_post_pic set pic_desc='".$picDesc."' where post_pic_id=".$row['post_pic_id'];
	         $ds->query($sql); 
         }

         //处理图片描述 结束     
                 
        
		        //post pic 处理 开始  
		        $temppostpicDao = Temppostpic_TemppostpicDao::getInstance();
		        $temppiclists = $temppostpicDao->getListByPostId($postId,0,0,1);
				$postpicDao = Postpic_PostpicDao::getInstance();
				$config = Zend_Registry::get('config');
				$file_path = $config->upload->root ;
				$log = Zend_Registry::get('log');
		        foreach ($temppiclists as $temppostpicInfo){
		            $postpicInfo = new Postpic_PostpicInfo();
		            $postpicInfo->temp2online($temppostpicInfo);
		            $postpicInfo->setPicPath(str_replace('/temp','',$postpicInfo->getPicPath())); 
		            $postpicInfo->setPicThumb(str_replace('/temp','',$postpicInfo->getPicThumb()));
		            $postpicInfo->setPicThumb2(str_replace('/temp','',$postpicInfo->getPicThumb2()));
	                $postpicInfo->setPicThumb3(str_replace('/temp','',$postpicInfo->getPicThumb3()));
	                
		            $postpicInfo->setPostId($postInfo->getPostId());
		            //注意顺序，需要先增加一条记录，再去更新
		            $postpicDao->add($postpicInfo);
		            $log->debug("####".$postpicInfo->getPicPath());
	                $postpicInfo->setPicName(str_replace($postInfo->getPostId().'_'.$temppostpicInfo->getPostPicId(),$postInfo->getPostId().'_'.$postpicInfo->getPostPicId(),$postpicInfo->getPicName()));	            
		            $postpicInfo->setPicPath(str_replace($postInfo->getPostId().'_'.$temppostpicInfo->getPostPicId(),$postInfo->getPostId().'_'.$postpicInfo->getPostPicId(),$postpicInfo->getPicPath())); 
		            $postpicInfo->setPicThumb(str_replace($postInfo->getPostId().'_'.$temppostpicInfo->getPostPicId(),$postInfo->getPostId().'_'.$postpicInfo->getPostPicId(),$postpicInfo->getPicThumb()));
		            $postpicInfo->setPicThumb2(str_replace($postInfo->getPostId().'_'.$temppostpicInfo->getPostPicId(),$postInfo->getPostId().'_'.$postpicInfo->getPostPicId(),$postpicInfo->getPicThumb2()));
	                $postpicInfo->setPicThumb3(str_replace($postInfo->getPostId().'_'.$temppostpicInfo->getPostPicId(),$postInfo->getPostId().'_'.$postpicInfo->getPostPicId(),$postpicInfo->getPicThumb3()));
	                $postpicInfo->setPicDesc($this->_getParam('picDesc'.$temppostpicInfo->getPostPicId()));
	                $postpicDao->modify($postpicInfo);
		            $log->debug("####".$postpicInfo->getPicPath());	                
	                //注意源文件名和目标文件名	            
	                $log->debug("####".$file_path.$temppostpicInfo->getPicPath());
	                $log->debug("####".$file_path.$postpicInfo->getPicPath());
	                $path=pathinfo($file_path.$postpicInfo->getPicPath());
				    if (!file_exists($path['dirname'])){
							Zend_Search_Lucene_Storage_Directory_Filesystem::mkdirs($path['dirname']);
					}
		            rename($file_path.$temppostpicInfo->getPicPath(),$file_path.$postpicInfo->getPicPath());
	                $path=pathinfo($file_path.$postpicInfo->getPicThumb());
				    if (!file_exists($path['dirname'])){
						Zend_Search_Lucene_Storage_Directory_Filesystem::mkdirs($path['dirname']);
					}
	                rename($file_path.$temppostpicInfo->getPicThumb(), $file_path.$postpicInfo->getPicThumb());
		        }
	            //post pic 处理 结束
        
        
   
        
        Logging::audit("Editing Post -> postId: " . $postId . " title: " . $title . " tags:" . $tags . "text: " . $html . " by user " . Util::getUserId());
        $this->_redirect("/post/modifypostok");
    }    
    
    
    function  delpostAction(){
    	//$this->_helper->viewRenderer->setNoRender();
    	$log = Zend_Registry::get('log');
        $log->debug( "################### test");		    	
	    $postId = $this->_getParam('postId');;
	    $postDao = Post_PostDao::getInstance();
	    $log->debug( "################### postId".$postId);
	    $postDao->delByIdUpdate($postId);
	    
	     $ds = new DbModel("","");
         $sql = "select * from pw_post_pic where post_id=$postId";
         $list = $ds->fetchAll($sql);
		 $config = Zend_Registry::get('config');
		 $file_path = $config->upload->root;
         foreach($list as $row){
         	 $pic_path = $row['pic_path'];
         	 $pic_thumb = $row['pic_thumb'];
         	 $path_deleted=pathinfo($file_path."/deleted".$pic_path);
			 if (!file_exists($path_deleted['dirname'])){
				Zend_Search_Lucene_Storage_Directory_Filesystem::mkdirs($path_deleted['dirname']);
			 }
			 if (file_exists($file_path.$pic_path)){
             	rename($file_path.$pic_path, $file_path."/deleted".$pic_path);
             }
			 if (file_exists($file_path.$pic_thumb)){
                 rename($file_path.$pic_thumb, $file_path."/deleted".$pic_thumb);
             }             
         }
         $sql = "update pw_post_pic set status=0 where post_id=$postId";
         $ds->query($sql);     
	    
	    
	    $currentUserId = Rbm_RbmService::getInstance()->getCurrentLoginUserId();
	    Logging::audit("Deleting Post -> postId:" . $postId . " by user " . $currentUserId);
	    $this->_redirect("/post/$postId");          	
	   
    }
    
    function  undelpostAction(){
    	//$this->_helper->viewRenderer->setNoRender();
    	$log = Zend_Registry::get('log');
        $log->debug( "################### test");		    	
	    $postId = $this->_getParam('postId');;
	    $postDao = Post_PostDao::getInstance();
	    $log->debug( "################### postId".$postId);
	    $postDao->undelByIdUpdate($postId);
	    
	    
	     $ds = new DbModel("","");
         $sql = "select * from pw_post_pic where post_id=$postId";
         $list = $ds->fetchAll($sql);
		 $config = Zend_Registry::get('config');
		 $file_path = $config->upload->root;
         foreach($list as $row){
         	 $pic_path = $row['pic_path'];
         	 $pic_thumb = $row['pic_thumb'];
         	 $path=pathinfo($file_path.$pic_path);
			 if (!file_exists($path['dirname'])){
				Zend_Search_Lucene_Storage_Directory_Filesystem::mkdirs($path['dirname']);
			 }
			 if (file_exists($file_path."/deleted".$pic_path)){
             	rename($file_path."/deleted".$pic_path, $file_path.$pic_path);
             }
			 if (file_exists($file_path."/deleted".$pic_thumb)){
                 rename($file_path."/deleted".$pic_thumb, $file_path.$pic_thumb);
             }             
         }
         $sql = "update pw_post_pic set status=1 where post_id=$postId";
         $ds->query($sql);
         
	    $currentUserId = Rbm_RbmService::getInstance()->getCurrentLoginUserId();
        Logging::audit("Recover Post -> postId:" . $postId . " by user " . $currentUserId);
	    $this->_redirect("/post/$postId");          	
    }
    
    function managetagAction(){
    	$currentUserId = Rbm_RbmService::getInstance()->getCurrentLoginUserId();
        if(!Rbm_RbmService::getInstance()->isOpenUser($currentUserId)){
            $this->_redirect('/index/notfound');
        }
            
        if(!Rbm_RbmService::getInstance()->isAdministrator($currentUserId) &&
            !Rbm_RbmService::getInstance()->isBanzhu($currentUserId)){
            $this->_redirect('/index/notfound');
            return;     
        }
            
        $postId = intval($this->_getParam('postId'));
        if($postId == 0){
        	$this->_redirect('/index/notfound');
            return ; 
        }
        $topId = Post_PostDao::getInstance()->getTopId($postId);
        $postInfo = Post_PostDao::getInstance()->get($topId);
        $tagInfoArray = Tag_TagDao::getInstance()->getTagInfoByPostId($topId);
        $userInfoMap = array();
        foreach ($tagInfoArray as $tagInfo){
        	$userId = $tagInfo->getUserId();
        	$userInfo = User_UserDao::getInstance()->get($userId);
        	$userInfoMap[$userId] = $userInfo;
        }
        $this->view->post = $postInfo;
        $this->view->tags = $tagInfoArray;
        $this->view->users = $userInfoMap;
    }    
    
    function deletetagAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	$postId = intval($this->_getParam('postId'));
        $tagName = $this->_getParam('tagName');
        $userId = intval($this->_getParam('userId'));
        Tag_TagDao::getInstance()->deleteTag($postId, $userId, $tagName);
        $currentUserId = Rbm_RbmService::getInstance()->getCurrentLoginUserId();
        Logging::audit("Deleting Tag -> tagName: $tagName postId: $postId userId: $userId by user $currentUserId");
        echo "{success: true, messages: { message: '标签删除成功!'}}"; 
    }
    
    
//    function  ceilpostAction(){
//    	//$this->_helper->viewRenderer->setNoRender();
//    	$log = Zend_Registry::get('log');
//        $log->debug( "################### test");		    	
//	    $postId = $this->_getParam('postId');;
//	    $postDao = Post_PostDao::getInstance();
//	      	$log->debug( "################### postId".$postId);
//	      	$postDao->ceilByIdUpdate($postId);
//	    $this->_redirect("/post/$postId");          	
//	   
//    }
//    
//    function  unceilpostAction(){
//    	//$this->_helper->viewRenderer->setNoRender();
//    	$log = Zend_Registry::get('log');
//        $log->debug( "################### test");		    	
//	    $postId = $this->_getParam('postId');;
//	    $postDao = Post_PostDao::getInstance();
//	      	$log->debug( "################### postId".$postId);
//	      	$postDao->unceilByIdUpdate($postId);
//	    $this->_redirect("/post/$postId");          	
//    }
    
    function  testAction(){
    	
    }
    
    function testpicuploadAction(){
    	
        $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
    	$log = Zend_Registry::get('log');
    	$postIdStr = $this->_getParam('postId');
        $log->debug( "################### test:".$postIdStr);	
        
        $forumId = $this->_getParam('forumId');
        $postId = intval($this->_getParam('postId'));
        $editFlag = intval($this->_getParam('editFlag'));
        
        if($_FILES["myfile"]["size"]> PaowangData::$orderedForumsUploadPicSizeById[$forumId]){
        	 $log->debug( "################### size:".$_FILES["myfile"]["size"]);
        	 $rtn = array('error' => '上传文件超过'.(PaowangData::$orderedForumsUploadPicSizeById[$forumId]/1000)."K");
        	 echo json_encode($rtn);
        	 return ;	
        }
        
        //扩展名判断 开始
        $pic_org_name = $_FILES["myfile"]["name"];
		$log->debug("pic_org_name:".$pic_org_name);
		$orig_arr = explode(".", $pic_org_name);
		$nr    = count($orig_arr);
		$file_ext  = strtolower(trim($orig_arr[$nr-1]));
		if(!($file_ext=='jpg' || $file_ext=='gif')){
        	 $rtn = array('error' => '请上传jpg或者gif格式的文件，其它格式的文件暂时不支持！');
        	 echo json_encode($rtn);
        	 return ;	
		}	
        //扩展名判断 结束        
        
        
        if($postId==0){
        	$postId = $this->dealhavepicAction();
        }else{
		    //加载postInfo 和 postTextInfo	
		    	$log->debug("editFlag:".$editFlag);
		    if($editFlag==1){		
              	$postDao = Post_PostDao::getInstance();
              	$log->debug("111111");
	        }else{
	        	$postDao = Temppost_TemppostDao::getInstance();
	        }	
	        $postInfo = $postDao->get($postId);
	$log->debug("sssssssss");
		    if($editFlag==1){		
	        	$posttextDao = Posttext_PosttextDao::getInstance();
	        }else{
	        	$posttextDao = Tempposttext_TempposttextDao::getInstance();
	        }		        
	        $posttextInfo = $posttextDao->get($postId);
	
	        
	        $forumId = intval($postInfo->getForumId());
	        
	        $log = Zend_Registry::get('log');
	        //text and length
	        /*
	        $html = $this->_getParam('text');
	        $log->debug("111:".$html);
	        $html = preg_replace("/<a(((?!\starget).)+?)>/i","<a\\1 target=\"_blank\">",$html);
	        $log->debug("222:".$html); 
	        
	        $text = Util::stripText($html);
	        $textLength = Util::strLenUtf8($text);
	        */
	        $html = "";
            $text = "";
            $textLength = 0;
	        
	        
	        //request
	        //$title = $this->_getParam('title');
	        //$tags = $this->_getParam('tags');
	        $title = "";
	        $tags = "";	        
	        $userId = Util::getUserId();
	        $userIp = Util::getUserIp();
	        $updateIp = $userIp;
	        $createAt = time();
	        $updateAt = $createAt;
	        $updateUserId = Util::getUserId();
	        $log->debug("updateUserId:".$updateUserId);
	        
	        $userDao = User_UserDao::getInstance();
	        $updateUserName =  $userDao->getUserNameByUserId($updateUserId);
	        
	                
	
	        //info
	        $postInfo->setTitle($title);
	        $postInfo->setTags($tags);
	        $postInfo->setUpdateUserId($updateUserId);
	        $postInfo->setUpdateUserName($updateUserName);
	        $postInfo->setTextLength($textLength);
	        $postInfo->setUpdateAt($updateAt);
	        $postInfo->setUpdateIp($updateIp);        
	        
	        $posttextInfo->setText($text);
	        $posttextInfo->setHtml($html);
	        
	
	        //dao
	        if($editFlag==1){
	        	//编辑的时候 不保存
            }else{
		        $postDao->modify($postInfo);
		        $posttextDao->modify($posttextInfo);            	
            }  	
        }
//        $title = $this->_getParam('title');
//        $text = $this->_getParam('text');
//        $tags = $this->_getParam('tags');
//        $log->debug( "################### forumId:".$forumId);
//        $log->debug( "################### postId:".$postId);
//        $log->debug( "################### title:".$title);
//        $log->debug( "################### text:".$text);
//        $log->debug( "################### tags:".$tags);
    
                    
//        $postId = 1;
	    if($editFlag==1){		
          	//$postDao = Post_PostDao::getInstance();
          	//编辑的时候，新增的图片也存放在临时表中
          	$postDao = Temppost_TemppostDao::getInstance();
        }else{
        	$postDao = Temppost_TemppostDao::getInstance();
        }
        $rtn = $postDao->uploadPicturesOne($postId,$_FILES["myfile"],$editFlag);
//		//随机回传成功或失败
//		if (mt_rand(1, 10) != 1) {
//		  //成功时回传处理过后的档名
//		  $random = rand(1, 5);
//		  $rtn = array('success' => '/images/sample'. $random .'.jpg');
//		} else {
//		  //失败时回传错误讯息
//		  $rtn = array('error' => 'upload file too big or something!');
//		}
		echo json_encode($rtn);
		
		//sleep(rand(1, 3));  //模拟上传过程的耗时时间    	
    }
    
    function delpicAction(){
		$this->_helper->viewRenderer->setNoRender();
		$config = Zend_Registry::get('config');
		$file_path = $config->upload->root ;	          
        $postPicId = intval($this->_getParam('postPicId'));
        $editFlag = intval($this->_getParam('editFlag'));
        
	    if($editFlag==1){		
          	$postpicDao = Postpic_PostpicDao::getInstance();
        }else{
        	$postpicDao = Temppostpic_TemppostpicDao::getInstance();
        }        
        
        $postpicInfo = $postpicDao->get($postPicId);
        unlink($file_path.$postpicInfo->getPicPath());
        unlink($file_path.$postpicInfo->getPicThumb());
        unlink($file_path.$postpicInfo->getPicThumb2());
        unlink($file_path.$postpicInfo->getPicThumb3());
        $postpicDao->del($postpicInfo);
    }
    
    function temp2onlineAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	Logging::debug("entering temp2onlin");
    	$forumId = intval($this->_getParam('forumId'));
    	Logging::debug("------------forumId " . $forumId);
        if($forumId==0){
            $forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖论剑页面
        } 
        $currentUserId = Util::getUserId();
        $canAddPost = Rbm_RbmService::getInstance()->hasPermission($currentUserId, $forumId, Rbm_RbmService::USER_ADD_POST);
        if(!$canAddPost){
        	echo "{success: false, messages: { message: '只有剑的会员才可以在剑里加帖。请参考使用说明。'}}";
        	return;
        }
        Logging::debug("---------currentUserId " . $currentUserId);
        $isUserOpen = Rbm_RbmService::getInstance()->isOpenUser($currentUserId);
        if(!$isUserOpen){
            echo "{success: false, messages: { message: '此用户已被关闭。'}}";
            return;
        }


       
   	    $latest_title = $this->_getParam('title');
  	    $latestHtml = $this->_getParam('text');
  	    $keyfileName = $_SERVER['DOCUMENT_ROOT']."/../../picture/data/key.txt";
	    $keycontent = file_get_contents($keyfileName);
	    	    
	    $keyArray =   preg_split ('/([\s,;]+)/', $keycontent);
	    
	    foreach($keyArray as $keyname){
	        if(preg_match("/".$keyname."/",$latestHtml) || preg_match("/".$keyname."/",$latest_title) ){
	            echo "{success: false, messages: { message: '发贴失败，请检查您的发帖内容!'}}";
	            return;
	        }
        }
                
        $postId = intval($this->_getParam('postId'));
        Logging::debug("temp2online postId is " . $postId);
          if($postId>0){

          	    $latest_tags = $this->_getParam('tags');
                $log = Zend_Registry::get('log');

                
                $latestHtml = preg_replace("/<a(((?!\starget).)+?)>/i","<a\\1 target=\"_blank\">",$latestHtml);
                
//                $latestHtml = str_replace("\\\"", "\"", $latestHtml);//替换一下\"
                $log->debug("--------------text:".$latestHtml); 
                $latestText = Util::stripText($latestHtml);
                $latestTextLength = Util::strLenUtf8($latestText);          	    
          	
                //上传 图片预览 的上线      
		        //post 处理 开始  
		        $temppostDao = Temppost_TemppostDao::getInstance();
		        $postDao = Post_PostDao::getInstance();
                    $userId = Util::getUserId();
                    
$registerDays = $postDao->getRegisterDays($userId);
if($registerDays<31){
	//发贴受用户id限制
                    //$maxPostCount = $postDao->getPostDayMaxCountByUserId($userId);
                    $maxPostCount = 6;
                    $nowPostCount = $postDao->getPostDayCountByUserId($userId);
                    if($nowPostCount>=$maxPostCount){
                          echo "{success: false, messages: { message: '发贴失败，您今天最多允许发帖数为$maxPostCount!'}}";
                          return;
                    }
   //发贴受用户ip限制
                   $nowPostCountByIp = $postDao->getPostDayCountByUserIp(Util::getUserIp());
                    if($nowPostCountByIp>=$maxPostCount){
                          echo "{success: false, messages: { message: '发贴失败，您今天最多允许发帖数为$maxPostCount!'}}";
                          return;
                    }                    

}



	            $temppostInfo = $temppostDao->get($postId);
	            $postInfo = new Post_PostInfo();
	            $postInfo->temp2online($temppostInfo); 
	            $postInfo->setPostId(0);
	            $postInfo->setTitle($latest_title);
	            $postInfo->setTextLength($latestTextLength);
	            $postDao->add($postInfo);
		        $postInfo->setTopId($postInfo->getPostId());
		        $postDao->modify($postInfo);	            
	            //post 处理 结束
	            
		        //post text 处理 开始  
		        
		        $tempposttextDao = Tempposttext_TempposttextDao::getInstance();
		        $posttextDao = Posttext_PosttextDao::getInstance();
	            $tempposttextInfo = $tempposttextDao->get($postId);
	            $posttextInfo = new Posttext_PosttextInfo();
	            
	            $posttextInfo->temp2online($tempposttextInfo); 
	            $posttextInfo->setPostId($postInfo->getPostId());
	            $posttextInfo->setText($latestText);
	            $posttextInfo->setHtml($latestHtml);
	            $posttextDao->add($posttextInfo);
	            //post text 处理 结束
	            
		        //post pic 处理 开始  
		        $temppostpicDao = Temppostpic_TemppostpicDao::getInstance();
		        $temppiclists = $temppostpicDao->getListByPostId($postId);
				$postpicDao = Postpic_PostpicDao::getInstance();
				$config = Zend_Registry::get('config');
				$file_path = $config->upload->root ;
				$log = Zend_Registry::get('log');
		        foreach ($temppiclists as $temppostpicInfo){
		            $postpicInfo = new Postpic_PostpicInfo();
		            $postpicInfo->temp2online($temppostpicInfo);
		            $postpicInfo->setPicPath(str_replace('/temp','',$postpicInfo->getPicPath())); 
		            $postpicInfo->setPicThumb(str_replace('/temp','',$postpicInfo->getPicThumb()));
		            $postpicInfo->setPicThumb2(str_replace('/temp','',$postpicInfo->getPicThumb2()));
	                $postpicInfo->setPicThumb3(str_replace('/temp','',$postpicInfo->getPicThumb3()));
	                
		            $postpicInfo->setPostId($postInfo->getPostId());
		            //注意顺序，需要先增加一条记录，再去更新
		            $postpicDao->add($postpicInfo);
		            $log->debug("####".$postpicInfo->getPicPath());
	                $postpicInfo->setPicName(str_replace($temppostInfo->getPostId().'_'.$temppostpicInfo->getPostPicId(),$postInfo->getPostId().'_'.$postpicInfo->getPostPicId(),$postpicInfo->getPicName()));	            
		            $postpicInfo->setPicPath(str_replace($temppostInfo->getPostId().'_'.$temppostpicInfo->getPostPicId(),$postInfo->getPostId().'_'.$postpicInfo->getPostPicId(),$postpicInfo->getPicPath())); 
		            $postpicInfo->setPicThumb(str_replace($temppostInfo->getPostId().'_'.$temppostpicInfo->getPostPicId(),$postInfo->getPostId().'_'.$postpicInfo->getPostPicId(),$postpicInfo->getPicThumb()));
		            $postpicInfo->setPicThumb2(str_replace($temppostInfo->getPostId().'_'.$temppostpicInfo->getPostPicId(),$postInfo->getPostId().'_'.$postpicInfo->getPostPicId(),$postpicInfo->getPicThumb2()));
	                $postpicInfo->setPicThumb3(str_replace($temppostInfo->getPostId().'_'.$temppostpicInfo->getPostPicId(),$postInfo->getPostId().'_'.$postpicInfo->getPostPicId(),$postpicInfo->getPicThumb3()));
	                $postpicInfo->setPicDesc($this->_getParam('picDesc'.$temppostpicInfo->getPostPicId()));
	                $postpicDao->modify($postpicInfo);
		            $log->debug("####".$postpicInfo->getPicPath());	                
	                //注意源文件名和目标文件名	            
	                $log->debug("####".$file_path.$temppostpicInfo->getPicPath());
	                $log->debug("####".$file_path.$postpicInfo->getPicPath());
	                $path=pathinfo($file_path.$postpicInfo->getPicPath());
				    if (!file_exists($path['dirname'])){
							Zend_Search_Lucene_Storage_Directory_Filesystem::mkdirs($path['dirname']);
					}
		            rename($file_path.$temppostpicInfo->getPicPath(),$file_path.$postpicInfo->getPicPath());
	                $path=pathinfo($file_path.$postpicInfo->getPicThumb());
				    if (!file_exists($path['dirname'])){
						Zend_Search_Lucene_Storage_Directory_Filesystem::mkdirs($path['dirname']);
					}
	                rename($file_path.$temppostpicInfo->getPicThumb(), $file_path.$postpicInfo->getPicThumb());
	                /*
	                $path=pathinfo($file_path.$postpicInfo->getPicThumb2());
				    if (!file_exists($path['dirname'])){
						Zend_Search_Lucene_Storage_Directory_Filesystem::mkdirs($path['dirname']);
					}
	                rename($file_path.$temppostpicInfo->getPicThumb2(), $file_path.$postpicInfo->getPicThumb2());
	                $path=pathinfo($file_path.$postpicInfo->getPicThumb3());
				    if (!file_exists($path['dirname'])){
						Zend_Search_Lucene_Storage_Directory_Filesystem::mkdirs($path['dirname']);
					}
	                rename($file_path.$temppostpicInfo->getPicThumb3(), $file_path.$postpicInfo->getPicThumb3());
	                */
		        }
	            //post pic 处理 结束
	            $userId = Util::getUserId();
	            $createAt = time();
	            $userIp = Util::getUserIp();
	            //标签 处理 开始
		        $tagArray =   preg_split ('/([\s,;]+)/', $latest_tags);
		        $tagDao = Tag_TagDao::getInstance();
		        $tagDao->delAllByUserIdAndTopId($userId,$postInfo->getTopId());
		        foreach($tagArray as $tagName){
		            $tagInfo = new Tag_TagInfo();
		            $tagInfo->setPostId($postInfo->getPostId());
		            $tagInfo->setTagName($tagName);
		            $tagInfo->setStatus(1);
		            $tagInfo->setUserId($userId);
		            $tagInfo->setCreateAt($createAt);
		            $tagInfo->setUserIp($userIp);
		            $tagInfo->setTagType(1);//帖子
		            $tagInfo->setTagUserId($userId);//帖子发布者的ID
		            
		            if($tagDao->getListAllCountByUserIdAndCreateUserIdAddTagNameAndTopId($userId,$postInfo->getUserId(),$tagName,$postInfo->getTopId())<1){
		              $tagDao->add($tagInfo);    
		            }           
		        }	            
	            //标签 处理 结束
	            
	            //删除临时表里的数据
	            $temppostDao->delById($postId);
//		    	$this->_redirect("/post/addpostok");
                echo "{success: true,forumId:$forumId }";
		    	//不再上传图片 结束
          }else{
              //不上传图片的处理         	
          	  $this->dealnopicAction();
          }	    	    	
    }
    
	    function  jsongetAction(){
		   $postId = intval($this->_getParam('postId'));
		   $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
 	       
        	//加载postInfo 和 postTextInfo         
            $postDao = Post_PostDao::getInstance();
            $postInfo = $postDao->get($postId);
            $posttextDao = Posttext_PosttextDao::getInstance();
            $posttextInfo = $posttextDao->get($postId);
//            $postpicDao = Postpic_PostpicDao::getInstance();
//            $piclists =  $postpicDao->getListByPostId($postId);
            $isManage = false;            
            if( Rbm_RbmService::canEditPost(Util::getUserId(), $postInfo->getForumId(), $postInfo->getPostId())){
                $isManage = true;	
            }
             	       
           $data = array('postInfo' => $postInfo,
                         'posttextInfo'=>$posttextInfo,
                         //'piclists'=>$piclists,
                         'isManage'=>$isManage
                   );
	       $json = Zend_Json::encode($data);
	       echo $json;
	    }
	    
	    
	    function  xmlgetAction(){
		   $postId = intval($this->_getParam('postId'));
		   $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
 	       
        	//加载postInfo 和 postTextInfo         
            $postDao = Post_PostDao::getInstance();
            $postInfo = $postDao->get($postId);
            $posttextDao = Posttext_PosttextDao::getInstance();
            $posttextInfo = $posttextDao->get($postId);
//            $postpicDao = Postpic_PostpicDao::getInstance();
//            $piclists =  $postpicDao->getListByPostId($postId);
            $isManage = false;            
            if( Rbm_RbmService::canEditPost(Util::getUserId(), $postInfo->getForumId(), $postInfo->getPostId())){
                $isManage = true;	
            }
             	       
           $data = array('postInfo' => $postInfo,
                         'posttextInfo'=>$posttextInfo,
                         //'piclists'=>$piclists,
                         'isManage'=>$isManage
                   );
	       $json = Zend_Json::encode($data);
include_once( $_SERVER['DOCUMENT_ROOT']."/../library/xml/util.php");	       
include_once( $_SERVER['DOCUMENT_ROOT']."/../library/xml/json2xml.php");
			$dom = json2xml($data, 'root', "utf-8");
			$dom->formatOutput = true;
			echo $dom->saveXML();	       
	       //echo $json;
	    }	    
	    
	    function  jsongetbytopidAction(){
		   $topId = intval($this->_getParam('topId'));
 	       $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
 	       
        	//加载postInfo 和 postTextInfo         
           $postDao = Post_PostDao::getInstance();
           $postArray = $postDao->getPostListByTopId($topId);
           $postpicDao = Postpic_PostpicDao::getInstance();
           $piclists =  $postpicDao->getListByPostId($topId);                      
           $config = Zend_Registry::get('config');
           $data = array('postArray' => $postArray,
                         'piclists'=>$piclists,
                         'preurl'=>$config->general->picture->url
                   );
           
	       $json = Zend_Json::encode($data);
	       echo $json;
	    }



	    	    
	    function upgrade1Action(){
         $ds = new DbModel("","");
         $sql = "select * from pw_post where top_id=0 limit 0,30000";
         $this->_helper->viewRenderer->setNoRender();
         $list = $ds->fetchAll($sql);
         $postDao = Post_PostDao::getInstance();
         foreach($list as $row){
         	$sql = "update pw_post set top_id=".$postDao->getTopId($row['post_id']).",depth=".$postDao->getDepth($row['post_id'])." where post_id=".$row['post_id'];
         	echo $sql."<br />";
         	$ds->query($sql);
         }
         //echo "####:".$postDao->getTopId(4000135); 
	    }
	    
	    /*
	     * 
	     */
	    function upgrade2Action(){
         $ds = new DbModel("","");
         $sql = "select * from pw_post p ,pw_post_text pt where p.post_id=pt.post_id and top_id>0 and user_id=0 limit 0,25000";
         $this->_helper->viewRenderer->setNoRender();
         $list = $ds->fetchAll($sql);
         $postDao = Post_PostDao::getInstance();
         $userId = 0;
         $canHaschild = 1;
         $textLength = 0;
//$text = Util::stripText($html);
//        $textLength = Util::strLenUtf8($text);
         $click = rand(0, 2);
         $isPic = 0;  
         $lastCchildUpdateAt = 0;
         $childPosts = 0;         
         foreach($list as $row){
            $postId = $row['post_id'];
            $parentId = $row['parent_id'];
            $html = $row['html'];
            $userName = $row['user_name'];
            $userId = $postDao->getUserId($userName);
            $text = Util::stripText($html);
            $textLength = Util::strLenUtf8($text); 
            $click = $postDao->getClick($postId);
            $isPic = $postDao->getIsPic($postId);
            if($parentId==0){
               $lastChildUpdateAt = $postDao->getLastChildUpdateAt($postId);
               $childPosts  = $postDao->getChildPosts ($postId);
            }else{
                $lastChildUpdateAt = 0;
                $childPosts  = 0;
            }	
         	$sql = "update pw_post set user_id=".$userId.",text_length=".$textLength.",click=".$click.",is_pic=".$isPic.",last_child_update_at=".$lastChildUpdateAt.",child_posts=".$childPosts." where post_id=".$postId;
         	echo $sql."<br />";
         	$ds->query($sql);
         	//$sql = "update pw_post_text set text='".$text."' where post_id=".$postId;
         	//echo $sql."<br />";
         	//$ds->query($sql);         	
         }
         //echo "####:".$postDao->getTopId(4000135); 
	    }    

       function upgrade3Action(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
		$table = "";
		$msgId = 0;
		$forumId = intval($this->_getParam('forumId'));
		if($forumId==0){
			$forumId = 1;
		}
		if($forumId == 1){
			$table = "msg_paowang";
		}else if($forumId == 2){
			$table = "msg_qin";
		}else if($forumId == 3){
			$table = "msg_inn";
		}else if($forumId == 4){
			$table = "msg_weapon";
		}else if($forumId == 5){
			$table = "msg_music";
		}else if($forumId == 6){
			$table = "msg_money";
		}else if($forumId == 7){
			$table = "msg_it";
		}else if($forumId == 8){
			$table = "msg_parenting";
		}else if($forumId == 9){
			$table = "msg_photo";
		}else if($forumId == 11){
			$forumId = 1;
			$table = "msg_paowang1";
		}         
         $sql = "select msgid,subject,mm_url from $table where parent=0 and mm_url!='' and not exists (select 'x' from pw_post_inc where ($table.msgid+$forumId*1000000)=pw_post_inc.post_id) limit 0,4000";
         // limit 0,100
         //$sql = "select msgid,subject,mm_url from msg_photo where msgid=269174 limit 0,1";
         echo $sql."<br />";
         $list = $ds->fetchAll($sql);
         $postDao = Post_PostDao::getInstance();
         $rtn = array();
         foreach($list as $row){
         	echo $row['msgid']."<br />";
         	$postDao->upgradePicturesThread($row['msgid'],$row['msgid'],$table,$forumId,$rtn);
         	$sql = "insert into pw_post_inc values($forumId*1000000+".$row['msgid'].")";
            echo $sql."<br />";         	
         	$ds->query($sql);
         }
         
	    }	  
	    
	    
       function upgrade11Action(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
         $sql = "select post_id,pic_path from pw_post_pic where not exists (select 'x' from pw_post_pic_h where pw_post_pic.post_id=pw_post_pic_h.post_id) order by post_id desc limit 0,5000";
         // limit 0,100
         //$sql = "select msgid,subject,mm_url from msg_photo where msgid=269174 limit 0,1";
         echo $sql."<br />";
         $list = $ds->fetchAll($sql);
         $postDao = Post_PostDao::getInstance();
         $rtn = array();
         foreach($list as $row){
         	echo $row['pic_path']."<br />";
         	$postDao->upgradePicturesHeight($row['post_id'],$row['pic_path']);
         	$sql = "insert into pw_post_pic_h values(".$row['post_id'].")";
            echo $sql."<br />";         	
         	$ds->query($sql);
         }
         
	    }
	    	    
	    
       function upgradebypostidAction(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
		$table = "";
		$msgId = 0;
		$forumId = intval($this->_getParam('forumId'));
		$postId = intval($this->_getParam('postId'));
		if($forumId==0){
			$forumId = 1;
		}
		if($forumId == 1){
			$table = "msg_paowang";
		}else if($forumId == 2){
			$table = "msg_qin";
		}else if($forumId == 3){
			$table = "msg_inn";
		}else if($forumId == 4){
			$table = "msg_weapon";
		}else if($forumId == 5){
			$table = "msg_music";
		}else if($forumId == 6){
			$table = "msg_money";
		}else if($forumId == 7){
			$table = "msg_it";
		}else if($forumId == 8){
			$table = "msg_parenting";
		}else if($forumId == 9){
			$table = "msg_photo";
		}else if($forumId == 11){
			$forumId = 1;
			$table = "msg_paowang1";
		}         
         $sql = "select msgid,subject,mm_url from $table where parent=0 and msgid=$postId-$forumId*1000000";
         //and mm_url!='' 2009/07/07 去掉此条件，因为旧帖子有些图片不是从根贴开始加的
         // limit 0,100
         //$sql = "select msgid,subject,mm_url from msg_photo where msgid=269174 limit 0,1";
         echo $sql."<br />";
         
         $list = $ds->fetchAll($sql);
         $postDao = Post_PostDao::getInstance();
         $rtn = array();
         foreach($list as $row){
         	echo $row['msgid']."<br />";
         	$sql = "delete from  pw_post_pic where post_id=".$postId;
            echo $sql."<br />";
         	$ds->query($sql);
         	$sql = "delete from  pw_post_inc where post_id=".$postId;
            echo $sql."<br />";
         	$ds->query($sql);         	
         	$postDao->upgradePicturesThread($row['msgid'],$row['msgid'],$table,$forumId,$rtn);
         	$sql = "insert into pw_post_inc values($forumId*1000000+".$row['msgid'].")";
            echo $sql."<br />";         	
         	$ds->query($sql);
         }
         
	    }	  
	    /*
	     * 补救没有转好的图片
	     */
       function upgrade4Action(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
		$table = "";
		$msgId = 0;
		$forumId = intval($this->_getParam('forumId'));
		$postId = intval($this->_getParam('postId'));
		if($forumId==0){
			$forumId = 1;
		}
		if($forumId == 1){
			$table = "msg_paowang";
		}else if($forumId == 2){
			$table = "msg_qin";
		}else if($forumId == 3){
			$table = "msg_inn";
		}else if($forumId == 4){
			$table = "msg_weapon";
		}else if($forumId == 5){
			$table = "msg_music";
		}else if($forumId == 6){
			$table = "msg_money";
		}else if($forumId == 7){
			$table = "msg_it";
		}else if($forumId == 8){
			$table = "msg_parenting";
		}else if($forumId == 9){
			$table = "msg_photo";
		}else if($forumId == 11){
			$forumId = 1;
			$table = "msg_paowang1";
		}           
		//select count(*) from msg_photo where parent=0 and mm_url like '/photo/uploaded/%' 
         $sql = "select msgid,subject,mm_url from $table where parent=0  and mm_url like '/".(str_replace("msg_", "", $table))."/uploaded/%' and (upper(mm_url) like '%JPG' or upper(mm_url) like '%GIF' or upper(mm_url) like '%JPEG' or mm_url like '%1jpg'  or mm_url like '%2jpg'  or mm_url like '%1'  or mm_url like '%2') and not exists (select 'x' from pw_post_pic where ($table.msgid+$forumId*1000000)=pw_post_pic.post_id) limit 0,4000";
         // limit 0,100
         //$sql = "select msgid,subject,mm_url from msg_photo where msgid=269174 limit 0,1";
         echo $sql."<br />";
         $log = Zend_Registry::get('log');
         $log->debug($sql);         
         
         $list = $ds->fetchAll($sql);
         $postDao = Post_PostDao::getInstance();
         $rtn = array();
         foreach($list as $row){
         	echo $row['msgid']."<br />";
         	
         	$postId =$row['msgid']+$forumId*1000000;
         	system("curl \"http://dev.uupage.com/post/upgradebypostid/forumId/$forumId/postId/$postId\"");
         	/*
         	$sql = "delete from  pw_post_pic where post_id=".$postId;
         	$log->debug($sql);
            echo $sql."<br />";
         	$ds->query($sql);
         	$sql = "delete from  pw_post_inc where post_id=".$postId;
            echo $sql."<br />";
         	$ds->query($sql);
         	$log->debug($sql); 
         	        	
         	$postDao->upgradePicturesThread($row['msgid'],$row['msgid'],$table,$forumId,$rtn);
         	sleep(1);
         	
         	$sql = "insert into pw_post_inc values($forumId*1000000+".$row['msgid'].")";
         	$log->debug($sql);
            echo $sql."<br />";         	
         	$ds->query($sql);
         	*/
         	
         }
         
	    }	    
	    
 
	    /*
	    *处理帖子前面的非本站图片和后面的相关链接
	    */
       function upgrade5Action(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
		$table = "";
		$msgId = 0;
		$forumId = intval($this->_getParam('forumId'));
		if($forumId==0){
			$forumId = 1;
		}
		if($forumId == 1){
			$table = "msg_paowang";
		}else if($forumId == 2){
			$table = "msg_qin";
		}else if($forumId == 3){
			$table = "msg_inn";
		}else if($forumId == 4){
			$table = "msg_weapon";
		}else if($forumId == 5){
			$table = "msg_music";
		}else if($forumId == 6){
			$table = "msg_money";
		}else if($forumId == 7){
			$table = "msg_it";
		}else if($forumId == 8){
			$table = "msg_parenting";
		}else if($forumId == 9){
			$table = "msg_photo";
		}else if($forumId == 11){
			$forumId = 1;
			$table = "msg_paowang1";
		}         
         $sql = "select $table.msgid+$forumId*1000000 post_id,msgid,subject,mm_url,quote_url,quote_title,case when mm_url!='' then concat('<img src=\"',mm_url,'\" border=\"0\"/>') else '' end img_html,  case when quote_url is not null && quote_url!='' then   concat('<a href=\"',quote_url,'\" target=\"_blank\">',quote_title,'</a>') else '' end a_html from $table where mm_url like 'http://%' and not exists (select 'x' from pw_post_inc5 where ($table.msgid+$forumId*1000000)=pw_post_inc5.post_id) limit 0,8000";
         // limit 0,100
         //$sql = "select msgid,subject,mm_url from msg_photo where msgid=269174 limit 0,1";
         echo $sql."<br />";
         $log = Zend_Registry::get('log');
         $log->debug($sql);
         $list = $ds->fetchAll($sql);
         $postDao = Post_PostDao::getInstance();
         $rtn = array();
         foreach($list as $row){
         	echo $row['msgid']."<br />";
         	echo $row['post_id']."<br />";
         	
         	$a_html = "";
         	//echo "row['a_html']:".$row['a_html'];
         	if($row['a_html']!=''){
         		$a_html .=$row['a_html']."\n";
         	}
         	$img_html = "";
			//echo "row['img_html']:".$row['img_html'];         	
         	if($row['img_html']!=''){
         		$img_html .=$row['img_html']."\n";
         	}         	
         	
         	//echo "a_html:".$a_html."<br />";
         	//echo "img_html:".$img_html."<br />";
         	$exception = 0;
   	        try{
	         	$sql = "update  pw_post_text set html=concat('".$img_html."',html,'".$a_html."') where post_id=".$row['post_id'];
	         	$log->debug($sql);
	            echo $sql."<br />";         	
	         	$ds->query($sql);
	        }catch (Exception $e){
	            $log->debug("Exception caught : {$e->getMessage()}\n");
	            $exception = 1;
	        }
         	$sql = "insert into pw_post_inc5 values(".$row['post_id'].",$exception)";
         	$log->debug($sql);
            echo $sql."<br />";         	
         	$ds->query($sql);	        
	        
	        
         }
       } 
	    
	    
	    /*
	    *处理帖子前面的非本站图片和后面的相关链接
	    */
       function upgrade55Action(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
		$table = "";
		$msgId = 0;
		$forumId = intval($this->_getParam('forumId'));
		if($forumId==0){
			$forumId = 1;
		}
		if($forumId == 1){
			$table = "msg_paowang";
		}else if($forumId == 2){
			$table = "msg_qin";
		}else if($forumId == 3){
			$table = "msg_inn";
		}else if($forumId == 4){
			$table = "msg_weapon";
		}else if($forumId == 5){
			$table = "msg_music";
		}else if($forumId == 6){
			$table = "msg_money";
		}else if($forumId == 7){
			$table = "msg_it";
		}else if($forumId == 8){
			$table = "msg_parenting";
		}else if($forumId == 9){
			$table = "msg_photo";
		}else if($forumId == 11){
			$forumId = 1;
			$table = "msg_paowang1";
		}         
         $sql = "select $table.msgid+$forumId*1000000 post_id,msgid,subject,mm_url,quote_url,quote_title,  case when quote_url is not null && quote_url!='' then   concat('<a href=\"',quote_url,'\" target=\"_blank\">',quote_title,'</a>') else '' end a_html from $table where quote_title!='' and not exists (select 'x' from pw_post_inc5 where  ($table.msgid+$forumId*1000000)=pw_post_inc5.post_id) limit 0,8000";
         // limit 0,100
         //$sql = "select msgid,subject,mm_url from msg_photo where msgid=269174 limit 0,1";
         echo $sql."<br />";
         $log = Zend_Registry::get('log');
         $log->debug($sql);
         $list = $ds->fetchAll($sql);
         $postDao = Post_PostDao::getInstance();
         $rtn = array();
         foreach($list as $row){
         	echo $row['msgid']."<br />";
         	echo $row['post_id']."<br />";
         	
         	$a_html = "";
         	//echo "row['a_html']:".$row['a_html'];
         	if($row['a_html']!=''){
         		$a_html .=$row['a_html']."\n";
         	}
         	$exception = 0;
   	        try{
	         	$sql = "update  pw_post_text set html=concat(html,'".$a_html."') where post_id=".$row['post_id'];
	         	$log->debug($sql);
	            echo $sql."<br />";         	
	         	$ds->query($sql);
	        }catch (Exception $e){
	            $log->debug("Exception caught : {$e->getMessage()}\n");
	            $exception = 1;
	        }
         	$sql = "insert into pw_post_inc5 values(".$row['post_id'].",$exception)";
         	$log->debug($sql);
            echo $sql."<br />";         	
         	$ds->query($sql);	        
	        
	        
         }
       } 
       	    
	    /*
	    *处理帖子前面的非本站图片和后面的相关链接
	    */
       function upgrade5bypostidAction(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
		$table = "";
		$msgId = 0;
		$forumId = intval($this->_getParam('forumId'));
		$postId = intval($this->_getParam('postId'));
		if($forumId==0){
			$forumId = 1;
		}
		if($forumId == 1){
			$table = "msg_paowang";
		}else if($forumId == 2){
			$table = "msg_qin";
		}else if($forumId == 3){
			$table = "msg_inn";
		}else if($forumId == 4){
			$table = "msg_weapon";
		}else if($forumId == 5){
			$table = "msg_music";
		}else if($forumId == 6){
			$table = "msg_money";
		}else if($forumId == 7){
			$table = "msg_it";
		}else if($forumId == 8){
			$table = "msg_parenting";
		}else if($forumId == 9){
			$table = "msg_photo";
		}else if($forumId == 11){
			$forumId = 1;
			$table = "msg_paowang1";
		}         
         $sql = "select $table.msgid+$forumId*1000000 post_id,msgid,subject,mm_url,quote_url,quote_title,case when mm_url!='' then concat('<img src=\"',mm_url,'\" border=\"0\"/>') else '' end img_html,  case when quote_url is not null && quote_url!='' then   concat('<a href=\"',quote_url,'\" target=\"_blank\">',quote_title,'</a>') else '' end a_html from $table where mm_url like 'http://%' and msgid=$postId-$forumId*1000000 ";
         // limit 0,100
         //$sql = "select msgid,subject,mm_url from msg_photo where msgid=269174 limit 0,1";
         echo $sql."<br />";
         $list = $ds->fetchAll($sql);
         $postDao = Post_PostDao::getInstance();
         $rtn = array();
         foreach($list as $row){
         	echo $row['msgid']."<br />";
         	echo $row['post_id']."<br />";
         	
         	$sql = "delete from  pw_post_inc5 where post_id=".$postId;
            echo $sql."<br />";
         	$ds->query($sql);
         	$a_html = "";
         	echo "row['a_html']:".$row['a_html'];
         	if($row['a_html']!=''){
         		$a_html .=$row['a_html']."\n";
         	}
         	$img_html = "";
			echo "row['img_html']:".$row['img_html'];         	
         	if($row['img_html']!=''){
         		$img_html .=$row['img_html']."\n";
         	}         	
         	
         	echo "a_html:".$a_html."<br />";
         	echo "img_html:".$img_html."<br />";
         	$sql = "update  pw_post_text set html=concat('".$img_html."',html,'".$a_html."') where post_id=".$row['post_id'];
            echo $sql."<br />";         	
         	$ds->query($sql);
         	$sql = "insert into pw_post_inc5 values(".$row['post_id'].")";
            echo $sql."<br />";         	
         	$ds->query($sql);
         }
         
	    }	
	    
       function upgrade55bypostidAction(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
		$table = "";
		$msgId = 0;
		$forumId = intval($this->_getParam('forumId'));
		$postId = intval($this->_getParam('postId'));
		if($forumId==0){
			$forumId = 1;
		}
		if($forumId == 1){
			$table = "msg_paowang";
		}else if($forumId == 2){
			$table = "msg_qin";
		}else if($forumId == 3){
			$table = "msg_inn";
		}else if($forumId == 4){
			$table = "msg_weapon";
		}else if($forumId == 5){
			$table = "msg_music";
		}else if($forumId == 6){
			$table = "msg_money";
		}else if($forumId == 7){
			$table = "msg_it";
		}else if($forumId == 8){
			$table = "msg_parenting";
		}else if($forumId == 9){
			$table = "msg_photo";
		}else if($forumId == 11){
			$forumId = 1;
			$table = "msg_paowang1";
		}         
         $sql = "select $table.msgid+$forumId*1000000 post_id,msgid,subject,quote_url,quote_title,  case when quote_url is not null && quote_url!='' then   concat('<a href=\"',quote_url,'\" target=\"_blank\">',quote_title,'</a>') else '' end a_html from $table where msgid=$postId-$forumId*1000000 ";
         // limit 0,100
         //$sql = "select msgid,subject,mm_url from msg_photo where msgid=269174 limit 0,1";
         echo $sql."<br />";
         $list = $ds->fetchAll($sql);
         $postDao = Post_PostDao::getInstance();
         $rtn = array();
         foreach($list as $row){
         	echo $row['msgid']."<br />";
         	echo $row['post_id']."<br />";
         	
         	$sql = "delete from  pw_post_inc5 where post_id=".$postId;
            echo $sql."<br />";
         	$ds->query($sql);
         	$a_html = "";
         	echo "row['a_html']:".$row['a_html'];
         	if($row['a_html']!=''){
         		$a_html .=$row['a_html']."\n";
         	}
         	echo "a_html:".$a_html."<br />";
         	$sql = "update  pw_post_text set html=concat(html,'".$a_html."') where post_id=".$row['post_id'];
            echo $sql."<br />";         	
         	$ds->query($sql);
         	$sql = "insert into pw_post_inc5 values(".$row['post_id'].")";
            echo $sql."<br />";         	
         	$ds->query($sql);
         }
	    }
	    
	    function upgrade6Action(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
         $forumId = intval($this->_getParam('forumId'));
         if($forumId<1 && $forumId>9){
         	echo "forumId error";
         	return;
         }
         $sql = "select * from  pw_post_pic where post_id in (select post_id from pw_post where forum_id=$forumId)";
         $list = $ds->fetchAll($sql);
         foreach($list as $row){
            $path = "/www/site".$row['pic_path'];
         	if(!file_exists($path)){
         	   echo $row['post_id']." ".$path."<br />";	
         	   system("curl \"http://dev.uupage.com/post/upgradebypostid/forumId/$forumId/postId/".$row['post_id']."\"");
         	}
         	//$ds->query($sql);
         }
         //echo "####:".$postDao->getTopId(4000135); 
	    }
	    
	    /*
	     * 监控upgragde6的执行情况，不执行curl。
	     */
	    function upgrade7Action(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
         $forumId = intval($this->_getParam('forumId'));
         if($forumId<1 && $forumId>9){
         	echo "forumId error";
         	return;
         }
         $sql = "select * from  pw_post_pic where post_id in (select post_id from pw_post where forum_id=$forumId)";
         $list = $ds->fetchAll($sql);
         foreach($list as $row){
            $path = "/www/site".$row['pic_path'];
         	if(!file_exists($path)){
         	   echo $row['post_id']." ".$path."<br />";	
         	}
         	//$ds->query($sql);
         }
         //echo "####:".$postDao->getTopId(4000135); 
	    }
	    
	    
	   /*
	     * 监控msg_photo里没有转换成功的图片
	     */
	    function upgrade8Action(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
         $sql = "select concat('ll \"','/www/backup/infodaily/infodaily/htdocs',mm_url,'\"') full_path,msgid+9000000 post_id from msg_photo where msgid+9000000  in (9210144,9211355,9120429,9179309,9175932,9187214,9178290,9165706,9178579,9250976,9017614,9219564,9185892,9249674,9208709,9002373,9169834,9222270,9011936,9028188,9023356,9161155,9129392,9005378,9257679,9041878,9258236,9005268,9035145,9011264,9186428,9005589,9082497,9257491,9035914,9026136,9050630,9021220,9127383,9036840,9037592,9044673,9177007,9121891,9261640,9029842,9012642,9012625,9261967,9262784,9033080,9265727,9266675,9043041,9011176,9265732,9270275,9270665,9033916,9122917,9020703,9021223,9271485,9271620,9133994,9001107,9036669,9018138,9254006,9222285,9194581,9190675,9000401,9026830,9019620,9021912,9004367,9027254,9043786,9019907,9020466,9127165,9124161,9142726,9050504,9124124,9019606,9044464,9044155,9120136,9050564,9016405,9018624,9118002,9047807,9045234,9024273,9118621,9049281,9050507,9027153,9024154,9015736,9175072,9049121,9177471,9067288,9014338,9033419,9137364,9027414,9029108,9005437,9035913,9005610,9257543,9014613,9222076,9130526,9209754,9022873,9033335,9011901,9115829,9222956,9012185,9012617,9012726,9012729,9012861,9034149,9013120,9013246,9013445,9005440,9014044,9014429,9063403,9014867,9015000,9074049,9015006,9016891,9049230,9030138,9016412,9077906,9016429,9077907,9016433,9016501,9016565,9089256,9017049,9026320,9036503,9028014,9081163,9005439,9115858,9018688,9019512,9018958,9024448,9019619,9019903,9020030,9138230,9117785,9020444,9138662,9021788,9028392,9028403,9028753,9022874,9022046,9029369,9029383,9022213,9029576,9022875,9022878,9029771,9023051,9023355,9023061,9030651,9023087,9030980,9023368,9023391,9049909,9023401,9023412,9031092,9023416,9023417,9023533,9023560,9023604,9023741,9034342,9024248,9024274,9029212,9024450,9024643,9033333,9024666,9033377,9024832,9024839,9033440,9216470,9026273,9033954,9038353,9050509,9034070,9018637,9026676,9026798,9026806,9035603,9026889,9029296,9026890,9027041,9027155,9027160,9027173,9140299,9027742,9004447,9042677,9028362,9036660,9028391,9041886,9029750,9026500,9124162,9030070,9043747,9030750,9132043,9251555,9030986,9031072,9031088,9031591,9031791,9032116,9032947,9032807,9257680,9028307,9074576,9033054,9033160,9166302,9248286,9199321,9254882,9005438,9254482,9007083,9033212,9012646,9077934,9077933,9263590,9117852,9005615,9263281,9124164,9034181,9115322,9118245,9034151,9134886,9034008,9118010,9033965,9140977,9121333,9122697,9121560,9121678,9122829,9034234,9122309,9122667,9034182,9034238,9019905,9123779,9004226,9123921,9124930,9123197,9129373,9034594,9260911,9123823,9004278,9126583,9248199,9137616,9126804,9183169,9127393,9127412,9127739,9128518,9005562,9006177,9132040,9257678,9129543,9117012,9117272,9117270,9123635,9256985,9008331,9120201,9182834,9121418,9121303,9123918,9141462,9121434,9157144,9035144,9122918,9122081,9122405,9122439,9020437,9123899,9124749,9125807,9144188,9139719,9129932,9129299,9140124,9140279,9120870,9129746,9129857,9150909,9130437,9141334,9147795,9132045,9132172,9139886,9132171,9141495,9132542,9155575,9143543,9123003,9263562,9161034,9143565,9134990,9135058,9136410,9135884,9156905,9136549,9136413,9168009,9137716,9137723,9131179,9129187,9138771,9139100,9158468,9140093,9170005,9174623,9187679,9120698,9166836,9140370,9174622,9142403,9204299,9140452,9140818,9140696,9134398,9136576,9169322,9188275,9141189,9143797,9144013,9142400,9162592,9143556,9194582,9157944,9163288,9157124,9157714,9160039,9157139,9143542,9145879,9168279,9150600,9151422,9144626,9144370,9144897,9152122,9145681,9147140,9153475,9151911,9173277,9153467,9152125,9168008,9155610,9142951,9188678,9143570,9156291,9156760,9156815,9156881,9166816,9165182,9166834,9157304,9160319,9159807,9173468,9270775,9139885,9191444,9176601,9162238,9178866,9174997,9161800,9160856,9166831,9174497,9163737,9162171,9161245,9163110,9161407,9163113,9161801,9167664,9163290,9175575,9162706,9157140,9166840,9176699,9158701,9163286,9166188,9231618,9166833,9161797,9188755,9168392,9169060,9166842,9168010,9181966,9183168,9190674,9165708,9170744,9200790,9164375,9142972,9172367,9160972,9181708,9179855,9169349,9183413,9185272,9184268,9209768,9158771,9169399,9173673,9160768,9185730,9185113,9182199,9263117,9213787,9181240,9175931,9191451,9205974,9175187,9188272,9176139,9207051,9175457,9042491,9175665,9175706,9176454,9175774,9176547,9180768,9196367,9187789,9176599,9175090,9180973,9190829,9177109,9213401,9218186,9177865,9185864,9203109,9213018,9182907,9186566,9173691,9182390,9213388,9194792,9182498,9180227,9025067,9184239,9213625,9231748,9200827,9206927,9192812,9183092,9190292,9183869,9183155,9143540,9182715,9183071,9196882,9185019,9185662,9183093,9184251,9183194,9206115,9177985,9184386,9184054,9185575,9192172,9184335,9193241,9203001,9194583,9186284,9204594,9017382,9213648,9188457,9196795,9209773,9258259,9196931,9192672,9187788,9200922,9218607,9142855,9207549,9198678,9222417,9206926,9188680,9209436,9188871,9188872,9188975,9162147,9199544,9201801,9203952,9190731,9200360,9191137,9196737,9200789,9192674,9201146,9207859,9209545,9201770,9234211,9206454,9203034,9218593,9202506,9183072,9196444,9203382,9195941,9205932,9195269,9196698,9196204,9196436,9199326,9208805,9216717,9207559,9206995,9197129,9197208,9203441,9197288,9197944,9197371,9197483,9197718,9198738,9197841,9198690,9207897,9198329,9198275,9198276,9208710,9198279,9198368,9222128,9198367,9198442,9236560,9198723,9198746,9199015,9199012,9199043,9209762,9199041,9199285,9199334,9025283,9226611,9209792,9210037,9265315,9210010,9211038,9211807,9211813,9116959,9213140,9204917,9188273,9208108,9214879,9215789,9206161,9134320,9206272,9207294,9214780,9206923,9234819,9198274,9232256,9207260,9209356,9207895,9207604,9207187,9226930,9218156,9207356,9218187,9207551,9207550,9227873,9207724,9222811,9197294,9208109,9209791,9208630,9220226,9208503,9208685,9208692,9246071,9209313,9222416,9229792,9209707,9257977,9192229,9224917,9236288,9231098,9215200,9121279,9177744,9188274,9231785,9232097,9184385,9215097,9213019,9216899,9218227,9227547,9218670,9012084,9117606,9222803,9092439,9222439,9254690,9196814,9220227,9198119,9209619,9209893,9222728,9222587,9222801,9222440,9210036,9231336,9230390,9230617,9014999,9170648,9173470,9004445,9232957,9232345,9232958,9248399,9017847,9050271,9232094,9245705,9049243,9254099,9034682,9227133,9227666,9210566,9196918,9235910,9005616,9228840,9229642,9229793,9164202,9230831,9230900,9239911,9247585,9231509,9231614,9240888,9231679,9197319,9214908,9247871,9232096,9227031,9232098,9247804,9232257,9249554,9232255,9249928,9232258,9232137,9248327,9226932,9262490,9233228,9233168,9002087,9254112,9181540,9255447,9005591,9257443,9222418,9233679,9234573,9234664,9203775,9203297,9259880,9222806,9204622,9230013,9223611,9261663,9222809,9104541,9196800,9236796,9237219,9237197,9237520,9238067,9238173,9238559,9023931,9239475,9239477,9239478,9239858,9242733,9240754,9240753,9240612,9239837,9245392,9016566,9245515,9271576,9245953,9135730,9226211,9232100,9250432,9222814,9181338,9252066,9023052,9124167,9253900,9049320,9200422,9254190,9034769,9257757,9256076,9196733,9050634,9035063,9035961,9036502,9067233,9117787,9173753,9220148,9118574,9261944,9109564,9261965,9264343,9037655,9265617,9031590,9017783,9015005,9033894,9268324,9197990,9026277,9035554,9271127,9122027,9170006,9000018,9005614,9139324,9012517,9028212,9030913,9043832,9122598,9029470,9231990,9153465,9004922,9005566,9215431,9269977,9189905,9077932,9117536,9023531,9142726,9166839,9168653,9169323,9175273,9169792,9181243,9181242,9186907,9161244,9201404,9232099,9200051,9209779,9209906,9210116,9215432,9222039,9172670,9198436,9008660,9234693,9243375,9226727,9254111,9256989,9020678,9016878,9247819,9021926,9022047,9063402,9025265,9044594,9026203,9033174,9036423,9036572,9044008,9121043,9125185,9139559,9004918,9019605,9030985,9032793,9032958,9074630,9118008,9139187,9166825,9164379,9181239,9195928,9187527,9199340,9199557,9209355,9008661,9219562,9209781,9209895,9210007,9210143,9262490,9003442,9006080,9018735,9268323,9050569)";
         $list = $ds->fetchAll($sql);
         foreach($list as $row){
            $full_path = $row['full_path'];
            $postId = $row['post_id'];
            echo $row['post_id']."<br />";
            echo $full_path."<br />";
            //system("curl \"http://dev.uupage.com/post/upgradebypostid/forumId/9/postId/$postId\"");
         	//$ds->query($sql);
         }
         //echo "####:".$postDao->getTopId(4000135); 
	    }
	    
	   /*
	     * 比较色里的图片数和旧论坛的图片数，打印出来需要重新转的post_id
	     */
	    function upgrade9Action(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
         $sql = "select p.post_id post_id from pw_post p where forum_id=9 and parent_id=0 and post_id<10000000";
         $list = $ds->fetchAll($sql);
         foreach($list as $row){
            $postId = $row['post_id'];
            //echo $row['post_id']."<br />";
            $sql = "select count(*) c1  from pw_post_pic where post_id=$postId";
            $c1  = intval($ds->fetchOne($sql));
            $sql = "select count(distinct mm_url) c2 from msg_photo t where (parent=$postId-9*1000000 or msgid=$postId-9*1000000) and mm_url!='' and  mm_url not like 'http://%' and mm_url like '/photo/%'";
            $c2  = intval($ds->fetchOne($sql));            
            if($c1!=$c2){
            	echo "curl \"http://dev.uupage.com/post/upgradebypostid/forumId/9/postId/$postId\""."<br />";
            }	
            //system("curl \"http://dev.uupage.com/post/upgradebypostid/forumId/9/postId/$postId\"");
         	//$ds->query($sql);
         }
         //echo "####:".$postDao->getTopId(4000135); 
	    }	    
	   
	    	
	    	
	    function test2Action(){
         $this->_helper->viewRenderer->setNoRender();
         system("cp /root/paowang-backup/www/infodaily/htdocs/photo/uploaded/1999-06-04/red.jpg /usr/local/apache2/htdocs/newpaowang/www/upload_files/1999/199906/19990604/9000003_424.jpg");
         //system("cp /root/paowang-backup/www/infodaily/htdocs/photo/uploaded/1999-06-04/aa.txt /root/paowang-backup/www/infodaily/htdocs/photo/uploaded/1999-06-04/bb.txt");
         //copy("/root/paowang-backup/www/infodaily/htdocs/photo/uploaded/1999-06-04/aa.txt","/root/paowang-backup/www/infodaily/htdocs/photo/uploaded/1999-06-04/cc.txt");
         
         system("cp /usr/local/apache2/htdocs/newpaowang/uploaded/1999-06-04/aa.txt /usr/local/apache2/htdocs/newpaowang/uploaded/bb.txt");
         copy("/usr/local/apache2/htdocs/newpaowang/uploaded/1999-06-04/aa.txt","/usr/local/apache2/htdocs/newpaowang/uploaded/1999-06-04/cc.txt");
         
//         system("cp /usr/local/apache2/htdocs/newpaowang/www/a.txt /usr/local/apache2/htdocs/newpaowang/www/b.txt");
         ///
         

         copy("/usr/local/apache2/htdocs/newpaowang/www/a.txt","/usr/local/apache2/htdocs/newpaowang/www/c.txt");
         
         echo "ok";
	    }	          
	    
	    function infoAction(){
         $this->_helper->viewRenderer->setNoRender();
         phpinfo();
	    }	    

	    /*
	    *处理帖子前面的非本站图片和后面的相关链接
	    */
       function testnullAction(){
         $this->_helper->viewRenderer->setNoRender();
         $ds = new DbModel("","");
	     $sql = "select * from test ";
         echo $sql."<br />";
         $list = $ds->fetchAll($sql);
         $postDao = Post_PostDao::getInstance();
         $rtn = array();
         foreach($list as $row){
         	echo $row['test_id']."<br />";
         	echo $row['test_value']."<br />";
         	echo $row['test_empty']."<br />";
         	$test_empty = $row['test_empty'];
         	echo "test_empty:".strlen($test_empty);
         	echo "test_empty trim:".strlen(trim($test_empty));
         	 
         	echo $row['test_null']."<br />";
         	$test_null = $row['test_null'];
         	if($test_null == null){
         		echo "test_null==null"."<br />";
         	}
         	if($test_null == ''){
         		echo "test_null== ''"."<br />";
         	}
         	if($test_null === null){
         		echo "test_null===null"."<br />";
         	}
         	if($test_null === ''){
         		echo "test_null=== ''"."<br />";
         	}    
         	     	         	if($test_empty == null){
         		echo "test_empty==null"."<br />";
         	}
         	$test_empty = trim($row['test_empty']);
         	
         	if($test_empty == ''){
         		echo "test_empty== ''"."<br />";
         	}
         	if($test_empty === null){
         		echo "test_empty===null"."<br />";
         	}
         	if($test_empty === ''){
         		echo "test_empty=== ''"."<br />";
         	}
         	echo "test_null:".strlen($test_null);
         	echo "test_null trim:".strlen(trim($test_null));
         	
         }
       } 
}
