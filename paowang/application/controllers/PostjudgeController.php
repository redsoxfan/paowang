<?php
    /**
     * Title  PostjudgeController
     */

    class PostjudgeController extends CommonController{ 
	    
            
        function  voteAction(){
            $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
            //request
            $postId = $this->_getParam('postId');
            $userId = Util::getUserId();
            $flag = $this->_getParam('flag');
            $createAt = time();
            $userIp = Util::getUserIp();
            $judgeType = 1;//暂时都为1          
            //dao
            $postjudgeDao = Postjudge_PostjudgeDao::getInstance();
            $count = $postjudgeDao->getListCountByPostAndUser($postId,$userId);
            
            $rtn = array();
            if($count>0){
                //已经投过票了
                $rtn['msg'] = "0";
            }else{
                //info
                $postjudgeInfo = new Postjudge_PostjudgeInfo();
                $postjudgeInfo->setPostId($postId);
                $postjudgeInfo->setUserId($userId);
                $postjudgeInfo->setFlag($flag);
                $postjudgeInfo->setCreateAt($createAt);
                $postjudgeInfo->setUserIp($userIp);
                $postjudgeInfo->setJudgeType($judgeType);

                $postjudgeDao->add($postjudgeInfo);
                $rtn['msg'] = "1";//投票成功
            }
            $rtn['voteyes'] = $postjudgeDao->getListCountVoteYes($postId);
            $rtn['voteno'] = $postjudgeDao->getListCountVoteNo($postId);            
            $json = Zend_Json::encode($rtn);
            echo $json;
        }       
        
        function __call($action, $arguments)
        {
            //return $this->defaultAction();
            //throw new Zend_Controller_Exception('Invalid method called');
            //echo "Action = " . $action . "<br />";
            //echo "Arguments = " . $arguments;
        }

        //
        // 下面的程序以后可以删除。如果有需要的部分，请挪到这行注释的上方。
        //
    	
	    function  indexAction(){
	        $this->_redirect('/postjudge/list');
	    }
	    
	    function  listAction(){
	    	
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
	
			$perpage = 5;

            $postjudgeDao = Postjudge_PostjudgeDao::getInstance();
            $this->view->lists = $postjudgeDao->getList($perpage,$page);
			$this->view->total= $postjudgeDao->getListAllCount();
			$this->view->perpage= $perpage;
			$this->view->page= $page;
	    }
	    
	    function  newAction(){
            $postjudgeInfo = new Postjudge_PostjudgeInfo();
            $this->view->postjudgeInfo = $postjudgeInfo;
	    }
	    
	    function  addAction(){
            //request
            $postId = $this->_getParam('postId');
            $userId = $this->_getParam('userId');
            $flag = $this->_getParam('flag');
            $createAt = $this->_getParam('createAt');
            $userIp = $this->_getParam('userIp');
            $judgeType = $this->_getParam('judgeType');
            
            //info
            $postjudgeInfo = new Postjudge_PostjudgeInfo();
            $postjudgeInfo->setPostId($postId);
            $postjudgeInfo->setUserId($userId);
            $postjudgeInfo->setFlag($flag);
            $postjudgeInfo->setCreateAt($createAt);
            $postjudgeInfo->setUserIp($userIp);
            $postjudgeInfo->setJudgeType($judgeType);
            
            //dao
            $postjudgeDao = Postjudge_PostjudgeDao::getInstance();
            $postjudgeDao->add($postjudgeInfo);
			$this->_redirect('/postjudge/list');
	    }
	    
	    function  editAction(){
			 $postJudgeId = intval($this->_getParam('postJudgeId'));
			 if(empty($postJudgeId)){
				$this->_redirect('/postjudge/list/');
			 }else{
	            $postjudgeDao = Postjudge_PostjudgeDao::getInstance();
	            $this->view->postjudgeInfo = $postjudgeDao->get($postJudgeId);
			 }
	    }
	    	    
	    function  modifyAction(){
            //request
			$postJudgeId = intval($this->_getParam('postJudgeId'));
            $postId = $this->_getParam('postId');
            $userId = $this->_getParam('userId');
            $flag = $this->_getParam('flag');
            $createAt = $this->_getParam('createAt');
            $userIp = $this->_getParam('userIp');
            $judgeType = $this->_getParam('judgeType');
            
            //dao,info
	        $postjudgeDao = Postjudge_PostjudgeDao::getInstance();
            $postjudgeInfo = $postjudgeDao->get($postJudgeId);
            $postjudgeInfo->setPostId($postId);
            $postjudgeInfo->setUserId($userId);
            $postjudgeInfo->setFlag($flag);
            $postjudgeInfo->setCreateAt($createAt);
            $postjudgeInfo->setUserIp($userIp);
            $postjudgeInfo->setJudgeType($judgeType);
            $postjudgeDao->modify($postjudgeInfo);
			$this->_redirect('/postjudge/list');
	    	
	    }

	    function  viewAction(){
			$postJudgeId = intval($this->_getParam('postJudgeId'));
			if(empty($postJudgeId)){
				$this->_redirect('/postjudge/list/');
			}else{
	            $postjudgeDao = Postjudge_PostjudgeDao::getInstance();
	            $this->view->postjudgeInfo = $postjudgeDao->get($postJudgeId);
			}
	    }	    	   
	    
	    function  delAction(){
			$postjudgeDao = Postjudge_PostjudgeDao::getInstance();
	    	$postJudgeIdArray = $this->_getParam('postJudgeId');
	        if(is_array($postJudgeIdArray)){	    	
		    	foreach( $postJudgeIdArray as $postJudgeId){
			    	$postJudgeId = intval($postJudgeId);
			    	if (!$postJudgeId) {
			    	  //null
			    	}else {
			    		$postjudgeDao->delById($postJudgeId);
			    	}
		    	}
	        }else{
		    	$postJudgeId = intval($postJudgeIdArray);
		    	if (!$postJudgeId) {
		    		//null
		    	}else {
		    		$postjudgeDao->delById($postJudgeId);
		    	}
	        }
	    	$this->_redirect('/postjudge/list');
	    }

	    function  ajaxlistAction(){
	    }

	    function  jsonlistAction(){
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
			$perpage = 5;
            $postjudgeDao = Postjudge_PostjudgeDao::getInstance();
	        $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $total = $postjudgeDao->getListAllCount();
 		    $ajaxpage = new PageUtil(array('total'=>$total,'perpage'=>$perpage,'ajax'=>'ajax_page','nowindex'=>$page,'page_name'=>'page','url'=>"/postjudge/jsonlist/"));
	        $data = array('list' => $postjudgeDao->getList($perpage,$page) ,
	                      'total' => $total,
	                      'perpage' => $perpage,
	                      'page' => $page,
	                      'pagebar' => $ajaxpage->show()
	                );
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    
	    function  jsongetAction(){
		   $postJudgeId = intval($this->_getParam('postJudgeId'));
           $postjudgeDao = Postjudge_PostjudgeDao::getInstance();
           $postjudgeInfo = $postjudgeDao->get($postJudgeId);
 	      
 	       $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
           $data = array('postjudgeInfo' => $postjudgeInfo);
	       $json = Zend_Json::encode($data);
	       echo $json;
	    }
	    	 
	    function  jsonsaveAction(){
	    	$postJudgeId = intval($this->_getParam('postJudgeId'));
            $postId = $this->_getParam('postId');
            $userId = $this->_getParam('userId');
            $flag = $this->_getParam('flag');
            $createAt = $this->_getParam('createAt');
            $userIp = $this->_getParam('userIp');
            $judgeType = $this->_getParam('judgeType');
            
	        $postjudgeDao = Postjudge_PostjudgeDao::getInstance();
            $cmd ="";
            if($postJudgeId>0){
            	//modify
            	$cmd = "modify";
		        $postjudgeInfo = $postjudgeDao->get($postJudgeId);
                $postjudgeInfo->setPostId($postId);
                $postjudgeInfo->setUserId($userId);
                $postjudgeInfo->setFlag($flag);
                $postjudgeInfo->setCreateAt($createAt);
                $postjudgeInfo->setUserIp($userIp);
                $postjudgeInfo->setJudgeType($judgeType);
		        $postjudgeDao->modify($postjudgeInfo);
            	
            }else{
                //add
                $cmd = "add";
	            //info
	            $postjudgeInfo = new Postjudge_PostjudgeInfo();
                $postjudgeInfo->setPostId($postId);
                $postjudgeInfo->setUserId($userId);
                $postjudgeInfo->setFlag($flag);
                $postjudgeInfo->setCreateAt($createAt);
                $postjudgeInfo->setUserIp($userIp);
                $postjudgeInfo->setJudgeType($judgeType);
	            //dao
	            $postjudgeDao->add($postjudgeInfo);                
            }
		    $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $data = array('cmd'=>$cmd,'postjudgeInfo' => $postjudgeInfo);
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    	    
	    function  jsondelAction(){
	    	$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
			$postjudgeDao = Postjudge_PostjudgeDao::getInstance();
	    	$postJudgeIdArray = $this->_getParam('postJudgeId');
	        if(is_array($postJudgeIdArray)){	    	
		    	foreach( $postJudgeIdArray as $postJudgeId){
			    	$postJudgeId = intval($postJudgeId);
			    	if (!$postJudgeId) {
			    	  //null
			    	}else {
			    		$postjudgeDao->delById($postJudgeId);
			    	}
		    	}
	        }else{
		    	$postJudgeId = intval($postJudgeIdArray);
		    	if (!$postJudgeId) {
		    		//null
		    	}else {
		    		$postjudgeDao->delById($postJudgeId);
		    	}
	        }
	        $json = Zend_Json::encode($postJudgeIdArray);
	        echo $json;
	    }
	    

	}
?>