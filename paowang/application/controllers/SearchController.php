<?php

class SearchController extends CommonController{

	public function listAction(){
        header("Content-type: text/html; charset=utf-8");
        $q = $this->_getParam('q');
        if(empty($q)){
           $q = "";
        }
        $q_bak = $q;
        
        $q = urlencode($q);
        //$q="jonathan";
        $page = intval($this->_getParam('page'));
        $url = "http://localhost:8080/ls_xml.jsp?q=".$q."&page=".$page;//113.31.21.177
        //$snoopy = new Snoopy;
        //$snoopy->fetch($url);   
        //echo $snoopy->results;
	
	    //echo "test";
		$dom = new domDocument();
		$xml = file_get_contents($url);
		
		$dom->loadXML($xml);
		$root = $dom->documentElement;
		//print_r($root->childNodes);
		//print_r($root->childNodes);
		
        $rs = $dom->getElementsByTagName( "rs" )->item(0);
        $total = $rs->getAttribute("total");
        $start = $rs->getAttribute("start");
        $end = $rs->getAttribute("end");
        $pages = $rs->getAttribute("pages");
        $perpage = $rs->getAttribute("perpage");        
        $time = $rs->getAttribute("time");
        /*
	    echo "num:".$num."<br />";
	    echo "start:".$start."<br />";
	    echo "end:".$end."<br />";
	    echo "time:".$time."<br />";
	    */	
       
		$documentElements = $dom->getElementsByTagName( "r" );
		echo $documentElements->length;

        $rows = array();
		foreach($documentElements as $r){
			$post_id =  $r->getElementsByTagName("post_id")->item(0)->nodeValue;
			$title =  $r->getElementsByTagName("title")->item(0)->nodeValue;
			$content =  $r->getElementsByTagName("content")->item(0)->nodeValue;
			$publishDate =  $r->getElementsByTagName("publishDate")->item(0)->nodeValue;
			$forumName =  $r->getElementsByTagName("forumName")->item(0)->nodeValue;
			$author =  $r->getElementsByTagName("author")->item(0)->nodeValue;
			$pics =  $r->getElementsByTagName("pics")->item(0)->nodeValue;
			$summary =  $r->getElementsByTagName("summary")->item(0)->nodeValue;
			//echo "$post_id $title $author $summary<br />";
		    array_push($rows, array(
				"post_id"=>$post_id,
				"title"=>$title,
				"content"=>$content,
				"publishDate"=>$publishDate,
				"forumName"=>$forumName,
				"author"=>$author,
				"pics"=>$pics,
				"summary"=>$summary
		    )); 			
		}
		$this->view->q = $q_bak;
		$this->view->time = $time;		
		$this->view->perpage = $perpage;
		$this->view->total = $total;
		$this->view->rows = $rows;		
		$this->view->page = $page;
		$this->view->start = ($total==0?-1:$start);
		$this->view->end = ($total-1<$end?$total-1:$end);
	}

}
