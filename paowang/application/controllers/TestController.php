<?php
class TestController extends CommonController
{
    public $contexts = array(
        'xml'     => array('xml'),
        'json1' => array('json')
    );

    public function init()
    {
        $this->_helper->contextSwitch()->initContext();
    }    
    
	function indexAction()
	{	
	}
	
	// Return in xml format using template
	function xmlAction()
	{
	    // in the url do this to retrieve xml data: 
	    // http://pw.cn/test/xml/format/xml. or http://pw.cn/test/xml?format=xml. Use format "xml".
	    // the xml data is at xml.xml.phtml (with xml.phtml suffix)
	}
	
	// One way to return json object. Note that php objects can be automatically converted to json
	function jsonAction()
	{
	    $data = array('name' => 'fanwu', 'project' => 'paowang');
	    $this->_helper->json($data);   
	}
	
	public function namingconventionAction()
    {
        // Renders test/naming-convention.phtml
        // in url use http://pw.cn/test/namingConvention
        $this->render();
    }
	
    function layoutAction()
    {
        // Renders test/header.phtml
        $this->render('header');
        
        $this->render('body');
        
        // This is a global footer, which is not in test directory.
        $this->render('footer', null, true);   
        
        // To research: name segment
        // Renders my/login.phtml to the 'form' segment of the response object
        // $this->render('login', 'form');

    }
	
	// Another way to return json object
	function json1Action()
	{
	    $data1 = array('name' => 'fanwu', 'project' => 'paowang');
	    $this->view->data1 = $data1; 
	    
	    $data2 = array('name' => 'wanglei', 'project' => 'paowang-new');
	    $this->view->data2 = $data2;
	}
	
	function plaintextAction()
	{
	    // disable autorendering for this action only:
        $this->_helper->viewRenderer->setNoRender();
        echo "pure plain text";   
	}
	
	
	function testaesAction(){
		$this->_helper->viewRenderer->setNorender();
		
		$cookieArray = array(
		   "userId"=>"1",
		   "userName"=>"xxsh97",
		    "forumId"=>"2",
		    "style"=>"classic"
		);
       //加密
       $aes = new AES('PAOWANG19990603a');
       $str = json_encode($cookieArray,true);
       $aesString = $aes->encrypt($str);
       //echo $aesString;
       $str = base64_encode($aesString);
	   echo $str;
	}
	
	function testunaesAction(){
		$this->_helper->viewRenderer->setNorender();
		/*
		$str = "8hHAZAi4px2IMLJa5oceviaIYSfrky+jADrcLgF5eZX+LI17yswvbc/Pqkfh8WbvXSNrCYh5RTEGlzs0r3feM2RRpTsY8Krt2xkoqS+HLFY=";
	    $str = base64_decode($str);
		$aes = new AES('PAOWANG19990603a');
        $str = $aes->decrypt($str);
        $cookieArray = json_decode($str,true);
        var_dump($cookieArray);
		*/
		echo "userId:".Util::getUserId()."<br />";
		echo "userName:".Util::getUserName()."<br />";
	}
	
	
	function testusernameAction(){
		$this->_helper->viewRenderer->setNoRender();
 $username ="中午dasg";
 $log = Zend_Registry::get('log');
 $log->debug("dagga");
 $log->debug("get:");
  header("Content-type: text/html; charset=utf-8");
  //判断网名是否合法
  if(!preg_match("/^[\x{4e00}-\x{9fa5}a-zA-Z0-9_]+$/u",$username)){
  	 //
  	 //UTF-8汉字字母数字下划线正则表达式
     echo "网名只能是中文、英文或数字";
  }else{
   echo '成功';		
  }
  
  	
	}	



	function lajiAction(){
		$this->_helper->viewRenderer->setNoRender();
        $username ="ab   abc  12435";
		header("Content-type: text/html; charset=utf-8");
		//判断网名是否合法
		if(preg_match("/作弊器/",$username) || preg_match("/12345/",$username)){
		  	 //
		  	 //UTF-8汉字字母数字下划线正则表达式
		     echo "匹配";
		}else{
		   echo 'not';		
		}
	}
	
	function mainlayoutAction()
	{
	    // disable autorendering for this action only:
        $this->_helper->viewRenderer->setNoRender();
        
        $this->view->headerTitle = "Plain Text Title";
        echo "pure plain text";   
	}
	
	function forumlayoutAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		
		$this->_helper->layout->setLayout('forumlayout');	
        
        $this->view->forumName = "Paowang Photo";
        
        echo "test forum layout";
	}
		
	
	function editorAction()
	{
		
		
	}
	
	
	function fckmodifyAction(){

	    $this->_helper->viewRenderer->setNoRender();
	    $edit = stripslashes($this->_getParam('FCKeditor1')); //得到html的值
        echo $edit;
        echo "<br />";
        
        
        $s = new Snoopy();
        $text = $s->_striptext($edit); //得到 text值 
        echo $text;
        
        echo "<br/>";
        echo "文字长度：".Util::strLenUtf8($text);
	}
	
	    function  testAction(){
	        // disable autorendering for this action only:
	        $this->_helper->viewRenderer->setNoRender();
		    echo "ddddddd";
	    }

	    function  jAction(){
	        //$this->_helper->viewRenderer->setNoRender();
	    }

	    function  jdoAction(){
	        $this->_helper->viewRenderer->setNoRender();
		    //echo "ddddddd";
		   foreach ($_POST['prog'] as $prog){
			$prog = htmlspecialchars($prog, ENT_QUOTES);
			echo $prog, '<br>', "\n";
           }		    
	    }	    

        function yzmFormAction(){
        }
        
        function checkimageAction(){
  	        // disable autorendering for this action only:
	        $this->_helper->viewRenderer->setNoRender();
	        
			header ("Pragma: no-cache");
			//$string="1A3B4C";
			session_start();
			//$protect_code=$_SESSION['protect_code'];
			$randval=rand(1000,9999);
			//session_register("randval");
			$_SESSION["randval"] = $randval;
			header ("Content-type: image/png");
			
			//ͼ���С
			$im = @imagecreate (38,15)
			    or die ("ͼ�δ������");
			$background_color = imagecolorallocate ($im, 255, 255, 255);
			$text_color = imagecolorallocate ($im, 233, 14, 91);
			imagestring ($im, 5, 1, 1,  $randval, $text_color);
			imagepng ($im);
        }        
	
    function uAction(){
       
    }	
    
    
   function snoopyAction(){
	    // disable autorendering for this action only:
	    $this->_helper->viewRenderer->setNoRender();
        $src = "<h3>你好啊噶a</h3>";
        $snoopy = new Snoopy;
        echo $src."<br />";
        echo $snoopy-> _striptext($src)."<br />";   
        echo Util::strLenUtf8($snoopy-> _striptext($src))."<br />";
   }

   function udealAction(){
       	        // disable autorendering for this action only:
	        $this->_helper->viewRenderer->setNoRender();
	        echo "dd";
    }	
    
    //
    // Start of Fan's tests
    //
    
    
    function brAction(){
    	    	$this->_helper->viewRenderer->setNoRender();
      $test = '<div>
<table cellspacing="2" cellpadding="4" width="100%" border="0">
    <tbody>
        <tr>
            <td class="listTitle">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tbody>
                    <tr>
                        <td height="6">
                        <table cellspacing="2" cellpadding="4" width="100%" border="0">
                            <tbody>
                                <tr>
                                    <td class="listTitle">
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tbody>
                                            <tr>
                                                <td>作者：龚轩&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 发布时间：2009-06-03-19-27&nbsp;&nbsp;&nbsp;浏览次数: 70</td>
                                                <td align="right">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="listTitle">
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tbody>
                                            <tr>
                                                <td align="left">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="6">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="center"><strong><font color="#990000" size="4">武汉警方成功处置一起劫持人质事件</font></strong></td>
                                            </tr>
                                            <tr>
                                                <td height="6">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="center">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="6">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="list" valign="top" align="left"><font size="3" style="line-height: 200%">&nbsp;&nbsp;&nbsp; 6月3日上午10时许，一名歹徒持自制手枪，窜至武汉大学行政楼，劫持一名武大女职工。武汉市公安局接警后，迅速启动应急预案，组织解救行动。在5小时谈判未果后，为确保人质安全，公安机关于下午3时采取行动，成功将人质安全解救，并将向民警开枪的歹徒击毙，一名特警光荣负伤。 <br />
                                    &nbsp;&nbsp;&nbsp; 案发后，湖北省、武汉市领导罗清泉、李鸿忠、杨松等立即作出批示，对武汉公安民警成功解救人质表示祝贺，并要求全力抢救受伤民警。省市领导张昌尔、吴永文、阮成发、胡绪鹍和武汉大学党委书记李健、校长顾海良等专程赶到中南医院，组织对民警的抢救工作，并看望受伤民警，慰问参战民警。 <br />
                                    &nbsp;&nbsp;&nbsp; 目前，受伤民警经医院抢救，已脱离危险。学校秩序正常。 <br />
                                    &nbsp;&nbsp;&nbsp; 据查，歹徒周某，40岁，系原武大后勤集团员工，2008年7月因非法拘禁罪被判刑2年，缓刑3年。（稿件来源：武汉市公安局）</font> <br />
                                    &nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><strong><font color="#990000" size="4">武大校园内发生一起持枪劫持人质事件</font></strong></td>
                    </tr>
                    <tr>
                        <td height="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="6">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td class="list" valign="top" align="left">
            <p><font size="3" style="line-height: 200%">&nbsp;&nbsp;&nbsp;&nbsp;6月3日上午9时10分左右，一名劫持者闯入武汉大学行政楼，持枪劫持党委办公室正在上班的一名女同志，拉动枪栓，谩骂工作人员。 <br />
            &nbsp;&nbsp;&nbsp;&nbsp;事件发生后，学校保卫部工作人员迅速赶到现场并报警，同时疏散楼内工作人员。武汉市公安局干警和特警于9时20分左右赶到现场进行处置，谈判专家进入党委办公室内同劫持者进行谈判。 <br />
            &nbsp;&nbsp;&nbsp; 14时50分左右，公安部门采取了果断措施，被劫持人质安全解救，劫持者已被制服。目前，学校教学科研秩序正常。 <br />
            &nbsp;&nbsp;&nbsp;&nbsp;据查，劫持者周某，1969年9月生，曾为武汉大学后勤集团总机室线务工人，曾于1989年12月至1992年12月在武警某部服役，因非法拘禁罪被判刑二年零六个月，缓刑三年，现正在服刑中。</font></p>
            </td>
        </tr>
    </tbody>
</table>
</div>';    	
	  $test = preg_replace('/\s*(<br\s*\/?\s*>\s*){1,}/im',"",$test);
	  echo $test;
    }	
    
    function serverAction(){
    	$this->_helper->viewRenderer->setNoRender();
    	echo json_encode($_SERVER);
    }	
    
    
       /*
       * mailtest
       */
	    function mailtestAction(){
         $this->_helper->viewRenderer->setNoRender();
         
			if (!defined( "PLOG_CLASS_PATH" )) {
			    define( "PLOG_CLASS_PATH", $_SERVER['DOCUMENT_ROOT']."/../library/plog/");
			}            
		    include_once( PLOG_CLASS_PATH."class/mail/emailmessage.class.php" );
		    include_once( PLOG_CLASS_PATH."class/mail/emailservice.class.php" );                
         $log = Zend_Registry::get('log');
			//发信 开始
		    $message = new EmailMessage();
		    $message->setCharset("utf-8");
		    $message->addTo( "wanglei8898@gmail.com" );
		    
		    	$message->addBcc( "xxsh97@yahoo.com.cn" );	
		    //$message->addTo( "mrfanwu@gmail.com" );
		    $message->setFrom( "paowang@gmail.com" );
		    $message->setSubject("欢迎您在泡网俱乐部注册");
		    		
		    
		    
		    $message->setBody( "您好！\n 欢迎您在泡网注册，请点击下面的链接完成注册  。\n  祝好！" );
		    $service = new EmailService();
		    if( $service->sendMessage( $message )){
		    	$log->debug( "################### send ok<br />");
		        echo  "################### send ok";
		    }  
		    else{
		      $log->debug( "################### send fail");
		      echo "################### send fail<br />";
		    }		    
			//发信 结束		    
         //echo "####:".$postDao->getTopId(4000135); 
	    }
	        
    function fanAction(){
    	$this->_helper->viewRenderer->setNoRender();
    	
    	$userId = intval($this->_getParam('userId'));
    	if($userId==0){
            	$userId = Util::getUserId();
    	}
    	echo $userId . "<BR>";
    	
//    	$canManageForum = Rbm_RbmService::getInstance()->hasPermission(
//    		"42", "2", Rbm_RbmService::ADMIN_MANAGE_FORUM);
//	    echo $canManageForum;

//    	$canAssignMembership = Rbm_RbmService::getInstance()->hasPermission(
//    		"11", "6", Rbm_RbmService::ADMIN_ASSIGN_MEMBERSHIP);
//    	echo $canAssignMembership;

//    	$canAddPost = Rbm_RbmService::getInstance()->hasPermission(
//    		"26", "1", Rbm_RbmService::USER_ADD_POST);
//    	echo $canAddPost;

    	$canEditPost = Rbm_RbmService::getInstance()->hasPermission(
    		"1", "1", Rbm_RbmService::USER_EDIT_POST, "1000");
    	
    	echo $canEditPost;

//    	$canUploadPicture = Rbm_RbmService::getInstance()->hasPermission(
//    		"12", "4", Rbm_RbmService::USER_UPLOAD_PICTURE);
//    	echo "<br>can upload picuture? " . $canUploadPicture;
//    	
//    	$canAddTag = Rbm_RbmService::getInstance()->hasPermission(
//    		"25", "1", Rbm_RbmService::USER_ADD_TAG);
//    	echo "<br> can add tag? " . $canAddTag;
//    	
//    	$canRate = Rbm_RbmService::getInstance()->hasPermission(
//    		"25", "1", Rbm_RbmService::USER_RATE);
//    	echo "<br> can rate? " . $canRate;
    }
    
    //
    // End of Fan's tests
    //
    function dateAction(){
    	$this->_helper->viewRenderer->setNoRender();
    	$nowtime = time();
    	echo $nowtime."<br />";
    	$lasttime = 1283270400;
    	echo $lasttime."<br />";
    	echo ($nowtime - $lasttime)."<br />";
    	$days = intval(($nowtime - $lasttime)/(24*60*60));
    	echo $days."<br />";
    	echo Util::getUserIp()."<br />";
               $postDao = Post_PostDao::getInstance();
$userId = Util::getUserId();
                    $maxPostCount = $postDao->getPostDayMaxCountByUserId($userId);
                    $nowPostCount = $postDao->getPostDayCountByUserId($userId);
                    echo  $maxPostCount."<br />"; 	
                    echo $postDao->getRegisterDays($userId)."<br />";
    	//
    }
    
	function searchAction(){
		$this->_helper->viewRenderer->setNoRender();
        header("Content-type: text/html; charset=utf-8");
	    $this->_helper->viewRenderer->setNoRender();
        $q = $this->_getParam('q');
        $q = urlencode($q);
        $page = intval($this->_getParam('page'));
        $url = "http://localhost:8080/ls.jsp?q=".$q."&page=".$page;
        $snoopy = new Snoopy;
        $snoopy->fetch($url);   
        echo $snoopy->results;
	}   
	
}
