<?php
class TestajaxController extends CommonController
{
    public function init()
    {
        // Local to this controller only; affects all actions, as loaded in init:
        $this->_helper->viewRenderer->setNoRender(true);

        // Globally:
        //$this->_helper->removeHelper('viewRenderer');

        // Also globally, but would need to be in conjunction with the local
        // version in order to propagate for this controller:
        //Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);
        
    }
    
    function indexAction()
    {
        echo "This is a test for ajax without using a template. <br> You can use testajax instead of testAjax in URL.";
    }
    
    function jsonAction()
    {
        $data = array('name' => 'fanwu', 'project' => 'paowang');
	    $this->_helper->json($data);   
    }

}