<?php

class TuanController extends CommonController{
    public function lashouapiAction(){
    	$this->_helper->viewRenderer->setNoRender();
    	header("Content-type: text/html; charset=utf-8");
        $url = "http://open.client.lashou.com/list/goods/cityid/2419";    	
		$dom = new domDocument();
		$xml = file_get_contents($url);
		
		$dom->loadXML($xml);
		$root = $dom->documentElement;        
    	$cityname = $dom->getElementsByTagName( "cityname" )->item(0)->nodeValue;
    	echo "cityname:".$cityname."<br />";
    	$goodsElements = $dom->getElementsByTagName("goods" )->item(0);
		$goods_title =  $goodsElements->getElementsByTagName("goods_title")->item(0)->nodeValue;
    	echo "goods_title:".$goods_title."<br />";
		$goods_start_time =  $goodsElements->getElementsByTagName("goods_start_time")->item(0)->nodeValue;
    	echo "goods_start_time:".$goods_start_time."<br />";
   		$goods_rules =  $goodsElements->getElementsByTagName("goods_rules")->item(0)->nodeValue;
		$goods_value =  $goodsElements->getElementsByTagName("goods_value")->item(0)->nodeValue;
		$goods_price =  $goodsElements->getElementsByTagName("goods_price")->item(0)->nodeValue;
		$goods_rebate =  $goodsElements->getElementsByTagName("goods_rebate")->item(0)->nodeValue;
		$goods_start_time =  $goodsElements->getElementsByTagName("goods_start_time")->item(0)->nodeValue;
		$goods_deadline =  $goodsElements->getElementsByTagName("goods_deadline")->item(0)->nodeValue;
		$goods_left_second =  $goodsElements->getElementsByTagName("goods_left_second")->item(0)->nodeValue;
		$goods_expire =  $goodsElements->getElementsByTagName("goods_expire")->item(0)->nodeValue;
		$goods_convey_fee =  $goodsElements->getElementsByTagName("goods_convey_fee")->item(0)->nodeValue;
		$goods_description =  $goodsElements->getElementsByTagName("goods_description")->item(0)->nodeValue;
		$goods_bought =  $goodsElements->getElementsByTagName("goods_bought")->item(0)->nodeValue;
		$goods_min_bought =  $goodsElements->getElementsByTagName("goods_min_bought")->item(0)->nodeValue;
		$goods_max_bought =  $goodsElements->getElementsByTagName("goods_max_bought")->item(0)->nodeValue;
		$goods_sp_name =  $goodsElements->getElementsByTagName("goods_sp_name")->item(0)->nodeValue;
		$goods_sp_url =  $goodsElements->getElementsByTagName("goods_sp_url")->item(0)->nodeValue;
		$goods_image_url =  $goodsElements->getElementsByTagName("goods_image_url")->item(0)->nodeValue;
		$goods_site_url =  $goodsElements->getElementsByTagName("goods_site_url")->item(0)->nodeValue;
		$goods_phone =  $goodsElements->getElementsByTagName("goods_phone")->item(0)->nodeValue;
	   	echo "goods_rules:".$goods_rules."<br />";
	   	echo "goods_value:".$goods_value."<br />";
	   	echo "goods_price:".$goods_price."<br />";
	   	echo "goods_rebate:".$goods_rebate."<br />";
	   	echo "goods_start_time:".$goods_start_time."<br />";
	   	echo "goods_deadline:".$goods_deadline."<br />";
	   	echo "goods_left_second:".$goods_left_second."<br />";
	   	echo "goods_expire:".$goods_expire."<br />";
	   	echo "goods_convey_fee:".$goods_convey_fee."<br />";
	   	echo "goods_description:".$goods_description."<br />";
	   	echo "goods_bought:".$goods_bought."<br />";
	   	echo "goods_min_bought:".$goods_min_bought."<br />";
	   	echo "goods_max_bought:".$goods_max_bought."<br />";
	   	echo "goods_sp_name:".$goods_sp_name."<br />";
	   	echo "goods_sp_url:".$goods_sp_url."<br />";
	   	echo "goods_image_url:".$goods_image_url."<br />";
	   	echo "goods_site_url:".$goods_site_url."<br />";
	   	echo "goods_phone:".$goods_phone."<br />";		
    	//echo "tuan list";
    }
    
    
    public function meituanapiAction(){
    	$this->_helper->viewRenderer->setNoRender();
    	header("Content-type: text/html; charset=utf-8");
        $url = "http://www.meituan.com/api/v1/beijing/deals";    	
		$dom = new domDocument();
		$xml = file_get_contents($url);
		
		$dom->loadXML($xml);
		$root = $dom->documentElement;        
		$date = $root->getAttribute("date");
		echo "date:".$date."<br />";
		
    	$dealsElements = $dom->getElementsByTagName( "deals" )->item(0);
    	$id =  $dealsElements->getElementsByTagName("id")->item(0)->nodeValue;
		$deal_url =  $dealsElements->getElementsByTagName("deal_url")->item(0)->nodeValue;
		$title =  $dealsElements->getElementsByTagName("title")->item(0)->nodeValue;
		$small_image_url =  $dealsElements->getElementsByTagName("small_image_url")->item(0)->nodeValue;
		$medium_image_url =  $dealsElements->getElementsByTagName("medium_image_url")->item(0)->nodeValue;
		$large_image_url =  $dealsElements->getElementsByTagName("large_image_url")->item(0)->nodeValue;
		$division_id =  $dealsElements->getElementsByTagName("division_id")->item(0)->nodeValue;
		$division_name =  $dealsElements->getElementsByTagName("division_name")->item(0)->nodeValue;
		$division_lat =  $dealsElements->getElementsByTagName("division_lat")->item(0)->nodeValue;
		$division_lng =  $dealsElements->getElementsByTagName("division_lng")->item(0)->nodeValue;
		$division_timezone =  $dealsElements->getElementsByTagName("division_timezone")->item(0)->nodeValue;
		$division_offset_gmt =  $dealsElements->getElementsByTagName("division_offset_gmt")->item(0)->nodeValue;
		$vendor_id =  $dealsElements->getElementsByTagName("vendor_id")->item(0)->nodeValue;
		$vendor_name =  $dealsElements->getElementsByTagName("vendor_name")->item(0)->nodeValue;
		$vendor_website_url =  $dealsElements->getElementsByTagName("vendor_website_url")->item(0)->nodeValue;
		$status =  $dealsElements->getElementsByTagName("status")->item(0)->nodeValue;
		$start_date =  $dealsElements->getElementsByTagName("start_date")->item(0)->nodeValue;
		$end_date =  $dealsElements->getElementsByTagName("end_date")->item(0)->nodeValue;
		$tipped =  $dealsElements->getElementsByTagName("tipped")->item(0)->nodeValue;
		$tipping_point =  $dealsElements->getElementsByTagName("tipping_point")->item(0)->nodeValue;
		$tipped_date =  $dealsElements->getElementsByTagName("tipped_date")->item(0)->nodeValue;
		$sold_out =  $dealsElements->getElementsByTagName("sold_out")->item(0)->nodeValue;
		$quantity_sold =  $dealsElements->getElementsByTagName("quantity_sold")->item(0)->nodeValue;
		$price =  $dealsElements->getElementsByTagName("price")->item(0)->nodeValue;
		$value =  $dealsElements->getElementsByTagName("value")->item(0)->nodeValue;
		$discount_amount =  $dealsElements->getElementsByTagName("discount_amount")->item(0)->nodeValue;
		$discount_percent =  $dealsElements->getElementsByTagName("discount_percent")->item(0)->nodeValue;
		$areas =  $dealsElements->getElementsByTagName("areas")->item(0)->nodeValue;    	
    	echo "id:".$id."<br />";
	   	echo "deal_url:".$deal_url."<br />";
	   	echo "title:".$title."<br />";
	   	echo "small_image_url:".$small_image_url."<br />";
	   	echo "medium_image_url:".$medium_image_url."<br />";
	   	echo "large_image_url:".$large_image_url."<br />";
	   	echo "division_id:".$division_id."<br />";
	   	echo "division_name:".$division_name."<br />";
	   	echo "division_lat:".$division_lat."<br />";
	   	echo "division_lng:".$division_lng."<br />";
	   	echo "division_timezone:".$division_timezone."<br />";
	   	echo "division_offset_gmt:".$division_offset_gmt."<br />";
	   	echo "vendor_id:".$vendor_id."<br />";
	   	echo "vendor_name:".$vendor_name."<br />";
	   	echo "vendor_website_url:".$vendor_website_url."<br />";
	   	echo "status:".$status."<br />";
	   	echo "start_date:".$start_date."<br />";
	   	echo "end_date:".$end_date."<br />";
	   	echo "tipped:".$tipped."<br />";
	   	echo "tipping_point:".$tipping_point."<br />";
	   	echo "tipped_date:".$tipped_date."<br />";
	   	echo "sold_out:".$sold_out."<br />";
	   	echo "quantity_sold:".$quantity_sold."<br />";
	   	echo "price:".$price."<br />";
	   	echo "value:".$value."<br />";
	   	echo "discount_amount:".$discount_amount."<br />";
	   	echo "discount_percent:".$discount_percent."<br />";
	   	echo "areas:".$areas."<br />";   
	   	
	   	$conditionsElements =  $dealsElements->getElementsByTagName("conditions")->item(0);   
	   	
	   	$limited_quantity =  $conditionsElements->getElementsByTagName("limited_quantity")->item(0)->nodeValue;
	   	$minimum_purchase =  $conditionsElements->getElementsByTagName("minimum_purchase")->item(0)->nodeValue;
	   	$maximum_purchase =  $conditionsElements->getElementsByTagName("maximum_purchase")->item(0)->nodeValue;
	   	$expiration_date =  $conditionsElements->getElementsByTagName("expiration_date")->item(0)->nodeValue;	   		   		   	
	   	echo "limited_quantity:".$limited_quantity."<br />"; 
	   	echo "minimum_purchase:".$minimum_purchase."<br />"; 
	   	echo "maximum_purchase:".$maximum_purchase."<br />"; 
	   	echo "expiration_date:".$expiration_date."<br />"; 
	   	
	   	$detailsElements =  $conditionsElements->getElementsByTagName("details")->item(0);
	   	$detail =  $detailsElements->getElementsByTagName("detail")->item(0)->nodeValue;
	   	echo "detail:".$detail."<br />"; 	   		
    	//echo "tuan list";
    }
        
    
	public function listAction(){
        header("Content-type: text/html; charset=utf-8");
        $q = $this->_getParam('q');
        if(empty($q)){
           $q = "";
        }
        $q_bak = $q;
        
        $q = urlencode($q);
        //$q="jonathan";
        $page = intval($this->_getParam('page'));
        $url = "http://localhost:8080/ls_xml.jsp?q=".$q."&page=".$page;//113.31.21.177
        //$snoopy = new Snoopy;
        //$snoopy->fetch($url);   
        //echo $snoopy->results;
	
	    //echo "test";
		$dom = new domDocument();
		$xml = file_get_contents($url);
		
		$dom->loadXML($xml);
		$root = $dom->documentElement;
		//print_r($root->childNodes);
		//print_r($root->childNodes);
		
        $rs = $dom->getElementsByTagName( "rs" )->item(0);
        $total = $rs->getAttribute("total");
        $start = $rs->getAttribute("start");
        $end = $rs->getAttribute("end");
        $pages = $rs->getAttribute("pages");
        $perpage = $rs->getAttribute("perpage");        
        $time = $rs->getAttribute("time");
        /*
	    echo "num:".$num."<br />";
	    echo "start:".$start."<br />";
	    echo "end:".$end."<br />";
	    echo "time:".$time."<br />";
	    */	
       
		$documentElements = $dom->getElementsByTagName( "r" );
		echo $documentElements->length;

        $rows = array();
		foreach($documentElements as $r){
			$post_id =  $r->getElementsByTagName("post_id")->item(0)->nodeValue;
			$title =  $r->getElementsByTagName("title")->item(0)->nodeValue;
			$content =  $r->getElementsByTagName("content")->item(0)->nodeValue;
			$publishDate =  $r->getElementsByTagName("publishDate")->item(0)->nodeValue;
			$forumName =  $r->getElementsByTagName("forumName")->item(0)->nodeValue;
			$author =  $r->getElementsByTagName("author")->item(0)->nodeValue;
			$pics =  $r->getElementsByTagName("pics")->item(0)->nodeValue;
			$summary =  $r->getElementsByTagName("summary")->item(0)->nodeValue;
			//echo "$post_id $title $author $summary<br />";
		    array_push($rows, array(
				"post_id"=>$post_id,
				"title"=>$title,
				"content"=>$content,
				"publishDate"=>$publishDate,
				"forumName"=>$forumName,
				"author"=>$author,
				"pics"=>$pics,
				"summary"=>$summary
		    )); 			
		}
		$this->view->q = $q_bak;
		$this->view->time = $time;		
		$this->view->perpage = $perpage;
		$this->view->total = $total;
		$this->view->rows = $rows;		
		$this->view->page = $page;
		$this->view->start = ($total==0?-1:$start);
		$this->view->end = ($total-1<$end?$total-1:$end);
	}

}
