<?php
    /**
     * Title  UserController
     */

    class UserController extends CommonController{ 
	    
	    function  indexAction(){
	        $this->_redirect('/user/list');
	    }
	    
	    function  addAction(){
            //request
            $userName = $this->_getParam('userName');
            $email = $this->_getParam('email');
            $password = $this->_getParam('password');
            $createIp = $this->_getParam('createIp');
            $createAt = $this->_getParam('createAt');
            $updateAt = $this->_getParam('updateAt');
            $lastLoginIp = $this->_getParam('lastLoginIp');
            $lastLoginAt = $this->_getParam('lastLoginAt');
            $isActived = $this->_getParam('isActived');
            $isAdministrator = $this->_getParam('isAdministrator');
            $status = $this->_getParam('status');
            $headingphoto = $this->_getParam('headingphoto');
            $comefrom = $this->_getParam('comefrom');
            $blog = $this->_getParam('blog');
            $blogName = $this->_getParam('blogName');
            $introduce = $this->_getParam('introduce');
            
            //info
            $userInfo = new User_UserInfo();
            $userInfo->setUserName($userName);
            $userInfo->setEmail($email);
            $userInfo->setPassword($password);
            $userInfo->setCreateIp($createIp);
            $userInfo->setCreateAt($createAt);
            $userInfo->setUpdateAt($updateAt);
            $userInfo->setLastLoginIp($lastLoginIp);
            $userInfo->setLastLoginAt($lastLoginAt);
            $userInfo->setIsActived($isActived);
            $userInfo->setIsAdministrator($isAdministrator);
            $userInfo->setStatus($status);
            $userInfo->setHeadingphoto($headingphoto);
            $userInfo->setComefrom($comefrom);
            $userInfo->setBlog($blog);
            $userInfo->setBlogName($blogName);
            $userInfo->setIntroduce($introduce);
            
            //dao
            $userDao = User_UserDao::getInstance();
            $userDao->add($userInfo);
			$this->_redirect('/user/list');
	    }
	    
	    function  editAction(){
			 $userId = intval($this->_getParam('userId'));
			 if(empty($userId)){
				$this->_redirect('/user/list/');
			 }else{
	            $userDao = User_UserDao::getInstance();
	            $this->view->userInfo = $userDao->get($userId);
			 }
	    }
	    	    
	    function  modifyAction(){
            //request
			$userId = intval($this->_getParam('userId'));
            $userName = $this->_getParam('userName');
            $email = $this->_getParam('email');
            $password1 = $this->_getParam('password1');
            $updateAt = time();
            //dao,info
	        $userDao = User_UserDao::getInstance();
            $userInfo = $userDao->get($userId);
            $userInfo->setUserName($userName);
            $userInfo->setEmail($email);
            if($password1!=""){
               $userInfo->setPassword(md5($password1));
            }
            $userInfo->setUpdateAt($updateAt);
            $userDao->modify($userInfo);
			$this->_redirect('/user/list');
	    	
	    }

	    function  viewAction(){
			$userId = intval($this->_getParam('userId'));
			if(empty($userId)){
				$this->_redirect('/user/list/');
			}else{
	            $userDao = User_UserDao::getInstance();
	            $this->view->userInfo = $userDao->get($userId);
			}
	    }	    	   
	    
	    function  delAction(){
			$userDao = User_UserDao::getInstance();
	    	$userIdArray = $this->_getParam('userId');
	        if(is_array($userIdArray)){	    	
		    	foreach( $userIdArray as $userId){
			    	$userId = intval($userId);
			    	if (!$userId) {
			    	  //null
			    	}else {
			    		$userDao->closeUser($userId);
			    	}
		    	}
	        }else{
		    	$userId = intval($userIdArray);
		    	if (!$userId) {
		    		//null
		    	}else {
		    		$userDao->closeUser($userId);
		    	}
	        }
	    	$this->_redirect('/user/list');
	    }

	    function  ajaxlistAction(){
	    }

	    function  jsonlistAction(){
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
			$perpage = 5;
            $userDao = User_UserDao::getInstance();
	        $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $total = $userDao->getListAllCount();
 		    $ajaxpage = new PageUtil(array('total'=>$total,'perpage'=>$perpage,'ajax'=>'ajax_page','nowindex'=>$page,'page_name'=>'page','url'=>"/user/jsonlist/"));
	        $data = array('list' => $userDao->getList($perpage,$page) ,
	                      'total' => $total,
	                      'perpage' => $perpage,
	                      'page' => $page,
	                      'pagebar' => $ajaxpage->show()
	                );
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    
	    function  jsongetAction(){
		   $userId = intval($this->_getParam('userId'));
           $userDao = User_UserDao::getInstance();
           $userInfo = $userDao->get($userId);
 	      
 	       $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
           $data = array('userInfo' => $userInfo);
	       $json = Zend_Json::encode($data);
	       echo $json;
	    }
	    	 
	    function  jsonsaveAction(){
	    	$userId = intval($this->_getParam('userId'));
            $userName = $this->_getParam('userName');
            $email = $this->_getParam('email');
            $password = $this->_getParam('password');
            $createIp = $this->_getParam('createIp');
            $createAt = $this->_getParam('createAt');
            $updateAt = $this->_getParam('updateAt');
            $lastLoginIp = $this->_getParam('lastLoginIp');
            $lastLoginAt = $this->_getParam('lastLoginAt');
            $isActived = $this->_getParam('isActived');
            $isAdministrator = $this->_getParam('isAdministrator');
            $status = $this->_getParam('status');
            $headingphoto = $this->_getParam('headingphoto');
            $comefrom = $this->_getParam('comefrom');
            $blog = $this->_getParam('blog');
            $blogName = $this->_getParam('blogName');
            $introduce = $this->_getParam('introduce');
            
	        $userDao = User_UserDao::getInstance();
            $cmd ="";
            if($userId>0){
            	//modify
            	$cmd = "modify";
		        $userInfo = $userDao->get($userId);
                $userInfo->setUserName($userName);
                $userInfo->setEmail($email);
                $userInfo->setPassword($password);
                $userInfo->setCreateIp($createIp);
                $userInfo->setCreateAt($createAt);
                $userInfo->setUpdateAt($updateAt);
                $userInfo->setLastLoginIp($lastLoginIp);
                $userInfo->setLastLoginAt($lastLoginAt);
                $userInfo->setIsActived($isActived);
                $userInfo->setIsAdministrator($isAdministrator);
                $userInfo->setStatus($status);
                $userInfo->setHeadingphoto($headingphoto);
                $userInfo->setComefrom($comefrom);
                $userInfo->setBlog($blog);
                $userInfo->setBlogName($blogName);
                $userInfo->setIntroduce($introduce);
		        $userDao->modify($userInfo);
            	
            }else{
                //add
                $cmd = "add";
	            //info
	            $userInfo = new User_UserInfo();
                $userInfo->setUserName($userName);
                $userInfo->setEmail($email);
                $userInfo->setPassword($password);
                $userInfo->setCreateIp($createIp);
                $userInfo->setCreateAt($createAt);
                $userInfo->setUpdateAt($updateAt);
                $userInfo->setLastLoginIp($lastLoginIp);
                $userInfo->setLastLoginAt($lastLoginAt);
                $userInfo->setIsActived($isActived);
                $userInfo->setIsAdministrator($isAdministrator);
                $userInfo->setStatus($status);
                $userInfo->setHeadingphoto($headingphoto);
                $userInfo->setComefrom($comefrom);
                $userInfo->setBlog($blog);
                $userInfo->setBlogName($blogName);
                $userInfo->setIntroduce($introduce);
	            //dao
	            $userDao->add($userInfo);                
            }
		    $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $data = array('cmd'=>$cmd,'userInfo' => $userInfo);
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    	    
	    function  jsondelAction(){
	    	$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
			$userDao = User_UserDao::getInstance();
	    	$userIdArray = $this->_getParam('userId');
	        if(is_array($userIdArray)){	    	
		    	foreach( $userIdArray as $userId){
			    	$userId = intval($userId);
			    	if (!$userId) {
			    	  //null
			    	}else {
			    		$userDao->delById($userId);
			    	}
		    	}
	        }else{
		    	$userId = intval($userIdArray);
		    	if (!$userId) {
		    		//null
		    	}else {
		    		$userDao->delById($userId);
		    	}
	        }
	        $json = Zend_Json::encode($userIdArray);
	        echo $json;
	    }
	    
	    
        function checkimageAction(){
  	        // disable autorendering for this action only:
	        $this->_helper->viewRenderer->setNoRender();
	        
			header ("Pragma: no-cache");
			//$string="1A3B4C";
			session_start();
			//$protect_code=$_SESSION['protect_code'];
			$randval=rand(1000,9999);
			//session_register("randval");
			$_SESSION["randval"] = $randval;
			header ("Content-type: image/png");
			
			//图象大小
			$im = @imagecreate (38,15)
			    or die ("图形处理错误");
			$background_color = imagecolorallocate ($im, 255, 255, 255);
			$text_color = imagecolorallocate ($im, 233, 14, 91);
			imagestring ($im, 5, 1, 1,  $randval, $text_color);
			imagepng ($im);
        }

        function registerproAction(){
           $this->_helper->viewRenderer->setNoRender();
           $name=$this->_getParam('name');
           $userDao = User_UserDao::getInstance();
           	if($userDao->existUser($name)){
				echo "对不起,您输入的用户已存在或者为空!";
			}else{
				echo "<font color='green'>恭喜您,可以注册...</font>";
			}
        }
        
        
        function  checkloginjsonAction(){
        	$rtn = array();
        	$log = Zend_Registry::get('log');
            $log->debug( "### userId".Util::getUserId());
        	if(Util::getUserId()<1){
	        	//$this->view->referer = $referer;
               $rtn['login'] = false;
	        	//$this->_redirect("/post/postaddpicpreview/postId/$postId"); 
	        }else{
	           $rtn['login'] = true;
	        }
	        $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $json = Zend_Json::encode($rtn);
	        echo $json;	        
        }

        function registerAction(){
        	
        }
        
        
	    function  registersaveAction(){
	    	$this->_helper->viewRenderer->setNoRender();
	    	session_start();
	    	$right = "";
			if (isset($_SESSION["randval"])) {
				$right = $_SESSION["randval"];
			}
			
			
			
//			else{
//				echo "正确";
//			}			
			
            //request
            $userName = $this->_getParam('userName');
            $userName = trim($userName);
            $email = $this->_getParam('email');
            $password = $this->_getParam('password');
            $password2 = $this->_getParam('password2');
            $createIp = Util::getUserIp();

            $createAt = time();            
            $lastLoginIp = $this->_getParam('lastLoginIp');
            $lastLoginAt = $this->_getParam('lastLoginAt');

            $comefrom = $this->_getParam('comefrom');
            $introduce = $this->_getParam('introduce');
            
            $isActived = 1;
            $isAdministrator = 0;
            $status = -1; //2009/08/22 修改为-1，用于区别注册和无效，刚注册为-1，无效为0
            $headingphoto = "";
            $blog = "";
            $blogName = "";
            
            $userDao = User_UserDao::getInstance();
            
			//用户名检查
			if(!preg_match("/^[\x{4e00}-\x{9fa5}a-zA-Z0-9_]+$/u",$userName)){
			   //UTF-8汉字字母数字下划线正则表达式
                echo "{success: false, messages: { message: '用户名只能是中文、英文或数字,也不能有空格.',tagid:'userName'}}";							   
                return;
			}			
			if ($userDao->checkUserIfExistsByUserName($userName)){
                echo "{success: false, messages: { message: '此用户名已经注册，请重新选择其它用户名来注册.',tagid:'userName'}}";				
				return;
			}

			//邮箱检查
			if($this->_getParam('email')==''){
                echo "{success: false, messages: { message: '您还没有输入邮箱，请输入.',tagid:'email'}}";				
				return;
			}
			$pattern = '/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])' .
				'(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i';
			if(!preg_match ($pattern, $email)){
                echo "{success: false, messages: { message: '邮箱格式不正确，请核实.',tagid:'email'}}";				
				return;
			}
			if ($userDao->checkUserIfExistsByEmail($this->_getParam('email'))){
                echo "{success: false, messages: { message: '此邮箱已经注册，请重新选择其它邮箱来注册.',tagid:'email'}}";				
				return;
			}

			
			//密码检查
			if($this->_getParam('password')=='' || $this->_getParam('password2')==''){
                echo "{success: false, messages: { message: '密码不能为空，请确认.',tagid:'password'}}";				
				return;
			}
			    
			if($this->_getParam('password') != $this->_getParam('password2')){
                echo "{success: false, messages: { message: '两次输入的密码不一致，请确认.',tagid:'password'}}";				
				return;
			}			
			
            //验证码确认
			if ( (trim($right) <> trim($_POST["qrm"])) || trim($_POST["qrm"]) == "") {
                echo "{success: false, messages: { message: '验证码错误，请输入正确的验证码.',tagid:'qrm'}}";				
				return;
			}						

            //info
            $userInfo = new User_UserInfo();
            $userInfo->setUserName($userName);
            $userInfo->setEmail($email);
            $userInfo->setPassword(md5($password));
            $userInfo->setCreateIp($createIp);

            $userInfo->setCreateAt($createAt);
            
            $userInfo->setUpdateAt($createAt);

            $userInfo->setLastLoginIp($lastLoginIp);
            $userInfo->setLastLoginAt($lastLoginAt);      
            
            $userInfo->setIsActived($isActived);
            $userInfo->setIsAdministrator($isAdministrator);
            $userInfo->setStatus($status);
            $userInfo->setHeadingphoto($headingphoto);
            $userInfo->setComefrom($comefrom);
            $userInfo->setBlog($blog);
            $userInfo->setBlogName($blogName);
            $userInfo->setIntroduce($introduce);            
            //dao
            $userDao->add($userInfo);
     	    $log = Zend_Registry::get('log');
            $log->debug( "### userId".$userInfo->getUserId());
			//$this->_redirect('/index/index');//指向网站首页
			
			//发信 开始
			if (!defined( "PLOG_CLASS_PATH" )) {
			    define( "PLOG_CLASS_PATH", $_SERVER['DOCUMENT_ROOT']."/../library/plog/");
			}            
		    include_once( PLOG_CLASS_PATH."class/mail/emailmessage.class.php" );
		    include_once( PLOG_CLASS_PATH."class/mail/emailservice.class.php" );
		    $message = new EmailMessage();
		    $message->setCharset("utf-8");
		    $message->addTo( $email );
		    
			$config = Zend_Registry::get('config');
			$admin_email = $config->admin_email;
			$log->debug( "################### $admin_email");
			$admimEmailArray = explode(",",$admin_email);
			foreach($admimEmailArray as $adminEmail){
		    	$message->addBcc( $adminEmail );	
		        $log->debug( "###################adminEmail:".$adminEmail);			
			}	
		    
		    
		    $log->debug( "################### $email");
		    //$message->addTo( "mrfanwu@gmail.com" );
		    $message->setFrom( "paowangjianghu@yahoo.com" );
		    $message->setFromName( "paowangjianghu@yahoo.com" );
		    $message->setSubject("欢迎您在泡网俱乐部注册");
		    $que = array('password' => md5($password) ,
	                      'email' => $email,
	                      'user_id' => $userInfo->getUserId(),
	                      'user_name' => $userName  
	                );		    
		    		
		    $linkUrl = "http://".$_SERVER['SERVER_NAME']."/user/activation/a/".$userDao->calculatePasswordResetHash($que)."/b/".md5($que['user_name']);
		    $log->debug( "###################linkurl:".$linkUrl);
		    $log->debug( "###################user_name".md5($que['user_name']));
		    $log->debug( "###################md5(user_name)".$que['user_name']);
		    
		    $message->setBody( $userName."您好！\n 欢迎您在泡网注册，请点击下面的链接完成注册 ".$linkUrl."  。\n  祝好！" );
		    $service = new EmailService();
		    //邮件发送不成功，注册不能完成
		    if( $service->sendMessage( $message )){
		    	$log->debug( "################### send ok");
		        //echo "{success: true, messages: { message: '邮件已经发送,请查收!',goto:'/' }}"; 		      
		        echo "{success: true, messages: { message: '注册成功，请在信箱里查看激活邮件，通过邮件里的链接激活用户。'}}";
		        return ;
		    }  
		    else{
		      $log->debug( "################### send fail");
              echo "{success: false, messages: { message: '邮件发送失败，请检查您的邮箱！',tagid:'email'}}";				
			  return;		      
		      //echo "{success: false, errors: { reason: '邮件发送失败,请稍后再重设密码!' }}";
		    }		    
			//发信 结束		    
			
	    }


        
        function loginAction(){
        }
        
        function logoutAction(){
        	$this->_helper->viewRenderer->setNoRender();
			setcookie("paowanguser", "", (time()-1), "/");
//			$this->_redirect('/index/index');//指向网站首页
            echo "success";
        }
        
        
	    function  loginproAction(){
	    	//https://www.us720.com/app/webroot/js/jquery/?C=S;O=A
	    
	    	$this->_helper->viewRenderer->setNoRender();
			$name = $this->_getParam('name');
			$password = $this->_getParam('password');
			$flag = intval($this->_getParam('flag'));
            
			$msg = "fail";           
			$result = ControllerUtils::login($name, $password, $flag);
			if($result){
				$msg = "success";
			}
			echo $msg;	
		}
		
		public function findpasswordproAction(){
	    	$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
			$userName = $this->_getParam('userName');
			$email = $this->_getParam('email');
  					$log = Zend_Registry::get('log'); 	
  					$log->debug( "################### name:$userName");					 
		            $log->debug( "################### email:$email");			
			$userDao = User_UserDao::getInstance();
			//用户名检查
			if (!$userDao->checkUserByName($userName)){
                echo "{success: false, messages: { message: '请输入正确的用户名！' }}";
				return;
			}
			//邮箱检查
			if (!$userDao->checkUserByEmail($email)){
                echo "{success: false, messages: { message: '请输入正确的邮箱！' }}";
				return;
			}
			
			$status = $userDao->getUserStatusByNameEmail($userName,$email);
			
           
            if($status == -9){
                echo "{success: false, messages: { message: '用户名和邮箱不对应,请核实！' }}";
				return ;            	
            }  
            
            if($status == 0){
                echo "{success: false, messages: { message: '该用户名已经关闭，如需使用，请联系系统管理员！' }}";
				return ;            	
            }	
                
            $list = $userDao->getUserByNameEmail($userName,$email);  
            $que = $list[0];
             
            if($status == -1){
            	//还没有收到信，直接发确认信
			     if($userDao->sendRegiserEmail($que)){
			     	 echo "{success: true, messages: { message: '请在信箱里查看激活邮件，通过邮件里的链接激活用户!',goto:'/' }}";
			     }else{
			     	 echo "{success: false, messages: { message: '邮件发送失败,请稍后再重设密码!' }}";
			     }; 
			     return ;           	
            }	
             
            //发信
	        $data = array('que' => $que);
	        $json = Zend_Json::encode($data);
	        $log->debug( "################### json:$json");
	        $log->debug( "################### que name:".$que['user_name']);
			$linkUrl = "http://".$_SERVER['SERVER_NAME']."/user/setnewpassword/a/".$userDao->calculatePasswordResetHash($que)."/b/".md5($que['user_name']);
            $log->debug( "################### linkUrl:".$linkUrl);

             //echo "{success: false, messages: { message: '$linkUrl!',goto:'/' }}"; 	
             //return;
             
					if (!defined( "PLOG_CLASS_PATH" )) {
					    define( "PLOG_CLASS_PATH", $_SERVER['DOCUMENT_ROOT']."/../library/plog/");
					}            
				    include_once( PLOG_CLASS_PATH."class/mail/emailmessage.class.php" );
				    include_once( PLOG_CLASS_PATH."class/mail/emailservice.class.php" );
				    $message = new EmailMessage();
				    $message->setCharset("utf-8");
				    $message->addTo( $email );
				    //$message->addTo( "mrfanwu@gmail.com" );
				    
				    $message->setFrom( "paowangjianghu@yahoo.com" );
				    $message->setFromName( "paowangjianghu@yahoo.com" );
				    
				    $message->setSubject("泡网邮箱密码重置");
				    $message->setBody( "您好！\n 您已经申请重置密码，请点击下面的链接 $linkUrl 完成操作。如果你不准备重置密码,请忽略这条信息!\n  祝好！" );
				    $service = new EmailService();				    
					    
				    if( $service->sendMessage( $message )){
				    	$log->debug( "################### 发送成功");
				    
				      //$this->send( "邮件已经发送,请查收!" );
				      //echo "ok";
				      echo "{success: true, messages: { message: '邮件已经发送,请查收!',goto:'/' }}"; 		      
				    }  
				    else{
				      //$this->send( "邮件已经失败,请查收!" );
				      $log->debug( "################### 发送失败");
				      echo "{success: false, messages: { message: '邮件发送失败,请稍后再重设密码!' }}";
				    }  
            	}		
	

		function setnewpasswordAction(){
           
           $a= $this->_getParam('a');
           $b= $this->_getParam('b');

           $userDao = User_UserDao::getInstance();
		   $userRow = $userDao->verifyRequest($b,$a);
		   
		   if(!$userRow || $userRow==null){
		   	
                $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
				echo "<script>";
				echo "alert('链接中的参数不正确！');";
				echo "</script>";
				return ;  		   	
		   }
		   //Zend::dump($userRow);
		   $this->view->a = $a;
		   $this->view->b = $b;
		   $this->view->userId =$userRow['user_id'];
		   $this->view->userName =$userRow['user_name'];
		   //$this->send('Register/NewPasswordForm.php');
		   //echo $view->render('Register/NewPasswordForm.php');
		}

		function activationAction(){
           
           $a= $this->_getParam('a');
           $b= $this->_getParam('b');

           $userDao = User_UserDao::getInstance();
		   $userRow = $userDao->verifyRequest($b,$a);

            $log = Zend_Registry::get('log');
            $log->debug( "################### a:".$a);
            $log->debug( "################### b:".$b);		   
		   
		   if(!$userRow || $userRow==null){
		   	
                $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
                header("Content-type: text/html; charset=utf-8");
				echo "<script>";
				echo "alert('链接中的参数不正确！');";
				echo "</script>";
				return ;  		   	
		   }
		   
		   if($userRow['status']=='1'){
		   	
                $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
                header("Content-type: text/html; charset=utf-8");
				echo "<script>";
				echo "alert('已经激活成功,不需要再次激活！');";
				echo "</script>";
				return ;  		   	
		   }		   
		   //Zend::dump($userRow);
		   $this->view->a = $a;
		   $this->view->b = $b;
		   $this->view->userId =$userRow['user_id'];
		   //$this->send('Register/NewPasswordForm.php');
		   //echo $view->render('Register/NewPasswordForm.php');
		}
		
		
		function activationokAction(){
    
            $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
            $a= $this->_getParam('a');
            $b= $this->_getParam('b');
            $userId= $this->_getParam('userId');

            $log = Zend_Registry::get('log');
            $log->debug( "################### a:".$a);
            $log->debug( "################### b:".$b);
            $log->debug( "################### userId:".$userId);


           //检查链接参数
		   $userDao = User_UserDao::getInstance();
		   $userRow = $userDao->verifyRequest($b,$a);
		   
		   if(!$userRow || $userRow==null){
                echo "{success: false, messages: { message: '链接中的参数不正确！'}}";
				return ;  		   	
		   }
			
       	    $db =  Zend_Registry::get('db');
            $table = "pw_user";
            $set = array(
                'status'=>1,
            );            
            $where = $db->quoteInto('user_id = ?',$userId);
            $rows_affected = $db->update($table,$set,$where);	
            
            $log = Zend_Registry::get('log');
            $log->debug( "###################".$rows_affected);

			//密码修改成功,去登录页面
            echo "{success: true, messages: { message: '帐号激活成功!',goto:'/'}}";

		}
		

		function updatepasswordAction(){
    
            $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
            $a= $this->_getParam('a');
            $b= $this->_getParam('b');
            $newPassword= $this->_getParam('newPassword');
            $retypePassword= $this->_getParam('retypePassword');
            $userId= $this->_getParam('userId');
//            Zend::dump($newPassword);
//            Zend::dump($retypePassword);
//			Zend::dump($id);

            $log = Zend_Registry::get('log');
            $log->debug( "################### a:".$a);
            $log->debug( "################### b:".$b);
            $log->debug( "################### userId:".$userId);
            $log->debug( "################### newPassword:".$newPassword);
            $log->debug( "################### retypePassword:".$retypePassword);	
            			
            //检查两次输入的密码
			if( $newPassword != $retypePassword ) {
	            //$view = $this->getView();
//	            $view->msg = "两次输入的密码不一致！";
//				$view->a = $a;
//				$view->b = $b;		   
//				$view->id = $id;	            
//				echo $view->render('Register/NewPasswordForm.php');
                echo "{success: false, messages: { message: '两次输入的密码不一致！'}}";
//				echo "两次输入的密码不一致！";
				return;
			}

           //检查链接参数
		   $userDao = User_UserDao::getInstance();
		   $userRow = $userDao->verifyRequest($b,$a);
		   
		   if(!$userRow || $userRow==null){
//	            $view = $this->getView();
//	            $view->msg = "链接中的参数不正确！";
//				$view->a = $a;
//				$view->b = $b;		   
//				$view->userId = $userId;		            
//				echo $view->render('Register/NewPasswordForm.php');
                echo "{success: false, messages: { message: '链接中的参数不正确！'}}";
				return ;  		   	
		   }
			
       	    $db =  Zend_Registry::get('db');
            $table = "pw_user";
            $set = array(
                'password'=>md5($newPassword),
            );            
            $where = $db->quoteInto('user_id = ?',$userId);
            $rows_affected = $db->update($table,$set,$where);	
            
            $log = Zend_Registry::get('log');
            $log->debug( "###################".$rows_affected);

//			if($rows_affected<1){
//	            $view = $this->getView();
//	            $view->msg = "密码修改失败,请重新修改密码！";
//				$view->a = $a;
//				$view->b = $b;		   
//				$view->userId = $userId;		            
//				echo $view->render('Register/NewPasswordForm.php');
//				echo "{success: false, messages: { message: '密码修改失败,请重新修改密码！'}}";
//				return ;  						
//			}
			
			//密码修改成功,去登录页面
            echo "{success: true, messages: { message: '密码修改成功!',goto:'/'}}";
			//$this->forward('/login/ui');
		}


		function passwordeditAction(){
	    	$userId = intval($this->_getParam('userId'));
	    	$msg = $this->_getParam('msg');
	    	$userposttypeDao = Userposttype_UserposttypeDao::getInstance();
            $this->view->postTypeList = $userposttypeDao->getListByUserIdAndType($userId,0);//文章的分类
            $this->view->picTypeList = $userposttypeDao->getListByUserIdAndType($userId,1);//图片的分类
            $this->view->userId = $userId;
            $this->view->msg = $msg;
		}
		
		function passwordmodifyAction(){
			$oldPassword= $this->_getParam('oldPassword');
			$newPassword= $this->_getParam('newPassword');
		    $retypePassword= $this->_getParam('retypePassword');
	    	$userId = intval($this->_getParam('userId'));
	    	if(!empty($oldPassword) && !empty($newPassword) && !empty($retypePassword)){
	    		//都不为空才可以修改
				if( $newPassword != $retypePassword ) {
	                //echo "{success: false, messages: { message: '两次输入的密码不一致！'}}";
	                $msg = "两次输入的密码不一致！";
	                $this->_redirect("/user/passwordedit/userId/$userId/msg/".urlencode($msg));
				}else{
				   $userDao = User_UserDao::getInstance();
				   $userInfo = $userDao->get($userId);
				   if($userInfo->getPassword()==md5($oldPassword)){
				   	    $userInfo->setPassword(md5($newPassword));
				   	    $userDao->modify($userInfo);
		                $msg = "密码已经修改！";
		                $this->_redirect("/user/passwordedit/userId/$userId/msg/".urlencode($msg));				   	
				   }else{
		                $msg = "原密码不正确！";
		                $this->_redirect("/user/passwordedit/userId/$userId/msg/".urlencode($msg));				   	
				   }
				   	
				}
	    	}else{
	    		$msg = "请输入密码修改的全部信息";
	    		$this->_redirect("/user/passwordedit/userId/$userId/msg/".urlencode($msg));
	    	}
	        /* 	
	    	$userposttypeDao = Userposttype_UserposttypeDao::getInstance();
            $this->view->postTypeList = $userposttypeDao->getListByUserIdAndType($userId,0);//文章的分类
            $this->view->picTypeList = $userposttypeDao->getListByUserIdAndType($userId,1);//图片的分类
            $this->view->userId = $userId;
            */
		}		
		    
            

		function mjeditAction(){
	    	$userId = intval($this->_getParam('userId'));
	    	$userposttypeDao = Userposttype_UserposttypeDao::getInstance();
            $this->view->postTypeList = $userposttypeDao->getListByUserIdAndType($userId,0);//文章的分类
            $this->view->picTypeList = $userposttypeDao->getListByUserIdAndType($userId,1);//图片的分类
            $this->view->userId = $userId;
		}


		/*
		 * 用户信息查看
		 */
		 function userinfoAction(){
    	    //编辑
            $userId = intval($this->_getParam('userId'));
	    	if($userId==0){
	    	   $this->_redirect('/forum/index');  
	    	   return ;	
	    	}
	    	
	    	$userposttypeDao = Userposttype_UserposttypeDao::getInstance();
	    	$userDao = User_UserDao::getInstance();
	    	
	    	$this->view->userInfo = $userDao->get($userId);
		 }
		
		/**
		 * 用户管理
		 */
		 function manageAction(){
		 	$currentUserId = Rbm_RbmService::getInstance()->getCurrentLoginUserId();
		 	if($currentUserId == 0){
		 		$this->_redirect('/index/notfound');
		 		return;        
		 	}
            
            if(!Rbm_RbmService::getInstance()->isAdministrator($currentUserId) &&
                !Rbm_RbmService::getInstance()->isBanzhu($currentUserId)){
                $this->_redirect('/index/notfound');
                return;    	
            }
		    
            $userId = intval($this->_getParam('userId'));
            if($userId == 0){
               $this->view->userInfo = null;  
               return ; 
            }
            
            $userposttypeDao = Userposttype_UserposttypeDao::getInstance();
            $userDao = User_UserDao::getInstance();
            $this->view->userInfo = $userDao->get($userId);
		 }
		 
		 function updateroleAction(){
		 	$this->_helper->viewRenderer->setNoRender(true);
		 	
		    $userId = intval($this->_getParam('userId'));
		    $forumId = intval($this->_getParam('forumId'));
		    $roleId = intval($this->_getParam('roleId'));
            if($userId==0 || $forumId==0 || $roleId==0){
               return ; 
            }
            $currentUserId = Rbm_RbmService::getInstance()->getCurrentLoginUserId();
            if(!Rbm_RbmService::getInstance()->isBanzhu($currentUserId) && !Rbm_RbmService::getInstance()->isAdministrator($currentUserId)){
            	echo "{success: false, messages: { message: '用户没有权限'}}";
            	return;
            }
            if($roleId != PaowangData::ROLE_MEMBER && $roleId != PaowangData::ROLE_FORUM_ADMIN && $roleId != PaowangData::ROLE_NORMAL_USER){
            	echo "{success: false, messages: { message: 'Invalid Role'}}";          
            	return;
            }
            Forumuser_ForumuserDao::getInstance()->updateRole($userId, $forumId, $roleId);
            Logging::audit("Modifying User Privilege -> userId:" . $userId . " forumId:" . $forumId . " roleId:" .$roleId . " by user " . $currentUserId);
            echo "{success: true, messages: { message: '用户权限修改成功!'}}";      
		 }
		 
		 function updatestatusAction(){
		 	$this->_helper->viewRenderer->setNoRender(true);
		    $currentUserId = Rbm_RbmService::getInstance()->getCurrentLoginUserId();
            if(!Rbm_RbmService::getInstance()->isBanzhu($currentUserId) && !Rbm_RbmService::getInstance()->isAdministrator($currentUserId)){
                echo "{success: false, messages: { message: '用户没有权限'}}";
                return;
            }
            $userId = intval($this->_getParam('userId'));
            $status = intval($this->_getParam('status'));
            if($status == 1){
                User_UserDao::getInstance()->openUser($userId, $status);	            
            }
            else{
            	User_UserDao::getInstance()->closeUser($userId, $status);
            }
            Logging::audit("Updating User Status-> userId:" . $userId . " status:" . $status . " by user " . $currentUserId);
            echo "{success: true, messages: { message: '用户状态修改成功!'}}";      
            
		 }
		 
		 function updateemailAction(){
            $this->_helper->viewRenderer->setNoRender(true);
            $currentUserId = Rbm_RbmService::getInstance()->getCurrentLoginUserId();
            if(!Rbm_RbmService::getInstance()->isBanzhu($currentUserId) && !Rbm_RbmService::getInstance()->isAdministrator($currentUserId)){
                echo "{success: false, messages: { message: '用户没有权限'}}";
                return;
            }
            $userId = intval($this->_getParam('userId'));
            $email = $this->_getParam('email');
            User_UserDao::getInstance()->updateEmail($userId, $email);

            Logging::audit("Modifying User Email -> userId:" . $userId . " email:" . $email . " by user " . $currentUserId);
            echo "{success: true, messages: { message: '用户邮箱修改成功!'}}";      
            
         }		 
		 
		 function lookupAction(){
		 	$this->_helper->viewRenderer->setNoRender(true);
		    $currentUserId = Rbm_RbmService::getInstance()->getCurrentLoginUserId();
            if(!Rbm_RbmService::getInstance()->isBanzhu($currentUserId) && !Rbm_RbmService::getInstance()->isAdministrator($currentUserId)){
                $this->_redirect('/index/notfound');
                return;
            }
            $userName = $this->_getParam('userName');
            if($userName == null){
            	$this->_redirect('/user/manage');
            	return;
            }
            $userInfo = User_UserDao::getInstance()->getInfoByName($userName);
            if($userInfo == null){
            	$this->_redirect('/user/manage');
            	return;
            }
            $userId = $userInfo->getUserId();
            $this->_redirect('/user/manage/userId/' . $userId);
		 }
		 
		 function findpasswordAction(){
		 	
		 }
		/*
		 * 用户信息编辑
		 */
		 function userinfoeditAction(){
    	    //编辑
	    	$userId = Util::getUserId();
	    	if($userId==0){
	    	   $this->_redirect('/forum/index');  
	    	   return ;	
	    	}
	    	
	    	$userposttypeDao = Userposttype_UserposttypeDao::getInstance();
	    	$userDao = User_UserDao::getInstance();
	    	$this->view->userInfo = $userDao->get($userId);
		 }
		 
		 function userinfomodifyAction(){
    		//保存
    		$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
            //request
			$userId = intval($this->_getParam('userId'));
            $email = $this->_getParam('email');
            
            
            $password = $this->_getParam('password');
            $password2 = $this->_getParam('password2');
            $updateAt = time();
            $comefrom = $this->_getParam('comefrom');
            $introduce = $this->_getParam('introduce');
            
            
            //dao,info
	        $userDao = User_UserDao::getInstance();
            $userInfo = $userDao->get($userId);
            $userInfo->setUpdateAt($updateAt);
            $userInfo->setComefrom($comefrom);
            $userInfo->setIntroduce($introduce);
            
			//邮箱检查
			if($this->_getParam('email')==''){
                echo "{success: false, messages: { message: '您还没有输入邮箱，请输入.',tagid:'email'}}";				
				return;
			}
			$pattern = '/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])' .
				'(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i';
			if(!preg_match ($pattern, $email)){
                echo "{success: false, messages: { message: '邮箱格式不正确，请核实.',tagid:'email'}}";				
				return;
			}
			
			//邮箱不修改，则不检查邮箱是否已经使用过，从而兼容以前不同用户使用同一个邮箱的情况
			if(trim($email)!=$userInfo->getEmail()){
				$userInfo->setEmail($email);
				if ($userDao->checkUserIfExistsByEmail($this->_getParam('email'),$userId)){
	                echo "{success: false, messages: { message: '此邮箱已经注册，请重新选择其它邮箱来注册.',tagid:'email'}}";				
					return;
				}
			}	
			
			
			            
            if( !empty($password) && strlen($password)<6 ){
                echo "{success: false, messages: { message: '您设置的密码长度太短！'}}";				
				return;               
            }else if(!empty($password2) && strlen($password2)<6){
            	echo "{success: false, messages: { message: '您设置的密码长度太短！'}}";
                return;
            }else if($password!=$password2){
            	echo "{success: false, messages: { message: '两次密码不一致！'}}";
                return;
            }else{
            	if($password!=''){
                    //密码不为空，才修改密码
                    $userInfo->setPassword(md5($password));
            	}    
                $userDao->modify($userInfo);
                echo "{success: true, messages: { message: '设置成功！'}}";
            }  
            $this->view->userInfo = $userDao->get($userId);
            return ;
		 }
		 
		 
		 function photolistAction(){
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
	
			$perpage = PaowangData::USER_PHOTO_PER_PAGE;
			
			$userId = intval($this->_getParam('userId'));
			$userDao = User_UserDao::getInstance();
			if($userId==0){
			   $qUserName = $this->_getParam('userName');				
			   $userId = $userDao->getUserIdByUserName($qUserName);
			}else{
				
			}	

			$sort = $this->_getParam('sort');
			if(empty($sort) || $sort==''){
				$sort ="create_at";
			}
			
            $postpicDao = Postpic_PostpicDao::getInstance();
            $this->view->lists = $postpicDao->getListRowByUserId($userId, $perpage, $page,$sort);
			$this->view->total= $postpicDao->getListRowByUserIdCount($userId);
			$this->view->perpage= $perpage;
			$this->view->page= $page;
			$this->view->userId= $userId;
			$this->view->userName= $userDao->getUserNameByUserId($userId);
			
			if($userId>0){
				$qUserName=$this->view->userName;
			}
			
			$this->view->qUserName= $qUserName; 
			
            $this->view->sort= $sort; 
			
			$photoClubMembers = $userDao->getPhotoClubMembers();
			$this->view->clubMembers = $photoClubMembers;
		 }		 
		 
 
				
	    function __call($action, $arguments)
	    {
	        //return $this->defaultAction();
	        throw new Zend_Controller_Exception('Invalid method called');
	        //echo "Action = " . $action . "<br />";
	        //echo "Arguments = " . $arguments;
	    }
	    function resendmailAction(){
		 	//检查权限，只有系统管理员可以重新发确认信
		 	$currentUserId = Rbm_RbmService::getInstance()->getCurrentLoginUserId();
		 	if(!Rbm_RbmService::getInstance()->isOpenUser($currentUserId)){
		 		$this->_redirect('/index/notfound');
		 	}
            
            if(!Rbm_RbmService::getInstance()->isAdministrator($currentUserId) ){
                $this->_redirect('/index/notfound');
                return;    	
            }
            
	         $this->_helper->viewRenderer->setNoRender();
	         $log = Zend_Registry::get('log');
	         $ds = new DbModel("","");
	         $userIdStr = $this->_getParam('userIdStr');
	        
	         
			 $userIdArray = @split(",",$userIdStr);
			 $userIdNum = "";
			 foreach($userIdArray as $userId){
				$userIdNum .= ",".$userId;
			 }
	 	     //echo $userIdNum."<br />";
			 
			 if(strlen($userIdNum)>1){
			 	 $userIdNum = substr($userIdNum,1);
			 }else{
			    echo "error";
			    return ;
			 } 
	         
	         $sql = "select * from pw_user where user_id in ($userIdNum)";
	         //return;
	         //user_id>53539 and user_id in (53541,53544,53546,53547,53563,53572,53575,53582,53583,53584,53585,53586,53587,53588,53589,53590,53591)
	         //
	         $list = $ds->fetchAll($sql);

				
			 $userDao = User_UserDao::getInstance();
				
	         foreach($list as $row){
				//发信 开始
			     if($userDao->sendRegiserEmail($row)){
			     	 echo $row["user_id"]." ################### send ok<br />";
			     }else{
			     	 echo $row["user_id"]." ################### send fail<br />";
			     };
				//发信 结束		    
	         }
	         //echo "####:".$postDao->getTopId(4000135); 
	    }	    




	    
	}
?>
