<?php
    /**
     * Title  UserposttypeController
     */

    class UserposttypeController extends CommonController{ 
	    
	    function  indexAction(){
	        $this->_redirect('/userposttype/list');
	    }
	    
	    function  listAction(){
	    	
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
	
			$perpage = 5;

            $userposttypeDao = Userposttype_UserposttypeDao::getInstance();
            $this->view->lists = $userposttypeDao->getList($perpage,$page);
			$this->view->total= $userposttypeDao->getListAllCount();
			$this->view->perpage= $perpage;
			$this->view->page= $page;
	    }
	    
	    function  newAction(){
            $userposttypeInfo = new Userposttype_UserposttypeInfo();
            $this->view->userposttypeInfo = $userposttypeInfo;
	    }
	    
	    function  addAction(){
            //request
            $userPostTypeName = $this->_getParam('userPostTypeName');
            $status = $this->_getParam('status');
            $userId = $this->_getParam('userId');
            $createAt = $this->_getParam('createAt');
            $isPic = $this->_getParam('isPic');
            
            //info
            $userposttypeInfo = new Userposttype_UserposttypeInfo();
            $userposttypeInfo->setUserPostTypeName($userPostTypeName);
            $userposttypeInfo->setStatus($status);
            $userposttypeInfo->setUserId($userId);
            $userposttypeInfo->setCreateAt($createAt);
            $userposttypeInfo->setIsPic($isPic);
            
            //dao
            $userposttypeDao = Userposttype_UserposttypeDao::getInstance();
            $userposttypeDao->add($userposttypeInfo);
			$this->_redirect('/userposttype/list');
	    }
	    
	    function  editAction(){
			 $userPostTypeId = intval($this->_getParam('userPostTypeId'));
			 if(empty($userPostTypeId)){
				$this->_redirect('/userposttype/list/');
			 }else{
	            $userposttypeDao = Userposttype_UserposttypeDao::getInstance();
	            $this->view->userposttypeInfo = $userposttypeDao->get($userPostTypeId);
			 }
	    }
	    	    
	    function  modifyAction(){
            //request
			$userPostTypeId = intval($this->_getParam('userPostTypeId'));
            $userPostTypeName = $this->_getParam('userPostTypeName');
            $status = $this->_getParam('status');
            $userId = $this->_getParam('userId');
            $createAt = $this->_getParam('createAt');
            $isPic = $this->_getParam('isPic');
            
            //dao,info
	        $userposttypeDao = Userposttype_UserposttypeDao::getInstance();
            $userposttypeInfo = $userposttypeDao->get($userPostTypeId);
            $userposttypeInfo->setUserPostTypeName($userPostTypeName);
            $userposttypeInfo->setStatus($status);
            $userposttypeInfo->setUserId($userId);
            $userposttypeInfo->setCreateAt($createAt);
            $userposttypeInfo->setIsPic($isPic);
            $userposttypeDao->modify($userposttypeInfo);
			$this->_redirect('/userposttype/list');
	    	
	    }

	    function  viewAction(){
			$userPostTypeId = intval($this->_getParam('userPostTypeId'));
			if(empty($userPostTypeId)){
				$this->_redirect('/userposttype/list/');
			}else{
	            $userposttypeDao = Userposttype_UserposttypeDao::getInstance();
	            $this->view->userposttypeInfo = $userposttypeDao->get($userPostTypeId);
			}
	    }	    	   
	    
	    function  delAction(){
			$userposttypeDao = Userposttype_UserposttypeDao::getInstance();
	    	$userPostTypeIdArray = $this->_getParam('userPostTypeId');
	        if(is_array($userPostTypeIdArray)){	    	
		    	foreach( $userPostTypeIdArray as $userPostTypeId){
			    	$userPostTypeId = intval($userPostTypeId);
			    	if (!$userPostTypeId) {
			    	  //null
			    	}else {
			    		$userposttypeDao->delById($userPostTypeId);
			    	}
		    	}
	        }else{
		    	$userPostTypeId = intval($userPostTypeIdArray);
		    	if (!$userPostTypeId) {
		    		//null
		    	}else {
		    		$userposttypeDao->delById($userPostTypeId);
		    	}
	        }
	    	$this->_redirect('/userposttype/list');
	    }

	    function  ajaxlistAction(){
	    }

	    function  jsonlistAction(){
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
			$perpage = 5;
            $userposttypeDao = Userposttype_UserposttypeDao::getInstance();
	        $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $total = $userposttypeDao->getListAllCount();
 		    $ajaxpage = new PageUtil(array('total'=>$total,'perpage'=>$perpage,'ajax'=>'ajax_page','nowindex'=>$page,'page_name'=>'page','url'=>"/userposttype/jsonlist/"));
	        $data = array('list' => $userposttypeDao->getList($perpage,$page) ,
	                      'total' => $total,
	                      'perpage' => $perpage,
	                      'page' => $page,
	                      'pagebar' => $ajaxpage->show()
	                );
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    
	    function  jsongetAction(){
		   $userPostTypeId = intval($this->_getParam('userPostTypeId'));
           $userposttypeDao = Userposttype_UserposttypeDao::getInstance();
           $userposttypeInfo = $userposttypeDao->get($userPostTypeId);
 	      
 	       $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
           $data = array('userposttypeInfo' => $userposttypeInfo);
	       $json = Zend_Json::encode($data);
	       echo $json;
	    }
	    	 
	    function  jsonsaveAction(){
	    	$userPostTypeId = intval($this->_getParam('userPostTypeId'));
            $userPostTypeName = $this->_getParam('userPostTypeName');
            $status = $this->_getParam('status');
            $userId = $this->_getParam('userId');
            $createAt = $this->_getParam('createAt');
            $isPic = $this->_getParam('isPic');
            
	        $userposttypeDao = Userposttype_UserposttypeDao::getInstance();
            $cmd ="";
            if($userPostTypeId>0){
            	//modify
            	$cmd = "modify";
		        $userposttypeInfo = $userposttypeDao->get($userPostTypeId);
                $userposttypeInfo->setUserPostTypeName($userPostTypeName);
                $userposttypeInfo->setStatus($status);
                $userposttypeInfo->setUserId($userId);
                $userposttypeInfo->setCreateAt($createAt);
                $userposttypeInfo->setIsPic($isPic);
		        $userposttypeDao->modify($userposttypeInfo);
            	
            }else{
                //add
                $cmd = "add";
	            //info
	            $userposttypeInfo = new Userposttype_UserposttypeInfo();
                $userposttypeInfo->setUserPostTypeName($userPostTypeName);
                $userposttypeInfo->setStatus($status);
                $userposttypeInfo->setUserId($userId);
                $userposttypeInfo->setCreateAt($createAt);
                $userposttypeInfo->setIsPic($isPic);
	            //dao
	            $userposttypeDao->add($userposttypeInfo);                
            }
		    $this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
	        $data = array('cmd'=>$cmd,'userposttypeInfo' => $userposttypeInfo);
	        $json = Zend_Json::encode($data);
	        echo $json;
	    }
	    	    
	    function  jsondelAction(){
	    	$this->_helper->viewRenderer->setNoRender();// disable autorendering for this action only:
			$userposttypeDao = Userposttype_UserposttypeDao::getInstance();
	    	$userPostTypeIdArray = $this->_getParam('userPostTypeId');
	        if(is_array($userPostTypeIdArray)){	    	
		    	foreach( $userPostTypeIdArray as $userPostTypeId){
			    	$userPostTypeId = intval($userPostTypeId);
			    	if (!$userPostTypeId) {
			    	  //null
			    	}else {
			    		$userposttypeDao->delById($userPostTypeId);
			    	}
		    	}
	        }else{
		    	$userPostTypeId = intval($userPostTypeIdArray);
		    	if (!$userPostTypeId) {
		    		//null
		    	}else {
		    		$userposttypeDao->delById($userPostTypeId);
		    	}
	        }
	        $json = Zend_Json::encode($userPostTypeIdArray);
	        echo $json;
	    }
	    
	    function typelistAction(){
	    	$userId = intval($this->_getParam('userId'));
	    	$userposttypeDao = Userposttype_UserposttypeDao::getInstance();
            $this->view->postTypeList = $userposttypeDao->getListByUserIdAndType($userId,0);//文章的分类
            $this->view->picTypeList = $userposttypeDao->getListByUserIdAndType($userId,1);//图片的分类
            $this->view->userId = $userId;
            
			$page = intval($this->_getParam('page'));
			if(empty($page)) $page = 1;
	
			$perpage = 5;

            $userposttypeDao = Userposttype_UserposttypeDao::getInstance();
            $this->view->lists = $userposttypeDao->getList($perpage,$page);
			$this->view->total= $userposttypeDao->getListAllCount();
			$this->view->perpage= $perpage;
			$this->view->page= $page;            	    	
	    }

	    function __call($action, $arguments)
	    {
	        //return $this->defaultAction();
	        //throw new Zend_Controller_Exception('Invalid method called');
	        //echo "Action = " . $action . "<br />";
	        //echo "Arguments = " . $arguments;
	    }
	}
?>