<?php
class Cache_Engine{
	
	const TAG_FORUM = "forum_";
	const TAG_FORUM_TOP = "forum_top_";
	const TAG_FORUM_HOT = "forum_hot_";
	const TAG_FEED_PAOWANG = "feed_paowang_";
	const TAG_FEED_BLOG = "feed_blog_";
	const TAG_PHOTO_MEMBERS = "photo_members_";
	const TAG_INDEX = "index_";
	
	public static function generateId($parameters){
		return implode('_', $parameters);
	}
	
	public static function load($cacheId){
		$cache = Zend_Registry::get('cache');
		$result = $cache->load($cacheId);
		return $result;
	}
	
	public static function save($cacheObject, $cacheId, $tags = array(), $specificLifetime = false){
		$cache = Zend_Registry::get('cache');
		$cache->save($cacheObject, $cacheId, $tags, $specificLifetime);
	}
	
	public static function clean($mode = 'all', $tags = array()){
		$cache = Zend_Registry::get('cache');
		$cache->clean($mode, $tags);
	}
}