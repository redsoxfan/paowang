<?php
class CommonController extends Zend_Controller_Action{ 
    
    /**
     * init of ForumController
     *
     */
    public function init(){
        parent::init();
        // get resource timestamp folder name
        $this->view->resourceDir = Util::getResourceTimestampDir(); 
    }
}   