<?php
/**
 * Common utilities used by controllers
 *
 */

class ControllerUtils{

	public static function login($username, $password, $rememberpass) {
        $log = Zend_Registry::get('log');
		$name = Util::loginname2loginname($username);//多个空格合并成一个空格
		$username = Util::loginname2username($name);
		
		$returnValue = false;
		if(isset($name) && isset($password)){
			$userDao = User_UserDao::getInstance();
			$userInfo = $userDao->getInfoByNameAndPassword($username,$password);
			if($userInfo != null)
			{
				if($rememberpass == 1){
					//设定 cookie 30 天后失效
					$expire = time()+60*60*24*30;
				}else{
					$expire = 0;
				}
				$log->debug( "################### userId ".$userInfo->getUserId());
				$log->debug( "################### userName ".$userInfo->getUserName());
				
				//加密 开始
				$cookieArray = array(
				   "userId"=>"".$userInfo->getUserId(),
				   "userName"=>$name
			    );
		       $aes = new AES('PAOWANG19990603a');
		       $str = json_encode($cookieArray);
		       $aesString = $aes->encrypt($str);
		       //echo $aesString;
		       $str = base64_encode($aesString);
		      // $log->debug( "################### paowanguser ".$str);
				
				//加密 结束
				//setcookie("userId", "".$userInfo->getUserId(), $expire, "/");
				//setcookie("userName", $name, $expire, "/");
				setcookie("paowanguser", $str, $expire, "/");
				
				return $userInfo->getUserId();
				//$this->_redirect('/index/index');
			}else{
				$returnValue = false;
				//$this->_redirect('/user/login');
			}
		}
		
		return $returnValue;
	}

	private static function addNewPost($parameters){
		$forumId = $parameters['forumId'];
		$userId = $parameters['userId'];
		$userName = $parameters['userName'];
		$title = $parameters['title'];
		$html = $parameters['text'];
		$tags = $parameters['tags'];

		$log = Zend_Registry::get('log');

		if($forumId==0){
			$forumId = PaowangData::FORUM_ID_DEFAULT;//默认江湖论剑页面
		}
		$forumName = Forum_ForumDao::getInstance()->getForumName($forumId);

		//request
		$parentId = 0;
		$topId = 0;//?
		$isDigest = "0";
		$depth = 1;
		$showOrder = 1;
		$listOrder = 1;
		$canHaschild = 1;
		$isTail = 1;
		$status = 1;
		$createAt = time();
		$userIp = Util::getUserIp();
		$click = rand(0, 2);
		$updateAt = time();
		$updateIp = $userIp;
		$isCeil = 0;
		$isRelive = 0;
		$reliveTime = 0;
		$userPostTypeId = 0;
		$childType = 0;
		$isPic = 0;
		$childPosts = 1;

		//text and length
		$html = preg_replace("/<a(((?!\starget).)+?)>/i","<a\\1 target=\"_blank\">",$html);
		//        $html = str_replace("\\\"", "\"", $html);//替换一下\"
		//$html = preg_replace('/\s*(<br\s*\/?\s*>\s*){1,}/im',"",$html);  //把br给去掉
		$text = Util::stripText($html);
		$textLength = Util::strLenUtf8($text);

		//info
		$postInfo = new Post_PostInfo();
		$postInfo->setForumId($forumId);
		$postInfo->setUserId($userId);
		$postInfo->setUserName($userName);
		$postInfo->setParentId($parentId);
		$postInfo->setTopId($topId);
		$postInfo->setIsDigest($isDigest);
		$postInfo->setTitle($title);
		$postInfo->setDepth($depth);
		$postInfo->setShowOrder($showOrder);
		$postInfo->setListOrder($listOrder);
		$postInfo->setCanHaschild($canHaschild);
		$postInfo->setIsTail($isTail);
		$postInfo->setStatus($status);
		$postInfo->setCreateAt($createAt);
		$postInfo->setUserIp($userIp);
		$postInfo->setClick($click);
		$postInfo->setTags($tags);

		$postInfo->setTextLength($textLength);
		$postInfo->setUpdateAt($updateAt);
		$postInfo->setUpdateIp($updateIp);
		$postInfo->setIsCeil($isCeil);
		$postInfo->setIsRelive($isRelive);
		$postInfo->setReliveTime($reliveTime);
		$postInfo->setUserPostTypeId($userPostTypeId);
		$postInfo->setChildType($childType);
		$postInfo->setIsPic($isPic);
		$postInfo->setChildPosts($childPosts);

		//dao
		$postDao = Post_PostDao::getInstance();

		$postDao->add($postInfo);
		$postId = $postInfo->getPostId();
		$postInfo->setTopId($postId);
		$postDao->modify($postInfo);

		//info
		$posttextInfo = new Posttext_PosttextInfo();
		$posttextInfo->setPostId($postId);
		$posttextInfo->setText($text);
		$posttextInfo->setHtml($html);

		if(!empty($tags) && $tags!=''){
			$tagArray =   preg_split ('/([\s,;]+)/', $tags);
			$tagDao = Tag_TagDao::getInstance();
			foreach($tagArray as $tagName){
				$tagInfo = new Tag_TagInfo();
				$tagInfo->setPostId($postId);
				$tagInfo->setTagName($tagName);
				$tagInfo->setStatus(1);
				$tagInfo->setUserId($userId);
				$tagInfo->setCreateAt($createAt);
				$tagInfo->setUserIp($userIp);
				$tagInfo->setTagType(1);//帖子
				$tagInfo->setTagUserId($userId);//帖子发布者的ID

				if($tagDao->getListAllCountByUserIdAndCreateUserIdAddTagNameAndTopId($userId,$postInfo->getUserId(),$tagName,$postInfo->getTopId())<1){
					$tagDao->add($tagInfo);
				}
			}
		}

		//dao
		$posttextDao = Posttext_PosttextDao::getInstance();
		$posttextDao->add($posttextInfo);
		
		return $postId;
	}
	
	public static function replyPost($parameters){
//		$forumId = $parameters['forumId'];
        $parentId = $parameters['parentId'];
        $userId = $parameters['userId'];
        $userName = $parameters['userName'];
        $title = $parameters['title'];
        $html = $parameters['text'];
        $tags = $parameters['tags'];
        
        $userIp = Util::getUserIp();
        $updateIp = Util::getUserIp();
        
        $isDigest = "0";
        $showOrder = 1;
        $listOrder = 1;
        $canHaschild = 1;
        $isTail = 1;
        $status = 1;
        $createAt = time();
        $click = rand(0, 2);
        $updateAt = time();
        $isCeil = 0;
        $isRelive = 0;
        $reliveTime = 0;
        $userPostTypeId = 0;
        $childType = 0;
        $isPic = 0;
        
        //text and length
        $html = preg_replace("/<a(((?!\starget).)+?)>/i","<a\\1 target=\"_blank\">",$html);
//        $html = str_replace("\\\"", "\"", $html);//替换一下\"
//        $html = preg_replace('/\s*(<br\s*\/?\s*>\s*){1,}/im',"",$html);  //把br给去掉         
        $text = Util::stripText($html);
        $textLength = Util::strLenUtf8($text);
        
        $postDao = Post_PostDao::getInstance();
        $postInfoParent = $postDao->get($parentId);
        $topId =  $postInfoParent->getTopId();
        $depth = intval($postInfoParent->getDepth())+1;

        //info
        $postInfo = new Post_PostInfo();
        $postInfo->setForumId($postInfoParent->getForumId());
        $postInfo->setUserId($userId);  
        $postInfo->setUserName($userName);
        $postInfo->setParentId($parentId);
        $postInfo->setTopId($topId);
        $postInfo->setIsDigest($isDigest);
        $postInfo->setTitle($title);
        $postInfo->setDepth($depth);
        $postInfo->setShowOrder($showOrder);
        $postInfo->setListOrder($listOrder);
        $postInfo->setCanHaschild($canHaschild);
        $postInfo->setIsTail($isTail);
        $postInfo->setStatus($status);
        $postInfo->setCreateAt($createAt);
        $postInfo->setUserIp($userIp);
        $postInfo->setClick($click);
        $postInfo->setTags($tags);
        $postInfo->setTextLength($textLength);
        $postInfo->setUpdateAt($updateAt);
        $postInfo->setUpdateIp($updateIp);
        $postInfo->setIsCeil($isCeil);
        $postInfo->setIsRelive($isRelive);
        $postInfo->setReliveTime($reliveTime);
        $postInfo->setUserPostTypeId($userPostTypeId);
        $postInfo->setChildType($childType);
        $postInfo->setIsPic($isPic);
        
        //reply post
        $postDao->add($postInfo);
        $postId = $postInfo->getPostId();
                
        //add text
        $posttextInfo = new Posttext_PosttextInfo();
        $posttextInfo->setPostId($postId);
        $posttextInfo->setText($text);
        $posttextInfo->setHtml($html);
        
        $posttextDao = Posttext_PosttextDao::getInstance();
        $posttextDao->add($posttextInfo);
        
        //update parent
        $postInfoParent->setIsTail(0);
        $postDao->modify($postInfoParent);
        
        //update top
        $postInfoTop = $postDao->get($topId);
        $postInfoTop->setLastChildUpdateAt($createAt);//更新主贴的“最后子帖更新时间”
        $postInfoTop->setChildPosts($postDao->getChildPostsCount($topId));
        $postDao->modify($postInfoTop);   
        
        return $postId;  
	}

	public static function simpleAddPost($parameters, $isReply){
		if(!$isReply){
			return self::addNewPost($parameters);
		}
		else{
			return self::replyPost($parameters);
		}
	}
}
