<?php
class DbModel{
	private $_db;
	private $_tableName;
	private $_primaryKeyName;

	public function __construct($tableName = null, $primaryKeyName = null)
	{
		$this->_db = Zend_Registry::get('db');
		$this->_tableName = $tableName; 
		$this->_primaryKeyName = $primaryKeyName;
	}
	
	public function getList($whereSql = '1=1', $limit = 0, $page = 0,$fieldStr = "*",$orderSql = "")
	{
		$select = $this->_db->select();
		if($orderSql==""){
		    $orderSql = ($this->_primaryKeyName).' desc';
		}
		$select->from($this->_tableName, $fieldStr);
		$select->where($whereSql)
			   ->order($orderSql);
			   
		// 分页
		if ($limit > 0 && $page > 0) {
			$select->limitPage($page, $limit);
		}elseif ($limit > 0) {
			$select->limit($limit);
		}
				
		$sql = $select->__toString();
		
		$log = Zend_Registry::get('log');
        //$log->debug( "################### sql:".$sql);		
		return $this->_db->fetchAll($sql);
	}
	
	public function getRow($id)
	{
		$select = $this->_db->select();
		$select->from($this->_tableName, "*");
		$select->where(($this->_primaryKeyName)."=?", $id);
		
		$sql = $select->__toString();
		
		return $this->_db->fetchRow($sql);
	}
	

	public function fetchOne($sql)
	{
		$row = $this->_db->fetchOne($sql);
		return $row;
	}
	
	public function query($sql)
	{
		$this->_db->query($sql);
	}

	public function fetchRow($sql)
	{
		return $this->_db->fetchRow($sql);
	}

	public function fetchAll($sql)
	{
		return $this->_db->fetchAll($sql);
	}

				
	public function delete($id)
	{
		
		$where = $this->_db->quoteInto(($this->_primaryKeyName).'=?',$id);
		return $this->_table->delete($where);
	}
	
	public function getListCount($whereSql = '1=1' )
	{
		$select = $this->_db->select();
		$select->from($this->_tableName,'count(*)')
			   ->where($whereSql);
		$sql = $select->__toString(); 
		return intval($this->_db->fetchOne($sql));		
	}
	
	public function getDb($whereSql = '1=1' )
	{
		return $this->_db;		
	}	
	
}

