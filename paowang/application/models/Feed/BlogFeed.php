<?php
class Feed_BlogFeed{
    
    const FEED_URL = "http://go.paowang.net/blogplanet/rss20.xml";
    const WORDPRESS_FEED_URL = ".blog.paowang.net/?feed=rss2";
    const MT_FEED_URL1 = "http://paowang.net/blog/";
    const MT_FEED_URL2 = "/index.rdf";
    
    private $_rssUrl;
    private $_feedChannel;
    private $_wordpressChannels;
    
   public function __construct($feedUrl = self::FEED_URL){
        $this->_rssUrl = $feedUrl;
        //$this->_feedChannel = new Zend_Feed_Rss($this->_rssUrl);
        $this->_wordpressChannels = array();
        $blogInfoList = Blog_BlogDao::getInstance()->getBlogs();
        foreach($blogInfoList as $blogInfo){
        	$userId = $blogInfo->getAuthorId();
        	$blogUrl = $blogInfo->getBlogUrl();
        	if(strpos($blogUrl, '/blog/') || strpos($blogUrl, 'go/')){
        		continue;
        	}
        	Logging::error("user: + $userId");
            $keepTrying = 0;
            while($keepTrying < 3){
                try{
                    $channel = new Zend_Feed_Rss('http://' . $userId . self::WORDPRESS_FEED_URL);
            		$this->_wordpressChannels[$userId] = $channel;
                    $keepTrying = 10;
                } catch (Exception $e) {
                    Logging::error($e->getMessage(), "\n");
                    $keepTrying++;
                }
            }
        }
   }

   public function getMainPageFeeds($number = 35){
        return $this->getFeeds($number);
   }
   
   public function getAllFeeds(){
   	    return $this->getFeeds(null, true);
   }
   
   private function getFeeds($number = null, $withDescription = false){
        $feed = array();
        $index = 0;
        foreach(array_keys($this->_wordpressChannels) as $user){
            $channel = $this->_wordpressChannels[$user];
            foreach($channel as $item){
                $dateString = $item->pubDate();
                $dateNumber = strtotime($dateString);
                $singleFeed = new Blogentry_BlogentryInfo();
                $singleFeed->setEntryUrl($item->link());
                $singleFeed->setEntryTitle($item->title());
                $singleFeed->setAuthor($item->creator());
                $singleFeed->setEntryDate($dateNumber);
                if($withDescription){
                	$singleFeed->setEntryDescription(Feed_FeedUtils::getShortDescription($item->description()));
                }
                $feed[$dateNumber] = $singleFeed;  
            }
        }
        
/**
        foreach($this->_feedChannel as $item){
            if($number != null && $index >= $number){
                break;
            }
            $index++;
            $dateString = $item->pubDate();
            $dateNumber = strtotime($dateString);
            $fullTitle = $item->title();
            $titleArray = split(":", $fullTitle, 2);
            $user;
            $title;
            if(count($titleArray) == 1){
            	continue;
            }
            else{
            	$user = $titleArray[0];
                $title = $titleArray[1];
            }
            $singleFeed = new Blogentry_BlogentryInfo();
            $singleFeed->setEntryUrl($item->link());
            $singleFeed->setEntryTitle($title);
            $singleFeed->setAuthor($user);
            $singleFeed->setEntryDate($dateNumber);
            if($withDescription){
                $singleFeed->setEntryDescription(Feed_FeedUtils::getShortDescription($item->description()));
            }
            $feed[$dateNumber] = $singleFeed;  
            
        }
*/
        krsort($feed);
        $valueArray = array_values($feed);
        if($number){
            return array_slice($valueArray, 0, $number);
        } 
        return $valueArray;   	
   }
}
