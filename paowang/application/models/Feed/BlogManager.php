<?php
class Feed_BlogManager
{
	public static function fetchFeeds(){
		$feed = new Feed_BlogFeed();
		$array = $feed->getAllFeeds();
		$dao = Blogentry_BlogentryDao::getInstance();
		foreach($array as $blogEntryInfo){
			$dao->update($blogEntryInfo);
		}
	}
	
	public static function fetchBlogTitles(){
		$blogList = Blog_BlogDao::getInstance()->getAll();
		foreach($blogList as $blogInfo){
			$feedUrl = $blogInfo->getFeedUrl();
			$feed = new Zend_Feed_Rss($feedUrl);
			$title = $feed->title();
			if($title != null && $title != ""){
			     $blogInfo->setBlogTitle($title);
			}
			Blog_BlogDao::getInstance()->update($blogInfo);
		}
		return $blogList;
	}
	
	public static function getFeedsToManage(){
		//self::fetchFeeds();
		$array = Blogentry_BlogentryDao::getInstance()->getLatestEntries(100);
		return $array;
	}
	
	public static function updateType($list){
		foreach($list as $map){
			$url = $map['entryUrl'];
			$type = $map['entryType'];
			Blogentry_BlogentryDao::getInstance()->updateType($url, $type);
		}
	}
	
	public static function getFeedsByType(){
		$list0 = Blogentry_BlogentryDao::getInstance()->getLatestEntries(8, 0);
		$list1 = Blogentry_BlogentryDao::getInstance()->getLatestEntries(8, 1);
		$list2 = Blogentry_BlogentryDao::getInstance()->getLatestEntries(8, 2);
		$list3 = Blogentry_BlogentryDao::getInstance()->getLatestEntries(8, 3);
		$list4 = Blogentry_BlogentryDao::getInstance()->getLatestEntries(8, 4);
		$list5 = Blogentry_BlogentryDao::getInstance()->getLatestEntries(8, 5);
		
		// 未分类
		$newList0 = array();
		$yesterday = time() - (3 * 24 * 60 * 60);
		foreach($list0 as $info){
			if($info->getEntryDate() > $yesterday){
				$newList0[] = $info;
			}
		}
		
		$array = array();
		$array[0] = $newList0;
		$array[1] = $list1;
		$array[2] = $list2;
		$array[3] = $list3;
		$array[4] = $list4;
        $array[5] = $list5;
		return $array;
	}
	
	public static function getFeedsForMainPage(){
		$list = Blogentry_BlogentryDao::getInstance()->getLatestEntries(35);
		foreach($list as $info){
			$info->setEntryDescription('');
		}
		$extra = new Blogentry_BlogentryInfo();
        $extra->setAuthor('=>更多博客');
        $extra->setBlogUrl('/blog');
        $list[] = $extra;
		return $list;
	}
}