<?php

class Feed_FeedUtils{
    
    public static function getShortDescription($description){
        return Util::getSubString($description, 120);    
    }
    
    public static function getRssDate($dateNumber){
        if (empty($dateNumber)){
        	return null;
        }
        return date('D, d M Y g:i:s O', $dateNumber);
    }
}