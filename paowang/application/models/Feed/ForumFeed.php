<?php
class Feed_ForumFeed{

	private $_baseUrl;
    private $_pictureUrl;
	private $_forumId;

	public function __construct($forumId){

		$this->_forumId = $forumId;
		$config = Zend_Registry::get('config');
		$this->_baseUrl = $config->general->url;
		$this->_pictureUrl = $config->general->picture->url;
	}
	
	public function send(){
        
		$forumName = Forum_ForumDao::getInstance()->getForumName($this->_forumId);
		$currentYear = Zend_Registry::get('config')->general->currentyear;
		
    	$feedArray = array(
    		'title' => '泡网 - ' . $forumName,
            'link' => $this->_baseUrl . '/feed/forum/' . $this->_forumId,
            'description' => $forumName .  '的RSS',
            'language' => 'zh-cn',
    	    'charset' => 'utf-8',
            'entries' => array(),
    	    'copyright' => "Copyright (c) " . $currentYear . " Paowang.Com All rights reserved."
        );
        
        $dao = Post_PostDao::getInstance();
        $posts = $dao->getRssPosts($this->_forumId);
        
 		foreach($posts as $post) {
            $feedArray['entries'][] = $this->createEntry($post);
        };        		
            
		$feed = Zend_Feed::importArray($feedArray, 'rss');

    	//return $feed->saveXml();
    	
    	$feed->send();
	}
	
	private function createEntry($post){
		$posttextDao = Posttext_PosttextDao::getInstance();
	    $postText = $posttextDao->get($post->getPostId());
	    $content = $postText->getHtml();
	    if($content == null){
	    	$content = "";	
	    }
        $url = $this->_baseUrl . '/post/' . $post->getPostId();
        
        $picInfoArray = Postpic_PostpicDao::getInstance()->getListByPostId($post->getPostId());
        $picContent = "";
        foreach($picInfoArray as $picInfo) {
        	$picUrl = $this->_pictureUrl . $picInfo->getPicPath();
        	$picContent =  $picContent . '<img src="' . $picUrl . '" /><br>';
        	$picContent =  $picContent . $picInfo->getPicDesc() . '<br><br>';
        }
        if($picContent != "") {
            $content = $picContent . "<br>" .$content;        	
        }
	    
		$feedArray = array(
			'title' => $post->getTitle(),
            'link' => $url,
		    'comments' => $url,
            'author' => $post->getUserName(), 
		    'dc:creator' =>  $post->getUserName(),
            'description' => $content,
            'lastUpdate' => $post->getUpdateAt()
		);	
		return $feedArray;
	}
	
}