<?php
class Feed_GeneralFeed{

	const UNLIMITED_NUMBER = null;
	
	private $_rssUrl;
	private $_feedChannel;
	private $_number;
	private $_withDescription;
	private $_blogUrl;

	/**
	 * By default all entries are loaded if the number is not specified
	 *
	 * @param unknown_type $feedUrl
	 * @param unknown_type $number
	 */
	public function __construct($feedUrl, $number = null, $withDescription = false, $blogUrl = null){
		$this->_rssUrl = $feedUrl;
		$this->_number = $number;
		$this->_withDescription = $withDescription;
		$this->_blogUrl = $blogUrl;
		$this->_feedChannel = new Zend_Feed_Rss($this->_rssUrl);
	}

	public function getFeeds(){
		$feed = array();
		$index = 0;
		foreach($this->_feedChannel as $item){
			if($this->_number != null && $index >= $this->_number){
				break;
			}
			Logging::debug("author is " . $item->creator());
			$index++;
			$singleFeed = array(
               "title" => $item->title(),
               "link" => $item->link(),
               "user" => $item->creator()
               );
            if($this->_withDescription){
            	$singleFeed['description'] = Feed_FeedUtils::getShortDescription($item->description());
            }
            if($this->_blogUrl){
            	$singleFeed['blogLink'] = $this->_blogUrl;
            }
            array_push($feed, $singleFeed);

		}
		return $feed;
	}
}