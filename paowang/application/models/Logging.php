<?php

/**
 * A wrapper class to help with easy logging using plog library
 *
 */

class Logging{
	
	public static function debug($message){
		$log = Zend_Registry::get('log');
        $log->debug($message);
	}
	
	public static function error($message){
		$log = Zend_Registry::get('log');
        $log->error($message);
	}
	
	public static function audit($message){
		$log = Zend_Registry::get('audit');
        $log->info($message);
	}
}