<?php

/**
 * 一些相关参数。比如泡网江湖弹琴和江湖论剑的论坛ID。为了避免多余的数据库请求，把这些ID
 * hardcode在这里。把这些参数集中在同一个class里面，以方便将来根据数据库的变动或者其它
 * 变化随时修改。
 *
 */
class PaowangData
{
    const ROLE_ADMINISTRATOR = 30; //系统管理员
    const ROLE_FORUM_ADMIN = 20; //版主
    const ROLE_MEMBER = 10; // 会员制论坛会员
    const ROLE_NORMAL_USER = 1; //普通会员
    const ROLE_ANONYMOUS = 0; //未登陆用户
    
    const FORUM_ID_ALL = 0; //用ID“0”代表所有论坛

    const FORUM_ID_JIAN = 1; //江湖论剑ID  old 2
    const FORUM_ID_QIN = 2; //江湖谈琴ID old 1
    const FORUM_ID_INN = 3;//old 5
    const FORUM_ID_WEAPON = 4; //old 6
    const FORUM_ID_MUSIC = 5; //old 3
    const FORUM_ID_MONEY = 6 ;//old 10;
    const FORUM_ID_IT = 7;//old 11
    const FORUM_ID_PARENTING = 8;//old 12
    const FORUM_ID_PHOTO = 9; //江湖绝色ID old 4
    const FORUM_ID_CHUANGKETIE = 10; //old 12
    const FORUM_ID_HELP = 11;

    const FORUM_ID_DEFAULT = PaowangData::FORUM_ID_PHOTO;
    
    const FORUM_NAME_QIN = "江湖谈琴";
    const FORUM_NAME_JIAN = "江湖论剑";
    const FORUM_NAME_MUSIC = "寻音觅影";
    const FORUM_NAME_PHOTO = "江湖色";
    const FORUM_NAME_INN = "江湖客栈";
    const FORUM_NAME_WEAPON = "江湖兵器";
    const FORUM_NAME_MONEY = "江湖理财";
    const FORUM_NAME_IT = "IT江湖";
    const FORUM_NAME_PARENTING = "小小江湖";
    const FORUM_NAME_CHUANGKETIE = "创可贴";
    const FORUM_NAME_HELP = "帮助中心";
    
    const FORUM_URL_QIN = 'qin'; //江湖谈琴ID
    const FORUM_URL_JIAN = 'club'; //江湖论剑ID
    const FORUM_URL_MUSIC = 'music';
    const FORUM_URL_PHOTO = 'photo'; //江湖绝色ID
    const FORUM_URL_INN = 'inn';
    const FORUM_URL_WEAPON = 'weapon';
    const FORUM_URL_MONEY = 'money';
    const FORUM_URL_IT = 'it';
    const FORUM_URL_PARENTING = 'parenting';
    const FORUM_URL_CHUANGKETIE = 'tie';
    const FORUM_URL_HELP = 'help';
    
    // 论贴显示顺序
    static $orderedForumsById = array(
        self::FORUM_ID_JIAN => self::FORUM_NAME_JIAN,
        self::FORUM_ID_QIN => self::FORUM_NAME_QIN,
        self::FORUM_ID_INN => self::FORUM_NAME_INN,
        self::FORUM_ID_WEAPON => self::FORUM_NAME_WEAPON,
        self::FORUM_ID_MUSIC => self::FORUM_NAME_MUSIC,
        self::FORUM_ID_MONEY => self::FORUM_NAME_MONEY,
        self::FORUM_ID_IT => self::FORUM_NAME_IT,
        self::FORUM_ID_PARENTING => self::FORUM_NAME_PARENTING,
        self::FORUM_ID_PHOTO => self::FORUM_NAME_PHOTO,
        self::FORUM_ID_CHUANGKETIE => self::FORUM_NAME_CHUANGKETIE,
        self::FORUM_ID_HELP => self::FORUM_NAME_HELP        
    );
    
    static $orderedDisplayForumsById = array(
        self::FORUM_ID_JIAN => self::FORUM_NAME_JIAN,
        self::FORUM_ID_QIN => self::FORUM_NAME_QIN,
        self::FORUM_ID_INN => self::FORUM_NAME_INN,
        self::FORUM_ID_WEAPON => self::FORUM_NAME_WEAPON,
        self::FORUM_ID_MUSIC => self::FORUM_NAME_MUSIC,
        self::FORUM_ID_MONEY => self::FORUM_NAME_MONEY,
        self::FORUM_ID_IT => self::FORUM_NAME_IT,
        self::FORUM_ID_PARENTING => self::FORUM_NAME_PARENTING,
        self::FORUM_ID_PHOTO => self::FORUM_NAME_PHOTO,
        self::FORUM_ID_CHUANGKETIE => self::FORUM_NAME_CHUANGKETIE      
    );
    
    static $forumIdRequiredLogin = array(
        self::FORUM_ID_JIAN => self::FORUM_NAME_JIAN,
        self::FORUM_ID_INN => self::FORUM_NAME_INN
    );
    
    //有江湖色权限的非江湖色会员
    static $photoExcludedMembers = array(
        '1656' => 'redsox',
        '2967' => 'dodo',
        '50265' => 'hulianxintong',
    );
    
    //各个论坛上传文件大小
    static $orderedForumsUploadPicSizeById = array(
        self::FORUM_ID_JIAN => 200000,
        self::FORUM_ID_QIN => 200000,
        self::FORUM_ID_INN => 200000,
        self::FORUM_ID_WEAPON => 200000,
        self::FORUM_ID_MUSIC => 200000,
        self::FORUM_ID_MONEY => 200000,
        self::FORUM_ID_IT => 200000,
        self::FORUM_ID_PARENTING => 200000,
        self::FORUM_ID_PHOTO => 512000,
        self::FORUM_ID_CHUANGKETIE => 512000      
    );    
    
    // url名称对应id
    static $forumsByUrl = array(
        self::FORUM_URL_JIAN => self::FORUM_ID_JIAN,
        self::FORUM_URL_QIN => self::FORUM_ID_QIN,
        self::FORUM_URL_INN => self::FORUM_ID_INN,
        self::FORUM_URL_WEAPON => self::FORUM_ID_WEAPON,
        self::FORUM_URL_MONEY => self::FORUM_ID_MONEY,
        self::FORUM_URL_MUSIC => self::FORUM_ID_MUSIC,
        self::FORUM_URL_IT => self::FORUM_ID_IT,
        self::FORUM_URL_PARENTING => self::FORUM_ID_PARENTING,
        self::FORUM_URL_PHOTO => self::FORUM_ID_PHOTO,
        self::FORUM_URL_CHUANGKETIE => self::FORUM_ID_CHUANGKETIE,
        self::FORUM_URL_HELP => self::FORUM_ID_HELP
    );
    
    // id对应url
    static $forumUrlById = array(
        self::FORUM_ID_JIAN => self::FORUM_URL_JIAN,
        self::FORUM_ID_QIN => self::FORUM_URL_QIN,
        self::FORUM_ID_INN => self::FORUM_URL_INN,
        self::FORUM_ID_WEAPON => self::FORUM_URL_WEAPON,
        self::FORUM_ID_MUSIC => self::FORUM_URL_MUSIC,
        self::FORUM_ID_MONEY => self::FORUM_URL_MONEY,
        self::FORUM_ID_IT => self::FORUM_URL_IT,
        self::FORUM_ID_PARENTING => self::FORUM_URL_PARENTING,
        self::FORUM_ID_PHOTO => self::FORUM_URL_PHOTO,
        self::FORUM_ID_CHUANGKETIE => self::FORUM_URL_CHUANGKETIE,
        self::FORUM_ID_HELP => self::FORUM_URL_HELP
    );
    
    static $forumsBanzhu = array(
        self::FORUM_ID_JIAN => '',
        self::FORUM_ID_QIN => '',
        self::FORUM_ID_INN => '',
        self::FORUM_ID_WEAPON => '',
        self::FORUM_ID_MONEY => '阿塔',
        self::FORUM_ID_MUSIC => '',
        self::FORUM_ID_IT => '猛小蛇',
        self::FORUM_ID_PARENTING => 'AMIGO 龙二 阿北',
        self::FORUM_ID_PHOTO => 'Hass Bozeman 速度',
        self::FORUM_ID_CHUANGKETIE => 'RedRocks　莽',
        self::FORUM_ID_HELP => '管理员'
    );
    
    static $forumsUploadPics = array(
        self::FORUM_ID_JIAN => 0,
        self::FORUM_ID_QIN => 0,
        self::FORUM_ID_INN => 0,
        self::FORUM_ID_WEAPON => 3,
        self::FORUM_ID_MONEY => 0,
        self::FORUM_ID_MUSIC => 0,
        self::FORUM_ID_IT => 0,
        self::FORUM_ID_PARENTING => 3,
        self::FORUM_ID_PHOTO => 9,
        self::FORUM_ID_CHUANGKETIE => 6,
        self::FORUM_ID_HELP => 0,
    );        
    
    // 热门帖的天数
    static $hotTopicDaysById = array(
        self::FORUM_ID_JIAN => 1,
        self::FORUM_ID_QIN => 7,
        self::FORUM_ID_INN => 2,
        self::FORUM_ID_WEAPON => 3,
        self::FORUM_ID_MUSIC => 7,
        self::FORUM_ID_MONEY => 7,
        self::FORUM_ID_IT => 7,
        self::FORUM_ID_PARENTING => 7,
        self::FORUM_ID_PHOTO => 3,
        self::FORUM_ID_CHUANGKETIE => 3,
        self::FORUM_ID_HELP => 7        
    );    
    
    const HOT_TOPIC_ALGORITHM = "click*1+child_posts*5+(vote_yes+vote_no)*10";
    
    static $photoForums = array(
        self::FORUM_ID_PHOTO,
        self::FORUM_ID_CHUANGKETIE
    );
    
    static $nonDisplayForums = array(
        self::FORUM_ID_HELP
    );
    
    const NAVIGATION_BAR_MAINPAGE = 1;
    const NAVIGATION_BAR_FORUM = 2;
    const NAVIGATION_BAR_NEWS = 3;
    const NAVIGATION_BAR_BLOG = 4;
    const NAVIGATION_BAR_CHAT = 5;
    
    const FORUM_TAB_ALL = "all";
    const FORUM_TAB_HOT = "hot";
    const FORUM_TAB_NEW = "new";
    const FORUM_TAB_NEWREPLY = "newreply";
    const FORUM_TAB_WAKEUP = "wakeup";
    const FORUM_TAB_MYPOST = "mypost";
    const FORUM_TAB_MYVOTE = "myvote";

    
    static $forumTabArray = array(
        self::FORUM_TAB_ALL => 0,
        self::FORUM_TAB_NEW => 1,
        self::FORUM_TAB_NEWREPLY => 2,
        self::FORUM_TAB_WAKEUP => 3,
        self::FORUM_TAB_MYPOST => 4,
        self::FORUM_TAB_MYVOTE => 5
    );
        
    
    const FORUM_TAB_MY_MAINPOST = "mymainpost";
    const FORUM_TAB_MY_REPLY = "myreply";
    const FORUM_TAB_MY_VOTE_YES = "myvoteyes";
    const FORUM_TAB_MY_VOTE_NO = "myvoteno";
    
    const FORUM_POSTS_PER_PAGE = 50;
    const FORUM_IMAGE_PER_PAGE = 25;
    const FORUM_IMAGE_PER_PAGE_CLASSIC = 24;    
    const FORUM_MYPOSTS_PER_PAGE = 20;
    const TAG_POSTS_PER_PAGE = 20;
    const USER_PHOTO_PER_PAGE = 60;
    
    public static function isPhotoForum($forumId){
    	return in_array($forumId, self::$photoForums);
    }
    
    const POST_STATUS_NORMAL = 0;
    const POST_STATUS_TOP = 1;
    const POST_STATUS_HOT = 2;
    
    // 论坛标题显示的长度
    const POST_TITLE_DISPLAY_LENGTH = 40;
    
    // 首页帖子显示数
    static $generalForumHotPosts = array(
        PaowangData::FORUM_ID_JIAN => 3,
        PaowangData::FORUM_ID_QIN => 1,
        PaowangData::FORUM_ID_INN => 3,
        PaowangData::FORUM_ID_WEAPON => 3,
        PaowangData::FORUM_ID_MUSIC => 1,
        PaowangData::FORUM_ID_MONEY => 1,
        PaowangData::FORUM_ID_IT => 1,
        PaowangData::FORUM_ID_PARENTING => 1
    );
    
    static $photoForumHotPosts = array(
        PaowangData::FORUM_ID_PHOTO => 5,
        PaowangData::FORUM_ID_CHUANGKETIE => 5
    );
    
    const BLOG_TYPE_LIFE = 1;
    const BLOG_TYPE_CULTURE = 2;
    const BLOG_TYPE_NEWS = 3;
    const BLOG_TYPE_IT = 4;
}