<?php

/**
 * RBM access for system administrators and forum admins
 */
class Rbm_RbmService implements Rbm_Interface{
	
	const ADMIN_MANAGE_FORUM = 101; //管理论坛,包括置顶，删贴
	const ADMIN_ASSIGN_BANZHU = 102; //任命版主
	const ADMIN_ASSIGN_MEMBERSHIP = 103; //升级成会员
	
	const USER_ADD_POST = 201;
	const USER_EDIT_POST = 202;
	const USER_DELETE_POST = 203;
	const USER_UPLOAD_PICTURE = 204; 	
	const USER_ADD_TAG = 205; //加标签
	const USER_RATE = 206; //赞成反对
	    
	/**
     * Singleton instance
     *
     * @var Rbm_AdminRbm
     */
    protected static $_instance = null;

    /**
     * Singleton pattern implementation makes "new" unavailable
     *
     * @return void
     */
    private function __construct()
    {}

    /**
     * Singleton pattern implementation makes "clone" unavailable
     *
     * @return void
     */
    private function __clone()
    {}

    /**
     * Returns an instance of Rbm_AdminRbm
     *
     * Singleton pattern implementation
     *
     * @return Rbm_AdminRbm Provides a fluent interface
     */
    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
	
    public function isUserLogin(){
    	if(array_key_exists("paowanguser",$_COOKIE)){
    		return true;
    	}
    	return false;
    }
    
    public function getCurrentLoginUserId(){
    	return Util::getUserId();
    }
    
    public function isOpenUser($userId){
    	return User_UserDao::getInstance()->isOpenUser($userId);
    }
    
    public function isBanzhu($userId, $forumId = PaowangData::FORUM_ID_ALL){
    	if($forumId == PaowangData::FORUM_ID_ALL){
    		return $this->isOneBanZhu($userId);
    	}
        $roleId = Forumuser_ForumuserDao::getInstance()->getRoleId($userId, $forumId);
        if($roleId != null && (int)$roleId >= PaowangData::ROLE_FORUM_ADMIN){
            return true;
        }
        return false;
    }
    
    private function isOneBanZhu($userId){
    	foreach(PaowangData::$orderedForumsById as $forumId => $forumName){
	    	$roleId = Forumuser_ForumuserDao::getInstance()->getRoleId($userId, $forumId);
	        if($roleId != null && (int)$roleId >= PaowangData::ROLE_FORUM_ADMIN){
	            return true;
	        }	
    	}
    	return false;
    }
    
    public function isMember($userId, $forumId){
    	$roleId = Forumuser_ForumuserDao::getInstance()->getRoleId($userId, $forumId);
        if($roleId != null && (int)$roleId >= PaowangData::ROLE_MEMBER){
            return true;
        }
        return false;
    }
	
	public function hasPermission($userId, $forumId, $functionId, $otherId = null){
		switch ($functionId){
			// 管理员相关的RBM
			case self::ADMIN_MANAGE_FORUM:
				return $this->canManageForum($userId, $forumId);
			case self::ADMIN_ASSIGN_BANZHU:
				return $this->canAssignBanzhu($userId, $forumId);
			case self::ADMIN_ASSIGN_MEMBERSHIP:
				return $this->canAssignMembership($userId, $forumId);
				
			// 普通用户相关的RBM
			case self::USER_ADD_POST:
				return $this->canAddPost($userId, $forumId);
			case self::USER_EDIT_POST:
				return $this->canEditPost($userId, $forumId, $otherId);
			// 目前只有可以管理论坛的用户才可以删贴子
			case self::USER_DELETE_POST:
				return $this->canManageForum($userId, $forumId);
			case self::USER_UPLOAD_PICTURE:
				return $this->canUploadPicture($userId, $forumId);
			case self::USER_ADD_TAG:
			case self::USER_RATE:
				return $this->isRegisteredUser($userId);
			default:
				return false;
		}
	}
	
	/**
	 * 判断用户是否登陆，是依靠客户端的cookie看用户有没有userId在cookie里，还是在服务器端
	 * 做双重确认？在服务器端确认会增加服务器的负担。
	 */
	private function isRegisteredUser($userId){
		return ($userId != null);	
	}
	
	/**
	 * 江湖色只有会员才可以上传图片，其它论坛目前暂定都可以上传图片
	 */
	private function canUploadPicture($userId, $forumId){
		if($forumId == PaowangData::FORUM_ID_PHOTO){
			$roleId = Forumuser_ForumuserDao::getInstance()->getRoleId($userId, PaowangData::FORUM_ID_PHOTO);
			if($roleId != null && (int)$roleId >= PaowangData::ROLE_MEMBER){
				return true;
			}
			return false;
		}
		return $this->isRegisteredUser($userId);
	}
	
	/**
	 * 论坛版主可以编辑贴子，自己发的贴子自己可以编辑
	 */
	public static function canEditPost($userId, $forumId, $postId){
	    if($userId == null || $forumId == null){
	        return false;   
	    }
		$userDao = User_UserDao::getInstance();
		if($userDao->isAdministrator($userId)){
			return true;	
		}
		
		$roleId = Forumuser_ForumuserDao::getInstance()->getRoleId($userId, $forumId);
		if($roleId != null && (int)$roleId >= PaowangData::ROLE_FORUM_ADMIN){
			return true;
		}
		
		$postDao = Post_PostDao::getInstance();
		$postInfo = $postDao->get($postId);
		$postUserId = $postInfo->getUserId();
		$postTime = $postInfo->getCreateAt();
		$now = time();
		if($postUserId != $userId){
            return false;
        }
		if(($now - $postTime) > 3600){
			return false;
		}
		
		return true;		
	}

	/**
	 * 只有剑会员才可以在剑论坛发贴；其它论坛注册会员都可以发贴
	 * 在pw_forum_user数据库里，所有的琴剑会员都归类到剑的论坛ID
	 */
	private function canAddPost($userId, $forumId){
		$forumDao = Forum_ForumDao::getInstance();
		if($forumId == PaowangData::FORUM_ID_JIAN){
			$roleId = Forumuser_ForumuserDao::getInstance()->getRoleId($userId, PaowangData::FORUM_ID_JIAN);
			if($roleId != null && $roleId >= PaowangData::ROLE_MEMBER){
				return true;
			}
			return false;
		}
		return true;
	}
	
	/**
	 * 剑的用户只有管理员和剑的版主可以升级
	 * 其它如江湖色会员则只有管理员和江湖色版主可以升级。
	 */
	private function canAssignMembership($userId, $forumId){
		$userDao = User_UserDao::getInstance();
		if($userDao->isAdministrator($userId)){
			return true;	
		}
//		$forumDao = Forum_ForumDao::getInstance();
//		if($forumDao->isQinOrJian($forumId)){
//			return $this->isQinJianBanzhu($userId);
//		}
		
		$roleId = Forumuser_ForumuserDao::getInstance()->getRoleId($userId, $forumId);
		if($roleId != null && (int)$roleId >= PaowangData::ROLE_FORUM_ADMIN){
			return true;
		}
		return false;
	}
	
	private function isQinJianBanzhu($userId){
		$roleIdQin = Forumuser_ForumuserDao::getInstance()->getRoleId($userId, PaowangData::FORUM_ID_QIN);
		if($roleIdQin != null && (int)$roleIdQin >= PaowangData::ROLE_FORUM_ADMIN){
			return true;
		}
		$roleIdJian = Forumuser_ForumuserDao::getInstance()->getRoleId($userId, PaowangData::FORUM_ID_JIAN);
		if($roleIdJian != null && (int)$roleIdJian >= PaowangData::ROLE_FORUM_ADMIN){
			return true;
		}
		return false;
	}
	
	
	/**
	 * 是否系统管理员
	 */
	public function isAdministrator($userId){
		$userDao = User_UserDao::getInstance();
		if($userDao->isAdministrator($userId)){
			return true;	
		}
		return false;
	}
		
	/**
	 * Only administrator can assign banzhu
	 */
	private function canAssignBanzhu($userId, $forumId){
		$userDao = User_UserDao::getInstance();
		if($userDao->isAdministrator($userId)){
			return true;	
		}
		return false;
	}
	
	private function canManageForum($userId, $forumId){
		
		$userDao = User_UserDao::getInstance();
		if($userDao->isAdministrator($userId)){
			return true;	
		}
		
		$roleId = Forumuser_ForumuserDao::getInstance()->getRoleId($userId, $forumId);

		if($roleId != null && (int)$roleId >= PaowangData::ROLE_FORUM_ADMIN){
			return true;
		}
		return false;
	}
	
}