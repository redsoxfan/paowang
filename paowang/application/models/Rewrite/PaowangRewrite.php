<?php

class Rewrite_PaowangRewrite extends Zend_Controller_Router_Rewrite {
	
    public function route(Zend_Controller_Request_Abstract $request) {
        if (!$request instanceof Zend_Controller_Request_Http) {
            throw new Zend_Controller_Router_Exception('Zend_Controller_Router_Rewrite requires a Zend_Controller_Request_Http-based request object');
        }

        if ($this->_useDefaultRoutes) {
            $this->addDefaultRoutes();
        }

        $pathInfo = $request->getPathInfo();

        /** Find the matching route */
        foreach (array_reverse($this->_routes) as $name => $route) {
        	$thePathInfo = $pathInfo;
        	if($route instanceof Rewrite_RouteRegexFullUrl) {
        		$thePathInfo = $request->getRequestUri();
        	}
            if ($params = $route->match($thePathInfo)) {
                $this->_setRequestParams($request, $params);
                $this->_currentRoute = $name;
                break;
            }
        }

        return $request;

    } 
}