<?php

class Rewrite_RewriteUtils{
	
    public static function addRewriteRules($router){
        $router->addRoute('cgiPost', self::getCgiPostRoute());
        $router->addRoute('feed', self::getFeedRoute());
        $router->addRoute('forumByType', self::getForumByTypeRoute());
        $router->addRoute('forumMyPost', self::getForumMyPostRoute());
        $router->addRoute('forumMyVote', self::getForumMyVoteRoute());
        $router->addRoute('forumPhoto', self::getForumPhotoRoute());
        $router->addRoute('forum', self::getForumRoute());
        $router->addRoute('post', self::getPostRoute());            
    }
    
    public static function getFeedRoute(){
        return new Zend_Controller_Router_Route(
            'feed/forum/:id',
            array(
                'controller' => 'feed',
                'action' => 'forum',
            )
        );
    }
    
    public static function getPostRoute(){
        return new Zend_Controller_Router_Route_Regex(
            'post/(\d+)',
            array(
                'controller' => 'post',
                'action' => 'view',
            ),
            array(
                'postId' => 1
            )
        );
    }
    
    public static function getCgiPostRoute(){
        return new Rewrite_RouteRegexFullUrl(
            "cgi-bin/forum/viewpost\.cgi\?which=(\w+)&id=(\d+)",
            array(
                'controller' => 'post',
                'action' => 'view',
            ),
            array(
                'oldForumUrlName' => 1,
                'oldForumPostId' => 2
            )
        );
    }
    
    public static function getNewForumId($oldForumName){
        if($oldForumName == 'paowang' || $oldForumName == 'paowang1'){
            return PaowangData::FORUM_ID_JIAN;
        }
        if(!array_key_exists($oldForumName, PaowangData::$forumsByUrl)){
            return 0;
        }
        return PaowangData::$forumsByUrl[$oldForumName];
    }
    
    public static function getNewPostId($oldForumName, $oldPostId){
        $newForumId = self::getNewForumId($oldForumName);
        return $newForumId * 1000000 + $oldPostId;
    }
    
    public static function getForumRoute(){
        return new Zend_Controller_Router_Route_Regex(
            '(club|qin|inn|weapon|music|money|it|parenting|photo|tie|help)',
            array(
                'controller' => 'forum',
                'action' => 'index',
            ),
            array(
                'forumUrlName' => 1
            )
        );
    }
    
    public static function getForumMyPostRoute(){
        return new Zend_Controller_Router_Route(
            'forum/:forumUrlName/mypost',
            array(
                'controller' => 'forum',
                'action' => 'mypost',
            )
        );
    }
    
    public static function getForumMyVoteRoute(){
        return new Zend_Controller_Router_Route(
            'forum/:forumUrlName/myvote',
            array(
                'controller' => 'forum',
                'action' => 'myvote',
            )
        );
    }
    
    public static function getForumPhotoRoute(){
        return new Zend_Controller_Router_Route_Regex(
            'forum/(\w+)/(list|pic)',
            array(
                'controller' => 'forum',
                'action' => 'index',
            ),
            array(
                'forumUrlName' => 1,
                'listType' => 2,            
            )
        );
    }
    
    public static function getForumByTypeRoute(){
        return new Zend_Controller_Router_Route(
            'forum/:forumUrlName/:type',
            array(
                'controller' => 'forum',
                'action' => 'index',
            )
        );
    }
}