<?php
/**
 * 用Regular Expression做URL Rewrite。ZendFramework自己的Route_Regex把URL里？后面的
 * 的query string全都去掉了。这个class支持包括query string的全部url的rewrite。
 * 
 * 这样可以把旧泡网的url：http://paowang.com/cgi-bin/viewpost.cgi?which=paowang&id=200
 * 转换成 http://paowang.com/post/1000200
 *
 */
class Rewrite_RouteRegexFullUrl extends Zend_Controller_Router_Route_Regex {
    
	public function __construct($route, $defaults = array(), $map = array(), $reverse = null) {
        parent::__construct($route, $defaults, $map, $reverse);
    }
}