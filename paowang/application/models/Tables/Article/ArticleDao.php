<?php
class Article_ArticleDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }		
	
    public function get($id){
		$dbModel = new DbModel("pw_article","article_id");
		$row = $dbModel->getRow($id);
		$articleInfo = new Article_ArticleInfo($row);
    	return $articleInfo;
    }

    public function load(&$articleInfo){
		$dbModel = new DbModel("pw_article","article_id");
		$row = $dbModel->getRow($articleInfo->getArticleId());
		$articleInfo = new Article_ArticleInfo($row);
    }
    
    public function add(&$articleInfo){
        $row['forum_id'] = $articleInfo->getForumId();
        $row['user_id'] = $articleInfo->getUserId();
        $row['user_name'] = $articleInfo->getUserName();
        $row['status'] = $articleInfo->getStatus();
        $row['create_at'] = $articleInfo->getCreateAt();
        $row['user_ip'] = $articleInfo->getUserIp();
        $row['update_at'] = $articleInfo->getUpdateAt();
        $row['update_ip'] = $articleInfo->getUpdateIp();
        $row['title'] = $articleInfo->getTitle();
        $row['text'] = $articleInfo->getText();
        $row['user_post_type_id'] = $articleInfo->getUserPostTypeId();
	    $table = new Article_ArticleTable();
		$id = $table->insert($row);
		$articleInfo->setArticleId($id);
    }

    public function modify(&$articleInfo){
        $row['forum_id'] = $articleInfo->getForumId();
        $row['user_id'] = $articleInfo->getUserId();
        $row['user_name'] = $articleInfo->getUserName();
        $row['status'] = $articleInfo->getStatus();
        $row['create_at'] = $articleInfo->getCreateAt();
        $row['user_ip'] = $articleInfo->getUserIp();
        $row['update_at'] = $articleInfo->getUpdateAt();
        $row['update_ip'] = $articleInfo->getUpdateIp();
        $row['title'] = $articleInfo->getTitle();
        $row['text'] = $articleInfo->getText();
        $row['user_post_type_id'] = $articleInfo->getUserPostTypeId();

	    $table = new Article_ArticleTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("article_id=?", $articleInfo->getArticleId());
		$table->update($row, $where);
    }

    public function del($articleInfo){
		$this->delById($articleInfo->getArticleId());
    }

    public function delById($id){
		$table = new Article_ArticleTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('article_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_article","article_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $articleInfoArray = array();
        foreach($list as $row){
        	$articleInfo = new Article_ArticleInfo($row);
        	array_push($articleInfoArray, $articleInfo);
        }
        return $articleInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("pw_article","article_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     
    
}
