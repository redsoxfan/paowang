<?php
class Article_ArticleInfo
{
    public $articleId;//主键ID
    public $forumId;//论坛Id
    public $userId;//用户id
    public $userName;//用户名
    public $status;//状态
    public $createAt;//创建时间
    public $userIp;//用户ip
    public $updateAt;//修改时间
    public $updateIp;//修改IP
    public $title;//标题
    public $text;//内容
    public $userPostTypeId;//文章类别id

	function __construct($articleArray = null){
        if($articleArray != null){
		   if(empty($articleArray["article_id"])){
		      $this->articleId=null;
	  	   }else{
		      $this->articleId=$articleArray["article_id"];
		   }
		   $this->forumId = $articleArray["forum_id"];
		   $this->userId = $articleArray["user_id"];
		   $this->userName = $articleArray["user_name"];
		   $this->status = $articleArray["status"];
		   $this->createAt = $articleArray["create_at"];
		   $this->userIp = $articleArray["user_ip"];
		   $this->updateAt = $articleArray["update_at"];
		   $this->updateIp = $articleArray["update_ip"];
		   $this->title = $articleArray["title"];
		   $this->text = $articleArray["text"];
		   $this->userPostTypeId = $articleArray["user_post_type_id"];
	   }
	}

	function setArticleId($articleId){
		$this->articleId=$articleId;
	}

	function getArticleId(){
		return $this->articleId;
	}

	function setForumId($forumId){
		$this->forumId=$forumId;
	}

	function getForumId(){
		return $this->forumId;
	}

	function setUserId($userId){
		$this->userId=$userId;
	}

	function getUserId(){
		return $this->userId;
	}

	function setUserName($userName){
		$this->userName=$userName;
	}

	function getUserName(){
		return $this->userName;
	}

	function setStatus($status){
		$this->status=$status;
	}

	function getStatus(){
		return $this->status;
	}

	function setCreateAt($createAt){
		$this->createAt=$createAt;
	}

	function getCreateAt(){
		return $this->createAt;
	}

	function setUserIp($userIp){
		$this->userIp=$userIp;
	}

	function getUserIp(){
		return $this->userIp;
	}

	function setUpdateAt($updateAt){
		$this->updateAt=$updateAt;
	}

	function getUpdateAt(){
		return $this->updateAt;
	}

	function setUpdateIp($updateIp){
		$this->updateIp=$updateIp;
	}

	function getUpdateIp(){
		return $this->updateIp;
	}

	function setTitle($title){
		$this->title=$title;
	}

	function getTitle(){
		return $this->title;
	}

	function setText($text){
		$this->text=$text;
	}

	function getText(){
		return $this->text;
	}

	function setUserPostTypeId($userPostTypeId){
		$this->userPostTypeId=$userPostTypeId;
	}

	function getUserPostTypeId(){
		return $this->userPostTypeId;
	}

}
?>