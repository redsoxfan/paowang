<?php
/**
 * Article_ArticleTable
 */

class Article_ArticleTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_article";
    // 默认主键为’id’
    protected $_primary = "article_id";
}
