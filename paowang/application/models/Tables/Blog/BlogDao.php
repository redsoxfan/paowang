<?php
class Blog_BlogDao
{
	/**
	 * Singleton implementation
	 */
	protected static $_instance = null;

	private function __construct()
	{}

	private function __clone()
	{}

	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Add to database
	 *
	 * @param Blog_BlogInfo $blogInfo
	 */
	public function add(&$blogInfo){
		$row['blog_id'] = $blogInfo->getBlogId();
		$row['author_id'] = $blogInfo->getAuthorId();
		$row['author_name'] = $blogInfo->getAuthorName();
		$row['blog_title'] = $blogInfo->getBlogTitle();
		$row['blog_url'] = $blogInfo->getBlogUrl();
		$row['status'] = $blogInfo->getStatus();
			
		$table = new Blog_BlogTable();
		$id = $table->insert($row);
		$blogInfo->setBlogId($id);
	}

	/**
	 * Modify. Do not modify entry_type here.
	 *
	 * @param Blog_BlogInfo $blogInfo
	 */
	public function modify(&$blogInfo){
        $row['author_id'] = $blogInfo->getAuthorId();
        $row['author_name'] = $blogInfo->getAuthorName();
        $row['blog_title'] = $blogInfo->getBlogTitle();
        $row['blog_url'] = $blogInfo->getBlogUrl();
        $row['status'] = $blogInfo->getStatus();
        			
		$table = new Blog_BlogTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto("blog_id=?", $blogInfo->getBlogId());
		$table->update($row, $where);
	}

	public function getAll(){
		$dbModel = new DbModel("","");
		$sql = "SELECT * FROM pw_blog order by author_name asc";
		$list =  $dbModel->fetchAll($sql);
		$array = array();
		foreach($list as $row){
			$blogInfo = new Blog_BlogInfo($row);
			$array[] = $blogInfo;
		}
		return $array;
	}
	
	public function getBlogs($status = 1){
	    $dbModel = new DbModel("","");
		$sql = "SELECT * FROM pw_blog where status = '$status' order by author_name asc";
		$list =  $dbModel->fetchAll($sql);
		$array = array();
		foreach($list as $row){
			$blogInfo = new Blog_BlogInfo($row);
			$array[] = $blogInfo;
		}
		return $array;
	}

	/**
	 * Add or modify
	 *
	 * @param Blog_BlogInfo $blogInfo
	 */
	public function update($blogInfo){
		$blogUrl = $blogInfo->getBlogUrl();
		$dbModel = new DbModel("","");
		$sql = "SELECT blog_id FROM pw_blog where blog_url = '$blogUrl'";
		$id = intval($dbModel->fetchOne($sql));
		if($id > 0){
			$blogInfo->setBlogId($id);
			$this->modify($blogInfo);
		}
		else{
			$this->add($blogInfo);
		}
	}

}