<?php
class Blog_BlogInfo
{
	public $blogId;
	public $authorId;
	public $authorName;
	public $blogTitle;
	public $blogUrl;
	public $status;

	public function __construct($blogArray = null){
		if($blogArray != null){
			if(empty($blogArray["blog_id"])){
				$this->blogId=null;
			}else{
				$this->blogId=$blogArray["blog_id"];
			}
			$this->authorId = $blogArray["author_id"];
			$this->authorName = $blogArray["author_name"];
			$this->blogTitle = $blogArray["blog_title"];
			$this->blogUrl = $blogArray["blog_url"];
			$this->status = $blogArray["status"];

		}
	}

	public function getFeedUrl(){
		if(strpos($this->blogUrl, '/blog/') || strpos($this->blogUrl, 'go/')){
			return Feed_BlogFeed::MT_FEED_URL1 . $this->authorId . Feed_BlogFeed::MT_FEED_URL2;
		}
		return 'http://' . $this->authorId . Feed_BlogFeed::WORDPRESS_FEED_URL;
	}

	public function getBlogId(){
		return $this->blogId;
	}

	public function setBlogId($blogId){
		$this->blogId = $blogId;
	}

	public function getAuthorId(){
		return $this->authorId;
	}

	public function setAuthorId($authorId){
		$this->authorId = $authorId;
	}

	public function getAuthorName(){
		return $this->authorName;
	}

	public function setAuthorName($authorName){
		$this->authorName = $authorName;
	}

	public function getBlogTitle(){
		return $this->blogTitle;
	}

	public function setBlogTitle($blogTitle){
		$this->blogTitle = $blogTitle;
	}

	public function getBlogUrl(){
		return $this->blogUrl;
	}

	public function setBlogUrl($blogUrl){
		$this->blogUrl = $blogUrl;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}

}
