<?php

class Blog_BlogTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_blog";
    // 默认主键为’id’
    protected $_primary = "blog_id";
}
