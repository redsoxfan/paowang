<?php
class Blogentry_BlogentryDao
{
	/**
	 * Singleton implementation
	 */
	protected static $_instance = null;

	private function __construct()
	{}

	private function __clone()
	{}

	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Add to database
	 *
	 * @param Blogentry_BlogentryInfo $blogentryInfo
	 */
	public function add(&$blogentryInfo){
		$row['entry_url'] = $blogentryInfo->getEntryUrl();
		$row['author'] = $blogentryInfo->getAuthor();
		$row['blog_url'] = $blogentryInfo->getBlogUrl();
		$row['entry_title'] = $blogentryInfo->getEntryTitle();
		$row['entry_description'] = $blogentryInfo->getEntryDescription();
		$row['entry_date'] = $blogentryInfo->getEntryDate();
		$row['entry_type'] = $blogentryInfo->getEntryType();
			
		$table = new Blogentry_BlogentryTable();
		$id = $table->insert($row);
		$blogentryInfo->setEntryId($id);
	}

	/**
	 * Modify. Do not modify entry_type here.
	 *
	 * @param Blogentry_BlogentryInfo $blogentryInfo
	 */
	public function modify(&$blogentryInfo){
		$row['entry_url'] = $blogentryInfo->getEntryUrl();
		$row['author'] = $blogentryInfo->getAuthor();
		$row['blog_url'] = $blogentryInfo->getBlogUrl();
		$row['entry_title'] = $blogentryInfo->getEntryTitle();
		$row['entry_description'] = $blogentryInfo->getEntryDescription();
		$row['entry_date'] = $blogentryInfo->getEntryDate();
		//		$row['entry_type'] = $blogentryInfo->getEntryType();
			
		$table = new Blogentry_BlogentryTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto("entry_id=?", $blogentryInfo->getEntryId());
		$table->update($row, $where);
	}

	public function getLatestEntries($number = 100, $type = -1){
		$dbModel = new DbModel("","");
		$where = '';
		if($type != -1){
			$where = "WHERE entry_type = " . (string)$type;
		}
		$sql = "SELECT *
                FROM pw_blog_entry $where order by entry_date desc limit $number";
		$list =  $dbModel->fetchAll($sql);
		$array = array();
		foreach($list as $row){
			$blogInfo = new Blogentry_BlogentryInfo($row);
			$array[] = $blogInfo;
		}
		return $array;
	}

	/**
	 * Add or modify
	 *
	 * @param Blogentry_BlogentryInfo $blogEntryInfo
	 */
	public function update($blogEntryInfo){
		$entryUrl = $blogEntryInfo->getEntryUrl();
		$dbModel = new DbModel("","");
		$sql = "SELECT entry_id FROM pw_blog_entry where entry_url = '$entryUrl'";
		$id = intval($dbModel->fetchOne($sql));
		if($id > 0){
			$blogEntryInfo->setEntryId($id);
			$this->modify($blogEntryInfo);
		}
		else{
			$this->add($blogEntryInfo);
		}
	}

	public function updateType($entryUrl, $type){
		$row['entry_type'] = $type;
		 
		$table = new Blogentry_BlogentryTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto("entry_url=?", $entryUrl);
		$table->update($row, $where);
	}
}