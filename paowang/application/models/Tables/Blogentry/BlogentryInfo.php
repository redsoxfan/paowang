<?php
class Blogentry_BlogentryInfo
{
    public $entryId;
    public $entryUrl;
    public $author;
    public $blogUrl;
    public $entryTitle;
    public $entryDescription;
    public $entryDate;
    public $entryDateString;
    public $entryType = 0;
    
	public function __construct($blogArray = null){
        if($blogArray != null){
		   if(empty($blogArray["entry_id"])){
		      $this->entryId=null;
	  	   }else{
		      $this->entryId=$blogArray["entry_id"];
		   }
		   $this->entryUrl = $blogArray["entry_url"];
		   $this->author = $blogArray["author"];
		   $this->blogUrl = $blogArray["blog_url"];
		   $this->entryTitle = $blogArray["entry_title"];
		   $this->entryDescription = $blogArray["entry_description"];
		   $this->entryDate = $blogArray["entry_date"];
		   $this->entryType = $blogArray["entry_type"];
		   
		   $this->entryDateString = Feed_FeedUtils::getRssDate($this->entryDate);
	   }
	}

	public function getEntryId(){
		return $this->entryId;
	}
	
	public function setEntryId($entryId){
		$this->entryId = $entryId;
	}
	
	public function getEntryUrl(){
		return $this->entryUrl;
	}
	
	public function setEntryUrl($entryUrl){
		$this->entryUrl = $entryUrl;
	}
	
	public function getAuthor(){
		return $this->author;
	}
	
	public function setAuthor($author){
		$this->author = $author;
	}
	
	public function getBlogUrl(){
		return $this->blogUrl;
	}
	
	public function setBlogUrl($blogUrl){
		$this->blogUrl = $blogUrl;
	}
	
	public function getEntryTitle(){
		return $this->entryTitle;
	}
	
	public function setEntryTitle($entryTitle){
		$this->entryTitle = $entryTitle;
	}
	
	public function getEntryDescription(){
		return $this->entryDescription;
	}
	
	public function setEntryDescription($entryDescription){
		$this->entryDescription = $entryDescription;
	}
	
	public function getEntryDate(){
		return $this->entryDate;
	}
	
	public function setEntryDate($entryDate){
		$this->entryDate = $entryDate;
		$this->entryDateString = Feed_FeedUtils::getRssDate($this->entryDate);
	}
	
	public function getEntryType(){
		return $this->entryType;
	}
	
	public function setEntryType($entryType){
		$this->entryType = $entryType;
	}
	
}
