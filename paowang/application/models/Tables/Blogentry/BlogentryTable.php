<?php

class Blogentry_BlogentryTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_blog_entry";
    // 默认主键为’id’
    protected $_primary = "entry_id";
}
