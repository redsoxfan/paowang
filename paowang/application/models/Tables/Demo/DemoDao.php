<?php
class Demo_DemoDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }		
	
    public function get($id){
		$dbModel = new DbModel("tbl_demo","demo_id");
		$row = $dbModel->getRow($id);
		$demoInfo = new Demo_DemoInfo($row);
    	return $demoInfo;
    }

    public function load(&$demoInfo){
		$dbModel = new DbModel("tbl_demo","demo_id");
		$row = $dbModel->getRow($demoInfo->getDemoId());
		$demoInfo = new Demo_DemoInfo($row);
    }
    
    public function add(&$demoInfo){
	    $row['demo_varchar'] = $demoInfo->getDemoVarchar();
	    $row['demo_date'] = $demoInfo->getDemoDate();
	    $row['demo_long'] = $demoInfo->getDemoLong();
	    $row['demo_double'] = $demoInfo->getDemoDouble(); 
	    $table = new Demo_DemoTable();
		$id = $table->insert($row);
		$demoInfo->setDemoId($id);
    }

    public function modify(&$demoInfo){
	    $row['demo_varchar'] = $demoInfo->getDemoVarchar();
	    $row['demo_date'] = $demoInfo->getDemoDate();
	    $row['demo_long'] = $demoInfo->getDemoLong();
	    $row['demo_double'] = $demoInfo->getDemoDouble();        

	    $table = new Demo_DemoTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto(" demo_id = ?", $demoInfo->getDemoId());
		$table->update($row, $where);
    }

    public function del($demoInfo){
		$this->delById($demoInfo->getDemoId());
    }

    public function delById($id){
		$table = new Demo_DemoTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('demo_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("tbl_demo","demo_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $demoInfoArray = array();
        foreach($list as $row){
        	$demoInfo = new Demo_DemoInfo($row);
        	array_push($demoInfoArray, $demoInfo);
        }
        return $demoInfoArray;
    }    

    public function getArrayList($perpage,$page){
	        $log = Zend_Registry::get('log');
    	
		$dbModel = new DbModel("tbl_demo","demo_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $demoInfoArray = array();
        foreach($list as $row){
        	$demoInfo = new Demo_DemoInfo($row);
            //$log->debug( "################### page:".(Zend_Json::encode($row)));        	
        	array_push($demoInfoArray, $row);
        }
        return $demoInfoArray;
    }
    
    public function getListAllCount(){
         $dbModel = new DbModel("tbl_demo","demo_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     
    
}

