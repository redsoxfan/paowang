<?php
class Demo_DemoInfo{
	public $demoId;
	public $demoVarchar;
	public $demoDate;
	public $demoLong;
	public $demoDouble;

	function __construct($demoArray = null)
	{
		if($demoArray != null){
			if(empty($demoArray["demo_id"]))
			{
				$this->demoId=null;
			}else
			{
				$this->demoId=$demoArray["demo_id"];
			}
	
			$this->demoVarchar = $demoArray["demo_varchar"];
			$this->demoDate = $demoArray["demo_date"];
			$this->demoLong = $demoArray["demo_long"];
			$this->demoDouble = $demoArray["demo_double"];
		}
	}
	
	public function setDemoId($demoId){
		$this->demoId=$demoId;
	}
	
	public function getDemoId(){
		return $this->demoId;
	}

	public function setDemoVarchar($demoVarchar){
		$this->demoVarchar=$demoVarchar;
	}
	
	public function getDemoVarchar(){
		return $this->demoVarchar;
	}	

	public function setDemoDate($demoDate){
		$this->demoDate=$demoDate;
	}
	
	public function getDemoDate(){
		return $this->demoDate;
	}

	public function setDemoLong($demoLong){
		$this->demoLong=$demoLong;
	}
	
	public function getDemoLong(){
		return $this->demoLong;
	}

	public function setDemoDouble($demoDouble){
		$this->demoDouble=$demoDouble;
	}
	
	public function getDemoDouble(){
		return $this->demoDouble;
	}
		    
}

