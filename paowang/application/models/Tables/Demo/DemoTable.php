<?php
/**
 * Demo_DemoInfo
 */

class Demo_DemoTable extends Zend_Db_Table
{
    // 设置默认表名
	public $_name = "tbl_demo";
    // 默认主键为’id’
    public $_primary = "demo_id";
}
