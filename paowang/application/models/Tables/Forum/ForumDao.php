<?php
class Forum_ForumDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }		
	
    public function get($id){
		$dbModel = new DbModel("pw_forum","forum_id");
		$row = $dbModel->getRow($id);
		$forumInfo = new Forum_ForumInfo($row);
    	return $forumInfo;
    }

    public function load(&$forumInfo){
		$dbModel = new DbModel("pw_forum","forum_id");
		$row = $dbModel->getRow($forumInfo->getForumId());
		$forumInfo = new Forum_ForumInfo($row);
    }
    
    public function add(&$forumInfo){
        $row['forum_name'] = $forumInfo->getForumName();
        $row['forum_desc'] = $forumInfo->getForumDesc();
        $row['status'] = $forumInfo->getStatus();
        $row['type_id'] = $forumInfo->getTypeId();
        $row['can_upload_pic'] = $forumInfo->getCanUploadPic();
        $row['relive_overdue_day'] = $forumInfo->getReliveOverdueDay();
        $row['is_associate'] = $forumInfo->getIsAssociate();
	    $table = new Forum_ForumTable();
		$id = $table->insert($row);
		$forumInfo->setForumId($id);
    }

    public function modify(&$forumInfo){
        $row['forum_name'] = $forumInfo->getForumName();
        $row['forum_desc'] = $forumInfo->getForumDesc();
        $row['status'] = $forumInfo->getStatus();
        $row['type_id'] = $forumInfo->getTypeId();
        $row['can_upload_pic'] = $forumInfo->getCanUploadPic();
        $row['relive_overdue_day'] = $forumInfo->getReliveOverdueDay();
        $row['is_associate'] = $forumInfo->getIsAssociate();

	    $table = new Forum_ForumTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("forum_id=?", $forumInfo->getForumId());
		$table->update($row, $where);
    }

    public function del($forumInfo){
		$this->delById($forumInfo->getForumId());
    }

    public function delById($id){
		$table = new Forum_ForumTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('forum_id=?',$id);
		$table->delete($where);
    }
    
    public function getForumNameById($forumId){
       	$dbModel = new DbModel("","");
       	$sql = "select forum_name from pw_forum where forum_id=".$forumId;
       	return $dbModel->fetchOne($sql);
    }    
    public function getList($perpage = 0,$page = 0){
		$dbModel = new DbModel("pw_forum","forum_id");
        $list =  $dbModel->getList(' status=1 ',$perpage,$page);
        $forumInfoArray = array();
        foreach($list as $row){
        	$forumInfo = new Forum_ForumInfo($row);
        	array_push($forumInfoArray, $forumInfo);
        }
        return $forumInfoArray;
    }
    
    // please use getBasicForumInfo
//    public function getListByOrder($perpage = 0,$page = 0){
//		$dbModel = new DbModel("pw_forum","forum_id");
//        $list =  $dbModel->getList(' status=1 ',$perpage,$page,"*","forum_id");
//        $forumInfoArray = array();
//        foreach($list as $row){
//        	$forumInfo = new Forum_ForumInfo($row);
//        	array_push($forumInfoArray, $forumInfo);
//        }
//        return $forumInfoArray;
//    }        

    public function getListAllCount(){
         $dbModel = new DbModel("pw_forum","forum_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     
    
    public function isQinOrJian($id){
    	if(PaowangData::FORUM_ID_QIN == $id || PaowangData::FORUM_ID_JIAN == $id){
    		return true;
    	}
    	return false;
    }
    
    
    /**
     * 从PaowangData里面静态取得论坛信息和排列顺序
     */
    public function getBasicForumInfo(){
        $forumInfoArray = array();
        foreach(PaowangData::$orderedDisplayForumsById as $forumId =>$forumName){
            $forumInfo = new Forum_ForumInfo();
            $forumInfo->setForumId($forumId);
            $forumInfo->setForumName($forumName);
            array_push($forumInfoArray, $forumInfo); 
        }
        return $forumInfoArray;
    }

    /**
     * 从PaowangData静态数据里查得论坛名称（以后也可改为由数据库查询）
     */
    public function getForumName($forumId){
    	foreach(PaowangData::$orderedForumsById as $id =>$forumName){
            if($forumId == $id){
            	return $forumName;
            } 
        }
        return "";
    }
    
    public function getForumId($forumUrlName){
    	foreach(PaowangData::$forumsByUrl as $urlName => $forumId){
    		if($forumUrlName == $urlName){
    			return $forumId;
    		}
    	}
    }
    
}
