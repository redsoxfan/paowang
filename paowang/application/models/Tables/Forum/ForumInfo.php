<?php
class Forum_ForumInfo
{
    public $forumId;//主键ID
    public $forumName;//论坛名称
    public $englishName;
    public $forumDesc;//论坛介绍
    public $status;//状态（1：有效 0：无效）
    public $typeId;//论坛类型
    public $canUploadPic;//是否能上传图片
    public $reliveOverdueDay;//过期时间
    public $isAssociate;//是否会员制

	function __construct($forumArray = null){
        if($forumArray != null){
		   if(empty($forumArray["forum_id"])){
		      $this->forumId=null;
	  	   }else{
		      $this->forumId=$forumArray["forum_id"];
		   }
		   $this->forumName = $forumArray["forum_name"];
		   $this->forumDesc = $forumArray["forum_desc"];
		   $this->status = $forumArray["status"];
		   $this->typeId = $forumArray["type_id"];
		   $this->canUploadPic = $forumArray["can_upload_pic"];
		   $this->reliveOverdueDay = $forumArray["relive_overdue_day"];
		   $this->isAssociate = $forumArray["is_associate"];
		   if(isset($forumArray['english_name'])) {
		   	  $this->englishName = $forumArray['english_name'];
		   }
	   }
	}

	function setForumId($forumId){
		$this->forumId=$forumId;
	}

	function getForumId(){
		return $this->forumId;
	}

	function setForumName($forumName){
		$this->forumName=$forumName;
	}

	function getForumName(){
		return $this->forumName;
	}

    function setEnglishName($englishName){
        $this->englishName=$englishName;
    }

    function getEnglishName(){
        return $this->englishName;
    }
	
	function setForumDesc($forumDesc){
		$this->forumDesc=$forumDesc;
	}

	function getForumDesc(){
		return $this->forumDesc;
	}

	function setStatus($status){
		$this->status=$status;
	}

	function getStatus(){
		return $this->status;
	}

	function setTypeId($typeId){
		$this->typeId=$typeId;
	}

	function getTypeId(){
		return $this->typeId;
	}

	function setCanUploadPic($canUploadPic){
		$this->canUploadPic=$canUploadPic;
	}

	function getCanUploadPic(){
		return $this->canUploadPic;
	}

	function setReliveOverdueDay($reliveOverdueDay){
		$this->reliveOverdueDay=$reliveOverdueDay;
	}

	function getReliveOverdueDay(){
		return $this->reliveOverdueDay;
	}

	function setIsAssociate($isAssociate){
		$this->isAssociate=$isAssociate;
	}

	function getIsAssociate(){
		return $this->isAssociate;
	}

}
?>