<?php
/**
 * Forum_ForumTable
 */

class Forum_ForumTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_forum";
    // 默认主键为’id’
    protected $_primary = "forum_id";
}
