<?php
class Forumuser_ForumuserDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }	
	
	public function getRoleId($userId, $forumId){
       	$dbModel = new DbModel();
       	$sql = "select role_id from pw_forum_user where forum_id=" . $forumId . " and user_id=" . $userId;
       	return $dbModel->fetchOne($sql);				
	}
	
	public function updateRole($userId, $forumId, $roleId){
		$dbModel = new DbModel();
        $sql = "select role_id, forum_user_id from pw_forum_user where forum_id=" . $forumId . " and user_id=" . $userId;
        $row = $dbModel->fetchRow($sql);
        $forumUserInfo = new Forumuser_ForumuserInfo();
        $forumUserInfo->setUserId($userId);
        $forumUserInfo->setForumId($forumId);
        $forumUserInfo->setRoleId($roleId);
        $forumUserInfo->setCreateAt(time());
        if($row == null){
            if($roleId != PaowangData::ROLE_NORMAL_USER){
                $this->add($forumUserInfo);
            }
        }
        else{
        	if($roleId == PaowangData::ROLE_NORMAL_USER){
        		$this->del($forumUserInfo);
        	}
        	else{
        		$forumUserId = $row['forum_user_id'];
        		$forumUserInfo->setForumUserId($forumUserId); 
        		$this->modify($forumUserInfo);
        	}
        }
        
	}
	
    public function get($id){
		$dbModel = new DbModel("pw_forum_user","forum_user_id");
		$row = $dbModel->getRow($id);
		$forumuserInfo = new Forumuser_ForumuserInfo($row);
    	return $forumuserInfo;
    }

    public function load(&$forumuserInfo){
		$dbModel = new DbModel("pw_forum_user","forum_user_id");
		$row = $dbModel->getRow($forumuserInfo->getForumUserId());
		$forumuserInfo = new Forumuser_ForumuserInfo($row);
    }
    
    public function add(&$forumuserInfo){
        $row['forum_id'] = $forumuserInfo->getForumId();
        $row['user_id'] = $forumuserInfo->getUserId();
        $row['role_id'] = $forumuserInfo->getRoleId();
        $row['create_at'] = $forumuserInfo->getCreateAt();
	    $table = new Forumuser_ForumuserTable();
		$id = $table->insert($row);
		$forumuserInfo->setForumUserId($id);
    }

    public function modify(&$forumuserInfo){
        $row['forum_id'] = $forumuserInfo->getForumId();
        $row['user_id'] = $forumuserInfo->getUserId();
        $row['role_id'] = $forumuserInfo->getRoleId();
        $row['create_at'] = $forumuserInfo->getCreateAt();

	    $table = new Forumuser_ForumuserTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("forum_user_id=?", $forumuserInfo->getForumUserId());
		$table->update($row, $where);
    }

    public function del($forumuserInfo){
    	$forumUserId = $forumuserInfo->getForumUserId();
    	if($forumUserId != null){
    	   $this->delById($forumuserInfo->getForumUserId());	
    	}
    	else{
    		$userId = $forumuserInfo->getUserId();
    		$forumId = $forumuserInfo->getForumId();
    		$table = new Forumuser_ForumuserTable();
	        $db = $table->getAdapter();
	        $where = 'user_id=' . $userId . ' and forum_id=' . $forumId;
	        $table->delete($where);
    	}
		
    }

    public function delById($id){
		$table = new Forumuser_ForumuserTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('forum_user_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_forum_user","forum_user_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $forumuserInfoArray = array();
        foreach($list as $row){
        	$forumuserInfo = new Forumuser_ForumuserInfo($row);
        	array_push($forumuserInfoArray, $forumuserInfo);
        }
        return $forumuserInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("pw_forum_user","forum_user_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     
    
}
