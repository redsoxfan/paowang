<?php
class Forumuser_ForumuserInfo
{
    public $forumUserId;//主键ID
    public $forumId;//论坛ID
    public $userId;//用户ID
    public $roleId;//角色ID
    public $createAt;//创建时间

	function __construct($forumuserArray = null){
        if($forumuserArray != null){
		   if(empty($forumuserArray["forum_user_id"])){
		      $this->forumUserId=null;
	  	   }else{
		      $this->forumUserId=$forumuserArray["forum_user_id"];
		   }
		   $this->forumId = $forumuserArray["forum_id"];
		   $this->userId = $forumuserArray["user_id"];
		   $this->roleId = $forumuserArray["role_id"];
		   $this->createAt = $forumuserArray["create_at"];
	   }
	}

	function setForumUserId($forumUserId){
		$this->forumUserId=$forumUserId;
	}

	function getForumUserId(){
		return $this->forumUserId;
	}

	function setForumId($forumId){
		$this->forumId=$forumId;
	}

	function getForumId(){
		return $this->forumId;
	}

	function setUserId($userId){
		$this->userId=$userId;
	}

	function getUserId(){
		return $this->userId;
	}

	function setRoleId($roleId){
		$this->roleId=$roleId;
	}

	function getRoleId(){
		return $this->roleId;
	}

	function setCreateAt($createAt){
		$this->createAt=$createAt;
	}

	function getCreateAt(){
		return $this->createAt;
	}

}
?>