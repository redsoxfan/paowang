<?php
/**
 * Forumuser_ForumuserTable
 */

class Forumuser_ForumuserTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_forum_user";
    // 默认主键为’id’
    protected $_primary = "forum_user_id";
}
