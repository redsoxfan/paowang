<?php
class Log_LogDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }		
	
    public function get($id){
		$dbModel = new DbModel("pw_log","log_id");
		$row = $dbModel->getRow($id);
		$logInfo = new Log_LogInfo($row);
    	return $logInfo;
    }

    public function load(&$logInfo){
		$dbModel = new DbModel("pw_log","log_id");
		$row = $dbModel->getRow($logInfo->getLogId());
		$logInfo = new Log_LogInfo($row);
    }
    
    public function add(&$logInfo){
        $row['user_id'] = $logInfo->getUserId();
        $row['create_ip'] = $logInfo->getCreateIp();
        $row['create_at'] = $logInfo->getCreateAt();
        $row['action'] = $logInfo->getAction();
        $row['message'] = $logInfo->getMessage();
	    $table = new Log_LogTable();
		$id = $table->insert($row);
		$logInfo->setLogId($id);
    }

    public function modify(&$logInfo){
        $row['user_id'] = $logInfo->getUserId();
        $row['create_ip'] = $logInfo->getCreateIp();
        $row['create_at'] = $logInfo->getCreateAt();
        $row['action'] = $logInfo->getAction();
        $row['message'] = $logInfo->getMessage();

	    $table = new Log_LogTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("log_id=?", $logInfo->getLogId());
		$table->update($row, $where);
    }

    public function del($logInfo){
		$this->delById($logInfo->getLogId());
    }

    public function delById($id){
		$table = new Log_LogTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('log_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_log","log_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $logInfoArray = array();
        foreach($list as $row){
        	$logInfo = new Log_LogInfo($row);
        	array_push($logInfoArray, $logInfo);
        }
        return $logInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("pw_log","log_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     
    
}
