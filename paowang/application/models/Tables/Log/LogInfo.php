<?php
class Log_LogInfo
{
    public $logId;//主键ID
    public $userId;//用户ID
    public $createIp;//创建者IP
    public $createAt;//创建时间
    public $action;//action
    public $message;//消息

	function __construct($logArray = null){
        if($logArray != null){
		   if(empty($logArray["log_id"])){
		      $this->logId=null;
	  	   }else{
		      $this->logId=$logArray["log_id"];
		   }
		   $this->userId = $logArray["user_id"];
		   $this->createIp = $logArray["create_ip"];
		   $this->createAt = $logArray["create_at"];
		   $this->action = $logArray["action"];
		   $this->message = $logArray["message"];
	   }
	}

	function setLogId($logId){
		$this->logId=$logId;
	}

	function getLogId(){
		return $this->logId;
	}

	function setUserId($userId){
		$this->userId=$userId;
	}

	function getUserId(){
		return $this->userId;
	}

	function setCreateIp($createIp){
		$this->createIp=$createIp;
	}

	function getCreateIp(){
		return $this->createIp;
	}

	function setCreateAt($createAt){
		$this->createAt=$createAt;
	}

	function getCreateAt(){
		return $this->createAt;
	}

	function setAction($action){
		$this->action=$action;
	}

	function getAction(){
		return $this->action;
	}

	function setMessage($message){
		$this->message=$message;
	}

	function getMessage(){
		return $this->message;
	}

}
?>