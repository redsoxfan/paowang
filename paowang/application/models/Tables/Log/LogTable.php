<?php
/**
 * Log_LogTable
 */

class Log_LogTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_log";
    // 默认主键为’id’
    protected $_primary = "log_id";
}
