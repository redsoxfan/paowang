<?php
class Messageboard_MessageboardDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }		
	
    public function get($id){
		$dbModel = new DbModel("pw_message_board","message_board_id");
		$row = $dbModel->getRow($id);
		$messageboardInfo = new Messageboard_MessageboardInfo($row);
    	return $messageboardInfo;
    }

    public function load(&$messageboardInfo){
		$dbModel = new DbModel("pw_message_board","message_board_id");
		$row = $dbModel->getRow($messageboardInfo->getMessageBoardId());
		$messageboardInfo = new Messageboard_MessageboardInfo($row);
    }
    
    public function add(&$messageboardInfo){
        $row['parent_id'] = $messageboardInfo->getParentId();
        $row['user_id'] = $messageboardInfo->getUserId();
        $row['title'] = $messageboardInfo->getTitle();
        $row['message'] = $messageboardInfo->getMessage();
        $row['user_name'] = $messageboardInfo->getUserName();
        $row['from_user_id'] = $messageboardInfo->getFromUserId();
        $row['qq'] = $messageboardInfo->getQq();
        $row['location'] = $messageboardInfo->getLocation();
        $row['create_at'] = $messageboardInfo->getCreateAt();
        $row['issecret'] = $messageboardInfo->getIssecret();
	    $table = new Messageboard_MessageboardTable();
		$id = $table->insert($row);
		$messageboardInfo->setMessageBoardId($id);
    }

    public function modify(&$messageboardInfo){
        $row['parent_id'] = $messageboardInfo->getParentId();
        $row['user_id'] = $messageboardInfo->getUserId();
        $row['title'] = $messageboardInfo->getTitle();
        $row['message'] = $messageboardInfo->getMessage();
        $row['user_name'] = $messageboardInfo->getUserName();
        $row['from_user_id'] = $messageboardInfo->getFromUserId();
        $row['qq'] = $messageboardInfo->getQq();
        $row['location'] = $messageboardInfo->getLocation();
        $row['create_at'] = $messageboardInfo->getCreateAt();
        $row['issecret'] = $messageboardInfo->getIssecret();

	    $table = new Messageboard_MessageboardTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("message_board_id=?", $messageboardInfo->getMessageBoardId());
		$table->update($row, $where);
    }

    public function del($messageboardInfo){
		$this->delById($messageboardInfo->getMessageBoardId());
    }

    public function delById($id){
		$table = new Messageboard_MessageboardTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('message_board_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_message_board","message_board_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $messageboardInfoArray = array();
        foreach($list as $row){
        	$messageboardInfo = new Messageboard_MessageboardInfo($row);
        	array_push($messageboardInfoArray, $messageboardInfo);
        }
        return $messageboardInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("pw_message_board","message_board_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     
    
}
