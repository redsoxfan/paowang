<?php
class Messageboard_MessageboardInfo
{
    public $messageBoardId;//主键ID
    public $parentId;//回复的主留言ID
    public $userId;//用户ID
    public $title;//留言标题
    public $message;//留言
    public $userName;//发表者用户名称
    public $fromUserId;//发表者id
    public $qq;//qq
    public $location;//位置(来自)
    public $createAt;//创建时间
    public $issecret;//是否私密留言

	function __construct($messageboardArray = null){
        if($messageboardArray != null){
		   if(empty($messageboardArray["message_board_id"])){
		      $this->messageBoardId=null;
	  	   }else{
		      $this->messageBoardId=$messageboardArray["message_board_id"];
		   }
		   $this->parentId = $messageboardArray["parent_id"];
		   $this->userId = $messageboardArray["user_id"];
		   $this->title = $messageboardArray["title"];
		   $this->message = $messageboardArray["message"];
		   $this->userName = $messageboardArray["user_name"];
		   $this->fromUserId = $messageboardArray["from_user_id"];
		   $this->qq = $messageboardArray["qq"];
		   $this->location = $messageboardArray["location"];
		   $this->createAt = $messageboardArray["create_at"];
		   $this->issecret = $messageboardArray["issecret"];
	   }
	}

	function setMessageBoardId($messageBoardId){
		$this->messageBoardId=$messageBoardId;
	}

	function getMessageBoardId(){
		return $this->messageBoardId;
	}

	function setParentId($parentId){
		$this->parentId=$parentId;
	}

	function getParentId(){
		return $this->parentId;
	}

	function setUserId($userId){
		$this->userId=$userId;
	}

	function getUserId(){
		return $this->userId;
	}

	function setTitle($title){
		$this->title=$title;
	}

	function getTitle(){
		return $this->title;
	}

	function setMessage($message){
		$this->message=$message;
	}

	function getMessage(){
		return $this->message;
	}

	function setUserName($userName){
		$this->userName=$userName;
	}

	function getUserName(){
		return $this->userName;
	}

	function setFromUserId($fromUserId){
		$this->fromUserId=$fromUserId;
	}

	function getFromUserId(){
		return $this->fromUserId;
	}

	function setQq($qq){
		$this->qq=$qq;
	}

	function getQq(){
		return $this->qq;
	}

	function setLocation($location){
		$this->location=$location;
	}

	function getLocation(){
		return $this->location;
	}

	function setCreateAt($createAt){
		$this->createAt=$createAt;
	}

	function getCreateAt(){
		return $this->createAt;
	}

	function setIssecret($issecret){
		$this->issecret=$issecret;
	}

	function getIssecret(){
		return $this->issecret;
	}

}
?>