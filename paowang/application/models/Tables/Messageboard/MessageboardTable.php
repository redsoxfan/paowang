<?php
/**
 * Messageboard_MessageboardTable
 */

class Messageboard_MessageboardTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_message_board";
    // 默认主键为’id’
    protected $_primary = "message_board_id";
}
