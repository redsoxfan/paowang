<?php
/**
 * 热门贴子算法
 * 
 * TODO 转换成适应新数据库新程序
 */
class Post_HotTopic
{
    const NUMBER_HOT_TOPIC = 20;
    
    private $_forumId;
    private $_posts;
    private $_scores;

    public function __construct($forumId)
    {
        $this->_forumId = $forumId;
        $this->_posts = array();
        $this->_scores = array();
    }
    
    public function getSortedPosts()
    {
        
        $this->processPosts();
        arsort($this->_scores);
        $sortedPost = array();
        foreach($this->_scores as $id => $score)
        {
            $post = $this->_posts[$id];
            $sortedPost[] = $this->_posts[$id];
        }
        return array_slice($sortedPost, 0, self::NUMBER_HOT_TOPIC);
    }

    private function processPosts()
    {
        $db = Zend::registry('db');
        $select = $db->select();
        $select->from("pw_post", "*");
        $select->where("forum_id=?", $this->_forumId);
        $select->where("parent_id=?", "0");
        $select->where("status=?", "1");
        $select->order('id DESC');
        $select->limit(100);
        $sql = $select->__toString();  
        
        $rows = $db->fetchAll($sql);
        foreach($rows as $row)
        {
            $post = new Post_Post(null, $row);
            $id = $post->getId();
            $this->_posts[$id] = $post;
            $this->_scores[$id] = $this->getScore($post);
        }
    }

    /**
     * 算出贴子热门程度的总分
     * 
     * @param Post_Post $post
     */
    private static function getScore($post)
    {
        $click = $post->getClick();
        $reply = self::getNumberOfReplies($post);
        $dig = new Postjudge_Dig($post->getId());
        $digNum = $dig->countTotal(true);
        $tag = count($post->getTags());
        
        //Zend::dump("$click $reply $dugNum $tag");        
        $score = $click + $reply*20 + $digNum*20 + $tag*20;
        
        $now = time();
        $postTime = $post->getCreateAt();
        $age = $now - $postTime;
        $days = floor($age/(60*60*24));
        //Zend::dump($days);
        
        $score = $score * pow(0.8, $days);
        //Zend::dump("score is $score");
        return $score;
    }
    
    private static function getNumberOfReplies($post)
    {
        $postModel = new Post_PostModel();
        return $postModel->getNumberOfReplies($post->getId());
    }
}