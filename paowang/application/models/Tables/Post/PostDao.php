<?php
class Post_PostDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }		
	
    public function get($id){
		$dbModel = new DbModel("pw_post","post_id");
		$row = $dbModel->getRow($id);
		if($row == null){
			return null;
		}
		$postInfo = new Post_PostInfo($row);
		$dbModel1 = new DbModel("pw_post_text","post_id");
    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        $sql = "select count(*) from pw_post_pic where post_id=".$postInfo->getPostId();
		$count = intval($dbModel->fetchOne($sql));
		$postInfo->setPicCount($count);
	    if($postInfo->getPicCount()==0){
			$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
			$postInfo->setPicCount($count); 
	    }		
    	return $postInfo;
    }

    public function getRegisterDays($userId){
       $sql = 	"select create_at from pw_user where user_id=$userId";
       $dbModel = new DbModel("pw_post_text","post_id");
       $create_at = $dbModel->fetchOne($sql);
       $days = intval((time() - $create_at)/(24*60*60));
       return $days;
    }
    
    public function getPostDayMaxCountByUserId($userId){
       $sql = 	"select create_at from pw_user where user_id=$userId";
       $dbModel = new DbModel("pw_post_text","post_id");
       $create_at = $dbModel->fetchOne($sql);
       $days = intval((time() - $create_at)/(24*60*60));
       $count = 99;
       if($days<31){
       	   $count = 6;
       }	   
       /*
       $sql = "select round((to_days(now())-to_days(from_unixtime(create_at)))/365+5),from_unixtime(create_at) from pw_user where user_id=$userId";
       $dbModel = new DbModel("pw_post_text","post_id");
       $count = intval($dbModel->fetchOne($sql));
       */
       return $count;
    }


    public function getPostDayCountByUserId($userId){
       $nowDate = date('Y-m-d',time());
       $sql = "select count(*) from pw_post where user_id=$userId and post_id=top_id and left(from_unixtime(create_at),10)='$nowDate'";
       $dbModel = new DbModel("pw_post_text","post_id");
       $count = intval($dbModel->fetchOne($sql));
       return $count;
    }

    public function getPostDayCountByUserIp($userIp){
       $nowDate = date('Y-m-d',time());
       $sql = "select count(*) from pw_post where user_ip='$userIp' and post_id=top_id and left(from_unixtime(create_at),10)='$nowDate'";
       $dbModel = new DbModel("pw_post_text","post_id");
       $count = intval($dbModel->fetchOne($sql));
       return $count;
    }
    

 
    public function load(&$postInfo){
		$dbModel = new DbModel("pw_post","post_id");
		$row = $dbModel->getRow($postInfo->getPostId());
		$postInfo = new Post_PostInfo($row);
		$dbModel1 = new DbModel("pw_post_text","post_id");
    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        $sql = "select count(*) from pw_post_pic where post_id=".$postInfo->getPostId();
		$count = intval($dbModel->fetchOne($sql));
		$postInfo->setPicCount($count);    	        			
	    if($postInfo->getPicCount()==0){

			$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
			$postInfo->setPicCount($count); 
	    }		
    }
    
    public function add(&$postInfo){
        //随机选0，1，2作为起始点击数，和PostDao里的addClicks配合
//        $click = $postInfo->getClick();
//        Logging::debug("click is " . $click);
//    	if(intval($click) == 0){
//    		$click = rand(0, 2);
//    		Logging::debug("random click is " . $click);
//    	}
        $row['forum_id'] = $postInfo->getForumId();
        $row['user_id'] = $postInfo->getUserId();
        $row['user_name'] = $postInfo->getUserName();
        $row['parent_id'] = $postInfo->getParentId();
        $row['top_id'] = $postInfo->getTopId();
        $row['is_digest'] = $postInfo->getIsDigest();
        $row['title'] = $postInfo->getTitle();
        $row['depth'] = $postInfo->getDepth();
        $row['show_order'] = $postInfo->getShowOrder();
        $row['list_order'] = $postInfo->getListOrder();
        $row['can_haschild'] = $postInfo->getCanHaschild();
        $row['is_tail'] = $postInfo->getIsTail();
        $row['status'] = $postInfo->getStatus();
        $row['create_at'] = $postInfo->getCreateAt();
        $row['user_ip'] = $postInfo->getUserIp();
        $row['click'] = strval($postInfo->getClick());
        $row['tags'] = $postInfo->getTags();
        $row['text_length'] = $postInfo->getTextLength();
        $row['update_at'] = $postInfo->getUpdateAt();
        $row['update_ip'] = $postInfo->getUpdateIp();
        $row['is_ceil'] = $postInfo->getIsCeil();
        $row['is_relive'] = $postInfo->getIsRelive();
        $row['relive_time'] = $postInfo->getReliveTime();
        $row['user_post_type_id'] = $postInfo->getUserPostTypeId();
        $row['child_type'] = $postInfo->getChildType();
        $row['is_pic'] = $postInfo->getIsPic();
        $row['child_posts'] = $postInfo->getChildPosts();
        
        Logging::debug("after click is " . $row['click']);
	    $table = new Post_PostTable();
	    
        $log = Zend_Registry::get('log');
        $log->debug("###################TITLe:".$row['title']);	    
		$id = $table->insert($row);
		$postInfo->setPostId($id);
		
//		$this->initClick($id);
		
		// clear cache
		$forumId = $postInfo->getForumId();
		Cache_Engine::clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array(Cache_Engine::TAG_FORUM . $forumId));
    }

    public function modify(&$postInfo){
        $row['forum_id'] = $postInfo->getForumId();
        $row['user_id'] = $postInfo->getUserId();
        $row['user_name'] = $postInfo->getUserName();
        $row['parent_id'] = $postInfo->getParentId();
        $row['top_id'] = $postInfo->getTopId();
        $row['is_digest'] = $postInfo->getIsDigest();
        $row['title'] = $postInfo->getTitle();
        $row['depth'] = $postInfo->getDepth();
        $row['show_order'] = $postInfo->getShowOrder();
        $row['list_order'] = $postInfo->getListOrder();
        $row['can_haschild'] = $postInfo->getCanHaschild();
        $row['is_tail'] = $postInfo->getIsTail();
        $row['status'] = $postInfo->getStatus();
        $row['create_at'] = $postInfo->getCreateAt();
        $row['user_ip'] = $postInfo->getUserIp();
        $row['click'] = $postInfo->getClick();
        $row['tags'] = $postInfo->getTags();
        $row['text_length'] = $postInfo->getTextLength();
        $row['update_at'] = $postInfo->getUpdateAt();
        $row['update_ip'] = $postInfo->getUpdateIp();
        $row['is_ceil'] = $postInfo->getIsCeil();
        $row['is_relive'] = $postInfo->getIsRelive();
        $row['relive_time'] = $postInfo->getReliveTime();
        $row['user_post_type_id'] = $postInfo->getUserPostTypeId();
        $row['child_type'] = $postInfo->getChildType();
        $row['is_pic'] = $postInfo->getIsPic();
        $row['last_child_update_at'] = $postInfo->getLastChildUpdateAt();
        $row['child_posts'] = $postInfo->getChildPosts();
        $row['update_user_id'] = $postInfo->getUpdateUserId();
        $row['update_user_name'] = $postInfo->getUpdateUserName();

        $log = Zend_Registry::get('log');
        $log->debug("postInfo->getUpdateUserId():".$postInfo->getUpdateUserId());

        
	    $table = new Post_PostTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("post_id=?", $postInfo->getPostId());
		$table->update($row, $where);
		
        // clear cache
        $forumId = $postInfo->getForumId();
        Cache_Engine::clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array(Cache_Engine::TAG_FORUM . $forumId));
    }

    public function del($postInfo){
		$this->delById($postInfo->getPostId());
    }

    public function delById($id){
		$table = new Post_PostTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('post_id=?',$id);
		$table->delete($where);
    }
    
    
    public  function delChilds($postId){
		$dbModel = new DbModel("","");
		$sql = "update pw_post set status=0 where post_id=".$postId;
		$dbModel->query($sql);
		
        $sql = "select count(*) from pw_post where parent_id=".$postId;
		$count = intval($dbModel->fetchOne($sql));
		if($count>0){
            $sql = "select post_id from pw_post where parent_id=".$postId;        			
			$list = $dbModel->fetchAll($sql);
			foreach($list as $row){
			   delChilds($row["post_id"]);	
			}
		}    	

    }
    
    public function delByIdUpdate($postId){
		$dbModel = new DbModel("","");
		$sql = "update pw_post set status=0 where post_id=".$postId;
		$dbModel->query($sql);
		
        $sql = "select count(*) from pw_post where parent_id=".$postId;
		$count = intval($dbModel->fetchOne($sql));
		if($count>0){
            $sql = "select post_id from pw_post where parent_id=".$postId;        			
			$list = $dbModel->fetchAll($sql);
			foreach($list as $row){
			  $this->delByIdUpdate($row["post_id"]);	
			}
		}else{
			// clear cache
	        $sql = "select forum_id from pw_post where post_id=$postId";
			$forumId = intval($dbModel->fetchOne($sql));			
			Cache_Engine::clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
			     array(Cache_Engine::TAG_FORUM . $forumId, Cache_Engine::TAG_FORUM_HOT . $forumId));
		}
        
    }    
    
    public function undelByIdUpdate($postId){
		$dbModel = new DbModel("","");
		$sql = "update pw_post set status=1 where post_id=".$postId;
        $dbModel->query($sql);

        $sql = "select parent_id from pw_post where post_id=$postId";
		$parentId = intval($dbModel->fetchOne($sql));
		if($parentId>0){
		    $this->undelByIdUpdate($parentId);
		}else{
			// clear cache
	        $sql = "select forum_id from pw_post where post_id=$postId";
			$forumId = intval($dbModel->fetchOne($sql));			
			Cache_Engine::clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
                 array(Cache_Engine::TAG_FORUM . $forumId, Cache_Engine::TAG_FORUM_HOT . $forumId));
		}
    }
    
    
//    public function ceilByIdUpdate($postId){
//		$dbModel = new DbModel("","");
//		$sql = "update pw_post set is_ceil=1,update_at=".time()." where post_id=".$postId;
//        $dbModel->query($sql);
//    }    
//    
//    public function unceilByIdUpdate($postId){
//		$dbModel = new DbModel("","");
//		$sql = "update pw_post set is_ceil=0,update_at=".time()." where post_id=".$postId;
//        $dbModel->query($sql);
//    }
        
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_post","post_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $postInfoArray = array();
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	    	        	
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
        	array_push($postInfoArray, $postInfo);
        }
        return $postInfoArray;
    }    
    
    public function getRssPosts($forumId = 1){
		$dbModel = new DbModel("pw_post","post_id");
        $list =  $dbModel->getList("forum_id=$forumId and parent_id=0 and status=1", 20, 0, "*");
        $postArray = array();    	
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	    	        	
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
        	array_push($postArray, $postInfo);
        }
        return $postArray;
    }

    /**
     * 贴子显示信息。不同的type返回不同组织方式的贴子列表。
     */
    public function getListByForumId(
        $forumId = PaowangData::FORUM_ID_DEFAULT,
        $perpage = PaowangData::FORUM_POSTS_PER_PAGE, 
        $page = 1,
        $type = PaowangData::FORUM_TAB_ALL,
        $isManage = 0,
        $rootPostOnly = false){
        $cacheId = Cache_Engine::generateId(Array('PostDao', 'getListByForumId' ,'forum', $forumId, 
            'type', $type, $page, $perpage, $isManage, $rootPostOnly));
        $result = Cache_Engine::load($cacheId);
        if($result){
        	return $result;
        }
        

        
		$dbModel = new DbModel("pw_post","post_id");
        $list = null;
        switch($type){
            case PaowangData::FORUM_TAB_ALL:
            	$list = $dbModel->getList("forum_id=$forumId and parent_id=0  ".($isManage==1?"":"and status=1"), $perpage, $page, "*");
            	break;
            case PaowangData::FORUM_TAB_NEW:
            	$list = $dbModel->getList("forum_id=$forumId and parent_id=0  ".($isManage==1?"":"and status=1"), $perpage, $page, "*");
            	break;
            case PaowangData::FORUM_TAB_NEWREPLY:
            	//$list = $dbModel->getList("forum_id=$forumId and status=1 ", $perpage, $page, "*", "create_at desc");
                // 从旧paowang程序里copy过来的sql语句
            	//$sql = "select p2.* from ( select distinct  top_id from (select  top_id from pw_post where forum_id=".$forumId." order by create_at desc)  t  limit ".($pageId-1)*$numberPerPage.",".$numberPerPage.")p1 ,pw_post p2 where p1.top_id=p2.post_id ";
            	//$newReplyDbModel = new DbModel("",""); 
            	//$list = $newReplyDbModel->fetchAll($sql);
            	//主贴记录“最后子帖更新时间”，修改算法
            	$list = $dbModel->getList("forum_id=$forumId ".($isManage==1?"":"and status=1")." and last_child_update_at>0 ", $perpage, $page, "*", "last_child_update_at desc");
            	//$this->getNewReplyPosts($forumId);
            	break;
            case PaowangData::FORUM_TAB_WAKEUP:
            	//$time_one_day = time() - 1 * 24 * 60 *60;
                $time_three_day = time() - 3 * 24 * 60 *60; 
//                $wakeupDbModel = new DbModel("","");  
//                $sql = "SELECT *
//                    FROM pw_post p
//                    JOIN pw_post t ON p.top_id = t.post_id
//                    WHERE   
//                        p.forum_id = '$forumId'
//                        AND p.status = 1
//                        AND t.status = 1
//                        AND p.create_at > '$time_one_day'
//                        AND t.create_at < '$time_three_day'";
//                $list =  $wakeupDbModel->fetchAll($sql);
                $list = $dbModel->getList("forum_id=$forumId ".($isManage==1?"":"and status=1")." and last_child_update_at>0  and create_at<=$time_three_day  ", $perpage, $page, "*", "last_child_update_at desc");            	
            	break;
        }
        
        $rtnarray = array();
        foreach($list as $row){
        	
        	$postInfo = new Post_PostInfo($row);
        	
			$dbModel1 = new DbModel("pw_post_text","post_id");
        	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);
		    
	    
		    if($postInfo->getPicCount()==0){

				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }         	
 		    
        	if(!$rootPostOnly) {
        	   $childList = null;
	            $this->loadChilds($postInfo->getPostId(),$childList,false,$isManage);
	            
	            $postpicDao = Postpic_PostpicDao::getInstance();
	            	            $postpicDao = Postpic_PostpicDao::getInstance();
	            $piclists =  $postpicDao->getListByPostId($postInfo->getPostId());
	            
	            
	            
	            array_push($rtnarray, array(
	                  "postInfo"=>$postInfo,
	                  "childs"=>$childList,
	                  "piclists"=>$piclists,
	            ));	
        	}
        	else {
        		array_push($rtnarray, array(
                      "postInfo"=>$postInfo
                )); 
        	}
 		     	
        }
        Cache_Engine::save($rtnarray, $cacheId, array(Cache_Engine::TAG_FORUM . $forumId));
        return $rtnarray;
    }
    
    
    public function getPostListByUserIdAndTypeId($userId = 0,$typeId = 0,$perpage,$page){
		$dbModel = new DbModel("pw_post","post_id");
        $list =  $dbModel->getList("user_id=$userId ".($typeId>0?" and user_post_type_id=$typeId":"")." and parent_id=0 and is_pic=0 ",$perpage,$page,"*");
        $rtnarray = array();
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	    	        	
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
 		    $childList = null;
			$this->loadChilds($postInfo->getPostId(),$childList,false,0);
		    array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "childs"=>$childList
		    )); 	
        }
        return $rtnarray;
    }   
    
    
    public function getPostListByTopId($topId = 0){
		$dbModel = new DbModel("pw_post","post_id");
        $list =  $dbModel->getList("top_id=$topId ");
        $rtnarray = array();
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	    	
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
            $isManage = false;            
            if( Rbm_RbmService::canEditPost(Util::getUserId(), $postInfo->getForumId(), $postInfo->getPostId())){
                $isManage = true;	
            }	    	        	
		    array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "isManage"=>$isManage
		    )); 	
        }
        return $rtnarray;
    }     

    public function getPostListByUserIdAndTypeIdCount($userId,$typeId){
         $dbModel = new DbModel("pw_post","post_id");   	
         return $dbModel->getListCount("user_id=$userId ".($typeId>0?" and user_post_type_id=$typeId":"")." and parent_id=0 and is_pic=0 ");
    }    
    
    public function getListAllCount(){
         $dbModel = new DbModel("pw_post","post_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }
    
    /**
     * 根据不同显示类别，返回贴子总数
     */
    public function getListAllCountByForumId($forumId, $type = PaowangData::FORUM_TAB_ALL,$isManage = 0,$userId = 0){
    	$rtn = 0;
        $dbModel = new DbModel("pw_post","post_id");
        $log = Zend_Registry::get('log');
        
        switch($type){
            case PaowangData::FORUM_TAB_ALL:
            case PaowangData::FORUM_TAB_NEW:
            case PaowangData::FORUM_TAB_WAKEUP:
                $rtn = $dbModel->getListCount(" forum_id=$forumId and parent_id=0 ".($isManage==1?"":"and status=1"));
            	break;
            case PaowangData::FORUM_TAB_NEWREPLY:
                $rtn = $dbModel->getListCount(" forum_id=$forumId and parent_id=0 and last_child_update_at!=0 ".($isManage==1?"":"and status=1"));
            	break;
            case PaowangData::FORUM_TAB_MY_MAINPOST:
                $rtn = $dbModel->getListCount("forum_id=$forumId and user_id=$userId and parent_id=0 and status=1 ");
                break;
            case PaowangData::FORUM_TAB_MY_REPLY:    	
            	$dbModel = new DbModel();
                $sql = "select count(*) from  (select distinct  top_id from pw_post where
						forum_id=$forumId and user_id=$userId and parent_id!=0 and status=1 order by update_at desc )  p1 ,pw_post p2
						   where  p1.top_id=p2.post_id";
		        $log->debug("###################FORUM_TAB_MY_REPLY:".$sql);	    				
                $rtn =  $dbModel->fetchOne($sql);
                //$rtn = $dbModel->getListCount("forum_id=$forumId and user_id=$userId and parent_id!=0 and status=1 ");
                break;            	
            case PaowangData::FORUM_TAB_MY_VOTE_YES:
                $dbModel = new DbModel();
                $sql = "SELECT count(*)
                    FROM pw_post p, pw_post_judge t
                    WHERE
		            t.post_id = p.post_id
		            AND p.forum_id = '$forumId'
		            AND t.user_id = '$userId'
		            AND t.flag = '1'
		            AND p.parent_id = 0
		            AND p.status = '1'";
                $rtn =  $dbModel->fetchOne($sql);
                break;
            case PaowangData::FORUM_TAB_MY_VOTE_NO:
            	$dbModel = new DbModel("","");
                $sql = "SELECT count(*)
                    FROM pw_post p, pw_post_judge t
		            WHERE
		            t.post_id = p.post_id
		            AND p.forum_id = '$forumId'
		            AND t.user_id = '$userId'
		            AND t.flag = '-1'
		            AND p.parent_id = 0
		            AND p.status = '1'";
                $rtn =  $dbModel->fetchOne($sql);
                break;            	
            	
        }
        return $rtn;
    }     
   
    function loadChilds($postId,&$postInfoArray = null,$includeTop = false,$isManage = 0){ 
		$dbModel = new DbModel("pw_post","post_id");
        $list =  null;
        if($postInfoArray == null){
           $postInfoArray = array();
        }
        if($includeTop && count($postInfoArray)==0){   
           $list = $dbModel->getList("post_id=$postId ".($isManage==1?"":"and status=1")." ",0,0,"*","post_id desc");
        }else{
           $list = $dbModel->getList("parent_id=$postId  ".($isManage==1?"":"and status=1")." ",0,0,"*","post_id desc");	
        }
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
        	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);        	
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
        	array_push($postInfoArray, $postInfo);
        	$this->loadChilds($postInfo->getPostId(),$postInfoArray,false,$isManage);
        }
	}
	
    function getPostsById($postId,&$idstr){ 
		$dbModel = new DbModel("pw_post","post_id");
		if($idstr==''){
			$idstr = $postId;
		}
        $list = $dbModel->getList("parent_id=$postId",0,0,"*","post_id desc");	
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	    	
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }	    	        	
        	$idstr .=",".$postInfo->getPostId();
        	$this->getPostsById($postInfo->getPostId(),$idstr);
        }
	}
	
    function delPostsById($postId){ 
        $idStr = "";
        $this->getPostsById($postId,$idStr);
        
        $sql = "update pw_post set status=0 where post_id in ($idStr)";
        $dbModel = new DbModel("","");
        $dbModel->query($sql);
	}
			
    public function getCeilPosts($forumId)
    {
		$dbModel = new DbModel("pw_post","post_id");
        $list =  $dbModel->getList(" forum_id=$forumId and is_ceil=1 ",0,0,"*","update_at desc");
        
        $cacheId = Cache_Engine::generateId(Array('PostDao', 'getCeilPosts' ,'forum', $forumId));
        $result = Cache_Engine::load($cacheId);
        if($result){
            return $result;
        }
        
        $rtnarray = array();
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where  status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	    	        	
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
 		    $childList = null;
			$this->loadChilds($postInfo->getPostId(),$childList,false,0);
			
            $postpicDao = Postpic_PostpicDao::getInstance();
            $piclists =  $postpicDao->getListByPostId($postInfo->getPostId());			
			array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "childs"=>$childList,
		    	  "piclists"=>$piclists
		    )); 
        }
        Cache_Engine::save($rtnarray, $cacheId, array(Cache_Engine::TAG_FORUM_TOP));
        return $rtnarray;
    }

    /**
     * TODO 用HotTopic.php确定算法
     */
    public function getHotPosts($forumId, $postNumber = 3)
    {
        $cacheId = Cache_Engine::generateId(Array('PostDao', 'getHotPosts' ,'forum', $forumId, $postNumber));
        $result = Cache_Engine::load($cacheId);
        if($result){
            return $result;
        }
    	
        $numDays = PaowangData::$hotTopicDaysById[$forumId];
        //算法需要确定,目前的算法是click*5+child_posts*3+(vote_yes+vote_no)*1
		$dbModel = new DbModel("pw_post","post_id");
		$list = $dbModel->getList(" forum_id=$forumId and parent_id=0  and create_at>=UNIX_TIMESTAMP()-" . strval($numDays) . "*24*3600 and status=1", $postNumber, 1, "*", PaowangData::HOT_TOPIC_ALGORITHM . " desc");
        $rtnarray = array();
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));        	
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	          	
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
 		    $childList = null;
			$this->loadChilds($postInfo->getPostId(),$childList,false,0);
			$postpicDao = Postpic_PostpicDao::getInstance();
			$piclists =  $postpicDao->getListByPostId($postInfo->getPostId());			
			array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "childs"=>$childList,
		    	  "piclists"=>$piclists
		    ));        	
        }
        Cache_Engine::save($rtnarray, $cacheId, array(Cache_Engine::TAG_FORUM_HOT . $forumId), 3600);
        return $rtnarray;
    }
    
    /**
     * @deprecated 
     */
    public function getNewPosts($forumId)
    {
		$dbModel = new DbModel("","");
		$sql = "SELECT *                   
				FROM pw_post
				WHERE
				        forum_id='$forumId'
				        AND status=1
				        AND parent_id=0 order by post_id desc limit 1,20";
        $list =  $dbModel->fetchAll($sql);
        $rtnarray = array();
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	  	    	        	
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
 		    $childList = null;
			$this->loadChilds($postInfo->getPostId(),$childList,false,0);
			array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "childs"=>$childList
		    )); 
        }
        return $rtnarray;
       
    }
        
    /**
     * 
     * @deprecated 
     */
    public function getWeakUpPosts($forumId)
    {
        $time_one_day = time() - 1 * 24 * 60 *60;
        $time_three_day = time() - 3 * 24 * 60 *60; 
		$dbModel = new DbModel("","");
		$sql = "SELECT *
			FROM pw_post p
			JOIN pw_post t ON p.top_id = t.id
			WHERE
			        p.forum_id = '$forumId'
			        AND p.status = 1
			        AND t.status = 1
			        AND p.create_at > '$time_one_day'
			        AND t.create_at < '$time_three_day'";
        $list =  $dbModel->fetchAll($sql);
        $postInfoArray = array();
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    	  	    	        	
        	array_push($postInfoArray, $postInfo);
        }
        return $postInfoArray;
    }
        

    /**
     * @deprecated 
     */
    public function getNewReplyPosts($forumId)
    {
		$dbModel = new DbModel("","");
		$sql = "SELECT * 
				FROM pw_post
				WHERE
				        forum_id=$forumId
				        AND status=1
				        AND parent_id=0 and last_child_update_at>0 order by last_child_update_at desc";
     	    $log = Zend_Registry::get('log');
            $log->debug( "###################:".$sql);	
            		
        $list =  $dbModel->fetchAll($sql);
        $rtnarray = array();
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	  	    	        	
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
 		    $childList = null;
			$this->loadChilds($postInfo->getPostId(),$childList,false,0);
			array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "childs"=>$childList
		    )); 
        }
        return $rtnarray;
    }
    
    public function getIndexPics($forumId,$nums){
         $hotLists = $this->getHotPosts($forumId,$nums);
		   $pics = "";
		   $links = "";
		   $texts = "";
		   if($hotLists!=null){
		    foreach ($hotLists as $row) { 
			   $postInfo = $row["postInfo"];
		    if($row["piclists"]!=null){
				$i = 0;
				foreach ($row["piclists"] as $postpicInfo) { 
			            $thumbSrc = "http://p.paowang.com".$postpicInfo->getPicPath();//Thumb
	                    $img_info = getimagesize("/www/site".$postpicInfo->getPicPath());		            	
	                    //if($img_info[0] >= $img_info[1]){
				            $originalSrc = $postpicInfo->getPicPath();
				            $postId = $postInfo->getPostId();
				            $postUrl = "/post/" . $postId;
				            $imageCaption = "<a href=" .$postUrl . ' target=_blank>' . $postInfo->getUserName() . ' ' . $postpicInfo->getPicDesc() . "</a>";
				            $pics .= "|".$thumbSrc;
				            $links .= "|".$postUrl;
				            //$texts .= "|".$postpicInfo->getPicDesc();
				            $texts .= "|".$postInfo->getTitle();
				            $i++;
				            if($i==4){
				            	break;
				            }
			           // }
					}
			    }
		    }    	    
		   }
         
         if(strlen($pics)>0){
         	$pics = substr($pics,1);
         }
         if(strlen($links)>0){
         	$links = substr($links,1);
         }
         if(strlen($texts)>0){
         	$texts = substr($texts,1);
         }  
         $rtnarray = array();
         $rtnarray['pics']=$pics;
         $rtnarray['links']=$links;
         $rtnarray['texts']=$texts;                  
         return  $rtnarray;         
    }	

    /**
     * 我的主贴，我的跟贴，我赞成，反对的贴子
     */
    public function getUserPostsByForumId(
        $forumId,
        $userId,
        $perpage = PaowangData::FORUM_POSTS_PER_PAGE, 
        $page = 1,
        $type = PaowangData::FORUM_TAB_MY_MAINPOST){
        
        $dbModel = new DbModel("pw_post","post_id");
        $list;
        $log = Zend_Registry::get('log');
        switch($type){
            case PaowangData::FORUM_TAB_MY_MAINPOST:
                $list = $dbModel->getList("forum_id=$forumId and user_id=$userId and parent_id=0 and status=1 ", $perpage, $page, "*");
                break;
            case PaowangData::FORUM_TAB_MY_REPLY:    	
                //$list = $dbModel->getList("forum_id=$forumId and user_id=$userId and parent_id!=0 and status=1 ", $perpage, $page, "*");
                //break;
                $dbModel = new DbModel();
                $start = ($page-1)*$perpage;
                $sql = "select p2.* from  (select distinct  top_id from pw_post where
						forum_id=$forumId and user_id=$userId and parent_id!=0 and status=1 order by update_at desc limit $start,$perpage)  p1 ,pw_post p2
						   where  p1.top_id=p2.post_id";
		        $log->debug("###################FORUM_TAB_MY_REPLY:".$sql);				
                $list =  $dbModel->fetchAll($sql);
                
                break;                
            case PaowangData::FORUM_TAB_MY_VOTE_YES:
                $dbModel = new DbModel();
                $start = ($page-1)*$perpage;
                $sql = "SELECT *
                    FROM pw_post p, pw_post_judge t
                    WHERE
		            t.post_id = p.post_id
		            AND p.forum_id = '$forumId'
		            AND t.user_id = '$userId'
		            AND t.flag = '1'
		            AND p.parent_id = 0
		            AND p.status = '1' limit $start,$perpage";
                $list =  $dbModel->fetchAll($sql);
                break;
            case PaowangData::FORUM_TAB_MY_VOTE_NO:
            	$dbModel = new DbModel("","");
            	$start = ($page-1)*$perpage;
                $sql = "SELECT p.*
                    FROM pw_post p, pw_post_judge t
		            WHERE
		            t.post_id = p.post_id
		            AND p.forum_id = '$forumId'
		            AND t.user_id = '$userId'
		            AND t.flag = '-1'
		            AND p.parent_id = 0
		            AND p.status = '1'  limit $start,$perpage";
                $list =  $dbModel->fetchAll($sql);
                break;
        }
        
        $rtnarray = array();
        foreach($list as $row){
            
            $postInfo = new Post_PostInfo($row);
            
            $dbModel1 = new DbModel("pw_post_text","post_id");
            $postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));         
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	              
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
            $childList = null;
            $this->loadChilds($postInfo->getPostId(),$childList,false,0);
            
            //$postpicDao = Postpic_PostpicDao::getInstance();
            //$piclists =  $postpicDao->getListByPostId($postInfo->getPostId());
            
            array_push($rtnarray, array(
                  "postInfo"=>$postInfo,
                  "childs"=>$childList
            ));     
        }
        return $rtnarray;
    }
    
    /**
     * @deprecated 
     */
    public function getMyPostPosts($forumId, $userId)
    {
		$dbModel = new DbModel("","");
		$sql = "SELECT   * 
			FROM pw_post
			WHERE
			        forum_id='1'
			        AND status=1
			        AND parent_id = 0
			        AND user_id = '$userId'
			";
        $list =  $dbModel->fetchAll($sql);
        $rtnarray = array();
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	 
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
 		    $childList = null;
			$this->loadChilds($postInfo->getPostId(),$childList,false,0);
			array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "childs"=>$childList
		    )); 
        }
        return $rtnarray;
    }

    /**
     * @deprecated 
     */
    public function getMyReplyPosts($forumId,$userId)
    {
		$dbModel = new DbModel("","");
		$sql = "SELECT
			        *
			FROM pw_post
			WHERE
			        forum_id='$forumId'
			        AND status=1
			        AND parent_id != '0'
			        AND user_id = '$userId'
			";
        $list =  $dbModel->fetchAll($sql);
        $rtnarray = array();
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
 		    $childList = null;
			$this->loadChilds($postInfo->getPostId(),$childList,false,0);
			array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "childs"=>$childList
		    )); 
        }
        return $rtnarray;
    }
  
    /**
     * @deprecated 
     *
     */
    public function getMyVoteYesPosts($forumId,$userId)
    {
		$dbModel = new DbModel("","");
		$sql = "SELECT *
			FROM pw_post p, pw_post_judge t
			WHERE
	        t.post_id = p.post_id
	        AND p.forum_id = '$forumId'
	        AND t.user_id = '$userId'
	        AND t.flag = '1'
	        AND p.parent_id = 0
	        AND p.status = '1'";
        $list =  $dbModel->fetchAll($sql);
        $rtnarray = array();
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    	  	    	        	
 		    $childList = null;
			$this->loadChilds($postInfo->getPostId(),$childList,false,0);
			array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "childs"=>$childList
		    )); 
        }
        return $rtnarray;
    }

    /**
     * @deprecated 
     *
     */
    public function getMyVoteNoPosts($forumId,$userId)
    {
		$dbModel = new DbModel("","");
		$sql = "SELECT p.*
			FROM pw_post p, pw_post_judge t
			WHERE
	        t.post_id = p.post_id
	        AND p.forum_id = '$forumId'
	        AND t.user_id = '$userId'
	        AND t.flag = '-1'
	        AND p.parent_id = 0
	        AND p.status = '1'";
        $list =  $dbModel->fetchAll($sql);
        $rtnarray = array();
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
	    	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	  	    	        	
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(\s+)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
 		    $childList = null;
			$this->loadChilds($postInfo->getPostId(),$childList,false,0);
			array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "childs"=>$childList
		    )); 
        }
        return $rtnarray;
    }
    
    public function updateUserPostType($userPostTypeId,$postIdStr)
    {
		$dbModel = new DbModel("","");
		$sql = "update pw_post set user_post_type_id=$userPostTypeId where post_id in ($postIdStr)";
        $dbModel->query($sql);
    }
    
    
	/**
     * Read uploaded picture information from global variable $_FILES
     *
     * @param int $postId
     * @param string The parameter name which is defined in the uplaod form
     */
	public static function uploadPictures($postId, $formFiles,$formFileName)
	{
		$config = Zend_Registry::get('config');
		$log = Zend_Registry::get('log');
		$log->debug("into upload function");
		$picIdArray= array();
		$log->debug("file:".Zend_Json::encode($formFiles));
		foreach($formFiles as $key=>$picfiles)
		{
            $log->debug("key:".$key);
            if($key!=$formFileName){
            	continue;
            }
            if($key==$formFileName){
               $log->debug("formFile count:".count($picfiles['name']));
               $count = count($picfiles['name']);
               for($i=0;$i<$count;$i++){
		            $log->debug("picfiles['name'])".$picfiles['name'][$i]); 
					if (empty($picfiles['name'][$i]))
					{
						continue;
					}
					$log->debug($picfiles['name'][$i]);
					$pic_size = $picfiles['size'][$i];
					$pic_org_name = $picfiles['name'][$i];
					$log->debug("pic_org_name:".$pic_org_name);
					$orig_arr = explode(".", $pic_org_name);
					$nr    = count($orig_arr);
					$file_ext  = strtolower(trim($orig_arr[$nr-1]));
					$log->debug("file_ext:".Zend_Json::encode($file_ext));
					$pic_allow_files = Zend_Registry::get('PW_UPLOAD_PIC_MAP');
					
					$time_path = date("Y")."/".date("Ym")."/".date("Ymd")."/";
					
					
					$log->debug("time_path:".$time_path);
					$file_path = $config->upload->root . $config->upload->path . $time_path;
					$log->debug("file_path:".$file_path);
					if (!file_exists($file_path)){
						Zend_Search_Lucene_Storage_Directory_Filesystem::mkdirs($file_path);
					}
		            
					if (!in_array($file_ext,$pic_allow_files))
					{
						continue;
					}
					
					
					$log->debug("picfiles tmp:".$picfiles['tmp_name'][$i]);
					
					
					if (!is_uploaded_file($picfiles['tmp_name'][$i]))
					{
						continue;
					}
					
					
					
		            //info
		            $postpicInfo = new Postpic_PostpicInfo();
		            $postpicInfo->setPostId($postId);
		            $postpicInfo->setPicSize($pic_size);
		            $postpicInfo->setPicDomain($config->picfiles->domain);
		            $postpicInfo->setPicOrgName($pic_org_name);
		            //$postpicInfo->setUserPicTypeId($userPicTypeId);
		            
		            //dao
		            $postpicDao = Postpic_PostpicDao::getInstance();
		            $postpicDao->add($postpicInfo);
		
					// first time save picture and get id
		            $picId = $postpicInfo->getPostPicId(); 
					// use picture id as part of the file name
					$pic_name = $postId."_".$picId.".".$file_ext;
					$uploadfile = $file_path.$pic_name;
					
					$log->debug("uploadfile:".$uploadfile);
					// move picture file to the target path
					if (move_uploaded_file($picfiles['tmp_name'][$i], $uploadfile) == false)
					{
						continue;
					}
					$output_filename1 = $postId."_".$picId."_".$config->thumb1->width.".".$file_ext;
					$thumb1_file = self::mkThumb($pic_name,$file_ext,$file_path,$config->thumb1->width,$config->thumb1->height,$file_path.$output_filename1);
		
					$output_filename2 = $postId."_".$picId."_".$config->thumb2->width.".".$file_ext;
					//$thumb2_file = self::mkThumb($pic_name,$file_ext,$file_path,$config->thumb2->width,$config->thumb2->height,$file_path.$output_filename2);
		
					$output_filename3 = $postId."_".$picId."_".$config->thumb3->width.".".$file_ext;
					//$thumb3_file = self::mkThumb($pic_name,$file_ext,$file_path,$config->thumb3->width,$config->thumb3->height,$file_path.$output_filename3);
		
		
		            $postpicInfo->setPicName($pic_name);
		            $postpicInfo->setPicPath($config->upload->path.$time_path.$pic_name);
		            $postpicInfo->setPicThumb($config->upload->path.$time_path.$output_filename1);
		            $postpicInfo->setPicThumb2($config->upload->path.$time_path.$output_filename2);
		            $postpicInfo->setPicThumb3($config->upload->path.$time_path.$output_filename3);
		            
		            $postpicDao->modify($postpicInfo);
		               
					
					$picIdArray[] = $picId;
               	  
               }//end for i
            }

		}//end foreach
		
		return $picIdArray;

	}

	/**
     * 缩图
     * @param image_filename 图片名，不含路径
     * @paramformat 图片格式
     * @return 缩图文件名(含路径)
     */

	/**
     * @param
     */
	public static function mkThumb(
	$image_filename,
	$format='jpeg',
	$file_path,
	$width,
	$height,
	$output_filename)
	{
		$config = Zend_Registry::get('config');
		$input_filename = $file_path.$image_filename;

		$img_info = getimagesize($input_filename);
		
		if(intval($img_info[0])>intval($img_info[1])){
			$thumb_info = phpthumb_functions::ProportionalResize($img_info[0],$img_info[1],$width,false);
		}else{
		    $thumb_info = phpthumb_functions::ProportionalResize($img_info[0],$img_info[1],false,$height);		   
		}
		//$thumb_info = phpthumb_functions::ProportionalResize($img_info[0],$img_info[1],$width,false);
		//        $log->debug("filename=".$input_filename." output=".$output_filename." getsize width=".$thumb_info[0]." height=".$thumb_info[1]);

		$phpThumb = Zend_Registry::get('phpThumb');
		$phpThumb = new phpThumb();
		$phpThumb->config_document_root = $file_path;
		$phpThumb->setSourceFilename($input_filename);
		$phpThumb->w = $thumb_info[0];
		$phpThumb->h = $thumb_info[1];
		$phpThumb->q = $config->thumb->quality;
		$phpThumb->f = $format;
		$phpThumb->zc = 1;

		$phpThumb->config_output_format    = $format;

		if ($phpThumb->GenerateThumbnail()) {
			if ($phpThumb->RenderToFile($output_filename)) {
				//                $log->debug("successfully render to file");
				// do something on success
			} else {
				//                $log->debug("failed to render to file");
				// do something with debug/error messages
				return 0;
			}
		} else {
			//            $log->debug("failed to generate thumbnail");
			// do something with debug/error messages
			return 0;
		}
		return 1;
	}


    public function getNext($forumId,$postId){
    	$dbModel = new DbModel("","");
		$sql = "select max(post_id) from pw_post where forum_id=$forumId and parent_id=0  and status=1  and post_id<$postId";
		$postIdNext = intval($dbModel->fetchOne($sql));
		
        $log = Zend_Registry::get('log');
        //$log->debug("postIdNext:$postIdNext");		
		$rtn = null;
		if($postIdNext>0){
			$rtn = $this->get($postIdNext); 
		}
    	return $rtn;
    }

    public function getPrevious($forumId,$postId){
    	$dbModel = new DbModel("","");
		$sql = "select min(post_id) from pw_post where forum_id=$forumId  and parent_id=0 and status=1 and post_id>$postId";
		$postIdPrevious = $dbModel->fetchOne($sql);
		$log = Zend_Registry::get('log');
		//$log->debug("postIdPrevious:$postIdPrevious");
		$rtn = null;
		if($postIdPrevious>0){
			$rtn = $this->get($postIdPrevious); 
		}
    	return $rtn;
    	
    }
    
    public function addClicks($postId){
    	// randomly add click by 3 in 33% of possibility
    	if($this->shouldAddClick()){
	    	$dbModel = new DbModel("","");
			$sql = "update pw_post set click=click+3 where post_id=".$postId;
			Logging::debug("----------working sql is:" . $sql);
			$postIdPrevious = $dbModel->query($sql);
    	}
    }

    // hack: init random click number (sommehow the add() doesn't work)
    public function initClick($postId){
        // randomly add click by 3 in 33% of possibility
        $randomNumber = rand(0, 2);
        if(intval($randomNumber) == 0){
            return;	
        }
        Logging::debug("random number is " . $randomNumber);
        $dbModel = new DbModel("","");
        $sql = "update pw_post set click=click+" . $randomNumber . " where post_id=".$postId;
        Logging::debug("sql is " . $sql);
        $postIdPrevious = $dbModel->query($sql);
    }    
    
    private function shouldAddClick(){
    	$randomNumber = rand(1, 3);
    	if($randomNumber == 1){
    		return true;
    	}
    	false;
    }
            
    /**
     * 贴子显示信息。不同的type返回不同组织方式的贴子列表。
     */
    public function getListByForumIdAll(
        $forumId = PaowangData::FORUM_ID_DEFAULT,
        $perpage = PaowangData::FORUM_POSTS_PER_PAGE, 
        $page = 1,
        $type = PaowangData::FORUM_TAB_ALL){
        
		$dbModel = new DbModel("pw_post","post_id");
        $list;
        
        switch($type){
            case PaowangData::FORUM_TAB_ALL:
            	$list = $dbModel->getList("forum_id=$forumId and parent_id=0  ", $perpage, $page, "*");
            	break;
            case PaowangData::FORUM_TAB_NEW:
            	$list = $dbModel->getList("forum_id=$forumId and parent_id=0  ", $perpage, $page, "*");
            	break;
            case PaowangData::FORUM_TAB_NEWREPLY:
            	$list = $dbModel->getList("forum_id=$forumId  ", $perpage, $page, "*", "create_at desc");
                // 从旧paowang程序里copy过来的sql语句
            	//$sql = "select p2.* from ( select distinct  top_id from (select  top_id from pw_post where forum_id=".$forumId." order by create_at desc)  t  limit ".($pageId-1)*$numberPerPage.",".$numberPerPage.")p1 ,pw_post p2 where p1.top_id=p2.post_id ";
            	//$newReplyDbModel = new DbModel("",""); 
            	//$list = $newReplyDbModel->fetchAll($sql);
            	break;
            case PaowangData::FORUM_TAB_WAKEUP:
            	$time_one_day = time() - 1 * 24 * 60 *60;
                $time_three_day = time() - 3 * 24 * 60 *60; 
                $wakeupDbModel = new DbModel("","");  
                $sql = "SELECT *
                    FROM pw_post p
                    JOIN pw_post t ON p.top_id = t.post_id
                    WHERE   
                        p.forum_id = '$forumId'
                        AND p.create_at > '$time_one_day'
                        AND t.create_at < '$time_three_day'";
                $list =  $wakeupDbModel->fetchAll($sql);            	
            	break;
        }
        
        $rtnarray = array();
        foreach($list as $row){
        	
        	$postInfo = new Post_PostInfo($row);
        	
			$dbModel1 = new DbModel("pw_post_text","post_id");
        	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));        	
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	   		    
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
 		    $childList = null;
			$this->loadChilds($postInfo->getPostId(),$childList,false,$isManage);
		    
		    $postpicDao = Postpic_PostpicDao::getInstance();
			$piclists =  $postpicDao->getListByPostId($postInfo->getPostId());
            
		    array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "childs"=>$childList,
		    	  "piclists"=>$piclists,
		    )); 	
        }
        return $rtnarray;
    }
     
    function loadChildsAll($postId,&$postInfoArray = null,$includeTop = false){ 
		$dbModel = new DbModel("pw_post","post_id");
        $list =  null;
        if($postInfoArray == null){
           $postInfoArray = array();
        }
        if($includeTop && count($postInfoArray)==0){   
           $list = $dbModel->getList("post_id=$postId ",0,0,"*","post_id desc");
        }else{
           $list = $dbModel->getList("parent_id=$postId ",0,0,"*","post_id desc");	
        }
        foreach($list as $row){
        	$postInfo = new Post_PostInfo($row);
			$dbModel1 = new DbModel("pw_post_text","post_id");
        	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	          	
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
        	array_push($postInfoArray, $postInfo);
        	$this->loadChilds($postInfo->getPostId(),$postInfoArray,false,$isManage);
        }
	}
	
    public function getListAllCountByForumIdAll($forumId, $type = PaowangData::FORUM_TAB_ALL){
        $dbModel = new DbModel("pw_post","post_id");   	
        return $dbModel->getListCount(" forum_id=$forumId and parent_id=0");
    }
    
    public function getChildPostsCount($topId){
        $dbModel = new DbModel("pw_post","post_id");   	
        return $dbModel->getListCount(" top_id=$topId and status=1");
    } 
    
    public function addToTop($postId){
        $dbModel = new DbModel("","");
        $sql = "update pw_post set is_ceil=1 where post_id=".$postId;
        $postIdPrevious = $dbModel->query($sql);
        
        Cache_Engine::clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array(Cache_Engine::TAG_FORUM_TOP));
    }

    public function undoTop($postId){
        $dbModel = new DbModel("","");
        $sql = "update pw_post set is_ceil=0 where post_id=".$postId;
        $postIdPrevious = $dbModel->query($sql);
        
        Cache_Engine::clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array(Cache_Engine::TAG_FORUM_TOP));
    }
    

    public function getListByTagName($tagName, $perpage, $page){
        
		$dbModel = new DbModel("","");
		$start = ($page-1)*$perpage;
        $list = $dbModel->fetchAll("select p.* from pw_post p,pw_tag t where t.tag_name='$tagName' and p.post_id=t.post_id and p.status=1 order by p.post_id desc limit $start,$perpage");
        
        $rtnarray = array();
        $isManage = false;
        foreach($list as $row){
        	
        	$postInfo = new Post_PostInfo($row);
        	
			$dbModel1 = new DbModel("pw_post_text","post_id");
        	$postInfo->loadPosttextInfo($dbModel1->getRow($postInfo->getPostId()));        	
        	$sql = "select count(*) from pw_post_pic where status=1 and post_id=".$postInfo->getPostId();
		    $count = intval($dbModel->fetchOne($sql));
		    $postInfo->setPicCount($count);	   		    
		    if($postInfo->getPicCount()==0){
	
				$count = preg_match('/<img(.*?)src=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\"/i',$postInfo->getHtml());
				$postInfo->setPicCount($count); 
		    }		    
 		    $childList = null;
			$this->loadChilds($postInfo->getPostId(),$childList,false,$isManage);
		    
		    $postpicDao = Postpic_PostpicDao::getInstance();
			$piclists =  $postpicDao->getListByPostId($postInfo->getPostId());
            
		    array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "childs"=>$childList,
		    	  "piclists"=>$piclists,
		    )); 	
        }
        return $rtnarray;
    }


    public function getListByTagNameAllCount($tagName){
        $sql = "select count(*) from pw_post p,pw_tag t where t.tag_name='$tagName' and p.post_id=t.post_id  and p.status=1 ";
        $dbModel = new DbModel("","");   	
        return $dbModel->fetchOne($sql);
    }
    
    /*
     * 根据postid得到其topId，移植数据计算
     */
    public function getTopId($postId){
		$dbModel = new DbModel("","");
		$sql = "select parent_id from pw_post where post_id=".$postId;
        $pId =  intval($dbModel->fetchOne($sql));
        if($pId==0){
        	return $postId;
        }else{
            return $this->getTopId($pId);        	
        }
    }
    
    /*
     * 根据postid得到其depth，移植数据计算
     */
    public function getDepth($postId){
		$dbModel = new DbModel("","");
		$sql = "select parent_id from pw_post where post_id=".$postId;
        $pId =  intval($dbModel->fetchOne($sql));
        $depth = 1;
        while($pId!=0){
			$sql = "select parent_id from pw_post where post_id=".$pId;
	        $pId =  intval($dbModel->fetchOne($sql));
	        $depth++;        	
        }
        return $depth;        	
        
    }  
    
    /*
     * 根据postid得到其是否有图片，移植数据计算
     */
    public function getIsPic($postId){
		$dbModel = new DbModel("","");
		$table = "";
		$msgId = 0;
		if($postId>1000000 && $postId<2000000){
			$table = "msg_paowang";
			$msgId = $postId - 1000000;
		}else if($postId>2000000  &&  $postId<3000000){
			$table = "msg_qin";
			$msgId = $postId - 2000000;
		}else if($postId>3000000  &&  $postId<4000000){
			$table = "msg_inn";
			$msgId = $postId - 3000000;			
		}else if($postId>4000000  &&  $postId<5000000){
			$table = "msg_weapon";
			$msgId = $postId - 4000000;
		}else if($postId>5000000  &&  $postId<6000000){
			$table = "msg_music";
			$msgId = $postId - 5000000;
		}else if($postId>6000000  &&  $postId<7000000){
			$table = "msg_money";
			$msgId = $postId - 6000000;
		}else if($postId>7000000  &&  $postId<8000000){
			$table = "msg_it";
			$msgId = $postId - 7000000;
		}else if($postId>8000000  &&  $postId<9000000){
			$table = "msg_parenting";
			$msgId = $postId - 8000000;
		}else if($postId>9000000  &&  $postId<10000000){
			$table = "msg_photo";
			$msgId = $postId - 9000000;
		}
		if($msgId != 0){
			$sql = "select count(*) from $table where msgid=".$msgId." and mm_url!=''";
	        $rtn =  intval($dbModel->fetchOne($sql));
        }else{
        	$rtn = 0;
        }        
        return $rtn;        	
    }       


    /*
     * 根据username得到其user_id，移植数据计算
     */
    public function getUserId($userName){
		$dbModel = new DbModel("","");
		$sql = "SELECT min(user_id) from pw_user where user_name='".trim($userName)."'";
        $rtn =  intval($dbModel->fetchOne($sql));
        if($rtn ==0){
        	$rtn = -1;
        }	
        //$log = Zend_Registry::get('log');
        //$log->debug("###:".$sql);
        //$log->debug("###getUserId:".$rtn);        
        return $rtn;        	
    }       

    /*
     * 根据postid得到其click，移植数据计算
     */
    public function getClick($postId){
		$dbModel = new DbModel("","");
		$table = "";
		$msgId = 0;
		if($postId>1000000 && $postId<=1575676){
			$table = "msg_paowang";
			$msgId = $postId - 1000000;
		}else if($postId>1575676  &&  $postId<2000000){
			$table = "msg_paowang1";
			$msgId = $postId - 1000000;
		}else if($postId>2000000  &&  $postId<3000000){
			$table = "msg_qin";
			$msgId = $postId - 2000000;
		}else if($postId>3000000  &&  $postId<4000000){
			$table = "msg_inn";
			$msgId = $postId - 3000000;			
		}else if($postId>4000000  &&  $postId<5000000){
			$table = "msg_weapon";
			$msgId = $postId - 4000000;
		}else if($postId>5000000  &&  $postId<6000000){
			$table = "msg_music";
			$msgId = $postId - 5000000;
		}else if($postId>6000000  &&  $postId<7000000){
			$table = "msg_money";
			$msgId = $postId - 6000000;
		}else if($postId>7000000  &&  $postId<8000000){
			$table = "msg_it";
			$msgId = $postId - 7000000;
		}else if($postId>8000000  &&  $postId<9000000){
			$table = "msg_parenting";
			$msgId = $postId - 8000000;
		}else if($postId>9000000  &&  $postId<10000000){
			$table = "msg_photo";
			$msgId = $postId - 9000000;
		}
        
        if($msgId!=0){		
			$sql = "select min(hits) from $table where msgid=".$msgId;
	        //echo $sql;
	        $rtn =  intval($dbModel->fetchOne($sql));
        }else{
        	$rtn = 0;
        }
        return $rtn;   
    }
    
    /*
     * 根据postid得到其最后更新时间，移植数据计算
     */
    public function getLastChildUpdateAt($postId){
		$dbModel = new DbModel("","");
		$sql = "select max(post_id) from pw_post where top_id=".$postId;
        $lastReplyId =  intval($dbModel->fetchOne($sql));
        $sql = "select min(create_at) from pw_post where post_id=".$lastReplyId;
        $rtn =  intval($dbModel->fetchOne($sql));
        return $rtn;   
    }
    
    /*
     * 根据postid得到其最后更新时间，移植数据计算
     */
    public function getChildPosts($postId){
		$dbModel = new DbModel("","");
		$sql = "select count(*) from pw_post where top_id=".$postId;
        $rtn =  intval($dbModel->fetchOne($sql));
        return $rtn;   
    }
    
	/**
     * Read uploaded picture information from global variable $_FILES
     *
     * @param int $postId
     * @param string The parameter name which is defined in the uplaod form
     */
	public static function upgradePictures($topId,$msgId,$tableName,$forumId,$orgFileName)
	{
		$config = Zend_Registry::get('config');
		$log = Zend_Registry::get('log');
		$log->debug("into upgradePictures function");
		$picIdArray= array();
		            
         $log->debug($orgFileName);
         //$pathold = "/root/paowang-backup/www/infodaily/htdocs".$orgFileName;
         $pathold = "/www/backup/infodaily/infodaily/htdocs".$orgFileName;
         
         echo $pathold."<br />";
         $pathold = urldecode("/www/backup/infodaily/infodaily/htdocs".$orgFileName);
         echo $pathold."<br />";
         $pathold = iconv("UTF-8", "GB2312",$pathold);
         echo $pathold."<br />";
         ///usr/local/apache2/htdocs/newpaowang/uploaded/
         //mv /root/paowang-backup/www/infodaily/htdocs/photo/ /usr/local/apache2/htdocs/newpaowang/
         echo "<br />orgFileName:".$orgFileName;
         
         if(preg_match("/^(.*)(louvreN\.jpg)$/",$orgFileName)){
            $pathold = "/www/backup/infodaily/infodaily/htdocs/photo/uploaded/2002-04-02/louvreN.jpg";
            copy("/www/backup/infodaily/infodaily/htdocs/photo/uploaded/louvreN.jpg", "/www/backup/infodaily/infodaily/htdocs/photo/uploaded/2002-04-02/louvreN.jpg");
            $orgFileName = "/photo/uploaded/2002-04-02/louvreN.jpg";
         }else{
	         if(preg_match("/^(.*)([jpeg|1jpg|2jpg|1|2])$/",$orgFileName)){
	         	echo "<br />match";
	         	//preg_replace("/^(.*)jpeg$/","\${1}jpg",$orgFileName);
	         	copy($pathold, $pathold.".jpg");
	         	$orgFileName = $orgFileName.".jpg";
	         }
         }
         
         echo "<br />orgFileName:".$orgFileName;	

         
         if(preg_match("/^.*uploaded.*((\d{4})-(\d{2})-(\d{2}))\/(.*)$/",$orgFileName,$m)){
         $log->debug($m[1]);
         $log->debug($m[2]);
         $log->debug($m[3]);
         $log->debug($m[4]);
         $log->debug($m[5]);
         $time = mktime(0,0,0,$m[3],$m[4],$m[2]);
         $log->debug($time);

					$log->debug($pathold);
					$pic_size = filesize($pathold);
					$pic_org_name = $m[5];
					$log->debug("pic_org_name:".$pic_org_name);
					$orig_arr = explode(".", $pic_org_name);
					$nr    = count($orig_arr);
					$file_ext  = strtolower(trim($orig_arr[$nr-1]));
					$log->debug("file_ext:".Zend_Json::encode($file_ext));
					$pic_allow_files = Zend_Registry::get('PW_UPLOAD_PIC_MAP');
					
			         $time_path = date("Y",$time)."/".date("Ym",$time)."/".date("Ymd",$time)."/";
			        $log->debug($time_path);										
					$log->debug("time_path:".$time_path);
					$file_path = $config->upload->root . $config->upload->path . $time_path;
					$log->debug("file_path:".$file_path);
					if (!file_exists($file_path)){
						Zend_Search_Lucene_Storage_Directory_Filesystem::mkdirs($file_path);
					}
		            
					if (!in_array($file_ext,$pic_allow_files))
					{
						//continue;
						return ;
					}
					
					
		            //info
		            $postId =  $forumId*1000000+$topId;
		            
		            $postpicDao = Postpic_PostpicDao::getInstance();
		            if($postpicDao->ifHaveUpgrade($postId,$orgFileName)==0){
			            $postpicInfo = new Postpic_PostpicInfo();
			            $postpicInfo->setPostId($postId);
			            $postpicInfo->setPicSize($pic_size);
			            $postpicInfo->setPicDomain($config->picfiles->domain);
			            $postpicInfo->setPicOrgName($orgFileName);
			            //$postpicInfo->setUserPicTypeId($userPicTypeId);
			            
			            //dao
			            
			            $postpicDao->add($postpicInfo);
			
						// first time save picture and get id
			            $picId = $postpicInfo->getPostPicId(); 
						// use picture id as part of the file name
						$pic_name = $postId."_".$picId.".".$file_ext;
						$uploadfile = $file_path.$pic_name;
						
						$log->debug("uploadfile:".$uploadfile);
						//copy
						$log->debug("pathold:".$pathold); 
						$log->debug("uploadfile:".$uploadfile);
						
	//					echo "cp $pathold $uploadfile"."<br />";
	//					system("cp $pathold $uploadfile",$msg);
	//					echo $msg;
						if (copy($pathold, $uploadfile) == false)
						{
							//continue;
						}
						$output_filename1 = $postId."_".$picId."_".$config->thumb1->width.".".$file_ext;
						$thumb1_file = self::mkThumb($pic_name,$file_ext,$file_path,$config->thumb1->width,$config->thumb1->height,$file_path.$output_filename1);
			
						$output_filename2 = $postId."_".$picId."_".$config->thumb2->width.".".$file_ext;
						//$thumb2_file = self::mkThumb($pic_name,$file_ext,$file_path,$config->thumb2->width,$config->thumb2->height,$file_path.$output_filename2);
			
						$output_filename3 = $postId."_".$picId."_".$config->thumb3->width.".".$file_ext;
						//$thumb3_file = self::mkThumb($pic_name,$file_ext,$file_path,$config->thumb3->width,$config->thumb3->height,$file_path.$output_filename3);
			
			
			            $postpicInfo->setPicName($pic_name);
			            $postpicInfo->setPicPath($config->upload->path.$time_path.$pic_name);
			            $postpicInfo->setPicThumb($config->upload->path.$time_path.$output_filename1);
			            $postpicInfo->setPicThumb2($config->upload->path.$time_path.$output_filename2);
			            $postpicInfo->setPicThumb3($config->upload->path.$time_path.$output_filename3);
			            
			            $postpicDao->modify($postpicInfo);
						$picIdArray[] = $picId;
		            }//end if ifHaveUpgrade
		    }
		return $picIdArray;

	}    

    /*
     * 递归转换所有的图片
     */
    function upgradePicturesThread($topId,$msgId,$tableName,$forumId,&$msgRowArray = null){
        $list =  null;
        if($msgRowArray == null){
           $msgRowArray = array();
        }
        $ds = new DbModel("","");
        /*
        if(count($msgRowArray)==0){   
           $sql = "select msgid,subject,mm_url from $tableName where msgid=$msgId";
           //echo $sql."<br />";
           $list = $ds->fetchAll($sql);  
        }
        else{
           $sql = "select msgid,subject,mm_url from $tableName where msgid=$msgId or parent=$msgId order by msgid";
           //echo $sql."<br />";
           $list = $ds->fetchAll($sql);        	
        }
        */
        $sql = "select  distinct mm_url mm_url from $tableName where (msgid=$msgId or parent=$msgId) and mm_url like '/photo/%' order by msgid";
        //只处理江湖色里的帖子 and mm_url like '/photo/%'
        $list = $ds->fetchAll($sql);        	
        foreach($list as $row){
            echo $msgId."<br />";
            if($row['mm_url']!=''){
               $this->upgradePictures($topId,$msgId,$tableName,$forumId,$row['mm_url']);
            }
            $msgRowArray[] = $row;
        	//$this->upgradePicturesThread($topId,$row['msgid'],$tableName,$forumId,$msgRowArray);
        }
	}
	
	
	/**
     * 转换图片到受高度限制
     *
     * @param int $postId
     * @param string The parameter name which is defined in the uplaod form
     */
	public static function upgradePicturesHeight($postId,$picPath)
	{
		$config = Zend_Registry::get('config');
		$log = Zend_Registry::get('log');
		$log->debug("into upgradePicturesHeight function");
		$picIdArray= array();
		            
         $log->debug($picPath);
         $orgFileNameFull = "/www/site".$picPath;
         echo "<br />orgFileNameFull:".$orgFileNameFull;

         $path_parts = pathinfo($orgFileNameFull);
         $orgDirName = $path_parts["dirname"];
		 $orgFileBaseName = $path_parts["basename"];
		 $file_ext = $path_parts["extension"];
		 
		 $pic_name = $orgFileBaseName;
		 
		 //echo "<br />".$pic_name;
		 
		 $destDirName = str_replace("picture", "pictureh", $orgDirName);
         $destFileNameFull = str_replace("picture", "pictureh", $orgFileNameFull);
         
         $file_path	 = $destDirName."/";		
         //echo "<br />file_path".$file_path;         
					$log->debug("orgFileNameFull:".$orgFileNameFull);
					$log->debug("destDirName:".$destDirName);
					$log->debug("destFileNameFull:".$destFileNameFull);
					$log->debug("pic_name:".$pic_name);
					$log->debug("file_path:".$file_path);
					if (!file_exists($destDirName)){
						Zend_Search_Lucene_Storage_Directory_Filesystem::mkdirs($destDirName);
					}
		            
		            //info
		            
						copy($orgFileNameFull, $destFileNameFull);
						
						$output_filename1 =  str_replace(".", "_".$config->thumb1->width.".", $orgFileBaseName); 
						$thumb1_file = self::mkThumb($pic_name,$file_ext,$file_path,$config->thumb1->width,$config->thumb1->height,$file_path.$output_filename1);
			
						$output_filename2 =  str_replace(".", "_".$config->thumb2->width.".", $orgFileBaseName);
						//$thumb2_file = self::mkThumb($pic_name,$file_ext,$file_path,$config->thumb2->width,$config->thumb2->height,$file_path.$output_filename2);
			
						$output_filename3 =  str_replace(".", "_".$config->thumb3->width.".", $orgFileBaseName);
						//$thumb3_file = self::mkThumb($pic_name,$file_ext,$file_path,$config->thumb3->width,$config->thumb3->height,$file_path.$output_filename3);
			
			
		//return $picIdArray;

	}    

	public static function uploadPicturesOne($postId, $picfiles)
	{
		$config = Zend_Registry::get('config');
		$log = Zend_Registry::get('log');
		$log->debug("into upload function");
		//$picIdArray= array();
		$rtn = array('error' => 'upload file too big or something!');
		//$log->debug("file:".Zend_Json::encode($formFiles));
               $log->debug("formFile count:".count($picfiles['name']));
               $count = count($picfiles['name']);
               $log->debug("picfiles['name']):".$picfiles['name']);
               $i = "myfile";
		            $log->debug("picfiles['name']):".$picfiles['name'][$i]); 
					if (empty($picfiles['name']))
					{
						continue;
					}
					$log->debug($picfiles['name']);
					$pic_size = $picfiles['size'];
					$pic_org_name = $picfiles['name'];
					$log->debug("pic_org_name:".$pic_org_name);
					$orig_arr = explode(".", $pic_org_name);
					$nr    = count($orig_arr);
					$file_ext  = strtolower(trim($orig_arr[$nr-1]));
					$log->debug("file_ext:".Zend_Json::encode($file_ext));
					$pic_allow_files = Zend_Registry::get('PW_UPLOAD_PIC_MAP');
					
					$time_path = date("Y")."/".date("Ym")."/".date("Ymd")."/";
					
					
					$log->debug("time_path:".$time_path);
					$file_path = $config->upload->root . $config->upload->path . $time_path;
					$log->debug("file_path:".$file_path);
					if (!file_exists($file_path)){
						Zend_Search_Lucene_Storage_Directory_Filesystem::mkdirs($file_path);
					}
		            
					if (!in_array($file_ext,$pic_allow_files))
					{
						continue;
					}
					
					
					$log->debug("picfiles tmp:".$picfiles['tmp_name']);
					
					
					if (!is_uploaded_file($picfiles['tmp_name']))
					{
						continue;
					}
					
					
					
		            //info
		            $postpicInfo = new Postpic_PostpicInfo();
		            $postpicInfo->setPostId($postId);
		            $postpicInfo->setPicSize($pic_size);
		            $postpicInfo->setPicDomain($config->picfiles->domain);
		            $postpicInfo->setPicOrgName($pic_org_name);
		            //$postpicInfo->setUserPicTypeId($userPicTypeId);
		            
		            //dao
		            $postpicDao = Postpic_PostpicDao::getInstance();
		            $postpicDao->add($postpicInfo);
		
					// first time save picture and get id
		            $picId = $postpicInfo->getPostPicId(); 
					// use picture id as part of the file name
					$pic_name = $postId."_".$picId.".".$file_ext;
					$uploadfile = $file_path.$pic_name;
					
					$log->debug("uploadfile:".$uploadfile);
					// move picture file to the target path
					if (move_uploaded_file($picfiles['tmp_name'], $uploadfile) == false)
					{
						continue;
					}
					$output_filename1 = $postId."_".$picId."_".$config->thumb1->width.".".$file_ext;
					$thumb1_file = self::mkThumb($pic_name,$file_ext,$file_path,$config->thumb1->width,$config->thumb1->height,$file_path.$output_filename1);
		
					$output_filename2 = $postId."_".$picId."_".$config->thumb2->width.".".$file_ext;
					//$thumb2_file = self::mkThumb($pic_name,$file_ext,$file_path,$config->thumb2->width,$config->thumb2->height,$file_path.$output_filename2);
		
					$output_filename3 = $postId."_".$picId."_".$config->thumb3->width.".".$file_ext;
					//$thumb3_file = self::mkThumb($pic_name,$file_ext,$file_path,$config->thumb3->width,$config->thumb3->height,$file_path.$output_filename3);
		
		
		            $postpicInfo->setPicName($pic_name);
		            $postpicInfo->setPicPath($config->upload->path.$time_path.$pic_name);
		            $postpicInfo->setPicThumb($config->upload->path.$time_path.$output_filename1);
		            $postpicInfo->setPicThumb2($config->upload->path.$time_path.$output_filename2);
		            $postpicInfo->setPicThumb3($config->upload->path.$time_path.$output_filename3);
		            
		            $postpicDao->modify($postpicInfo);
		               
					$rtn = array('success' => $config->general->picture->url .$postpicInfo->getPicThumb(),'postId' => $postId,'postPicId' => $picId);

		
		return $rtn;

	}

        
}
