<?php
/**
 * Post_PostTable
 */

class Post_PostTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_post";
    // 默认主键为’id’
    protected $_primary = "post_id";
}
