<?php
class Postjudge_PostjudgeDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }		
	
    public function get($id){
		$dbModel = new DbModel("pw_post_judge","post_judge_id");
		$row = $dbModel->getRow($id);
		$postjudgeInfo = new Postjudge_PostjudgeInfo($row);
    	return $postjudgeInfo;
    }

    public function load(&$postjudgeInfo){
		$dbModel = new DbModel("pw_post_judge","post_judge_id");
		$row = $dbModel->getRow($postjudgeInfo->getPostJudgeId());
		$postjudgeInfo = new Postjudge_PostjudgeInfo($row);
    }
    
    public function add(&$postjudgeInfo){
        $row['post_id'] = $postjudgeInfo->getPostId();
        $row['user_id'] = $postjudgeInfo->getUserId();
        $row['flag'] = $postjudgeInfo->getFlag();
        $row['create_at'] = $postjudgeInfo->getCreateAt();
        $row['user_ip'] = $postjudgeInfo->getUserIp();
        $row['judge_type'] = $postjudgeInfo->getJudgeType();
	    $table = new Postjudge_PostjudgeTable();
		$id = $table->insert($row);
		$postjudgeInfo->setPostJudgeId($id);
		
		$dbModel = new DbModel("","");
		$sql = "select count(*) from pw_post_judge where post_id=".$postjudgeInfo->getPostId()." and flag=1";
		$voteYes =  $dbModel->fetchOne($sql);
		$sql = "select count(*) from pw_post_judge where post_id=".$postjudgeInfo->getPostId()." and flag=-1";
		$voteNo =  $dbModel->fetchOne($sql);
		$sql = "update pw_post set vote_yes=$voteYes,vote_no=$voteNo where post_id=".$postjudgeInfo->getPostId();
		$dbModel->query($sql);
		
    }

    public function modify(&$postjudgeInfo){
        $row['post_id'] = $postjudgeInfo->getPostId();
        $row['user_id'] = $postjudgeInfo->getUserId();
        $row['flag'] = $postjudgeInfo->getFlag();
        $row['create_at'] = $postjudgeInfo->getCreateAt();
        $row['user_ip'] = $postjudgeInfo->getUserIp();
        $row['judge_type'] = $postjudgeInfo->getJudgeType();

	    $table = new Postjudge_PostjudgeTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("post_judge_id=?", $postjudgeInfo->getPostJudgeId());
		$table->update($row, $where);
    }

    public function del($postjudgeInfo){
		$this->delById($postjudgeInfo->getPostJudgeId());
    }

    public function delById($id){
		$table = new Postjudge_PostjudgeTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('post_judge_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_post_judge","post_judge_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $postjudgeInfoArray = array();
        foreach($list as $row){
        	$postjudgeInfo = new Postjudge_PostjudgeInfo($row);
        	array_push($postjudgeInfoArray, $postjudgeInfo);
        }
        return $postjudgeInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("pw_post_judge","post_judge_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }
    
    public function getListCountByPostAndUser($postId,$userId){
         $dbModel = new DbModel("pw_post_judge","post_judge_id");   	
         return $dbModel->getListCount(" post_id=$postId and user_id=$userId");
    } 

    public function getListCountVoteYes($postId){
         $dbModel = new DbModel("pw_post_judge","post_judge_id");   	
         return $dbModel->getListCount(" post_id=$postId and flag=1");
    }            

    public function getListCountVoteNo($postId){
         $dbModel = new DbModel("pw_post_judge","post_judge_id");   	
         return $dbModel->getListCount(" post_id=$postId and flag=-1");
    }     
}
