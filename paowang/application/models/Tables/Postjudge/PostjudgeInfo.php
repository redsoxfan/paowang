<?php
class Postjudge_PostjudgeInfo
{
    public $postJudgeId;//主键ID
    public $postId;//帖子ID
    public $userId;//用户ID
    public $flag;//标志（1：支持 -1：反对）
    public $createAt;//建创时间
    public $userIp;//用户IP
    public $judgeType;//投票类型

	function __construct($postjudgeArray = null){
        if($postjudgeArray != null){
		   if(empty($postjudgeArray["post_judge_id"])){
		      $this->postJudgeId=null;
	  	   }else{
		      $this->postJudgeId=$postjudgeArray["post_judge_id"];
		   }
		   $this->postId = $postjudgeArray["post_id"];
		   $this->userId = $postjudgeArray["user_id"];
		   $this->flag = $postjudgeArray["flag"];
		   $this->createAt = $postjudgeArray["create_at"];
		   $this->userIp = $postjudgeArray["user_ip"];
		   $this->judgeType = $postjudgeArray["judge_type"];
	   }
	}

	function setPostJudgeId($postJudgeId){
		$this->postJudgeId=$postJudgeId;
	}

	function getPostJudgeId(){
		return $this->postJudgeId;
	}

	function setPostId($postId){
		$this->postId=$postId;
	}

	function getPostId(){
		return $this->postId;
	}

	function setUserId($userId){
		$this->userId=$userId;
	}

	function getUserId(){
		return $this->userId;
	}

	function setFlag($flag){
		$this->flag=$flag;
	}

	function getFlag(){
		return $this->flag;
	}

	function setCreateAt($createAt){
		$this->createAt=$createAt;
	}

	function getCreateAt(){
		return $this->createAt;
	}

	function setUserIp($userIp){
		$this->userIp=$userIp;
	}

	function getUserIp(){
		return $this->userIp;
	}

	function setJudgeType($judgeType){
		$this->judgeType=$judgeType;
	}

	function getJudgeType(){
		return $this->judgeType;
	}

}
?>