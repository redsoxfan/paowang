<?php
/**
 * Postjudge_PostjudgeTable
 */

class Postjudge_PostjudgeTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_post_judge";
    // 默认主键为’id’
    protected $_primary = "post_judge_id";
}
