<?php
class Postpic_PostpicDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }			
	
   public function get($id){
		$dbModel = new DbModel("pw_post_pic","post_pic_id");
		$row = $dbModel->getRow($id);
		$postpicInfo = new Postpic_PostpicInfo($row);
    	return $postpicInfo;
    }

    public function load(&$postpicInfo){
		$dbModel = new DbModel("pw_post_pic","post_pic_id");
		$row = $dbModel->getRow($postpicInfo->getPostPicId());
		$postpicInfo = new Postpic_PostpicInfo($row);
    }
    
    public function add(&$postpicInfo){
        $row['post_id'] = $postpicInfo->getPostId();
        $row['pic_name'] = $postpicInfo->getPicName();
        $row['pic_size'] = $postpicInfo->getPicSize();
        $row['pic_path'] = $postpicInfo->getPicPath();
        $row['pic_domain'] = $postpicInfo->getPicDomain();
        $row['pic_org_name'] = str_replace("'", "''", $postpicInfo->getPicOrgName());
        $row['pic_thumb'] = $postpicInfo->getPicThumb();
        $row['pic_thumb2'] = $postpicInfo->getPicThumb2();
        $row['pic_thumb3'] = $postpicInfo->getPicThumb3();
        $row['user_pic_type_id'] = $postpicInfo->getUserPicTypeId();
        $row['pic_desc'] = $postpicInfo->getPicDesc();
        $row['tags'] = $postpicInfo->getTags();
	    $table = new Postpic_PostpicTable();
		$id = $table->insert($row);
		$postpicInfo->setPostPicId($id);
    }

    public function modify(&$postpicInfo){
        $row['post_id'] = $postpicInfo->getPostId();
        $row['pic_name'] = $postpicInfo->getPicName();
        $row['pic_size'] = $postpicInfo->getPicSize();
        $row['pic_path'] = $postpicInfo->getPicPath();
        $row['pic_domain'] = $postpicInfo->getPicDomain();
        $row['pic_org_name'] = $postpicInfo->getPicOrgName();
        $row['pic_thumb'] = $postpicInfo->getPicThumb();
        $row['pic_thumb2'] = $postpicInfo->getPicThumb2();
        $row['pic_thumb3'] = $postpicInfo->getPicThumb3();
        $row['user_pic_type_id'] = $postpicInfo->getUserPicTypeId();
        $row['pic_desc'] = $postpicInfo->getPicDesc();
        $row['tags'] = $postpicInfo->getTags();

	    $table = new Postpic_PostpicTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("post_pic_id=?", $postpicInfo->getPostPicId());
		$table->update($row, $where);
    }

    public function del($postpicInfo){
		$this->delById($postpicInfo->getPostPicId());
    }

    public function delById($id){
		$table = new Postpic_PostpicTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('post_pic_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_post_pic","post_pic_id");
        $list =  $dbModel->getList(' status=1 ',$perpage,$page);
        $postpicInfoArray = array();
        foreach($list as $row){
        	$postpicInfo = new Postpic_PostpicInfo($row);
        	array_push($postpicInfoArray, $postpicInfo);
        }
        return $postpicInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("pw_post_pic","post_pic_id");   	
         return $dbModel->getListCount(' status=1 ');
    } 

    public function getListByPostId($postId,$perpage=0,$page=0){
		$dbModel = new DbModel("pw_post_pic","post_pic_id");
        $list =  $dbModel->getList(" post_id=$postId ",$perpage,$page,"*","post_pic_id asc");
        $postpicInfoArray = array();
        foreach($list as $row){
        	$postpicInfo = new Postpic_PostpicInfo($row);
        	array_push($postpicInfoArray, $postpicInfo);
        }
        return $postpicInfoArray;
                
    }    
    
    public function getListByForumId($forumId,$perpage=0,$page=0){
		$dbModel = new DbModel("pw_post_pic","post_pic_id");
        $list =  $dbModel->getList(" post_id in ( select post_id from pw_post where forum_id=$forumId and parent_id=0 and status=1) and pic_thumb3!=''",$perpage,$page,"*","post_id desc");
//        $postpicInfoArray = array();
//        foreach($list as $row){
//        	$postpicInfo = new Postpic_PostpicInfo($row);
//        	array_push($postpicInfoArray, $postpicInfo);
//        }
//        return $postpicInfoArray;
        $rtnarray = array();
        $postDao = Post_PostDao::getInstance();
        foreach($list as $row){
            $postpicInfo = new Postpic_PostpicInfo($row);
			$postInfo =  $postDao->get($postpicInfo->getPostId());
		    array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "postpicInfo"=>$postpicInfo
		    )); 	
        }
        return $rtnarray;
    }

    public function getListAllCountByForumId($forumId){
         $dbModel = new DbModel("pw_post_pic","post_pic_id");   	
         return $dbModel->getListCount(" post_id in ( select post_id from pw_post where forum_id=$forumId and parent_id=0  and status=1)  and pic_thumb3!='' ");
    }      
    

    public function getListRowByUserId($userId, $perpage, $page,$sort){
        
		$dbModel = new DbModel("","");
		$start = ($page-1)*$perpage;
		$sql = "select p1.title,p1.user_name,p1.vote_yes,p1.vote_no,p1.create_at,p2.* from pw_post p1,pw_post_pic p2 where (p1.forum_id=9 or p1.forum_id=10) and p1.user_id=$userId and p1.status=1 and p1.post_id=p2.post_id and p2.status=1 ";
		$sql .=" order by p1.$sort desc ";
		$sql .= "  limit $start,$perpage ";
        $list = $dbModel->fetchAll($sql);
        return $list;
    }


    public function getListRowByUserIdCount($userId){
        $sql = "select count(*)from pw_post p1,pw_post_pic p2 where (p1.forum_id=9 or p1.forum_id=10)  and p1.user_id=$userId  and p1.status=1 and p1.post_id=p2.post_id and p2.status=1 ";
        $dbModel = new DbModel("","");   	
        return $dbModel->fetchOne($sql);
    }
        
    public function ifHaveUpgrade($postId,$picOrgName){
        $dbModel = new DbModel("","");   
        $sql = "select count(*) from pw_post_pic where post_id=$postId and pic_org_name='".str_replace("'", "''",$picOrgName)."'";
	    echo $sql;
        return intval($dbModel->fetchOne($sql));
    }
    
    public function findLatestId($userId){
    	$dbModel = new DbModel("","");
        $id =  $dbModel->fetchOne("select pic.post_id from pw_post_pic pic 
            JOIN pw_post post ON pic.post_id = post.post_id 
            where post.user_id=$userId AND
            ((post.forum_id=9 or post.forum_id=10)) AND
            post.status = 1 and pic.status=1 
            ORDER BY pic.post_id DESC LIMIT 1");
        if($id == null) {
        	return -1;
        }
        return intval($id);
    }
    
}
