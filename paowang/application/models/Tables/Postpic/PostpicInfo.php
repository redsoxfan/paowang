<?php
class Postpic_PostpicInfo
{
    public $postPicId;//主键ID
    public $postId;//post主键
    public $picName;//图片名称
    public $picSize;//图片大小
    public $picPath;//图片相对路径
    public $picDomain;//图片路径前辍
    public $picOrgName;//原始文件名
    public $picThumb;//缩略图
    public $picThumb2;//缩略图2
    public $picThumb3;//缩略图3
    public $userPicTypeId;//用户图片类别id
    public $picDesc;//图片描述
    public $tags;//标签

	function __construct($postpicArray = null){
        if($postpicArray != null){
		   if(empty($postpicArray["post_pic_id"])){
		      $this->postPicId=null;
	  	   }else{
		      $this->postPicId=$postpicArray["post_pic_id"];
		   }
		   $this->postId = $postpicArray["post_id"];
		   $this->picName = $postpicArray["pic_name"];
		   $this->picSize = $postpicArray["pic_size"];
		   $this->picPath = $postpicArray["pic_path"];
		   $this->picDomain = $postpicArray["pic_domain"];
		   $this->picOrgName = $postpicArray["pic_org_name"];
		   $this->picThumb = $postpicArray["pic_thumb"];
		   $this->picThumb2 = $postpicArray["pic_thumb2"];
		   $this->picThumb3 = $postpicArray["pic_thumb3"];
		   $this->userPicTypeId = $postpicArray["user_pic_type_id"];
		   $this->picDesc = $postpicArray["pic_desc"];
		   $this->tags = $postpicArray["tags"];
	   }
	}


	function temp2online($temppostpicInfo){
        if($temppostpicInfo != null){
 	       $this->postId=  $temppostpicInfo->getPostId();
		   $this->picName = $temppostpicInfo->getPicName();
		   $this->picSize = $temppostpicInfo->getPicSize();
		   $this->picPath = $temppostpicInfo->getPicPath();
		   $this->picDomain = $temppostpicInfo->getPicDomain();
		   $this->picOrgName = $temppostpicInfo->getPicOrgName();
		   $this->picThumb = $temppostpicInfo->getPicThumb();
		   $this->picThumb2 = $temppostpicInfo->getPicThumb2();
		   $this->picThumb3 = $temppostpicInfo->getPicThumb3();
		   $this->userPicTypeId = $temppostpicInfo->getUserPictypeId();
		   $this->picDesc = $temppostpicInfo->getPicDesc();
		   $this->tags = $temppostpicInfo->getTags();
	   }
	}
	
	function setPostPicId($postPicId){
		$this->postPicId=$postPicId;
	}

	function getPostPicId(){
		return $this->postPicId;
	}

	function setPostId($postId){
		$this->postId=$postId;
	}

	function getPostId(){
		return $this->postId;
	}

	function setPicName($picName){
		$this->picName=$picName;
	}

	function getPicName(){
		return $this->picName;
	}

	function setPicSize($picSize){
		$this->picSize=$picSize;
	}

	function getPicSize(){
		return $this->picSize;
	}

	function setPicPath($picPath){
		$this->picPath=$picPath;
	}

	function getPicPath(){
		return $this->picPath;
	}

	function setPicDomain($picDomain){
		$this->picDomain=$picDomain;
	}

	function getPicDomain(){
		return $this->picDomain;
	}

	function setPicOrgName($picOrgName){
		$this->picOrgName=$picOrgName;
	}

	function getPicOrgName(){
		return $this->picOrgName;
	}

	function setPicThumb($picThumb){
		$this->picThumb=$picThumb;
	}

	function getPicThumb(){
		return $this->picThumb;
	}

	function setPicThumb2($picThumb2){
		$this->picThumb2=$picThumb2;
	}

	function getPicThumb2(){
		return $this->picThumb2;
	}

	function setPicThumb3($picThumb3){
		$this->picThumb3=$picThumb3;
	}

	function getPicThumb3(){
		return $this->picThumb3;
	}

	function setUserPicTypeId($userPicTypeId){
		$this->userPicTypeId=$userPicTypeId;
	}

	function getUserPicTypeId(){
		return $this->userPicTypeId;
	}

	function setPicDesc($picDesc){
		$this->picDesc=$picDesc;
	}

	function getPicDesc(){
		return $this->picDesc;
	}

	function setTags($tags){
		$this->tags=$tags;
	}

	function getTags(){
		return $this->tags;
	}

}
?>