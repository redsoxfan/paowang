<?php
/**
 * Postpic_PostpicTable
 */

class Postpic_PostpicTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_post_pic";
    // 默认主键为’id’
    protected $_primary = "post_pic_id";
}
