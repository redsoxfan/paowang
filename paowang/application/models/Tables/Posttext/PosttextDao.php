<?php
class Posttext_PosttextDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }			
	
    public function get($id){
		$dbModel = new DbModel("pw_post_text","post_id");
		$row = $dbModel->getRow($id);
		$row['html']=$row['html'];
		$posttextInfo = new Posttext_PosttextInfo($row);
    	return $posttextInfo;
    }

    public function load(&$posttextInfo){
		$dbModel = new DbModel("pw_post_text","post_id");
		$row = $dbModel->getRow($posttextInfo->getPostId());
		$row['html']=$row['html'];
		$posttextInfo = new Posttext_PosttextInfo($row);
    }
    
    public function add(&$posttextInfo){
    	$row['post_id'] = $posttextInfo->getPostid();
        $row['text'] = $posttextInfo->getText();
        $row['html'] = $posttextInfo->getHtml();
        $row['html']=$row['html'];

/*        $dbModel = new DbModel("t","");
        $sql = "insert into pw_post_text(post_id,text,html) values(".$row['post_id'].",'".$row['text']."','".$row['html']."')";
        
        $dbModel->query($sql);
        */
	    $table = new Posttext_PosttextTable();
		$id = $table->insert($row);
		$posttextInfo->setPostId($id);
 	    $log = Zend_Registry::get('log');
    }

    public function modify(&$posttextInfo){
        $row['text'] = $posttextInfo->getText();
        $row['html'] = $posttextInfo->getHtml();
        $row['html']=$row['html'];
	    $table = new Posttext_PosttextTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("post_id=?", $posttextInfo->getPostId());
		$table->update($row, $where);
    }

    public function del($posttextInfo){
		$this->delById($posttextInfo->getPostId());
    }

    public function delById($id){
		$table = new Posttext_PosttextTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('post_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_post_text","post_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $posttextInfoArray = array();
        foreach($list as $row){
        	$row['html']=$row['html'];
        	$posttextInfo = new Posttext_PosttextInfo($row);
        	array_push($posttextInfoArray, $posttextInfo);
        }
        return $posttextInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("pw_post_text","post_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     
    
}
