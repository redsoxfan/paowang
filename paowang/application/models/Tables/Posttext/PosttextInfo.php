<?php
class Posttext_PosttextInfo
{
    public $postId;//帖子ID
    public $text;//内容
    public $html;//html

	function __construct($posttextArray = null){
        if($posttextArray != null){
		   if(empty($posttextArray["post_id"])){
		      $this->postId = null;
	  	   }else{
		      $this->postId=$posttextArray["post_id"];
		   }
		   if(empty($posttextArray["text"])){
		   	  $this->text = null;
		   }
		   else{
		      $this->text = $posttextArray["text"];
		   }
		   if(empty($posttextArray["text"])){
		   	  $this->html = null;
		   }
		   else{
		      $this->html = $posttextArray["html"];
		   }
	   }
	}

	function temp2online($tempposttextInfo){
        if($tempposttextInfo != null){
 	       $this->postId=  $tempposttextInfo->getPostId();
		   $this->text = $tempposttextInfo->getText();;
	   }
	}
	function setPostId($postId){
		$this->postId=$postId;
	}

	function getPostId(){
		return $this->postId;
	}

	function setText($text){
		$this->text=$text;
	}

	function getText(){
		return $this->text;
	}

	function setHtml($html){ 
		$this->html=$html;
	}

	function getHtml(){
		return $this->html;
	}

}
?>