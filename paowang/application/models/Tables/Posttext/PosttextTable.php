<?php
/**
 * Posttext_PosttextTable
 */

class Posttext_PosttextTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_post_text";
    // 默认主键为’id’
    protected $_primary = "post_id";
}
