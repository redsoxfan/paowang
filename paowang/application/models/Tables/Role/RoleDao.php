<?php
class Role_RoleDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }				
	
    public function get($id){
		$dbModel = new DbModel("pw_role","role_id");
		$row = $dbModel->getRow($id);
		$roleInfo = new Role_RoleInfo($row);
    	return $roleInfo;
    }

    public function load(&$roleInfo){
		$dbModel = new DbModel("pw_role","role_id");
		$row = $dbModel->getRow($roleInfo->getRoleId());
		$roleInfo = new Role_RoleInfo($row);
    }
    
    public function add(&$roleInfo){
        $row['role_name'] = $roleInfo->getRoleName();
        $row['role_desc'] = $roleInfo->getRoleDesc();
        $row['status'] = $roleInfo->getStatus();
	    $table = new Role_RoleTable();
		$id = $table->insert($row);
		$roleInfo->setRoleId($id);
    }

    public function modify(&$roleInfo){
        $row['role_name'] = $roleInfo->getRoleName();
        $row['role_desc'] = $roleInfo->getRoleDesc();
        $row['status'] = $roleInfo->getStatus();

	    $table = new Role_RoleTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("role_id=?", $roleInfo->getRoleId());
		$table->update($row, $where);
    }

    public function del($roleInfo){
		$this->delById($roleInfo->getRoleId());
    }

    public function delById($id){
		$table = new Role_RoleTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('role_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_role","role_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $roleInfoArray = array();
        foreach($list as $row){
        	$roleInfo = new Role_RoleInfo($row);
        	array_push($roleInfoArray, $roleInfo);
        }
        return $roleInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("pw_role","role_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     
    
}
