<?php
class Role_RoleInfo
{
    public $roleId;//角色ID
    public $roleName;//角色名称
    public $roleDesc;//角色描述
    public $status;//状态

	function __construct($roleArray = null){
        if($roleArray != null){
		   if(empty($roleArray["role_id"])){
		      $this->roleId=null;
	  	   }else{
		      $this->roleId=$roleArray["role_id"];
		   }
		   $this->roleName = $roleArray["role_name"];
		   $this->roleDesc = $roleArray["role_desc"];
		   $this->status = $roleArray["status"];
	   }
	}

	function setRoleId($roleId){
		$this->roleId=$roleId;
	}

	function getRoleId(){
		return $this->roleId;
	}

	function setRoleName($roleName){
		$this->roleName=$roleName;
	}

	function getRoleName(){
		return $this->roleName;
	}

	function setRoleDesc($roleDesc){
		$this->roleDesc=$roleDesc;
	}

	function getRoleDesc(){
		return $this->roleDesc;
	}

	function setStatus($status){
		$this->status=$status;
	}

	function getStatus(){
		return $this->status;
	}

}
?>