<?php
/**
 * Role_RoleTable
 */

class Role_RoleTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_role";
    // 默认主键为’id’
    protected $_primary = "role_id";
}
