<?php
class Tag_TagDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }	
	
    public function get($id){
		$dbModel = new DbModel("pw_tag","tag_id");
		$row = $dbModel->getRow($id);
		$tagInfo = new Tag_TagInfo($row);
    	return $tagInfo;
    }

    public function load(&$tagInfo){
		$dbModel = new DbModel("pw_tag","tag_id");
		$row = $dbModel->getRow($tagInfo->getTagId());
		$tagInfo = new Tag_TagInfo($row);
    }
    
    public function add(&$tagInfo){
        $row['post_id'] = $tagInfo->getPostId();
        $row['tag_name'] = $tagInfo->getTagName();
        $row['status'] = $tagInfo->getStatus();
        $row['user_id'] = $tagInfo->getUserId();
        $row['create_at'] = $tagInfo->getCreateAt();
        $row['user_ip'] = $tagInfo->getUserIp();
        $row['tag_type'] = $tagInfo->getTagType();
        $row['tag_user_id'] = $tagInfo->getTagUserId();
	    $table = new Tag_TagTable();
		$id = $table->insert($row);
		$tagInfo->setTagId($id);
    }

    public function modify(&$tagInfo){
        $row['post_id'] = $tagInfo->getPostId();
        $row['tag_name'] = $tagInfo->getTagName();
        $row['status'] = $tagInfo->getStatus();
        $row['user_id'] = $tagInfo->getUserId();
        $row['create_at'] = $tagInfo->getCreateAt();
        $row['user_ip'] = $tagInfo->getUserIp();
        $row['tag_type'] = $tagInfo->getTagType();
        $row['tag_user_id'] = $tagInfo->getTagUserId();

	    $table = new Tag_TagTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("tag_id=?", $tagInfo->getTagId());
		$table->update($row, $where);
    }

    public function del($tagInfo){
		$this->delById($tagInfo->getTagId());
    }

    public function delById($id){
		$table = new Tag_TagTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('tag_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_tag","tag_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $tagInfoArray = array();
        foreach($list as $row){
        	$tagInfo = new Tag_TagInfo($row);
        	array_push($tagInfoArray, $tagInfo);
        }
        return $tagInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("pw_tag","tag_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     
 
    public function getListAllCountByUserIdAddTagName($userId,$tagName){
         $dbModel = new DbModel("pw_tag","tag_id");   	
         return $dbModel->getListCount(" user_id=$userId and tag_name='$tagName'");
    }
    
    public function getTagTop($count = 10){
    	$dbModel = new DbModel();
    	$sql = "select * from (
			select tag_name,count(*)  count from pw_tag where tag_name!=''  group by tag_name)  p
			order by count desc limit 0,".$count;
    	return $dbModel->fetchAll($sql);
    }
    
    public function getTagArrayByTopId($topId){
    	$dbModel = new DbModel();
    	$sql = "select distinct tag_name from pw_tag where post_id=$topId ";
    	return $dbModel->fetchAll($sql);
    }
    
    public function getListAllCountByUserIdAndCreateUserIdAddTagNameAndTopId($userId,$createUserId,$tagName,$topId){
         $dbModel = new DbModel("pw_tag","tag_id");
         //检查语句，修改为同一个帖子只允许加一次同名标签，暂时不修改函数名，根据需求以后再修改
         //(user_id=$userId or user_id=$createUserId) and 
         return $dbModel->getListCount(" tag_name='$tagName' and post_id=$topId");
    }   
    
    public function delAllByUserIdAndTopId($userId,$topId){
         $dbModel = new DbModel("","");
    	 $sql = "delete from pw_tag where user_id=$userId  and post_id=$topId";            	
         return $dbModel->query($sql);
    }       
    
    public function getTagInfoByPostId($postId){
    	$dbModel = new DbModel("pw_tag","tag_id");
        $list =  $dbModel->getList(" post_id=$postId ");
        $tagInfoArray = array();
        foreach($list as $row){
            $tagInfo = new Tag_TagInfo($row);
            array_push($tagInfoArray, $tagInfo);
        }
        return $tagInfoArray;
    }
    
    public function deleteTag($postId, $userId = 0, $tagName = null){
        $dbModel = new DbModel("","");
        $sql;
        if($userId == 0 && ($tagName == null || $tagName == '')){
            $sql = "delete from pw_tag where post_id=$postId";	
        }
        else{
            $sql = "delete from pw_tag where post_id=$postId  and tag_name='$tagName' and user_id=$userId";
        }             
        return $dbModel->query($sql);    	
    }
        
}
