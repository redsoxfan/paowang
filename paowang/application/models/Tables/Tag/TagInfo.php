<?php
class Tag_TagInfo
{
    public $tagId;//标签ID
    public $postId;//帖子ID
    public $tagName;//标签
    public $status;//状态(1有效0无效)
    public $userId;//用户ID
    public $createAt;//创建时间
    public $userIp;//用户IP
    public $tagType;//tag针对于文章、帖子、图片等
    public $tagUserId;//被加文章、帖子、图片的发布者ID

	function __construct($tagArray = null){
        if($tagArray != null){
		   if(empty($tagArray["tag_id"])){
		      $this->tagId=null;
	  	   }else{
		      $this->tagId=$tagArray["tag_id"];
		   }
		   $this->postId = $tagArray["post_id"];
		   $this->tagName = $tagArray["tag_name"];
		   $this->status = $tagArray["status"];
		   $this->userId = $tagArray["user_id"];
		   $this->createAt = $tagArray["create_at"];
		   $this->userIp = $tagArray["user_ip"];
		   $this->tagType = $tagArray["tag_type"];
		   $this->tagUserId = $tagArray["tag_user_id"];
	   }
	}

	function setTagId($tagId){
		$this->tagId=$tagId;
	}

	function getTagId(){
		return $this->tagId;
	}

	function setPostId($postId){
		$this->postId=$postId;
	}

	function getPostId(){
		return $this->postId;
	}

	function setTagName($tagName){
		$this->tagName=$tagName;
	}

	function getTagName(){
		return $this->tagName;
	}

	function setStatus($status){
		$this->status=$status;
	}

	function getStatus(){
		return $this->status;
	}

	function setUserId($userId){
		$this->userId=$userId;
	}

	function getUserId(){
		return $this->userId;
	}

	function setCreateAt($createAt){
		$this->createAt=$createAt;
	}

	function getCreateAt(){
		return $this->createAt;
	}

	function setUserIp($userIp){
		$this->userIp=$userIp;
	}

	function getUserIp(){
		return $this->userIp;
	}

	function setTagType($tagType){
		$this->tagType=$tagType;
	}

	function getTagType(){
		return $this->tagType;
	}

	function setTagUserId($tagUserId){
		$this->tagUserId=$tagUserId;
	}

	function getTagUserId(){
		return $this->tagUserId;
	}

}
?>