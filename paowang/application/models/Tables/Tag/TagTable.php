<?php
/**
 * Tag_TagTable
 */

class Tag_TagTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_tag";
    // 默认主键为’id’
    protected $_primary = "tag_id";
}
