<?php
class Temppost_TemppostInfo
{
    public $postId;//主键ID
    public $forumId;//论坛ID
    public $userId;//用户ID
    public $userName;//用户名称
    public $parentId;//父ID
    public $topId;//主贴ID
    public $isDigest;//是否精品
    public $title;//标题
    public $depth;//深度
    public $showOrder;//显示顺序
    public $listOrder;//列表顺序
    public $canHaschild;//是否可以跟贴
    public $isTail;//是否叶子节点
    public $status;//状态(1有效0无效)
    public $createAt;//创建时间
    public $userIp;//用户IP
    public $click;//点击数
    public $tags;//标签(未分割)
    public $textLength;//帖子内容长度
    public $updateAt;//新更时间
    public $updateIp;//更新IP
    public $isCeil;//是否置顶
    public $isRelive;//是否死灰复燃
    public $reliveTime;//死灰复燃时间
    public $userPostTypeId;//帖子类别id
    public $childType;//跟帖类型
    public $isPic;//是否原创图片帖子
    public $text = "";//内容
    public $html = "";//html 
    public $voteYes = "";//赞成数
    public $voteNo = "";//反对数
    public $lastChildUpdateAt;//最后子帖更新时间
    public $childPosts;//子帖数
    public $updateUserId;//用户ID
    public $updateUserName;//用户名称
    
       

	function __construct($postArray = null){
		
	    $log = Zend_Registry::get('log');
        if($postArray != null){
		   if(empty($postArray["post_id"])){
		      $this->postId=null;
	  	   }else{
		      $this->postId=$postArray["post_id"];
		   }
		   $this->forumId = $postArray["forum_id"];
		   $this->userId = $postArray["user_id"];
		   $this->userName = $postArray["user_name"];
		   $this->parentId = $postArray["parent_id"];
		   $this->topId = $postArray["top_id"];
		   $this->isDigest = $postArray["is_digest"];
		   $this->title = $postArray["title"];
		   $this->depth = $postArray["depth"];
		   $this->showOrder = $postArray["show_order"];
		   $this->listOrder = $postArray["list_order"];
		   $this->canHaschild = $postArray["can_haschild"];
		   $this->isTail = $postArray["is_tail"];
		   $this->status = $postArray["status"];
		   $this->createAt = $postArray["create_at"];
		   $this->userIp = $postArray["user_ip"];
		   $this->click = $postArray["click"];
		   $this->tags = $postArray["tags"];
		   $this->textLength = $postArray["text_length"];
		   $this->updateAt = $postArray["update_at"];
		   $this->updateIp = $postArray["update_ip"];
		   $this->isCeil = $postArray["is_ceil"];
		   $this->isRelive = $postArray["is_relive"];
		   $this->reliveTime = $postArray["relive_time"];
		   $this->userPostTypeId = $postArray["user_post_type_id"];
		   $this->childType = $postArray["child_type"];
		   $this->isPic = $postArray["is_pic"];
		   $this->voteYes = $postArray["vote_yes"];
		   $this->voteNo = $postArray["vote_no"];
		   $this->lastChildUpdateAt = $postArray["last_child_update_at"];
		   $this->childPosts = $postArray["child_posts"];
		   $this->updateUserId = $postArray["update_user_id"];
 		   $this->updateUserName = $postArray["update_user_name"];
	   }
	}

	function loadPosttextInfo($posttextArray = null){
        if($posttextArray != null){
		   $this->text = $posttextArray["text"];
		   $this->html = $posttextArray["html"];
	   }
	}
	
	function setPostId($postId){
		$this->postId=$postId;
	}

	function getPostId(){
		return $this->postId;
	}

	function setForumId($forumId){
		$this->forumId=$forumId;
	}

	function getForumId(){
		return $this->forumId;
	}

	function setUserId($userId){
		$this->userId=$userId;
	}

	function getUserId(){
		return $this->userId;
	}

	function setUserName($userName){
		$this->userName=$userName;
	}

	function getUserName(){
		return $this->userName;
	}

	function setParentId($parentId){
		$this->parentId=$parentId;
	}

	function getParentId(){
		return $this->parentId;
	}

	function setTopId($topId){
		$this->topId=$topId;
	}

	function getTopId(){
		return $this->topId;
	}

	function setIsDigest($isDigest){
		$this->isDigest=$isDigest;
	}

	function getIsDigest(){
		return $this->isDigest;
	}

	function setTitle($title){
		$this->title=$title;
	}

	function getTitle(){
		return $this->title;
	}

	function setDepth($depth){
		$this->depth=$depth;
	}

	function getDepth(){
		return $this->depth;
	}

	function setShowOrder($showOrder){
		$this->showOrder=$showOrder;
	}

	function getShowOrder(){
		return $this->showOrder;
	}

	function setListOrder($listOrder){
		$this->listOrder=$listOrder;
	}

	function getListOrder(){
		return $this->listOrder;
	}

	function setCanHaschild($canHaschild){
		$this->canHaschild=$canHaschild;
	}

	function getCanHaschild(){
		return $this->canHaschild;
	}

	function setIsTail($isTail){
		$this->isTail=$isTail;
	}

	function getIsTail(){
		return $this->isTail;
	}

	function setStatus($status){
		$this->status=$status;
	}

	function getStatus(){
		return $this->status;
	}

	function setCreateAt($createAt){
		$this->createAt=$createAt;
	}

	function getCreateAt(){
		return $this->createAt;
	}
	

	
	

	function setUserIp($userIp){
		$this->userIp=$userIp;
	}

	function getUserIp(){
		return $this->userIp;
	}

	function setClick($click){
		$this->click=$click;
	}

	function getClick(){
		return $this->click;
	}

	function setTags($tags){
		$this->tags=$tags;
	}

	function getTags(){
		return $this->tags;
	}

	function setTextLength($textLength){
		$this->textLength=$textLength;
	}

	function getTextLength(){
		return $this->textLength;
	}

	function setUpdateAt($updateAt){
		$this->updateAt=$updateAt;
	}

	function getUpdateAt(){
		return $this->updateAt;
	}

	function setUpdateIp($updateIp){
		$this->updateIp=$updateIp;
	}

	function getUpdateIp(){
		return $this->updateIp;
	}

	function setIsCeil($isCeil){
		$this->isCeil=$isCeil;
	}

	function getIsCeil(){
		return $this->isCeil;
	}

	function setIsRelive($isRelive){
		$this->isRelive=$isRelive;
	}

	function getIsRelive(){
		return $this->isRelive;
	}

	function setReliveTime($reliveTime){
		$this->reliveTime=$reliveTime;
	}

	function getReliveTime(){
		return $this->reliveTime;
	}

	function setUserPostTypeId($userPostTypeId){
		$this->userPostTypeId=$userPostTypeId;
	}

	function getUserPostTypeId(){
		return $this->userPostTypeId;
	}

	function setChildType($childType){
		$this->childType=$childType;
	}

	function getChildType(){
		return $this->childType;
	}

	function setIsPic($isPic){
		$this->isPic=$isPic;
	}

	function getIsPic(){
		return $this->isPic;
	}
	
	function setText($text){
		$this->text=$text;
	}

	function getText(){
		return $this->text;
	}

	function setHtml($html){
		$this->html=$html;
	}

	function getHtml(){
		return $this->html;
	}	


	function setVoteYes($voteYes){
		$this->voteYes=$voteYes;
	}

	function getVoteYes(){
		return $this->voteYes;
	}
	
	function setVoteNo($voteNo){
		$this->voteNo=$voteNo;
	}

	function getVoteNo(){
		return $this->voteNo;
	}

	function setLastChildUpdateAt($lastChildUpdateAt){
		$this->lastChildUpdateAt=$lastChildUpdateAt;
	}

	function getLastChildUpdateAt(){
		return $this->lastChildUpdateAt;
	}	


	function setChildPosts($childPosts){
		$this->childPosts=$childPosts;
	}

	function getChildPosts(){
		return $this->childPosts;
	}	

	function setUpdateUserId($updateUserId){
		$this->updateUserId=$updateUserId;
	}

	function getUpdateUserId(){
		return $this->updateUserId;
	}

	function setUpdateUserName($updateUserName){
		$this->updateUserName = $updateUserName;
	}

	function getUpdateUserName(){
		return $this->updateUserName;
	}



}
?>