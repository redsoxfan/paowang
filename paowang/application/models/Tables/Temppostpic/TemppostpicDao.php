<?php
class Temppostpic_TemppostpicDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }			
	
   public function get($id){
		$dbModel = new DbModel("temp_pw_post_pic","post_pic_id");
		$row = $dbModel->getRow($id);
		$postpicInfo = new Temppostpic_TemppostpicInfo($row);
    	return $postpicInfo;
    }

    public function load(&$postpicInfo){
		$dbModel = new DbModel("temp_pw_post_pic","post_pic_id");
		$row = $dbModel->getRow($postpicInfo->getPostPicId());
		$postpicInfo = new Temppostpic_TemppostpicInfo($row);
    }
    
    public function add(&$postpicInfo){
        $row['post_id'] = $postpicInfo->getPostId();
        $row['pic_name'] = $postpicInfo->getPicName();
        $row['pic_size'] = $postpicInfo->getPicSize();
        $row['pic_path'] = $postpicInfo->getPicPath();
        $row['pic_domain'] = $postpicInfo->getPicDomain();
        $row['pic_org_name'] = $postpicInfo->getPicOrgName();
        $row['pic_thumb'] = $postpicInfo->getPicThumb();
        $row['pic_thumb2'] = $postpicInfo->getPicThumb2();
        $row['pic_thumb3'] = $postpicInfo->getPicThumb3();
        $row['user_pic_type_id'] = $postpicInfo->getUserPicTypeId();
        $row['pic_desc'] = $postpicInfo->getPicDesc();
        $row['tags'] = $postpicInfo->getTags();
        $row['edit_flag'] = $postpicInfo->getEditFlag();
	    $table = new Temppostpic_TemppostpicTable();
		$id = $table->insert($row);
		$postpicInfo->setPostPicId($id);
    }

    public function modify(&$postpicInfo){
        $row['post_id'] = $postpicInfo->getPostId();
        $row['pic_name'] = $postpicInfo->getPicName();
        $row['pic_size'] = $postpicInfo->getPicSize();
        $row['pic_path'] = $postpicInfo->getPicPath();
        $row['pic_domain'] = $postpicInfo->getPicDomain();
        $row['pic_org_name'] = $postpicInfo->getPicOrgName();
        $row['pic_thumb'] = $postpicInfo->getPicThumb();
        $row['pic_thumb2'] = $postpicInfo->getPicThumb2();
        $row['pic_thumb3'] = $postpicInfo->getPicThumb3();
        $row['user_pic_type_id'] = $postpicInfo->getUserPicTypeId();
        $row['pic_desc'] = $postpicInfo->getPicDesc();
        $row['tags'] = $postpicInfo->getTags();
        $row['edit_flag'] = $postpicInfo->getEditFlag();        

	    $table = new Temppostpic_TemppostpicTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("post_pic_id=?", $postpicInfo->getPostPicId());
		$table->update($row, $where);
    }

    public function del($postpicInfo){
		$this->delById($postpicInfo->getPostPicId());
    }

    public function delById($id){
		$table = new Temppostpic_TemppostpicTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('post_pic_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("temp_pw_post_pic","post_pic_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $postpicInfoArray = array();
        foreach($list as $row){
        	$postpicInfo = new Temppostpic_TemppostpicInfo($row);
        	array_push($postpicInfoArray, $postpicInfo);
        }
        return $postpicInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("temp_pw_post_pic","post_pic_id");   	
         return $dbModel->getListCount(' 1=1 ');
    } 

    public function getListByPostId($postId,$perpage=0,$page=0,$editFlag = 0){
		$dbModel = new DbModel("temp_pw_post_pic","post_pic_id");
        $list =  $dbModel->getList(" post_id=$postId ",$perpage,$page,"*","post_pic_id asc");
        $postpicInfoArray = array();
        foreach($list as $row){
        	$postpicInfo = new Temppostpic_TemppostpicInfo($row);
        	array_push($postpicInfoArray, $postpicInfo);
        }
        return $postpicInfoArray;
                
    }    
    
    public function getListByForumId($forumId,$perpage=0,$page=0){
		$dbModel = new DbModel("temp_pw_post_pic","post_pic_id");
        $list =  $dbModel->getList(" post_id in ( select post_id from temp_pw_post where forum_id=$forumId and parent_id=0 ) and pic_thumb3!=''",$perpage,$page);
//        $postpicInfoArray = array();
//        foreach($list as $row){
//        	$postpicInfo = new Temppostpic_TemppostpicInfo($row);
//        	array_push($postpicInfoArray, $postpicInfo);
//        }
//        return $postpicInfoArray;
        $rtnarray = array();
        $postDao = Temppost_TemppostDao::getInstance();
        foreach($list as $row){
            $postpicInfo = new Temppostpic_TemppostpicInfo($row);
			$postInfo =  $postDao->get($postpicInfo->getPostId());
		    array_push($rtnarray, array(
		    	  "postInfo"=>$postInfo,
		    	  "postpicInfo"=>$postpicInfo
		    )); 	
        }
        return $rtnarray;
    }

    public function getListAllCountByForumId($forumId){
         $dbModel = new DbModel("temp_pw_post_pic","post_pic_id");   	
         return $dbModel->getListCount(" post_id in ( select post_id from temp_pw_post where forum_id=$forumId and parent_id=0 )  and pic_thumb3!='' ");
    }      
    

    public function getListRowByUserId($userId, $perpage, $page){
        
		$dbModel = new DbModel("","");
		$start = ($page-1)*$perpage;
		$sql = "select p1.title,p1.create_at,p2.* from temp_pw_post p1,temp_pw_post_pic p2 where p1.user_id=$userId and p1.post_id=p2.post_id ";
		$sql .= "  limit $start,$perpage ";
        $list = $dbModel->fetchAll($sql);
        return $list;
    }


    public function getListRowByUserIdCount($userId){
        $sql = "select count(*)from temp_pw_post p1,temp_pw_post_pic p2 where p1.user_id=$userId and p1.post_id=p2.post_id";
        $dbModel = new DbModel("","");   	
        return $dbModel->fetchOne($sql);
    }
        
    
    
}
