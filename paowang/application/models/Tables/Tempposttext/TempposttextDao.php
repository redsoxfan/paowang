<?php
class Tempposttext_TempposttextDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }			
	
    public function get($id){
		$dbModel = new DbModel("temp_pw_post_text","post_id");
		$row = $dbModel->getRow($id);
		$posttextInfo = new Tempposttext_TempposttextInfo($row);
    	return $posttextInfo;
    }

    public function load(&$posttextInfo){
		$dbModel = new DbModel("temp_pw_post_text","post_id");
		$row = $dbModel->getRow($posttextInfo->getPostId());
		$posttextInfo = new Tempposttext_TempposttextInfo($row);
    }
    
    public function add(&$posttextInfo){
    	$row['post_id'] = $posttextInfo->getPostid();
        $row['text'] = $posttextInfo->getText();
        $row['html'] = $posttextInfo->getHtml();

/*        $dbModel = new DbModel("t","");
        $sql = "insert into temp_pw_post_text(post_id,text,html) values(".$row['post_id'].",'".$row['text']."','".$row['html']."')";
        
        $dbModel->query($sql);
        */
	    $table = new Tempposttext_TempposttextTable();
		$id = $table->insert($row);
		$posttextInfo->setPostId($id);
 	    $log = Zend_Registry::get('log');
    }

    public function modify(&$posttextInfo){
        $row['text'] = $posttextInfo->getText();
        $row['html'] = $posttextInfo->getHtml();

	    $table = new Tempposttext_TempposttextTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("post_id=?", $posttextInfo->getPostId());
		$table->update($row, $where);
    }

    public function del($posttextInfo){
		$this->delById($posttextInfo->getPostId());
    }

    public function delById($id){
		$table = new Tempposttext_TempposttextTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('post_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("temp_pw_post_text","post_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $posttextInfoArray = array();
        foreach($list as $row){
        	$posttextInfo = new Tempposttext_TempposttextInfo($row);
        	array_push($posttextInfoArray, $posttextInfo);
        }
        return $posttextInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("temp_pw_post_text","post_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     
    
}
