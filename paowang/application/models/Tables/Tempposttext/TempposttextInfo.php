<?php
class Tempposttext_TempposttextInfo
{
    public $postId;//帖子ID
    public $text;//内容
    public $html;//html

	function __construct($posttextArray = null){
        if($posttextArray != null){
		   if(empty($posttextArray["post_id"])){
		      $this->postId=null;
	  	   }else{
		      $this->postId=$posttextArray["post_id"];
		   }
		   $this->text = $posttextArray["text"];
		   $this->html = $posttextArray["html"];
	   }
	}

	function setPostId($postId){
		$this->postId=$postId;
	}

	function getPostId(){
		return $this->postId;
	}

	function setText($text){
		$this->text=$text;
	}

	function getText(){
		return $this->text;
	}

	function setHtml($html){
		$this->html=$html;
	}

	function getHtml(){
		return $this->html;
	}

}
?>