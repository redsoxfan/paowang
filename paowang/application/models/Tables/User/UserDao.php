<?php
class User_UserDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }	
	
    public function get($id){
		$dbModel = new DbModel("pw_user","user_id");
		$row = $dbModel->getRow($id);
		$userInfo = new User_UserInfo($row);
    	return $userInfo;
    }

    public function load(&$userInfo){
		$dbModel = new DbModel("pw_user","user_id");
		$row = $dbModel->getRow($userInfo->getUserId());
		$userInfo = new User_UserInfo($row);
    }
    
    public function add(&$userInfo){
        $row['user_name'] = $userInfo->getUserName();
        $row['email'] = $userInfo->getEmail();
        $row['password'] = $userInfo->getPassword();
        $row['create_ip'] = $userInfo->getCreateIp();
        $row['create_at'] = $userInfo->getCreateAt();
        $row['update_at'] = $userInfo->getUpdateAt();
        $row['last_login_ip'] = $userInfo->getLastLoginIp();
        $row['last_login_at'] = $userInfo->getLastLoginAt();
        $row['is_actived'] = $userInfo->getIsActived();
        $row['is_administrator'] = $userInfo->getIsAdministrator();
        $row['status'] = $userInfo->getStatus();
        $row['headingphoto'] = $userInfo->getHeadingphoto();
        $row['comefrom'] = $userInfo->getComefrom();
        $row['blog'] = $userInfo->getBlog();
        $row['blog_name'] = $userInfo->getBlogName();
        $row['introduce'] = $userInfo->getIntroduce();
	    $table = new User_UserTable();
		$id = $table->insert($row);
		$userInfo->setUserId($id);
    }

    public function modify(&$userInfo){
        $row['user_name'] = $userInfo->getUserName();
        $row['email'] = $userInfo->getEmail();
        $row['password'] = $userInfo->getPassword();
        $row['create_ip'] = $userInfo->getCreateIp();
        $row['create_at'] = $userInfo->getCreateAt();
        $row['update_at'] = $userInfo->getUpdateAt();
        $row['last_login_ip'] = $userInfo->getLastLoginIp();
        $row['last_login_at'] = $userInfo->getLastLoginAt();
        $row['is_actived'] = $userInfo->getIsActived();
        $row['is_administrator'] = $userInfo->getIsAdministrator();
        $row['status'] = $userInfo->getStatus();
        $row['headingphoto'] = $userInfo->getHeadingphoto();
        $row['comefrom'] = $userInfo->getComefrom();
        $row['blog'] = $userInfo->getBlog();
        $row['blog_name'] = $userInfo->getBlogName();
        $row['introduce'] = $userInfo->getIntroduce();

	    $table = new User_UserTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("user_id=?", $userInfo->getUserId());
		$table->update($row, $where);
    }

    public function del($userInfo){
		$this->delById($userInfo->getUserId());
    }

    public function delById($id){
		$table = new User_UserTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('user_id=?',$id);
		$table->delete($where);
    }
    
    public function closeUser($userId){
		$dbModel = new DbModel("","");
		$sql = "update pw_user set status=0 where user_id=".$userId;
        $dbModel->query($sql);
    }    
    
    public function openUser($userId){
    	$dbModel = new DbModel("","");
        $sql = "update pw_user set status=1 where user_id=".$userId;
        $dbModel->query($sql);
    }
    
    public function isOpenUser($userId){
    	$userInfo = $this->get($userId);
    	if($userInfo->getStatus() == 1){
    		return true;
    	}
    	return false;
    }

    public function updateEmail($userId, $email){
        $dbModel = new DbModel("","");
        $sql = "update pw_user set email='$email' where user_id=".$userId;
        $dbModel->query($sql);
    }    
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_user","user_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $userInfoArray = array();
        foreach($list as $row){
        	$userInfo = new User_UserInfo($row);
        	array_push($userInfoArray, $userInfo);
        }
        return $userInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("pw_user","user_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     


    public function existUser($userName){
		$dbModel = new DbModel("pw_user","user_id");
        $list =  $dbModel->getList("user_name='".$userName."' ");
        
        if($list !=null){
           return true;
        }else{
           return false;
        }
	}
	
    //检测用户名和密码
	public function checkUp($userName,$password){
		$dbModel = new DbModel("pw_user","user_id");
		$password = md5($password);
		$list =  $dbModel->getList("user_name='".$userName."' and password='".$password."' ");
        
		if($list !=null && count($list)>0){		
		   $log = Zend_Registry::get('log');
            $log->debug( "################### sql: not null size > 0");					
			return true;
		}else{
			return false;
		}
	}
	
    public function getInfoByName($userName){
        $dbModel = new DbModel("pw_user","user_id");
        $row =  $dbModel->fetchRow("select * from pw_user where user_name='".$userName."'");
        if($row !=null){
           $userInfo = new User_UserInfo($row);
           return $userInfo;
        }else{
           return null; 
        }        
    }
	
    //根据用户名和密码得到用户info
	public function getInfoByNameAndPassword($userName,$password){
		$dbModel = new DbModel("pw_user","user_id");
		$password = md5($password);
		$row =  $dbModel->fetchRow("select * from pw_user where user_name='".$userName."' and password='".$password."' and status=1");
	    $log = Zend_Registry::get('log');

        if($row !=null){
           $log->debug("not null");
 		   $userInfo = new User_UserInfo($row);
	       return $userInfo;
           	
        }else{
           $log->debug("null");	
           return null;	
        }        
	}
		
	
	// 检查user表中是否存在该用户名

	public function checkUserByName($userName){
		$dbModel = new DbModel("pw_user","user_id");
        $list =  $dbModel->getList("user_name='".$userName."' ");
		return $list != null;
	}
	
	//检查user表中是否存在该email

	public function checkUserByEmail($email){
        $dbModel = new DbModel("pw_user","user_id");
        $list =  $dbModel->getList("email='".$email."' ");
		return $list != null;
	}
	/*
	* 返回用户状态，不存在则返回-9
	*/
	public function getUserStatusByNameEmail($userName, $email){
        $dbModel = new DbModel("pw_user","user_id");
        $list =  $dbModel->getList("user_name='".$userName."' and email='".$email."'");
        if($list != null){
        	return $list[0]['status'];
        }else{
            return -9;
        }
	}	
	
	public function getUserByNameEmail($userName, $email){
        $dbModel = new DbModel("pw_user","user_id");
        $list =  $dbModel->getList("user_name='".$userName."' and email='".$email."' and (status = 1 or status=-1) ");
		return $list;
	}
	
	public function calculatePasswordResetHash( $userArray )
	{
		$string = $userArray['password'].$userArray['email'].$userArray['user_id'];
		$requestHash = md5($string);
		return $requestHash;
	}	
	
	public function  verifyRequest( $userNameHash, $requestHash ){
		$dbModel = new DbModel("pw_user","user_id");
		//$list =  $dbModel->getList("md5(user_name)='".$userNameHash."'");
		$list =  $dbModel->getList("md5(CONVERT(`user_name` USING latin1))='".$userNameHash."' and (status=-1 or status=1)");
		
            $log = Zend_Registry::get('log');
            
            $log->debug( "################### userNameHash:".$userNameHash);
            $log->debug( "################### requestHash:".$requestHash);	
            $log->debug("md5(user_name)='".$userNameHash."'");
                        
		if( !$list ){
			return false;
		}
		$originalRequestHash = $this->calculatePasswordResetHash( $list[0] );
		
		if( $requestHash != $originalRequestHash ){
			return false;
		}
		return $list[0];
	}

	public function isAdministrator($userId){
		$userInfo = $this->get($userId);
		return $userInfo->getIsAdministrator();
	}
	
	public function getUserNameByUserId($userId){
		$userInfo = $this->get($userId);
		return $userInfo->getUserName();
	}
	
	public function getUserIdByUserName($userName){
		$dbModel = new DbModel("pw_user","user_id");
		$sql = "select user_id from pw_user where user_name='$userName'";
		return intval($dbModel->fetchOne($sql));
	}		
	
	public function checkUserIfExistsByEmail($email,$userId =0){
		$dbModel = new DbModel("pw_user","user_id");
		$sql = "select count(*) from pw_user where email='$email'";
		if($userId>0){
		   $sql .=" and user_id!=$userId";	
		}
		return intval($dbModel->fetchOne($sql))>0?1:0;
	}

	public function checkUserIfExistsByUserName($userName){
		$dbModel = new DbModel("pw_user","user_id");
		$sql = "select count(*) from pw_user where user_name='$userName'";
		return intval($dbModel->fetchOne($sql))>0?1:0;
	}	
	
	public function getPhotoClubMembers(){
		$cacheId = Cache_Engine::generateId(Array('UserDao', 'getPhotoClubMembers'));
		$members = Cache_Engine::load($cacheId);
		if($members) {
			return $members;
		}
        $dbModel = new DbModel("pw_user","user_id");
        $forumId = PaowangData::FORUM_ID_PHOTO;
        $roleId = PaowangData::ROLE_MEMBER;
        $list =  $dbModel->fetchAll("select * from pw_user u
            JOIN pw_forum_user f ON u.user_id = f.user_id
            where f.forum_id=$forumId AND u.status = 1 AND f.role_id>=$roleId");
        $postPicDao = Postpic_PostpicDao::getInstance();
        $userInfoArray = array();            
        foreach($list as $row){
           $userInfo = new User_UserInfo($row);
           if(array_key_exists($userInfo->getUserId(), PaowangData::$photoExcludedMembers)){
           	    continue;
           }
           $latestId = $postPicDao->findLatestId($userInfo->getUserId());
           $userInfoArray[$latestId] = $userInfo;
        }
        krsort($userInfoArray);
        Cache_Engine::save($userInfoArray, $cacheId, array(Cache_Engine::TAG_FEED_PAOWANG), 36000);
        return $userInfoArray; 		
        
	}
	
	/*
	*发送注册信，用于发邮箱确认信
	*/
	public function sendRegiserEmail($row){
            $user_id = $row['user_id'];
            $user_name = $row['user_name'];
            $email = $row['email'];
            $password = $row['password'];
            		
			if (!defined( "PLOG_CLASS_PATH" )) {
			    define( "PLOG_CLASS_PATH", $_SERVER['DOCUMENT_ROOT']."/../library/plog/");
			}            
		    include_once( PLOG_CLASS_PATH."class/mail/emailmessage.class.php" );
		    include_once( PLOG_CLASS_PATH."class/mail/emailservice.class.php" );         		    
			$config = Zend_Registry::get('config');
			$admin_email = $config->admin_email;
			$admimEmailArray =  explode(",",$admin_email);
            		
		    $message = new EmailMessage();
		    $message->setCharset("utf-8");
		    $message->addTo( $email );
		    
			foreach($admimEmailArray as $adminEmail){
		    	$message->addBcc( $adminEmail );	
			}	

			$message->setFrom( "paowangjianghu@yahoo.com" );
			$message->setFromName( "paowangjianghu@yahoo.com" );
		    $message->setSubject("欢迎您在泡网俱乐部注册");
		    $que = array('password' => $password ,
	                      'email' => $email,
	                      'user_id' => $user_id,
	                      'user_name' => $user_name  
	                );		    
		    		
		    $linkUrl = "http://".$_SERVER['SERVER_NAME']."/user/activation/a/".$this->calculatePasswordResetHash($que)."/b/".md5($que['user_name']);
		    $message->setBody( $user_name."您好！\n 欢迎您在泡网注册，请点击下面的链接完成注册 ".$linkUrl."  。\n  祝好！" );
		    $service = new EmailService();
		    if( $service->sendMessage( $message )){
		        //echo  "$user_id ################### send ok<br />";
		        return true;
		    }  
		    else{
		      return false;
		      //echo "$user_id ################### send fail<br />";
		    }		
	}	
	
	
}
