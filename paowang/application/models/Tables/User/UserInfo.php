<?php
class User_UserInfo
{
    public $userId;//主键ID
    public $userName;//用户名称
    public $email;//邮箱
    public $password;//口令
    public $createIp;//注册时IP
    public $createAt;//注册时间
    public $updateAt;//更新时间
    public $lastLoginIp;//最后登录时IP
    public $lastLoginAt;//最后登录时间
    public $isActived;//是否活动(1活动0:不活动)
    public $isAdministrator;//是否系统管理员
    public $status;//状态(1有效0无效)
    public $headingphoto;//头像
    public $comefrom;//来自哪里
    public $blog;//博客地址
    public $blogName;//博客名称
    public $introduce;//自我介绍

	function __construct($userArray = null){
        if($userArray != null){
		   if(empty($userArray["user_id"])){
		      $this->userId=null;
	  	   }else{
		      $this->userId=$userArray["user_id"];
		   }
		   $this->userName = $userArray["user_name"];
		   $this->email = $userArray["email"];
		   $this->password = $userArray["password"];
		   $this->createIp = $userArray["create_ip"];
		   $this->createAt = $userArray["create_at"];
		   $this->updateAt = $userArray["update_at"];
		   $this->lastLoginIp = $userArray["last_login_ip"];
		   $this->lastLoginAt = $userArray["last_login_at"];
		   $this->isActived = $userArray["is_actived"];
		   $this->isAdministrator = $userArray["is_administrator"];
		   $this->status = $userArray["status"];
		   $this->headingphoto = $userArray["headingphoto"];
		   $this->comefrom = $userArray["comefrom"];
		   $this->blog = $userArray["blog"];
		   $this->blogName = $userArray["blog_name"];
		   $this->introduce = $userArray["introduce"];
	   }
	}

	function setUserId($userId){
		$this->userId=$userId;
	}

	function getUserId(){
		return $this->userId;
	}

	function setUserName($userName){
		$this->userName=$userName;
	}

	function getUserName(){
		return $this->userName;
	}

	function setEmail($email){
		$this->email=$email;
	}

	function getEmail(){
		return $this->email;
	}

	function setPassword($password){
		$this->password=$password;
	}

	function getPassword(){
		return $this->password;
	}

	function setCreateIp($createIp){
		$this->createIp=$createIp;
	}

	function getCreateIp(){
		return $this->createIp;
	}

	function setCreateAt($createAt){
		$this->createAt=$createAt;
	}

	function getCreateAt(){
		return $this->createAt;
	}

	function setUpdateAt($updateAt){
		$this->updateAt=$updateAt;
	}

	function getUpdateAt(){
		return $this->updateAt;
	}

	function setLastLoginIp($lastLoginIp){
		$this->lastLoginIp=$lastLoginIp;
	}

	function getLastLoginIp(){
		return $this->lastLoginIp;
	}

	function setLastLoginAt($lastLoginAt){
		$this->lastLoginAt=$lastLoginAt;
	}

	function getLastLoginAt(){
		return $this->lastLoginAt;
	}

	function setIsActived($isActived){
		$this->isActived=$isActived;
	}

	function getIsActived(){
		return $this->isActived;
	}

	function setIsAdministrator($isAdministrator){
		$this->isAdministrator=$isAdministrator;
	}

	function getIsAdministrator(){
		return $this->isAdministrator;
	}

	function setStatus($status){
		$this->status=$status;
	}

	function getStatus(){
		return $this->status;
	}

	function setHeadingphoto($headingphoto){
		$this->headingphoto=$headingphoto;
	}

	function getHeadingphoto(){
		return $this->headingphoto;
	}

	function setComefrom($comefrom){
		$this->comefrom=$comefrom;
	}

	function getComefrom(){
		return $this->comefrom;
	}

	function setBlog($blog){
		$this->blog=$blog;
	}

	function getBlog(){
		return $this->blog;
	}

	function setBlogName($blogName){
		$this->blogName=$blogName;
	}

	function getBlogName(){
		return $this->blogName;
	}

	function setIntroduce($introduce){
		$this->introduce=$introduce;
	}

	function getIntroduce(){
		return $this->introduce;
	}

}
?>