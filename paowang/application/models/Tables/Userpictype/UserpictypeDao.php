<?php
class Userpictype_UserpictypeDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }	
	
    public function get($id){
		$dbModel = new DbModel("pw_user_pic_type","user_pic_type_id");
		$row = $dbModel->getRow($id);
		$userpictypeInfo = new Userpictype_UserpictypeInfo($row);
    	return $userpictypeInfo;
    }

    public function load(&$userpictypeInfo){
		$dbModel = new DbModel("pw_user_pic_type","user_pic_type_id");
		$row = $dbModel->getRow($userpictypeInfo->getUserPicTypeId());
		$userpictypeInfo = new Userpictype_UserpictypeInfo($row);
    }
    
    public function add(&$userpictypeInfo){
        $row['user_pic_type_name'] = $userpictypeInfo->getUserPicTypeName();
        $row['status'] = $userpictypeInfo->getStatus();
        $row['user_id'] = $userpictypeInfo->getUserId();
        $row['create_at'] = $userpictypeInfo->getCreateAt();
	    $table = new Userpictype_UserpictypeTable();
		$id = $table->insert($row);
		$userpictypeInfo->setUserPicTypeId($id);
    }

    public function modify(&$userpictypeInfo){
        $row['user_pic_type_name'] = $userpictypeInfo->getUserPicTypeName();
        $row['status'] = $userpictypeInfo->getStatus();
        $row['user_id'] = $userpictypeInfo->getUserId();
        $row['create_at'] = $userpictypeInfo->getCreateAt();

	    $table = new Userpictype_UserpictypeTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("user_pic_type_id=?", $userpictypeInfo->getUserPicTypeId());
		$table->update($row, $where);
    }

    public function del($userpictypeInfo){
		$this->delById($userpictypeInfo->getUserPicTypeId());
    }

    public function delById($id){
		$table = new Userpictype_UserpictypeTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('user_pic_type_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_user_pic_type","user_pic_type_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $userpictypeInfoArray = array();
        foreach($list as $row){
        	$userpictypeInfo = new Userpictype_UserpictypeInfo($row);
        	array_push($userpictypeInfoArray, $userpictypeInfo);
        }
        return $userpictypeInfoArray;
    }    

    public function getListAllCount(){
         $dbModel = new DbModel("pw_user_pic_type","user_pic_type_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     
    
}
