<?php
class Userpictype_UserpictypeInfo
{
    public $userPicTypeId;//用户图片类别ID
    public $userPicTypeName;//类别
    public $status;//状态(1有效0无效)
    public $userId;//用户ID
    public $createAt;//创建时间

	function __construct($userpictypeArray = null){
        if($userpictypeArray != null){
		   if(empty($userpictypeArray["user_pic_type_id"])){
		      $this->userPicTypeId=null;
	  	   }else{
		      $this->userPicTypeId=$userpictypeArray["user_pic_type_id"];
		   }
		   $this->userPicTypeName = $userpictypeArray["user_pic_type_name"];
		   $this->status = $userpictypeArray["status"];
		   $this->userId = $userpictypeArray["user_id"];
		   $this->createAt = $userpictypeArray["create_at"];
	   }
	}

	function setUserPicTypeId($userPicTypeId){
		$this->userPicTypeId=$userPicTypeId;
	}

	function getUserPicTypeId(){
		return $this->userPicTypeId;
	}

	function setUserPicTypeName($userPicTypeName){
		$this->userPicTypeName=$userPicTypeName;
	}

	function getUserPicTypeName(){
		return $this->userPicTypeName;
	}

	function setStatus($status){
		$this->status=$status;
	}

	function getStatus(){
		return $this->status;
	}

	function setUserId($userId){
		$this->userId=$userId;
	}

	function getUserId(){
		return $this->userId;
	}

	function setCreateAt($createAt){
		$this->createAt=$createAt;
	}

	function getCreateAt(){
		return $this->createAt;
	}

}
?>