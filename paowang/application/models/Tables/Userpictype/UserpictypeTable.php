<?php
/**
 * Userpictype_UserpictypeTable
 */

class Userpictype_UserpictypeTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_user_pic_type";
    // 默认主键为’id’
    protected $_primary = "user_pic_type_id";
}
