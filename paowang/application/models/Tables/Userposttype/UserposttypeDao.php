<?php
class Userposttype_UserposttypeDao{

	/**
     * Singleton implementation
     */
    protected static $_instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance(){
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }	
	
    public function get($id){
		$dbModel = new DbModel("pw_user_post_type","user_post_type_id");
		$row = $dbModel->getRow($id);
		$userposttypeInfo = new Userposttype_UserposttypeInfo($row);
    	return $userposttypeInfo;
    }

    public function load(&$userposttypeInfo){
		$dbModel = new DbModel("pw_user_post_type","user_post_type_id");
		$row = $dbModel->getRow($userposttypeInfo->getUserPostTypeId());
		$userposttypeInfo = new Userposttype_UserposttypeInfo($row);
    }
    
    public function add(&$userposttypeInfo){
        $row['user_post_type_name'] = $userposttypeInfo->getUserPostTypeName();
        $row['status'] = $userposttypeInfo->getStatus();
        $row['user_id'] = $userposttypeInfo->getUserId();
        $row['create_at'] = $userposttypeInfo->getCreateAt();
        $row['is_pic'] = $userposttypeInfo->getIsPic();
	    $table = new Userposttype_UserposttypeTable();
		$id = $table->insert($row);
		$userposttypeInfo->setUserPostTypeId($id);
    }

    public function modify(&$userposttypeInfo){
        $row['user_post_type_name'] = $userposttypeInfo->getUserPostTypeName();
        $row['status'] = $userposttypeInfo->getStatus();
        $row['user_id'] = $userposttypeInfo->getUserId();
        $row['create_at'] = $userposttypeInfo->getCreateAt();
        $row['is_pic'] = $userposttypeInfo->getIsPic();

	    $table = new Userposttype_UserposttypeTable();
	    $db = $table->getAdapter();
		$where = $db->quoteInto("user_post_type_id=?", $userposttypeInfo->getUserPostTypeId());
		$table->update($row, $where);
    }

    public function del($userposttypeInfo){
		$this->delById($userposttypeInfo->getUserPostTypeId());
    }

    public function delById($id){
		$table = new Userposttype_UserposttypeTable();
		$db = $table->getAdapter();
		$where = $db->quoteInto('user_post_type_id=?',$id);
		$table->delete($where);
    }
    
    public function getList($perpage,$page){
		$dbModel = new DbModel("pw_user_post_type","user_post_type_id");
        $list =  $dbModel->getList(' 1=1 ',$perpage,$page);
        $userposttypeInfoArray = array();
        foreach($list as $row){
        	$userposttypeInfo = new Userposttype_UserposttypeInfo($row);
        	array_push($userposttypeInfoArray, $userposttypeInfo);
        }
        return $userposttypeInfoArray;
    }    

    public function getListByUserIdAndType($userId,$type = 0 ,$perpage = 0,$page = 0){
		$dbModel = new DbModel("pw_user_post_type","user_post_type_id");
        $list =  $dbModel->getList(" status=1 and user_id=$userId and is_pic=$type",$perpage,$page,"*","user_post_type_id asc");
        $userposttypeInfoArray = array();
        foreach($list as $row){
        	$userposttypeInfo = new Userposttype_UserposttypeInfo($row);
        	array_push($userposttypeInfoArray, $userposttypeInfo);
        }
        return $userposttypeInfoArray;
    } 
    
    public function getListAllCount(){
         $dbModel = new DbModel("pw_user_post_type","user_post_type_id");   	
         return $dbModel->getListCount(' 1=1 ');
    }     
    
}
