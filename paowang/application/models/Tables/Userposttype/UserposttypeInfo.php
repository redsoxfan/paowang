<?php
class Userposttype_UserposttypeInfo
{
    public $userPostTypeId;//用户帖子(文章)类别ID
    public $userPostTypeName;//类别
    public $status;//状态(1有效0无效)
    public $userId;//用户ID
    public $createAt;//创建时间
    public $isPic;//0为帖子

	function __construct($userposttypeArray = null){
        if($userposttypeArray != null){
		   if(empty($userposttypeArray["user_post_type_id"])){
		      $this->userPostTypeId=null;
	  	   }else{
		      $this->userPostTypeId=$userposttypeArray["user_post_type_id"];
		   }
		   $this->userPostTypeName = $userposttypeArray["user_post_type_name"];
		   $this->status = $userposttypeArray["status"];
		   $this->userId = $userposttypeArray["user_id"];
		   $this->createAt = $userposttypeArray["create_at"];
		   $this->isPic = $userposttypeArray["is_pic"];
	   }
	}

	function setUserPostTypeId($userPostTypeId){
		$this->userPostTypeId=$userPostTypeId;
	}

	function getUserPostTypeId(){
		return $this->userPostTypeId;
	}

	function setUserPostTypeName($userPostTypeName){
		$this->userPostTypeName=$userPostTypeName;
	}

	function getUserPostTypeName(){
		return $this->userPostTypeName;
	}

	function setStatus($status){
		$this->status=$status;
	}

	function getStatus(){
		return $this->status;
	}

	function setUserId($userId){
		$this->userId=$userId;
	}

	function getUserId(){
		return $this->userId;
	}

	function setCreateAt($createAt){
		$this->createAt=$createAt;
	}

	function getCreateAt(){
		return $this->createAt;
	}

	function setIsPic($isPic){
		$this->isPic=$isPic;
	}

	function getIsPic(){
		return $this->isPic;
	}

}
?>