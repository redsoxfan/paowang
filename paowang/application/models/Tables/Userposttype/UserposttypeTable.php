<?php
/**
 * Userposttype_UserposttypeTable
 */

class Userposttype_UserposttypeTable extends Zend_Db_Table
{
    // 设置默认表名
	protected $_name = "pw_user_post_type";
    // 默认主键为’id’
    protected $_primary = "user_post_type_id";
}
