<?php
final class Util{

	/**
		 * 将unix时间戳格式化成正常时间显示
		 * 
		 * @param integer $datetime
		 * @param string $fmt
		 * @return string
		 */
	static public function date2string10($datetime, $fmt = 'Y-m-d'){
		if (empty($datetime)) return null;
		return date($fmt, $datetime);
	}

	/**
		 * 将unix时间戳格式化成正常时间显示
		 * 
		 * @param integer $datetime
		 * @param string $fmt
		 * @return string
		 */
	static public function date2string19($datetime, $fmt = 'Y-m-d H:i:s'){
		if (empty($datetime)) return null;
		return date($fmt, $datetime);
	}
	
	/**
		 * 将unix时间戳格式化成正常时间显示
		 * 
		 * @param integer $datetime
		 * @param string $fmt
		 * @return string
		 */
	static public function date2string16($datetime, $fmt = 'Y-m-d H:i'){
		if (empty($datetime)) return null;
		return date($fmt, $datetime);
	}	

	/**
	/**
		 * 计算utf-8字符串字长度
		 * 
		 * @param string $str
		 * @return integer
		 */
	static public function strLenUtf8($str) {
		$i = 0;
		$ret = 0;
		$len = strlen ($str);
		while ($i < $len){
			$chr = ord ($str[$i++]);
			$lilo = 1;
			$ret++;
			if($i >= $len) break;
			if($chr & 0x80){
				$chr <<= 1;
				while ($chr & 0x80){
					$lilo ++;
					$i++;
					$chr <<= 1;
				}
				if ($lilo == 3) $ret++;
			}
		}
		return $ret;
	}
	
	/**
	 * Strip pure text from html text
	 */
	public static function stripText($htmlText){
		$snoopy = new Snoopy();
		return $snoopy->_striptext($htmlText);
	}
	
	public static function isUserLogin(){
		if(self::getUserId()){
			return true;
		}
		return false;
	}
	
	/**
	 * utf-8字符串取一定长度
	 */
	public static function getSubString($str,$len)
	{
		if(strlen($str) <= $len){
			return $str;
		}
		$new_str = array();
		for($i=0;$i<$len;$i++)
		{
		$temp_str=substr($str,0,1);
		if(ord($temp_str) > 127)
		{
		$i++;
		if($i<$len)
		{
		$new_str[]=substr($str,0,3);
		$str=substr($str,3);
		}
		}
		else
		{
		$new_str[]=substr($str,0,1);
		$str=substr($str,1);
		}
		}
		return join($new_str) . "...";
	}
	
	public static function getUserId(){
      $rtn = 0;
	  if(array_key_exists("paowanguser",$_COOKIE)){
      	 $str = $_COOKIE["paowanguser"];
	     $str = base64_decode($str);
		 $aes = new AES('PAOWANG19990603a');
         $str = $aes->decrypt($str);
         $cookieArray = json_decode($str,true);
      	 $rtn = intval($cookieArray['userId']);
      }      
      return $rtn;
	}
	

	

    public static function getUserName(){
     /* if(array_key_exists("userName",$_COOKIE)){
         return $_COOKIE["userName"];
      }
      */
    
 	  if(array_key_exists("paowanguser",$_COOKIE)){
      	 $str = $_COOKIE["paowanguser"];
	     $str = base64_decode($str);
		 $aes = new AES('PAOWANG19990603a');
         $str = $aes->decrypt($str);
         $cookieArray = json_decode($str,true);
      	 return  $cookieArray['userName'];
      }   
     	 
      return "";
    }
    
    /*
    *图片论坛的列表显示方式
    */
    public static function getListType(){
      if(array_key_exists("listType",$_COOKIE)){
         return $_COOKIE["listType"];
      } 
      return "list";
    }

    /*
    *style
    */
    public static function getStyle(){
      if(array_key_exists("style",$_COOKIE)){
         return $_COOKIE["style"];
      } 
      return "classic";
    }
        

    /*
    *forumId
    */
    public static function getForumId(){
      if(array_key_exists("forumId",$_COOKIE)){
         return $_COOKIE["forumId"];
      } 
      return "";
    }
    
    /*
    *是否管理
    */    
    public static function getIsManage(){
      if(array_key_exists("isManage",$_COOKIE)){
         return $_COOKIE["isManage"];
      } 
      return "0";
    }
	
	public static function getUserIp(){
      return $_SERVER['REMOTE_ADDR'];;
	}

	public static function getDomainName(){
      return "http://".$_SERVER['HTTP_HOST'];;
	}	
	
	public static function getCurrentUri(){
          //return $_SERVER['SCRIPT_URI'];;
return "http://paowang.net".$_SERVER['DOCUMENT_URI'];
	}	
	
	public static function singleMainPostWithStyle($postInfo, $style = ""){
		if($style == 'classic'){
			return self::classicForumMainPost($postInfo);
		}
		return self::forumMainPost($postInfo);
	}
	
	public static function singleChildPostWithStyle($postInfo, $style = ""){
		if($style == 'classic'){
			return self::classicForumChildPost($postInfo);
		}
		return self::forumChildPost($postInfo);
	}
	
	/*
	*把登录名转成实际的用户名
	*/
	public static function loginname2username($loginname){
		$rtn = preg_replace('/\s/', '', trim($loginname));
		$rtn = str_replace("\\",'##',$rtn);//将\替换为#，也就是不允许使用\来登录
        return $rtn;
	}

	/*
	*把登录名转成“多个空格显示时缩减为一个空格 ”
	*/
	public static function loginname2loginname($loginname){
        return preg_replace('/\s(?=\s)/', '', trim($loginname));
	}

	
    public static function forumMainPost($postInfo,$spanClass = "")
    {
   	    $resourceDir = Util::getResourceTimestampDir();
    	
    	$postId = $postInfo->getPostId();
    	$postTitle = $postInfo->getTitle();
//    	$postTitle = Post_PostUtils::trimPostTitle($postTitle);
    	$userName = $postInfo->getUserName(); 
        if($userName == "") {
            $userName = "&nbsp;";
        }
    	$date = Util::date2string16($postInfo->getCreateAt()); 
    	$textLength = $postInfo->getTextLength();
    	$click = $postInfo->getClick();
        $voteYes =  $postInfo->getVoteYes();
        $voteNo =  $postInfo->getVoteNo();

        $li1Class = "li_1";
        if($spanClass != "") {
            $li1Class = $spanClass;        	
        }
        
        $return = 
              '<ul class="content_list">
          <li class="' . $li1Class .'"><a href="javascript:void(0);" target="_self"><img class="e_c" id="e_c'.$postId.'" src="' . $resourceDir . '/images/next_bg.jpg" /></a></li>
          <li class="li_2"><div style="padding-left:0px"><a href="/post/' . $postId . '" target="postcontent" title="' . $postTitle . '">'.$postTitle.($postInfo->getPicCount()>0?"【图】":"").'</a></div></li>
          <li class="li_3"><a href="/search/list/q/'.$userName.'" target="_blank">'.$userName.'</a></li>
          <li class="li_4">'.$date.'</li>
          <li class="li_5">'.$textLength.' </li>
          <li class="li_6">'.$click.' </li>
          <li class="li_7">'.$voteYes.'/'.$voteNo.'</li>
        </ul> ';
        return $return;
    }    
    
    public static function classicForumMainPost($postInfo, $expand = true, $status = PaowangData::POST_STATUS_NORMAL, $jsfunc = "e_c")
    {
        $resourceDir = Util::getResourceTimestampDir();
        
        $userString = $postInfo->getUserName();
        $forumId = $postInfo->getForumId();
        if(in_array($forumId, PaowangData::$photoForums)){
        	// TODO put in css file
        	$userString = '<a href="/user/photolist/userId/' . $postInfo->getUserid() . '" style="text-decoration:none;">'.$postInfo->getUserName().'</a>'; 
        }else{
        	$userString = '<a href="/search/list/q/'.$postInfo->getUserName().'" target="_blank" style="text-decoration:none;">'.$postInfo->getUserName().'</a>'; 
        }	
        $title = self::getSubString($postInfo->getTitle(), PaowangData::POST_TITLE_DISPLAY_LENGTH);
        
        
        $preImg = "<img id=\"img".$postInfo->getPostId()."\" src=\"" . $resourceDir . "/images/classic/next_bg.gif\" alt=\"\" /></a>";
        if($status == PaowangData::POST_STATUS_HOT){
            $preImg = "<img id=\"img_hot".$postInfo->getPostId()."\" src=\"" . $resourceDir . "/images/classic/title_bg.gif\" alt=\"\" /></a><img src=\"" . $resourceDir . "/images/classic/hot.gif\" alt=\"\" /> ";    
        }
        else if($status == PaowangData::POST_STATUS_TOP){
            $preImg = "<img id=\"img_ceil".$postInfo->getPostId()."\" src=\"" . $resourceDir . "/images/classic/title_bg.gif\" alt=\"\" /></a><img src=\"" . $resourceDir . "/images/classic/totop.gif\" alt=\"\" /> ";    
        }
        
        
        $return = "<div class=\"post_list\" id=\"post_list".$postInfo->getPostId()."\"><a href=\"javascript:void(0)\" onclick=\"".$jsfunc."(".$postInfo->getPostId().");\" class=\"tree\">".$preImg."<a href=\"/post/".$postInfo->getPostId()."\" target=\"_postcontent\"  class=\"posts\" id=\"post".$postInfo->getPostId()."\">".$title.($postInfo->getPicCount()>0?"【图】":"")."</a><span><".$postInfo->getTextLength()." 字节></span><span class=\"auto\">".$userString."</span><span>".Util::date2string19($postInfo->getCreateAt())." </span><span>(".$postInfo->getClick().")</span><span>[".$postInfo->getVoteYes()."/".$postInfo->getVoteNo()."]</span> ";
        /*
        if($status == PaowangData::POST_STATUS_HOT){
            $return .= "<span>热！<span>";    
        }
        else if($status == PaowangData::POST_STATUS_TOP){
            $return .= "<span>顶！<span>";
        }
        */
        $return .= "</div>\n";
        return $return;
    } 
	
    public static function forumChildPost($postInfoChild)
    {
    	$resourceDir = Util::getResourceTimestampDir();
    	
        $topId = $postInfoChild->getTopId();
        $depth = (intval($postInfoChild->getDepth())-1)*1;
    	$postId = $postInfoChild->getPostId();
        $postTitle = $postInfoChild->getTitle();
        //$postTitle = Post_PostUtils::trimPostTitle($postTitle);
        $userName = $postInfoChild->getUserName(); 
        if($userName == "") {
            $userName = "&nbsp;";
        }
        $date = Util::date2string16($postInfoChild->getCreateAt()); 
        $textLength = $postInfoChild->getTextLength();
        $click = $postInfoChild->getClick();
        $voteYes =  $postInfoChild->getVoteYes();
        $voteNo =  $postInfoChild->getVoteNo(); 
        
        $return = '<ul class="content_list child'.$topId.'" >
          <li class="li_1"><a href="javascript:void(0);" target="_self"></a></li>
          <li class="li_2"><div style="padding-left:'.(intval($depth)*15).'px"><img src="' . $resourceDir . '/images/small_title.gif"/><a href="/post/' . $postId . '" target="postcontent" title="' . $postTitle .'">'. $postTitle.($postInfoChild->getPicCount()>0?"【图】":"").'</a></div></li>
          <li class="li_3"><a href="/search/list/q/'.$userName.'" target="_blank">'.$userName.'</a></li>
          <li class="li_4">'.$date.'</li>
          <li class="li_5">'.$textLength.' </li>
          <li class="li_6">'.$click.' </li>
          <li class="li_7">'.$voteYes.'/'.$voteNo.'</li>
        </ul>';
            
        return $return;
    } 

    public static function classicForumChildPost($postInfoChild)
    {
        $resourceDir = Util::getResourceTimestampDir();
        $title = self::getSubString($postInfoChild->getTitle(), PaowangData::POST_TITLE_DISPLAY_LENGTH);
        
        $depth = intval($postInfoChild->getDepth());
        $return = "<div style=\"padding-left:" . (40 + ($depth-2)*30) . "px; background:url(" . $resourceDir . "/images/classic/dotted.gif) no-repeat " . (27 + ($depth - 2)*30) . "px 13px;\"><a href=\"/post/" . $postInfoChild->getPostId() ."\" target=\"_postcontent\" class=\"posts\" id=\"post".$postInfoChild->getPostId()."\">".$title.($postInfoChild->getPicCount()>0?"【图】":"")."</a><span><".$postInfoChild->getTextLength()." 字节></span> <span class=\"auto\"><a href=\"/search/list/q/".$postInfoChild->getUserName()."\" target=\"_blank\"  style=\"text-decoration:none;\">".$postInfoChild->getUserName()."</a></span><span>".Util::date2string19($postInfoChild->getCreateAt())."</span><span>(".$postInfoChild->getClick().")</span><span>[".$postInfoChild->getVoteYes()."/".$postInfoChild->getVoteNo()."]</span> </div>";
        // /post/".$postInfoChild->getPostId()."
            
        return $return;
    }  
    
    public static function getResourceTimestampDir(){
    	$config = Zend_Registry::get('config');
        $timestamp = $config->resource_dir;
        if($timestamp == ''){
        	return '';
        }
        return '/' . $timestamp;
    }
    
/**
 * EndsWith
 * Tests whether a text ends with the given
 * string or not.
 *
 * @param     string
 * @param     string
 * @return    bool
 */
public static function endsWith($Haystack, $Needle){
    // Recommended version, using strpos
    return strrpos($Haystack, $Needle) === strlen($Haystack)-strlen($Needle);
}
 
// Another way, using substr
public static function endsWith_Old($Haystack, $Needle){
    return substr($Haystack, strlen($Needle)*-1) == $Needle;
}    
    
}
?>
