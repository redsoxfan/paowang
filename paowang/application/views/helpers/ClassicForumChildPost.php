<?php
/**
 * View helper to display a a single child post in forum main page
 *
 */
class Zend_View_Helper_ClassicForumChildPost
{
    public function classicForumChildPost($postInfoChild)
    {
        return Util::classicForumChildPost($postInfoChild);
    }    
    
         
}