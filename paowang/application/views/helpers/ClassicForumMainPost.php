<?php
/**
 * View helper to display a a single rootpost in forum main page
 *
 */
class Zend_View_Helper_ClassicForumMainPost
{    
    public function classicForumMainPost($postInfo, $expand = true, $status = PaowangData::POST_STATUS_NORMAL, $jsfunc = "e_c")
    {
        return Util::classicForumMainPost($postInfo, $expand, $status, $jsfunc);
    }    
}