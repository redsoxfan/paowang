<?php
/**
 * View helper to display a a single child post in forum main page
 *
 */
class Zend_View_Helper_ForumChildPost
{
    public function forumChildPost($postInfoChild,$expand = true)
    {
    	$resourceDir = Util::getResourceTimestampDir();
    	
        $topId = $postInfoChild->getTopId();
        $depth = (intval($postInfoChild->getDepth())-1)*1;
    	$postId = $postInfoChild->getPostId();
        $postTitle = $postInfoChild->getTitle();
        //$postTitle = Post_PostUtils::trimPostTitle($postTitle);
        $userName = $postInfoChild->getUserName(); 
        if($userName == "") {
            $userName = "&nbsp;";
        }
        $date = Util::date2string16($postInfoChild->getCreateAt()); 
        $textLength = $postInfoChild->getTextLength();
        $click = $postInfoChild->getClick();
        $voteYes =  $postInfoChild->getVoteYes();
        $voteNo =  $postInfoChild->getVoteNo(); 
        
        $return = '<ul class="content_list child'.$topId.'"  '.($expand?'style="display:;"':'style="display:none;"').'>
          <li class="li_1"><a href="javascript:void(0);" target="_self"></a></li>
          <li class="li_2"><div style="padding-left:'.(intval($depth)*15).'px"><img src="' . $resourceDir . '/images/small_title.gif"/><a href="/post/' . $postId . '" target="postcontent" title="' . $postTitle .'">'. $postTitle.($postInfoChild->getPicCount()>0?"【图】":"") .($postInfoChild->getStatus()==0?"<span style=\"color:red;\">已经删除</span>":"").'</a></div></li>
          <li class="li_3"><a href="/search/list/q/'.$userName.'" target="_blank">'.$userName.'</a></li>
          <li class="li_4">'.$date.'</li>
          <li class="li_5">'.$textLength.' </li>
          <li class="li_6">'.$click.' </li>
          <li class="li_7">'.$voteYes.'/'.$voteNo.'</li>
        </ul>';
            
        return $return;
    }    
    
         
}