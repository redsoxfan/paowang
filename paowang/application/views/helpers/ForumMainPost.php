<?php
/**
 * View helper to display a a single rootpost in forum main page
 *
 */
class Zend_View_Helper_ForumMainPost
{    
    public function forumMainPost($postInfo,$spanClass = "",$expand = true)
    {
   	    $resourceDir = Util::getResourceTimestampDir();
    	
    	$postId = $postInfo->getPostId();
    	$postTitle = $postInfo->getTitle();
//    	$postTitle = Post_PostUtils::trimPostTitle($postTitle);
    	$userName = $postInfo->getUserName(); 
        if($userName == "") {
            $userName = "&nbsp;";
        }
    	$date = Util::date2string16($postInfo->getCreateAt()); 
    	$textLength = $postInfo->getTextLength();
    	$click = $postInfo->getClick();
        $voteYes =  $postInfo->getVoteYes();
        $voteNo =  $postInfo->getVoteNo();

        $li1Class = "li_1";
        if($spanClass != "") {
            $li1Class = $spanClass;        	
        }
        
        $return = 
              '<ul class="content_list">
          <li class="' . $li1Class .'"><a href="javascript:void(0);" target="_self"><img class="e_c" id="e_c'.$postId.'" src="'.($expand? $resourceDir. "/images/next_bg.jpg" : $resourceDir . "/images/title_bg.jpg").'" /></a></li>
          <li class="li_2"><div style="padding-left:0px"><a href="/post/' . $postId . '" target="postcontent" title="' . $postTitle . '">'.$postTitle.($postInfo->getPicCount()>0?"【图】":"").($postInfo->getStatus()==0?"<span style=\"color:red;\">已经删除</span>":"").'</a></div></li>
          <li class="li_3"><a href="/search/list/q/'.$userName.'" target="_blank">'.$userName.'</a></li>
          <li class="li_4">'.$date.'</li>
          <li class="li_5">'.$textLength.' </li>
          <li class="li_6">'.$click.' </li>
          <li class="li_7">'.$voteYes.'/'.$voteNo.'</li>
        </ul> ';
        return $return;
    }    
}