#
# Virtual Hosts
#
# If you want to maintain multiple domains/hostnames on your
# machine you can setup VirtualHost containers for them. Most configurations
# use only name-based virtual hosts so the server doesn't need to worry about
# IP addresses. This is indicated by the asterisks in the directives below.
#
# Please see the documentation at 
# <URL:http://httpd.apache.org/docs/2.2/vhosts/>
# for further details before you try to setup virtual hosts.
#
# You may use the command line option '-S' to verify your virtual host
# configuration.

#
# Use name-based virtual hosting.
#
NameVirtualHost *:80

#
# VirtualHost example:
# Almost any Apache directive may go into a VirtualHost container.
# The first VirtualHost section is used for all requests that do not
# match a ServerName or ServerAlias in any <VirtualHost> block.
#
<VirtualHost *:80>
    DocumentRoot /usr/local/apache2/htdocs
    ServerName 122.200.102.32
</VirtualHost>

<Location /svn>
  DAV svn
  SVNParentPath /www/svn-repos
  AuthType Basic
  AuthName "paowang subversion repository"
  AuthUserFile /www/svn-repos/svn-auth-file
  Require valid-user
</Location>

<VirtualHost *:80>
    DocumentRoot /www/site/wordpress-mu
    ServerName blog.paowang.com
    ServerAlias *.blog.paowang.com blog.paowang.net *.blog.paowang.net
    ErrorLog logs/paowang-blog-error.log
    CustomLog logs/paowang-blog-access.log common

    Options +FollowSymLinks
    RewriteEngine On
    RewriteCond %{HTTP_HOST} ([^.]+).blog.paowang.com [NC]
    RewriteRule ^(.*)$ http://%1.blog.paowang.net%{REQUEST_URI} [R=301,L]
    RewriteCond %{HTTP_HOST} ^blog.paowang.com [NC]
    RewriteRule ^(.*)$ http://blog.paowang.net%{REQUEST_URI} [R=301,L]
#    RewriteCond %{HTTP_HOST} ^blog.paowang.net [NC]
#    RewriteRule ^(/?)$ http://test.paowang.net/blog [L]

    <Directory "/www/site/wordpress-mu">
        Allow from all
        Options Includes -Indexes
	AllowOverride FileInfo Options
    </Directory>
</VirtualHost>

<VirtualHost *:80>
    DocumentRoot /www/site/mt
    ServerName mt.paowang.net
    ServerAlias mt.paowang.com
    ErrorLog logs/paowang-mt-error.log
    CustomLog logs/paowang-mt-access.log common

    <Directory "/www/site/mt">
        Allow from all
        Options Includes -Indexes ExecCGI
        AllowOverride All
        AddHandler cgi-script .cgi
    </Directory>
</VirtualHost>

<VirtualHost *:80>
    DocumentRoot /www/site
    ServerName p.paowang.com
    ServerAlias  p.uupage.com p.paowang.net
    
    RewriteEngine On
    RewriteCond %{HTTP_REFERER} !^$ [NC]
    RewriteCond %{HTTP_REFERER} !paowang.net [NC]
    RewriteCond %{HTTP_REFERER} !paowang.com [NC]
    RewriteCond %{HTTP_REFERER} !dev.uupage.com [NC]
    RewriteCond %{HTTP_REFERER} !google.com [NC]
    RewriteCond %{HTTP_REFERER} !bloglines.com [NC]
    RewriteCond %{HTTP_REFERER} !feedburner.com [NC]
    RewriteCond %{HTTP_REFERER} !feedsky.com [NC]
    RewriteCond %{HTTP_REFERER} !zhuaxia.com [NC]
    RewriteCond %{HTTP_REFERER} !xianguo.com [NC]
    RewriteCond %{HTTP_REFERER} !douban.com [NC]
    RewriteRule .*\.(gif|jpg)$ http://paowang.net/images/paowang_logo.jpg [R,NC,L]
    
    ErrorLog logs/paowang-p-error.log
    CustomLog logs/paowang-p-access.log common

    <Directory "/www/site">
        Allow from all
        Options Includes -Indexes
        AllowOverride All
    </Directory>
</VirtualHost>

<VirtualHost *:80>
    DocumentRoot /www/site/paowang-dev/www
    ServerName dev.uupage.com 
    ServerAlias test.paowang.net
    RewriteEngine On
    RewriteRule ^/news/(.*)$ http://go.paowang.com/news/$1 [R]
    RewriteRule ^/blog/(.*)$ http://go.paowang.com/blog/$1 [R]    
    RewriteCond %{REQUEST_URI} !^.*(\.css|\.js|\.gif|\.png|\.bmp|\.jpg|\.jpeg|\.rar|\.html|\.swf|\.json|\.xml|\.cur)$
    RewriteRule ^(/.*)$ /index.php
    ErrorLog logs/paowang-dev-error.log
    CustomLog logs/paowang-dev-access.log common

    <Directory "/www/site/paowang-dev/www">
        Allow from all
        Options Includes -Indexes
        AllowOverride All
    </Directory>
</VirtualHost>

<VirtualHost *:80>
    DocumentRoot /www/site/paowang-live/www
    ServerName paowang.com
    ServerAlias www.paowang.com paowang.net www.paowang.net live.uupage.com
    RewriteEngine On

    RewriteCond %{HTTP_HOST} paowang.com [NC]
    RewriteRule ^(.*)$ http://paowang.net%{REQUEST_URI} [R=301,L]
    RewriteCond %{HTTP_HOST} www.paowang.net [NC]
    RewriteRule ^(.*)$ http://paowang.net%{REQUEST_URI} [R=301,L]

    RewriteRule ^/zhao(.*)$ /usr/local/apache2/htdocs/zhao$1 [L]
    RewriteRule ^/info.txt$ /usr/local/apache2/htdocs/info.txt [L]

    RewriteRule ^/news/(.*)$ http://go.paowang.com/news/$1 [R]
    RewriteRule ^/blog/(.+)$ http://go.paowang.com/blog/$1 [R]
    #RewriteRule ^/blog$ http://go.paowang.com/blogplanet [R]
    RewriteCond %{REQUEST_URI} !^.*(\.css|\.js|\.gif|\.png|\.jpg|\.jpeg|\.rar|\.html|\.swf|\.json|\.xml|\.cur)$
    RewriteRule ^(/.*)$ /index.php

    ErrorLog logs/paowang-live-error.log
    CustomLog logs/paowang-live-access.log common
    
    DeflateFilterNote ratio
    #LogFormat '"%r" %b (%{ratio}n) "%{User-agent}i"' deflate
    #CustomLog logs/deflate_log deflate
    
    AddOutputFilterByType DEFLATE application/x-javascript
    AddOutputFilterByType DEFLATE application/javascript
    AddOutputFilterByType DEFLATE text/javascript
    AddOutputFilterByType DEFLATE text/css
    AddOutputFilterByType DEFLATE text/html
    AddOutputFilterByType DEFLATE text/plain 
    AddOutputFilterByType DEFLATE text/xml 
    BrowserMatch ^Mozilla/4 gzip-only-text/html
    BrowserMatch ^Mozilla/4\.0[678] no-gzip
    BrowserMatch \bMSIE !no-gzip !gzip-only-text/html    

    <Directory "/www/site/paowang-live/www">
        Allow from all
        Options Includes -Indexes
        AllowOverride All
        
        <FilesMatch "\.(js|css|jpg|png|gif)$">
            Header set Cache-Control "max-age=29030400, public"
            Header set Expires "Mon, 28 Jul 2020 23:30:00 GMT"
        </FilesMatch>
        
        #ExpiresByType text/html "access plus 1 day"
        #ExpiresByType text/css "access plus 1 month"
        #ExpiresByType text/javascript "access plus 1 month"
        #ExpiresByType image/gif "access plus 1 month"
        #ExpiresByType image/jpg "access plus 1 month"
        #ExpiresByType image/png "access plus 1 month"
        #ExpiresByType application/x-shockwave-flash "access plus 1 month"
    </Directory>
</VirtualHost>
