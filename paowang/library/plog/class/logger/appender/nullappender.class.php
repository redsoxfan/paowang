<?php

    include_once($_SERVER['DOCUMENT_ROOT']."/../library/plog/class/logger/appender/appender.class.php");

    /**
	 * Dummy appender that does nothing.
	 *
	 * \ingroup logger	 
     */
    class NullAppender extends Appender
    {
        /**
		 * This method does nothing since this is a dummy appender anyway!
         */
        function write ($message)
        {
			return( true );
        }
    }

?>