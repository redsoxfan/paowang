<?php
error_reporting(E_ALL|E_STRICT);
ini_set('display_errors', 1);
date_default_timezone_set('Asia/Shanghai');

// 目录设置和类装载
$paths = array(
    $_SERVER['DOCUMENT_ROOT'] . '/../library/',
    $_SERVER['DOCUMENT_ROOT'] . '/../application/models/',
    $_SERVER['DOCUMENT_ROOT'] . '/../application/models/Tables/',
    get_include_path(),
    '.'
);
set_include_path(implode(PATH_SEPARATOR, $paths));
require_once 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);


// load configuration
$config = new Zend_Config_Ini('../config/config_local.ini', 'general');
$registry = Zend_Registry::getInstance();
$registry->set('config', $config);
// setup database
$db = Zend_Db::factory($config->db);
Zend_Db_Table::setDefaultAdapter($db);
$registry->set('db', $db);

//设置log 开始
if (!defined( "LOGGER_CLASS_PATH" )) {
    define( "LOGGER_CLASS_PATH", $_SERVER['DOCUMENT_ROOT']."/../library/plog/");
}
include_once( LOGGER_CLASS_PATH."class/logger/loggermanager.class.php");
$log = & LoggerManager::getLogger( "default" );
$audit = & LoggerManager::getLogger( "audit" );
$registry->set('log', $log);
$registry->set('audit', $audit);
unset($log);
		
//设置log 结束


//允许上传的图片类型 开始
$PW_UPLOAD_PIC_MAP = array('jpg','gif','bmp','JPG','GIF','BMP');
$registry->set('PW_UPLOAD_PIC_MAP', $PW_UPLOAD_PIC_MAP);
//允许上传的图片类型 结束

//缩图工具类 开始
include_once($_SERVER['DOCUMENT_ROOT']."/../library/phpThumb/phpthumb.class.php");
$phpThumb = new phpThumb();
$registry->set('phpThumb', $phpThumb);
//缩图工具类 结束	

// 设置控制器
$frontController = Zend_Controller_Front::getInstance();
// 需要debug程序的时候把这一行打开就可以看到exception
//$frontController->throwExceptions(true);
$frontController->setControllerDirectory('../application/controllers');
//Zend_Layout::startMvc(array('layoutPath'=>'../application/views/layouts'));

// URL Rewrite
$frontController->setRouter(new Rewrite_PaowangRewrite());
$router = $frontController->getRouter();
Rewrite_RewriteUtils::addRewriteRules($router);

// Cache
$frontendOptions = array(
    'automatic_serialization' => true,
    'lifetime' => 36000
);

$backendFileOptions  = array(
    'cache_dir' => $config->cache->dir
);
$backendApcOptions = array();    
$backendOptions = array(
    'slow_backend' => 'File',
    'fast_backend' => 'Apc',
    'slow_backend_options' => $backendFileOptions,
    'fast_backend_options' => $backendApcOptions
);
$cache = null;
if($config->cache->apcEnabled){
    $cache = Zend_Cache::factory('Core',
                             'Two-Levels',
                             $frontendOptions,
                             $backendOptions);
}
else{
	$cache = Zend_Cache::factory('Core',
                             'File',
                             $frontendOptions,
                             $backendFileOptions);
}
$registry->set('cache', $cache); 
Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);          
$data_path = $config->data->path ;        
$classFileIncCache = $data_path . '/pluginLoaderCache.php';
if (file_exists($classFileIncCache)) {
    include_once $classFileIncCache;
}
if ($config->enablePluginLoaderCache) {
    Zend_Loader_PluginLoader::setIncludeFileCache($classFileIncCache);
}

// run!
$frontController->dispatch();