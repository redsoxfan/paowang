/* =============================================================================================
 * jsGears Image Uploader v1.0
 *
 * Copyright (c) 2008 Hunter Wu (http://jsgears.com/)
 * Code licensed under the MIT License.
 *
 * @class JSG.imgUploader
 * @Required jquery core
 * @Required jquery.form (http://malsup.com/jquery/form/)
 * =============================================================================================
 */
var JSG = JSG || {};

JSG.imgUploader = function(config) {

  //default config
 	var defConfig = {
    fileLimits: 1,                  //图档数量的限制 (-1 是不限制)
    actionUrl: null,                //图文件上传的处理程序
    fileInputName: 'myfile',        //档案输入框的名称
    inputContainer: null,           //档案输入框的放置位置
    previewContainer: null,         //预览图文件的放置位置
    hideInputIfReachLimits: true,   //达到档案数量限制时是否隐藏输入框，若否，则采用 disable
    confirmDeleteMsg: '确认删除?',
    previewClass: 'jsg_image_preview',
    elementPrefix: 'JSGImgUploader',
    loadingIcon: '/images/loading_indicator_big.gif',
    deleteIcon: '/images/icon_delete.gif',
    outputDelimiter: ':',
    uniqueId: null,
    currentFileCount:0,
    editFlag:0
 	};
 	config = $.extend(defConfig, config);

  //check containers
  config.inputContainer = $('#' + config.inputContainer);
  if (config.inputContainer.length == 0) {
    alert('Input container not exist!');
    return null;
  }
  config.previewContainer = $('#' + config.previewContainer);
  if (config.previewContainer.length == 0) {
    alert('Preview container not exist!');
    return null;
  }

  //generate unique id
  while (config.uniqueId == null) {
    var tmpId = parseInt(Math.random() * 10000, 10);
    if ($('#' + tmpId + '_form1').length == 0)
      config.uniqueId = tmpId;
  }

  var currentFileCount = config.currentFileCount;
  var currentFormCount = 0;
  var currentFormId = null;
  var files = [];

  /*
  files = [
    {
      available: true,
      ready: false,
      filename: ''
    },
    {
      available: false, //deleted
      ready: true,
      filename: '1.jpg'
    }
  ];
  */

  function toggleInputLimits() {
    if (config.fileLimits == -1 || (currentFileCount < config.fileLimits)) {
      if (config.hideInputIfReachLimits) {
        $('#' + currentFormId).show();
      } else {
        $('#' + currentFormId).children('input').attr('disabled', false);
      }
    } else {
      if (config.hideInputIfReachLimits) {
        $('#' + currentFormId).hide();
      } else {
        $('#' + currentFormId).children('input').attr('disabled', true);
      }
    }
  }

  function generatePreview(cnt) {
  	//$("input[type=file][name=myfile]").attr("disabled","disabled");  
  	
    var elmId = config.elementPrefix + config.uniqueId + '_preview' + cnt;
    $('<div class="' + config.previewClass + '" id="' + elmId + '" style="position: relative"></div>')
      .css('backgroundImage', 'url(' + config.loadingIcon + ')')
      .appendTo(config.previewContainer);
  }

  (function generateNewInput() {
    var uploadHandler = function() {
      files.push({
        available: true,
        postPicId:0,	
        ready: false,
        filename: ''
      });
      
          
      //$('#' + currentFormId).hide();
      //$('#' + currentFormId).children('input').hide();
      $('#image_input_block').hide();//上传时候隐藏
      var _seqid = parseInt($(this).attr('seqid'), 10);
      var handleUploadSuccess = function(data_text) {
        var fileseq = _seqid - 1;
        var elmId = config.elementPrefix + config.uniqueId + '_preview' + _seqid;
        files[fileseq].ready = true;
        //alert(data_text);
        var data = eval("("+data_text+")");

        //error
        if ('error' in data || !('success' in data)) {
          if ('error' in data)
            alert(data.error);
          else
            alert('unknow error!');
          $('#image_input_block').show();//上传失败重新显示
          files[fileseq].available = false;
          --currentFileCount;
          $('#' + elmId).fadeOut();
          toggleInputLimits();
          return;
        }

        files[fileseq].filename = data.success;
        files[fileseq].postPicId = data.postPicId;
        $("#postId").val(data.postId);
        var $deleteIcon = $('<div style="position: absolute; right: 0; top: 0; cursor: pointer"><img src="' + config.deleteIcon + '" /></div>')
          .click(function() {
            if (confirm(config.confirmDeleteMsg)) {
              //remove uploaded file
              $.post(encodeURI("/post/delpic/postPicId/"+files[fileseq].postPicId+"/editFlag/"+config.editFlag+"/"),function(data){});
              files[fileseq].available = false;
              --currentFileCount;
              $(this).parent().fadeOut();
              toggleInputLimits();
            }
          });
        $('#' + elmId)
          .css('backgroundImage', 'url(' + data.success + ')')
          .append($deleteIcon);
        //输入描述框 
        $inputArea =  $('<div style="position: absolute; left: 0; bottom: -25px;"><span></span><input type="text" value="" size="21" name="picDesc'+data.postPicId+'" id="picDesc'+data.postPicId+'"/></div>');
        $('#' + elmId)
            .append($inputArea);
        $('#image_input_block').show();//上传后重新显示
      }; //end of handleUploadSuccess
      
     var $forumIdInput = $('<input type="hidden" name="forumId"  value="'+$("#forumId").val()+'" />');
     var $postIdInput = $('<input type="hidden" name="postId"  value="'+$("#postId").val()+'" />');
     var $editFlagInput = $('<input type="hidden" name="editFlag"  value="'+config.editFlag+'" />');
     
     //var $titleInput = $('<input type="hidden" name="title"  value="'+$("#title").val()+'" />');
     //var $textInput = $('<input type="hidden" name="text"  value="'+$("#text").val()+'" />');
     //var $tagsInput = $('<input type="hidden" name="tags"  value="'+$("#tags").val()+'" />');
      $(this).parent().append($forumIdInput).append($postIdInput).append($editFlagInput);
      //.append($titleInput).append($textInput).append($tagsInput);
      //alert(currentFileCount);
      $(this).parent().ajaxSubmit({success: handleUploadSuccess, dataType: 'text'});
      ++currentFileCount;
      generatePreview(currentFormCount);
      generateNewInput();
      toggleInputLimits();
    }; //end of uploadHandler

    var preFormId = config.elementPrefix + config.uniqueId + '_form' + currentFormCount;
    $('#' + preFormId).hide();

    ++currentFormCount;
    currentFormId = config.elementPrefix + config.uniqueId + '_form' + currentFormCount;
    var currentInputId = config.elementPrefix + config.uniqueId + '_input' + currentFormCount;
    var $fileInput = $('<input type="file" id="' + currentInputId + '" name="' + config.fileInputName + '" style="height: 22px; border: 1px solid #cccccc;"/>')
      .change(uploadHandler)
      .attr('seqid', currentFormCount);
          $('#' + currentFormId).hide();   


	    $('<form id="' + currentFormId + '" action="' + config.actionUrl + '" method="POST" enctype="multipart/form-data" style="margin: 0 3px 0; display: inline"></form>')
	      .append($fileInput)
	      .appendTo(config.inputContainer);
     toggleInputLimits();
  }());

  //public functions
  return {
    isReady: function() {
      var len = files.length;
      for (var i = 0; i < len; ++i)
        if (files[i].available == true && files[i].ready == false)
          return false;
      return true;
    },
    getFiles: function() {
      var len = files.length;
      var filenames = [];
      for (var i = 0; i < len; ++i)
        if (files[i].available == true && files[i].ready == true)
          filenames.push(files[i].filename);
      return filenames.join(config.outputDelimiter);
    },
    getFilesAll: function() {
      var len = files.length;
      var filenames = [];
      for (var i = 0; i < len; ++i)
          filenames.push(files[i].postPicId+","+files[i].available+","+files[i].ready+","+files[i].filename);
      return filenames.join(config.outputDelimiter);
    },
    delIconClick: function(postPicId) {
        if (confirm(config.confirmDeleteMsg)) {
              //remove uploaded file
              $("#del_post_pic_id_str").val($("#del_post_pic_id_str").val()+","+postPicId);
              //$.post(encodeURI("/post/delpic/postPicId/"+postPicId+"/editFlag/1/"),function(data){});
              //alert(preid);
              //files[fileseq].available = false;
              --currentFileCount;
              //alert("3333");                            
              //alert(currentFileCount);
              $("#postpic"+postPicId).fadeOut();
              toggleInputLimits();
            }     
    } 
  };

};