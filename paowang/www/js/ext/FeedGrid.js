    /*
 * Ext JS Library 2.2.1
 * Copyright(c) 2006-2009, Ext JS, LLC.
 * licensing@extjs.com
 * 
 * http://extjs.com/license
 */

FeedGrid = function(config) {
    Ext.apply(this, config);

    var record = Ext.data.Record.create([
                  {name : 'entryTitle'},        
                  {name : 'author'},
                  {name : 'entryDateString'},
                  {name : 'entryUrl'},
                  {name : 'entryDescription'},
                  {name : 'blogUrl'}
                ]);
    this.store = new Ext.data.Store({
            reader: new Ext.data.JsonReader({}, record)
        });      

    this.columns = [{
        id: 'title',
        header: "标题",
        dataIndex: 'entryTitle',
        sortable:true,
        width: 420,
        renderer: this.formatTitle
      },{
        header: "作者",
        dataIndex: 'author',
        width: 100,
        sortable:true,
        renderer: this.formatAuthor
      }];

    FeedGrid.superclass.constructor.call(this, {
        region: 'center',
        //id: 'topic-grid',
        loadMask: {msg:'Loading Feed...'},

        sm: new Ext.grid.RowSelectionModel({
            singleSelect:true
        }),

        viewConfig: {
            forceFit:true,
            enableRowBody:true,
            showPreview:true,
            getRowClass : this.applyRowClass
        }
    });

    this.on('rowcontextmenu', this.onContextClick, this);
};

Ext.extend(FeedGrid, Ext.grid.GridPanel, {

    loadFeed : function(url) {
        this.store.baseParams = {
            feed: url
        };
        this.store.load();
    },

    togglePreview : function(show){
        this.view.showPreview = show;
        this.view.refresh();
    },

    // within this function "this" is actually the GridView
    applyRowClass: function(record, rowIndex, p, ds) {
        if (this.showPreview) {
            var xf = Ext.util.Format;
            p.body = '<p>' + xf.ellipsis(xf.stripTags(record.data.entryDescription), 200) + '</p>';
            return 'x-grid3-row-expanded';
        }
        return 'x-grid3-row-collapsed';
    },

    formatTitle: function(value, p, record) {
        return String.format(
                '<div class="topic"><b><a href="{1}" target=_blank>{0}</a></b></div>',
                value, record.data.entryUrl
                );
    },
    
    formatAuthor: function(value, p, record) {
        if(record.data.blogUrl != undefined && record.data.blogUrl != ''){
            return String.format(
                '<a href="{1}" target=_blank>{0}</a>',
                value, record.data.blogUrl
                );
        }
        return value;                
    }
});