FCKConfig.PaowangEditor = true ;

FCKConfig.AutoDetectLanguage = false ;
FCKConfig.DefaultLanguage = "zh-cn" ;

FCKConfig.LinkDlgHideTarget = true ;
FCKConfig.LinkUpload = false ;
FCKConfig.LinkDlgHideAdvanced = true ;
FCKConfig.LinkBrowser = false ;

FCKConfig.ImageDlgHideLink = true ;
FCKConfig.ImageUpload = false ;
FCKConfig.ImageDlgHideAdvanced = true ;
FCKConfig.ImageBrowser = false ;

FCKConfig.FlashUpload = false ;
FCKConfig.FlashDlgHideAdvanced = true ;
FCKConfig.FlashBrowser = false ;

FCKConfig.ToolbarStartExpanded = true ;
FCKConfig.EnterMode = 'div' ;			// p | div | br
FCKConfig.Plugins.Add( 'youtube', 'en,ja,zh-cn') ;

FCKConfig.ToolbarSets["Paowang"] = [
	['Bold','FontSize','TextColor','-','Link','Image','Flash','YouTube','-','Smiley','-','Source','-','Preview']
] ;