/*
 * For FCKeditor 2.3
 * 
 * 
 * File Name: zh-cn.js
 * 	Chinese(simplify) language file for the youtube plugin.
 * 
 * File Authors:
 * 		Uprush (uprushworld@yahoo.co.jp) 2008/03/22
 */
FCKLang['YouTubeTip']			= '视频插入/编辑' ;
FCKLang['DlgYouTubeTitle']		= '视频' ;
FCKLang['DlgYouTubeHelp']		= '拷贝视频嵌入html代码并粘贴到这里（以embed开始的代码）' ;
FCKLang['DlgYouTubeCode']		= '嵌入代码' ;
FCKLang['DlgAlertYouTubeCode']	= '请插入视频嵌入代码' ;
FCKLang['DlgAlertYouTubeSecurity']	= '非法的嵌入代码' ;
