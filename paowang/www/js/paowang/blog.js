Ext.namespace('Paowang');

Paowang.initBlog = function(){
    Ext.QuickTips.init();
    Ext.state.Manager.setProvider(new Ext.state.CookieProvider());

    var blogGrid0 = new FeedGrid({autoHeight: true});
    var blogGrid1 = new FeedGrid({height: 650});
    var blogGrid2 = new FeedGrid({height: 650});
    var blogGrid3 = new FeedGrid({height: 650});
    var blogGrid4 = new FeedGrid({height: 650});
    var blogGrid5 = new FeedGrid({height: 650});
    var authorGrid = new Paowang.PortalGrid({
        firstColumnName: '作者',
        firstColumnWidth: 120,
        secondColumnName: '博客',
        secondColumnWidth: 350,
        autoHeight: true,
        url: '/feed/getblogs',
        titleName: 'authorName',
        titleLinkName: 'blogUrl',
        noteName: 'blogTitle',
        noteLinkName: 'blogUrl'});
    
    var portlets = {
        'l1':   {
                    id: 'l1',
                    title: '文化',
                    layout:'fit',
                    //tools: getPorletTools('进入论坛', '/forum'),
                    items: blogGrid2
                },                
        'l2':   {
                    id: 'l2',
                    title: '生活',
                    layout:'fit',
                    //tools: getPorletTools('进入论坛', '/forum'),
                    items: blogGrid1
                },
        'l3':   {
                    id: 'l3',            
                    title: '科技',
                    layout:'fit',
                    //tools: getPorletTools('进入论坛', '/forum'),
                    items: blogGrid4
                },                
        'l4':   {
                    id: 'l4',        
                    title: '时事',
                    layout:'fit',
                    //tools: getPorletTools('进入论坛', '/forum'),
                    items: blogGrid3
                },
        'r1':   {
                    id: 'r1',        
                    title: '最新未分类',
                    layout:'fit',
                    //tools: getPorletTools('进入论坛', '/forum'),
                    items: blogGrid0
                },                   
        'r2':   {
                    id: 'r2',        
                    title: '艺术',
                    layout:'fit',
                    //tools: getPorletTools('进入论坛', '/forum'),
                    items: blogGrid5
                },
        'r3':   {
                    id: 'r3',            
                    title: '博客列表',
                    layout:'fit',
                    items: authorGrid.getPanel()
                }                              
    };

    var portal = new Paowang.Portal({
            margins:'35 5 5 0',
            stateKey: 'portalBlog09202009',
            portlets: portlets,
            bbar: [
                '->', 
                {
                text: '恢复页面', 
                tooltip: '页面板块可以用鼠标拖拉改变位置，点这里可以恢复。',
                handler: function(){
                    Ext.state.Manager.clear('portalBlog');
                    window.location.reload();
                }
                }],
            items:[{
                columnWidth:.5,
                style:'padding:10px 5px 10px 5px',
                items:[
                    portlets['l1'],
                    portlets['l2'],
                    portlets['l3'],
                    portlets['l4']
                ]
            },
            {
                columnWidth:.5,
                style:'padding:10px 5px 10px 5px',
                items:[
                    portlets['r1'],
                    portlets['r2'],
                    portlets['r3']
                ]
            }
            ]
            
    });

    var portalPanel = new Ext.Panel({
        layout:'fit',
        applyTo: 'blog_portal',
        border: false,
        height: 2950,
        items: portal
    });
    

    Ext.Ajax.request({
        url: '/feed/blogbytype',
        success: function(result, request){
            var data = Ext.decode(result.responseText);
            var type0 = data[0];
            var type1 = data[1];
            var type2 = data[2];
            var type3 = data[3];
            var type4 = data[4];
            var type5 = data[5];
            blogGrid0.getStore().loadData(type0);
            blogGrid1.getStore().loadData(type1);
            blogGrid2.getStore().loadData(type2);
            blogGrid3.getStore().loadData(type3);
            blogGrid4.getStore().loadData(type4);
            blogGrid5.getStore().loadData(type5);
            if(blogGrid0.getStore().getCount() == 0){
                var portlet0 = Ext.ComponentMgr.get('r1');
                portlet0.collapse(false);
            }
        }
        //failure: otherFn,
    });    
    
}

