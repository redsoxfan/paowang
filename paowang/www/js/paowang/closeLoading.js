function closeLoading()
{
    var ld=(document.all);
    var ns4=document.layers;
    var ns6=document.getElementById&&!document.all;
    var ie4=document.all;
    if (ns4)
        ld=document.loading;
    else if (ns6) {
        var temp = document.getElementById("loading");
        if(temp == undefined) {
            return;
        }
        ld=temp.style;
    }
    //else if (ie4) {
    //    var temp = document.all.loading;
    //    ld=temp.style;
    //}
        
    if(ld == undefined) return;    
    if(ns4){ld.visibility="hidden";}
    else if (ns6||ie4) ld.display="none";
}
closeLoading();