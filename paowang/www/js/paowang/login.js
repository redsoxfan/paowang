function getCookie(name) { 
    var search = name + "="; 
    if(document.cookie.length > 0){ 
        offset = document.cookie.indexOf(search); 
        if(offset != -1){ 
            offset += search.length ; 
            end = document.cookie.indexOf(";", offset); 
            if(end == -1){
                end = document.cookie.length; 
            }
            return unescape(document.cookie.substring(offset, end)); 
        } 
    }
    return "";   
}

function nl2br(str, is_xhtml) {
    var breakTag = '';
    breakTag = '<br />';
    if (typeof is_xhtml != 'undefined' && !is_xhtml) {
        breakTag = '<br>';
    }
    return (str + '').replace(/([^>]?)\n/g, '$1'+ breakTag +'\n');
}

function popupMessageBox(message, title, callback) {
	$('#message_box_content').empty();
    $('#message_box_content').html(message);
    if(title != undefined) {
        $('#message_box_title').html(title);    
    }
    if(callback != undefined){
        $('#confirmPopup').click(function(){callback();});
    }
    return hs.htmlExpand(document.getElementById('alert-message'), 
                            {   contentId: 'message-box-container', 
                                width: 350, 
                                height: 140, 
                                align: 'center',
                                wrapperClassName: 'login_highslide'
                                } );
                                    
}

//
// 登录相关
//
function isUserLogin() {
    if(getCookie("paowanguser")==''){
        return false;
    }
    return true;
}

function popupLoginDialog() {
    return hs.htmlExpand(document.getElementById('userLoginId'), 
                            {   contentId: 'login-container',
                                width: 400, 
                                height: 210, 
                                align: 'center',
                                wrapperClassName: 'login_highslide'} );
}

function closeLoginDialog() {
    hs.close('userLoginId');
}

function checkUserLogin() {
    if(isUserLogin()){
        return true;
    }    
    return popupLoginDialog();
}

function userLogin() {
    var username = $("#login #username").val();
    var password = $("#login #password").val();
    if(username == undefined || username == ''){
        $("#highslide_paowang").css("overflow","hidden");
        $("#login #msg").text("请输入用户名");
        return;
    } 
    if(password == undefined || password == ''){
        $("#highslide_paowang").css("overflow","hidden");
        $("#login #msg").text("请输入密码");
        return;
    }

    var flag =  $("input[@type=checkbox][@name=flag][@checked]","#login").val();
    if(flag==undefined){
    	flag = 0;
    }
    //alert(flag);
    var url ="/user/loginpro/name/"+$("#login #username").val()+"/password/"+$("#login #password").val()+"/flag/"+flag;
    $.ajax({
        url: encodeURI(url),
        cache: false,
        dataType: "text",
        success: function(data) {
            if(data.indexOf("success")!=-1){
                closeLoginDialog();
                document.location.reload();               
            }else{
            	$("#highslide_paowang").css("overflow","hidden");
            	$("#login  #msg").text("用户名或密码不正确，或者用户尚未激活。");
                return;
            }               
        },            
        complete: function(data) {
        }
    });
}

// Hit enter to login
function enterToLogin(keyEvent) {
    keyEvent = (keyEvent) ? keyEvent : window.event;

    if(keyEvent.type == "keyup" && keyEvent.keyCode == 13) {
        userLogin();
    } 
}

//
// 加帖子相关
//

function popupAddNewPostDialog() {
    /*return hs.htmlExpand(document.getElementById('popupNewPost'), 
                            {   contentId: 'highslide_add_new', 
                                width: 800, 
                                height: 700, 
                                align: 'center'
                                //preserveContent: false
                                } );
                                */
   $("#highslide_add_new").css("display","block");
   return false;

}

function checkAddNewPost() {
    if(isUserLogin()){
        return popupAddNewPostDialog();
    }    
    return popupLoginDialog();
}

function FCKEditorAjaxFix()
{
    this.UpdateEditorFormValue = function(){
        for ( i = 0; i < parent.frames.length; ++i ) {
            if ( parent.frames[i].FCK ){
                parent.frames[i].FCK.UpdateLinkedField();
            }
        }
    }
}

var fckEditorAjaxFixObject = new FCKEditorAjaxFix();

function submitAddNewPost(){
    
    fckEditorAjaxFixObject.UpdateEditorFormValue();
    if(!validateAddNewPost()){
        return false;
    }

    // HACK: Fix IE bug. 改动这个页面的form内容请注意，有可能影响form submit
    var queryString = $('#addNewPostForm').formSerialize(); 
//    alert(queryString);
    var subString = queryString;
    var positionToCut = queryString.indexOf('forumId', 7);
//    alert('postision is ' + positionToCut);
    if(positionToCut > 0){
        subString = queryString.substring(0, positionToCut-1);
    }
    
//    alert('subString is ' + subString); 
    
//    var formData = composeFormData();
//    alert('formData is ' + formData);
//    return false;
//    $('#addNewPostForm').submit();
//    $('#addNewPostForm').ajaxSubmit({
//            dataType: 'json',
//            data: subString,
////            data: $("#addNewPostForm").serialize(),
//            beforeSubmit: validateAddNewPost,
//            success: addNewPostSuccess
//        });
    $("#submit_button").attr("disabled",true);    
    $.ajax({
        type: "post",
        url: '/post/temp2online',
        dataType: "json",
        data: subString,
        success: function(data){
            addNewPostSuccess(data);
        }
    });
    return false; 
}

//function composeFormData(){
//    var postId = $('#postId').formSerialize();
//    alert('Serialize postId' + postId); 
//    alert('postId is' + $('#postId').val());
//    return postId;
//}

function cancelAddNewPost(){
    $.post(encodeURI("/post/postaddpiccancel/postId/"+$("$postId").val()+"/"),function(data){});
    //return closeAddPostWindow();
    $("#highslide_add_new").css("display","none");
    return false;    
}

function addNewPostSuccess(data){
    if(data != undefined){
        if(!data.success){
        	$("#submit_button").attr("disabled",false);         	
            popupMessageBox(data.messages.message, "加帖子");
        }
        else{
        	$("#submit_button").attr("disabled",false); 
            closeAddPostWindow();
            document.location.reload();    
        }
    }
    $("#submit_button").attr("disabled",false); 
}

function closeAddPostWindow(){
    return hs.close('popupNewPost');
}

function validateAddNewPost(){
    $("#addNewPostForm #msg").text("");
    var title = $.trim($('#title').val());
    if(title.length == 0){
        $("#title").focus();
        $("#addNewPostForm #msg").text("请输入标题");
        return false;
    }
    
    //var text = document.getElementById('text');
    /*
    var text = $.trim($('#text').val());
    if(text.length == 0){
        $("#text").focus();
        $("#addNewPostForm #msg").text("请输入帖子内容");
        return false;
    }
    */
    /*
    var tags = $.trim($('#tags').val());
    if(tags.length == 0){
        $("#tags").focus();     
        $("#addNewPostForm #msg").text("请输入帖子的标签");
        return false;
    }
    */   
    return true;
}

