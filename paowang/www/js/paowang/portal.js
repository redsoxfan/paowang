Ext.namespace('Paowang');

Paowang.Portal = function(config){
    Ext.ux.Portal.superclass.constructor.call(config);
};

Paowang.Portal = Ext.extend(Ext.ux.Portal, {
    portlets: null,
    stateKey: null,
    getPortlet: function(key){
        if(this.portlets != undefiend){
            return this.portlets[key];
        }
    },
    getStateInfo: function(){
        var o = {
            state:[[]]
        };
        
        for(var c = 0; c < this.items.getCount(); c++) {
            var col = this.items.get(c);    
            o.state[c] = [];
            if(col.items) {
                for(var s = 0; s < col.items.getCount(); s++) { 
                    var id = col.items.get(s).id;
                    if(id != undefined && id.indexOf('ext') < 0){
                        o.state[c].push(id);
                    }
                }
            }
        }
        return o;
    },
    onDrop: function(){
        var stateInfo = this.getStateInfo();
        Ext.state.Manager.set(this.stateKey, stateInfo);
    },
    initComponent:function() {
        var stateInfo = Ext.state.Manager.get(this.stateKey, undefined);
        if(stateInfo != undefined){
            for(var c = 0; c < this.items.length; c++) {
                if(stateInfo.state[c] && stateInfo.state[c].length != 0){
                    this.items[c].items = [];
                }
                for(var s = 0; s < stateInfo.state[c].length; s++) {
                    var id = stateInfo.state[c][s];
                    if(id != undefined){
                        this.items[c].items.push(this.portlets[id]);
                    }
	            }
	        }
        }
        Paowang.Portal.superclass.initComponent.apply(this, arguments);
        this.on('drop', this.onDrop.createDelegate(this));

    }
});

Ext.reg('paowangportal', Paowang.Portal); 

Paowang.PortalGrid = function(config){ 
    Ext.apply(this, config);
    this.init();
};

Paowang.PortalGrid.prototype = {
    id: null,
    name: null,
    url: null,
    urlParams: {},
    autoLoad: true,
    gridPanel: null,
    dataStore: null,
    height: 190,
    autoHeight: false,
    
    firstColumnName: '标题',
    firstColumnWidth: 350,
    secondColumnName: '作者',
    secondColumnWidth: 120,
    titleName: 'title',
    titleLinkName: 'titleLink',
    noteName: 'note',
    noteLinkName: 'noteLink',

    record: null,
    columnModel: null,
            
    getPanel: function(){
        return this.gridPanel;
    },     
    
    getName: function(){
        return this.name;
    },
    
    load: function(name, rows){
        this.name = name;
        this.dataStore.loadData(rows);
    }, 
    
    /**
    rowClicked: function(grid, rowIndex, columnIndex, e){
        var record = grid.getStore().getAt(rowIndex);  // Get the Record
        var link = record.get('link');
        window.open(link);    
    }, 
    **/     
    
    formatTitle: function(value, p, record) {
        if(record.data[this.titleLinkName] != undefined && record.data[this.titleLinkName] != ''){
            return String.format(
                '<div class="portal_grid_link"><a href="{1}" target=_blank>{0}</a></div>',
                value, record.data[this.titleLinkName]
                );
        }
        return value;
    },
    
    formatNote: function(value, p, record) {
        if(record.data[this.noteLinkName] != undefined && record.data[this.noteLinkName] != ''){
            return String.format(
                '<div class="portal_grid_link"><a href="{1}" target=_blank>{0}</a></div>',
                value, record.data[this.noteLinkName]
                );
        }
        return value;
    },                    
                
    init: function() {
        this.record = Ext.data.Record.create([
                  {name : this.titleName},        
                  {name : this.titleLinkName},
                  {name : this.noteName},
                  {name : this.noteLinkName}
                ]),
        this.columnModel = new Ext.grid.ColumnModel([{
                header: this.firstColumnName,
                sortable: true,
                width: this.firstColumnWidth,
                dataIndex: this.titleName,
                renderer: this.formatTitle.createDelegate(this)
            }, 
            {
                header: this.secondColumnName,
                sortable: true,
                width: this.secondColumnWidth,
                dataIndex: this.noteName,
                renderer: this.formatNote.createDelegate(this)
            }]);
        if(this.autoLoad){
            this.dataStore = new Ext.data.Store({
                autoLoad: true,
                proxy: new Ext.data.HttpProxy({
                    url: this.url,
                    method: 'POST'
                }),
	            reader: new Ext.data.JsonReader({}, this.record),
	            baseParams: this.urlParams
	        });   
        }
        else{
	        this.dataStore = new Ext.data.Store({
	            reader: new Ext.data.JsonReader({}, this.record)
	        });      
        }
        this.gridPanel = new Ext.grid.GridPanel({
            store: this.dataStore,
            cm :this.columnModel,
            loadMask: {msg: '下载中...'},
            //hideHeaders: true,
            autoWidth :true,
            //autoHeight: true,
            autoScroll :true,
            height: this.height
        }); 
        if(this.autoHeight){
            this.gridPanel.autoHeight = true;   
        }
        else{
            this.gridPanel.height = this.height;   
        }
        //this.gridPanel.on('cellclick', this.rowClicked.createDelegate(this));      
    }                
                    
}

function getPorletTools(text, url){
	var tools = [{
	        id:'maximize',
	        qtip: '进入' + text,
	        handler: function(){
	            window.open(url);
	        }
	    },{
	        id:'close',
	        qtip: '关闭窗口',
	        handler: function(e, target, panel){
	            panel.ownerCt.remove(panel, true);
	        }
	    }];
	return tools;    
}

Paowang.initPortal = function(){
    Ext.QuickTips.init();
    Ext.state.Manager.setProvider(new Ext.state.CookieProvider());

    // create some portlet tools using built in Ext tool ids
    var tools = [{
        id:'maximize',
        handler: function(){
            Ext.Msg.alert('Message', 'The Settings tool was clicked.');
        }
    },{
        id:'close',
        handler: function(e, target, panel){
            panel.ownerCt.remove(panel, true);
        }
    }];

    var generalForumGrid = new Paowang.PortalGrid({
        height: 420, 
        secondColumnName: "论坛", 
        url: '/index/generalforum'});
    var photoForumGrid = new Paowang.PortalGrid({
        height: 340, 
        secondColumnName: "论坛", 
        url: '/index/photoforum'});
    var tagMovieGrid = new Paowang.PortalGrid({
        height: 330, 
        secondColumnName: "作者",
        url: encodeURI('/index/singleblog'),
        urlParams: {feedUrl: 'http://movie.blog.paowang.net/feed/', blogUrl: 'http://movie.blog.paowang.net'}
        });
    var blogPoetGrid = new Paowang.PortalGrid({
        height: 330, 
        secondColumnName: "作者",
        url: encodeURI('/index/singleblog'),
        urlParams: {feedUrl: 'http://poet.blog.paowang.net/feed/', blogUrl: 'http://poet.blog.paowang.net'}
        });       
    var tagPpttGrid = new Paowang.PortalGrid({
        height: 330, 
        secondColumnName: "时间",
        url: encodeURI('/index/tagpost/tagName/普普通通')});
    var blogGrid = new Paowang.PortalGrid({
        height: 960,
        url: '/feed/paowangfeed',
        titleName: 'entryTitle',
        titleLinkName: 'entryUrl',
        noteName: 'author',
        noteLinkName: 'blogUrl'});
        
    var classicGrid = new Paowang.PortalGrid({height: 340, secondColumnName: "", autoLoad: false});
    var bestbookGrid = new Paowang.PortalGrid({
        height: 290, 
        autoLoad: false,
        titleLinkName: 'link',
        noteName: 'user'});
    var newsGrid = new Paowang.PortalGrid({
        height: 700, 
        secondColumnName: "时间", 
        autoLoad: false,
        titleLinkName: 'link',
        noteName: 'user'});
    
    var portlets = {
                'l1': {
                    id: 'l1',
                    title: '最新图片',
                    tools: getPorletTools('江湖色', '/photo'),
                    contentEl: 'portalPictures',
                    height: 360
                },
                'l2': {
                    id: 'l2',
                    title: '论坛聚合',
                    layout:'fit',
                    tools: getPorletTools('进入论坛', '/forum'),
                    items: generalForumGrid.getPanel()
                },
                'l3': {
                    id: 'l3',
                    title: '摄影论坛',
                    layout:'fit',
                    tools: getPorletTools('进入江湖色', '/photo'),
                    items: photoForumGrid.getPanel()
                },
                'l4': {
                    id: 'l4',
                    title: '看电影小组',
                    layout:'fit',
                    tools: getPorletTools('更多', 'http://movie.blog.paowang.net/'),
                    items: tagMovieGrid.getPanel()
                },
                'l5': {
                    id: 'l5',
                    title: '123诗社',
                    layout:'fit',
                    tools: getPorletTools('更多', 'http://poet.blog.paowang.net/'),
                    items: blogPoetGrid.getPanel()
                },
                'l6': {
                    id: 'l6',
                    title: '普普通通图片',
                    layout:'fit',
                    tools: getPorletTools('更多', '/forum/listbytagname/tagName/普普通通'),
                    items: tagPpttGrid.getPanel()
                },
                'r1': {
                    id: 'r1',
                    title: '洋洋大观',
                    layout:'fit',
                    tools: getPorletTools('洋洋大观', 'http://go.paowang.net/news/3'),
                    items: newsGrid.getPanel()
                },
                'r2': {
                    id: 'r2',
                    title: '泡网博客',
                    layout:'fit',
                    tools: getPorletTools('泡网博客', '/blog'),
                    items: blogGrid.getPanel()
                },
                'r3': {
                    id: 'r3',
                    title: '快乐老家',
                    layout:'fit',
                    tools: getPorletTools('快乐老家', 'http://go.paowang.net/cgi-bin/bestbook/bestbook.cgi'),
                    items: bestbookGrid.getPanel()
                },
                'r4': {
                    id: 'l7',
                    title: '过往专题',
                    layout:'fit',
                    tools: getPorletTools('旧首页', 'http://go.paowang.net'),
                    items: classicGrid.getPanel()
                }
            };
    
    var portal = new Paowang.Portal({
            margins:'35 5 5 0',
            stateKey: 'portalMain091204',
            portlets: portlets,
            bbar: [
                '->', 
                {
                text: '恢复页面', 
                tooltip: '页面板块可以用鼠标拖拉改变位置，点这里可以恢复。',
                handler: function(){
                    Ext.state.Manager.clear('portalMain');
                    window.location.reload();
                }
                }],
            items:[{
                columnWidth:.5,
                style:'padding:10px 5px 10px 5px',
                items:[
                    portlets['l1'],
                    portlets['l2'],
                    portlets['l3'],
                    portlets['l4'],
                    portlets['l5'],
                    portlets['l6']
                ]
            },
            {
                columnWidth:.5,
                style:'padding:10px 5px 10px 5px',
                items:[
                    portlets['r1'],
                    portlets['r2'],
                    portlets['r3'],
                    portlets['r4']
                ]
            }
            ]
            
            /*
             * Uncomment this block to test handling of the drop event. You could use this
             * to save portlet position state for example. The event arg e is the custom 
             * event defined in Ext.ux.Portal.DropZone.
             */
//            ,listeners: {
//                'drop': function(e){
//                    Ext.Msg.alert('Portlet Dropped', e.panel.title + '<br />Column: ' + 
//                        e.columnIndex + '<br />Position: ' + e.position);
//                }
//            }    
    });
    
    var portalPanel = new Ext.Panel({
        layout:'fit',
        applyTo: 'paowang_portal',
        border: false,
        height: 2650,
        items: portal
    });
    
    Ext.get('portalPictures').setDisplayed(true);
    /*
    Ext.Ajax.request({
        url: '/index/forumdata',
        success: function(result, request){
            var data = Ext.decode(result.responseText);
                for(var key in data){
                    forumGrids[key].load(data[key].name, data[key].data);
                }
            }
        //failure: otherFn,
    });

    */
      
    bestbookData.push({title: '', user: '=> 进入快乐老家', noteLink: 'http://go.paowang.net/cgi-bin/bestbook/bestbook.cgi'});
    bestbookGrid.load('快乐老家', bestbookData);
    newsData.push({title: '', user: '=> 旧版洋洋大观', noteLink: 'http://go.paowang.net/news/3'});
    newsData.push({title: '', user: '=> 新版洋洋大观', noteLink: 'http://yydg.paowang.net'});    
    newsGrid.load('洋洋大观', newsData);
    
    classicGrid.load('过往专题', [
        {title: '江湖之歌', note: '', titleLink: 'http://yydg.paowang.net/2014-01-02/11285.html'},
        {title: '江湖色影展', note: '', titleLink: 'http://go.paowang.net/news/15/'},
        {title: '江湖色精品', note: '', titleLink: 'http://go.paowang.net/news/11/'},
        {title: '泡网音乐杂志 ', note: '', titleLink: 'http://go.paowang.net/music/mag/'},
        {title: '江湖网刊', note: '', titleLink: 'http://go.paowang.net/mag/'},
        {title: '纪念绝色台北', note: '', titleLink: 'http://go.paowang.net/taipei/'},
        {title: '纪念王崴', note: '', titleLink: 'http://go.paowang.net/news/12/'}
    ]);
}

