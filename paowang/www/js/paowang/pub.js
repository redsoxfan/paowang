function   getLength(strValue)   {  
  //得到汉字长度，汉字视为两个
  var   len   =   0;  
  for(var   loop   =   0;   loop   <   strValue.length;   loop++)   {  
  len++;  
  if   (strValue.charCodeAt(loop)   >   255)   {  
  len++;  
  }  
  }  
  return   len;  
}   
  
function   trimstr(str,len){  
//   截取前len个字符     汉字视为两个    
//   如果出现汉字被拆开的时候   不加改汉字   长度为len-1  
  var   total=0;var   ret="";  
  for(var   i=0;i<str.length;i++){  
        var   nc=str.charCodeAt(i);  
	    var   m=(nc>255)?2:1;  
	    if(   total+m   <=   len   ){  
	        ret+=str.charAt(i);  
	        total+=m;  
	    }else{  
	        return   ret;  
	    }  
  }  
  return   ret;  
}