/*****************************************************
*[Partner Studio] (C)2008-2010 partner Inc.
*Author: SmallHe
*CreateDate:2008.3.15
*FileDesc:  会员注册验证
*BUG: 验证码获得值问题，需要使用AJAX解决
****************************************************/	
//表单获得与失去焦点的效果
function getFocus(obj){
	obj.style.color="#fe4b00";
	obj.style.background='#f1f5f8';
	
}
function getBlur(obj,x){
	obj.style.color='black';
	/*obj.style.background='white';*/
	switch (x)
 	{
 		case 1:
 			checkUser();
 	        break;
 	    case 2:
 	    	checkPassword();
 	        break;
 	    case 3:
			checkCpasswd();
			break;
		case 4:
			checkEmail();
			break;
		case 5:
			checkOnly();
			break;	
			
	}
}
//页面转向到注册页面的方法
function link()
{
	window.location.href="/usr/register";

} 


//检测用户名
 function checkUser()
 {
 	if(document.reg.userName.value==""){
 		name_info.innerHTML="<font color='red'>用户名不能为空!</font>"; 	
 	}else if(document.reg.userName.value.length<5 || document.reg.userName.value.length>20){
 		name_info.innerHTML="<font color='red'>用户名应该在5-20之间,请重新输入！</font>"; 
 	}else{
		name_info.innerHTML="<font color='green'>输入正确!</font>";
	}
}
 

//检测登录密码
function checkPassword()
{
	if(document.reg.password.value=="")
	{
		password_info.innerHTML="<font color='red'>密码不能为空!</font>";
	
	}else if(document.reg.password.value.length<3 || document.reg.password.value.length>18){

		password_info.innerHTML="<font color='red'>密码应该在3-18之间,请重新输入!</font>";
		}
		else{
			password_info.innerHTML="<font color='green'>输入正确!</font>";
		}
}


//检验密码长度...
function pwdLength(){
	var length=document.reg.password.value.length;
	if(length>2 && length<8)
	{
		password_info.innerHTML="<font color='red'>安全级别：弱.</font>";
	}else if(length>8 && length<15){
		password_info.innerHTML="<font color='#6688ff'>安全级别：中.</font>";
	}else if(length>15 && length<19){
		password_info.innerHTML="<font color='green'>安全级别：高.</font>";
	}else if(length>19){
		password_info.innerHTML="<font color='red'>密码过长，应该在3-18之间.</font>";
	}
}


//检测确认密码...
function checkCpasswd(){
	if(document.reg.ck_password.value != document.reg.password.value)
	{
		ckpassword_info.innerHTML="<font color='red'>两次输入的密码不符，请重新输入!</font>";
	}else if(document.reg.ck_password.value == ""){

		ckpassword_info.innerHTML="<font color='red'>重复密码不能为空！</font>";
	}else
	{
		ckpassword_info.innerHTML="<font color='green'>输入正确!</font>";
	}
}


//检测Email
function checkEmail()
{
	var str=document.reg.email.value;
	if(str=="")
	{
		email_info.innerHTML="<font color='red'>邮箱不能为空</font>";
	}else
	{
		var partent=/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/ ;
		if(partent.test(str))
		{
			email_info.innerHTML="<font color='green'>Email正确!</font> ";
		}else{
			
			email_info.innerHTML="<font color='red'>Email格式不对!</font> ";
		}
	}
}


//构造ajax引擎
function getHttpRequest()
{	
	var http_request=null;
	if(window.XMLHttpRequest) //检测firefox
	{
		http_request=new XMLHttpRequest();

		if(http_request.overrideMimeType)
		{
			http_request.overrideMimeType("text/xml");
		}
	}else if(window.ActiveXObject)  //检测ie
	{
		try{
			http_request=new ActiveXObject("Msxml2.XMLHTTP");	
		}catch(e){
			try{
				http_request=new ActiveXObject("Microsoft.XMLHTTP");
			}catch(e)
			{
				http_request=false;
			}

		}
	}
	return http_request;
}

//检测用户名唯一性
	var http_request=null;
	function checkOnly()
	{
		var name=document.reg.userName.value;
		http_request=getHttpRequest();
		http_request.onreadystatechange=process;
		
		var url="/user/registerpro/name/"+name+"/random/"+Math.random();
		http_request.open("get", url, true);
		http_request.send(null);
	}

	
	function process()
	{
		if(http_request.readyState==4)
		{
			if(http_request.status==200)
			{
			var str=http_request.responseText;
			//alert(str);
			//alert(Math.random());
			name_info.innerHTML="<font color='red'>"+str+"</font>";
			}else
			{
				alert("数据读取失败!");
			}
		}
	}
