package com.paowang.search;

import java.util.*;

public class PageUtil {
	/**
	  * config ,public
	  */
	private String page_name="page";//page标签，用来控制url页。比如说xxx.php?PB_page=2中的PB_page
	private String next_page=">";//下一页
	private String pre_page="<";//上一页
	private String first_page="First";//首页
	private String last_page="Last";//尾页
	private String pre_bar="<<";//上一分页条
	private String next_bar=">>";//下一分页条
	private String format_left="[";
	private String format_right="]";
	private boolean is_ajax=false;//是否支持AJAX分页模式

	/**
	  * private
	  *
	  */
	private int pagebarnum=10;//控制记录条的个数。
	private int totalpage=0;//总页数
	private String ajax_action_name="";//AJAX动作名
	private int nowindex=1;//当前页
	private String url="";//url地址头
	
	private String seq="&";//url 分割
	private int offset=0;

	/**
	  * constructor构造函数
	  *
	  * @param array $array["total"],$array["perpage"],$array["nowindex"],$array["url"],$array["ajax"]...
	  */
	public PageUtil(HashMap map)
	{
	 	

	     int total= Util.str2int((String)map.get("total"));
	     int perpage= Util.str2int((String)map.get("perpage"));
	     int nowindex= Util.str2int((String)map.get("nowindex"));
	     String url= Util.strNotNull((String)map.get("url"));
	     String page_name= Util.strNotNull((String)map.get("page_name"));
	     String ajax= Util.strNotNull((String)map.get("ajax"));
	     String seq= Util.strNotNull((String)map.get("seq"));
	     this.setPageName(page_name);   
    	 this._set_nowindex(nowindex);//设置当前页		 if(!"".equals(seq)){
			 this.seq=seq;//打开AJAX模式
		 }	
		 this._set_url(url);//设置链接地址
		 this.totalpage= (int)Math.ceil(total*1.0/(perpage*1.0));
		 this.offset= (this.nowindex-1)*perpage;
		 if(!"".equals(ajax)){
			 this.open_ajax(ajax);//打开AJAX模式
		 }
	 
	}

	
	
	public void setPageName(String pageName){
       this.page_name = pageName;
	}
	/**
	  * 打开倒AJAX模式
	  *
	  * @param string $action 默认ajax触发的动作。
	  */
	public void open_ajax(String action)
	{
	  this.is_ajax=true;
	  this.ajax_action_name=action;
	}
	/**
	  * 获取显示"下一页"的代码
	  *
	  * @param string $style
	  * @return string
	  */
	public String next_page(String style)
	{
	  if(this.nowindex<this.totalpage){
	   return this._get_link(this._get_url(this.nowindex+1),this.next_page,style);
	  }
	  //return "<span class=\""+style+"\">"+this.next_page+"</span>";
	  return "";
	}

	/**
	  * 获取显示“上一页”的代码
	  *
	  * @param string $style
	  * @return string
	  */
	public String pre_page(String style)
	{
	  if(this.nowindex>1){
	   return this._get_link(this._get_url(this.nowindex-1),this.pre_page,style);
	  }
	  //return "<span class=\""+style+"\">"+this.pre_page+"</span>";
	  return "";
	}

	/**
	  * 获取显示“首页”的代码
	  *
	  * @return string
	  */
	public String first_page(String style)
	{
	  if(this.nowindex==1){
	      return "<span class=\""+style+"\">"+this.first_page+"</span>";
	  }
	  return this._get_link(this._get_url(1),this.first_page,style);
	}

	/**
	  * 获取显示“尾页”的代码
	  *
	  * @return string
	  */
	public String last_page(String style)
	{
	  if(this.nowindex==this.totalpage){
	      return "<span class=\""+style+"\">"+this.last_page+"</span>";
	  }
	  return this._get_link(this._get_url(this.totalpage),this.last_page,style);
	}

	public String nowbar(String style,String nowindex_style)
	{
	  String rtn = "";	
	  int plus= (int)Math.ceil(this.pagebarnum*1.0/2.0);
	  if(this.pagebarnum-plus+this.nowindex>this.totalpage){
		  plus=(this.pagebarnum-this.totalpage+this.nowindex);
	  }
	  int begin=this.nowindex-plus+1;
	  begin=(begin>=1)?begin:1;
	  for(int i=begin;i<begin+this.pagebarnum;i++)
	  {
	   if(i<=this.totalpage){
	    if(i!=this.nowindex)
	        rtn+=this._get_text(this._get_link(this._get_url(i),""+i,style));
	    else
	    	rtn+=this._get_text("<span class=\""+nowindex_style+"\">"+i+"</span>");
	   }else{
	    break;
	   }
	   rtn+="\n";
	  }
	  return rtn;	
	}
	/**
	  * 获取显示跳转按钮的代码
	  *
	  * @return string
	  */
	public String select()
	{
	  String rtn="<select name=\"page\" onchange=\"pageform.submit();\">";
	  for(int i=1;i<=this.totalpage;i++)
	  {
	   if(i==this.nowindex){
		   rtn+="<option value=\""+i+"\" selected>"+i+"</option>";
	   }else{
		   rtn+="<option value=\""+i+"\">"+i+"</option>";
	   }
	  }
	  rtn+="</select>";
	  return rtn;
	}

	
	/**
	  * 获取显示跳转按钮的代码

	  *
	  * @return string
	  */
	public String select(int mode)
	{
	  String rtn="<select name=\"page\" onchange=\"pageform"+mode+".submit();\">";
	  for(int i=1;i<=this.totalpage;i++)
	  {
	   if(i==this.nowindex){
		   rtn+="<option value=\""+i+"\" selected>"+i+"</option>";
	   }else{
		   rtn+="<option value=\""+i+"\">"+i+"</option>";
	   }
	  }
	  rtn+="</select>";
	  return rtn;
	}	
	/**
	  * 获取mysql 语句中limit需要的值
	  *
	  * @return string
	  */
	public int offset()
	{
	  return this.offset;
	}
	

	/**
	  * 控制分页显示风格（你可以增加相应的风格）
	  *
	  * @param int $mode
	  * @return string
	  */
	public String show(int mode)
	{
	  String rtn = "";
	  switch (mode)
	  {
	   case 1:
	    this.next_page="下一页";
	    this.pre_page="上一页";
	    this.first_page="首页";
	    this.last_page="尾页";	    
	    rtn = "<form name=\"pageform1\" action=\""+this.url+"\" method=\"get\">"+this.first_page("")+this.pre_page(" ")+this.nowbar(" "," ")+this.next_page(" ")+this.last_page("")+"第"+this.select(mode)+"页"+"</form>";
	    break;
	   case 2:
	    this.next_page="下一页";
	    this.pre_page="上一页";
	    this.first_page="首页";
	    this.last_page="尾页";
	    rtn =  this.first_page("")+this.pre_page("")+"[第"+this.nowindex+"页] "+this.next_page("")+this.last_page("")+"第"+this.select()+"页 ";
	    break;
	   case 3:
	    this.next_page="下一页";
	    this.pre_page="上一页";
	    this.first_page="首页";
	    this.last_page="尾页";
	    rtn =  this.first_page("")+this.pre_page("")+this.next_page("")+this.last_page("");
	    
	    break;
	   case 4:
	    this.next_page="下一页";
	    this.pre_page="上一页";
	    rtn = this.pre_page("")+this.nowbar("","")+this.next_page("");
	    break;
	   case 5:
		rtn =  this.pre_bar+this.pre_page("")+this.nowbar("","")+this.next_page("")+this.next_bar;
	    break;
	   case 6:
		    this.next_page="下一页";
		    this.pre_page="上一页";
		    this.first_page="首页";
		    this.last_page="尾页";	    
		    rtn = "<form name=\"pageform6\" action=\""+this.url+"\" method=\"get\">"+this.first_page("")+this.pre_page(" ")+this.nowbar(" "," ")+this.next_page(" ")+this.last_page("")+"第"+this.select(mode)+"页"+"</form>";
		    break;	    
	  }
	  return rtn;
	  
	}
	/*----------------private function (私有方法)-----------------------------------------------------------*/
	/**
	  * 设置url头地址
	  * @param: String $url
	  * @return boolean
	  */
	public void _set_url(String url)
	{
//	  if(!"".equals(url)){
	   //手动设置
	   if(!this.seq.equals("/")){
	       this.url=url+((url.indexOf("?")>-1)?"&":"?")+this.page_name+"=";
	   }else{
		   this.url=url+((url.indexOf("?")>-1)?this.seq:this.seq)+this.page_name+this.seq;   
	   }
	   
	   
//	  }else{
//	      //自动获取
//	   if(empty($_SERVER["QUERY_STRING"])){
//	       //不存在QUERY_STRING时
//	    this.url=$_SERVER["REQUEST_URI"]."?".this.page_name."=";
//	   }else{
//	       //
//	    if(stristr($_SERVER["QUERY_STRING"],this.page_name."=")){
//	        //地址存在页面参数
//	     this.url=str_replace(this.page_name."=".this.nowindex,"",$_SERVER["REQUEST_URI"]);
//	     $last=this.url[strlen(this.url)-1];
//	     if($last=="?"||$last=="&"){
//	         this.url.=this.page_name."=";
//	     }else{
//	         this.url.="&".this.page_name."=";
//	     }
//	    }else{
//	        //
//	     this.url=$_SERVER["REQUEST_URI"]."&".this.page_name."=";
//	    }//end if   
//	   }//end if
//	  }//end if
	}

	/**
	  * 设置当前页面
	  *
	  */
	public void _set_nowindex(int nowindex)
	{
//		String rtn = "";
//	  if(nowindex==0){
//	   //系统获取
//	   if(isset($_GET[this.page_name])){
//	    this.nowindex=intval($_GET[this.page_name]);
//	   }
//	  }else{
	    //手动设置
	    this.nowindex=nowindex;
//	  }
//		return rtn;	
	}
	  
	/**
	  * 为指定的页面返回地址值
	  *
	  * @param int $pageno
	  * @return string $url
	  */
	public String  _get_url(int pageno)
	{
	  if(pageno==0){
		  pageno=1;
	  }	
	  return this.url+pageno;
	}

	/**
	  * 获取分页显示文字，比如说默认情况下_get_text("<a href="">1</a>")将返回[<a href="">1</a>]
	  *
	  * @param String $str
	  * @return string $url
	  */
	public String _get_text(String str)
	{
	  return this.format_left+str+this.format_right;
	}

	/**
	   * 获取链接地址
	*/
	public String _get_link(String url,String text,String style){
	  style = Util.strNotNull(style);	
	  style=("".equals(style))?"":"class=\""+style+"\"";
	  if(this.is_ajax){
	   //如果是使用AJAX模式
	   return "<a "+style+" href=\"javascript:"+this.ajax_action_name+"('"+url+"')\">"+text+"</a>";
	  }else{
	   return "<a "+style+" href=\""+url+"\">"+text+"</a>";
	  }
	}
	
	/**
	   * 出错处理方式
	*/
	public String error(String function,String errormsg)
	{
	     //die("Error in file <b>".__FILE__."</b> ,Function <b>".$function."()</b> :".$errormsg);
		return "";
	}



	public String getFormat_left() {
		return format_left;
	}



	public void setFormat_left(String format_left) {
		this.format_left = format_left;
	}



	public String getFormat_right() {
		return format_right;
	}



	public void setFormat_right(String format_right) {
		this.format_right = format_right;
	}
}
