package com.paowang.search;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.wltea.analyzer.lucene.IKAnalyzer;

public class PostDeleter {

	public static void main(String[] args) {
		delete();
	}
	
	public synchronized static void delete() {
		System.out.println("PostDeleter begin...");
        try {
        	
        	Directory directory = FSDirectory.getDirectory( PaowangUtil.INDEX_STORE_PATH, false);
            String url = "jdbc:mysql://localhost:3306/paowang_dev?user=root&password=wangleijxjy";
            String user = "root";  
            String password = "paowangpw";   //数据库密码为空  
            String sql = "select p.post_id from pw_post p where flag=1 and p.status=0 order by post_id desc";
            Connection conn = null;
            PreparedStatement pstmt = null;
            Statement stmt = null;
            
            try {  
                try {  
                	Class.forName("com.mysql.jdbc.Driver").newInstance(); 
                } catch (ClassNotFoundException e) {  
                    e.printStackTrace();  
                }  
                conn = DriverManager.getConnection(url);
                stmt = conn.createStatement();  
                ResultSet rs = stmt.executeQuery(sql);
                sql = "update pw_post set flag=2 where post_id=?";
                pstmt =conn.prepareStatement(sql);

                String post_id = "";
                
                while (rs.next()) {
                    post_id = rs.getString("post_id");
                    System.out.println(post_id);

                    
                    pstmt.setString(1, rs.getString("post_id"));
                    pstmt.executeUpdate();
                    IndexReader reader = IndexReader.open(directory);
                	Term term = new Term("post_id", post_id);
                	reader.deleteDocuments(term);
                	reader.close();
                }  
                rs.close();
            } catch (SQLException e) {  
                e.printStackTrace();  
            } finally{
            	if(pstmt!=null){
            		try {
    					pstmt.close();
    				} catch (SQLException e) {
    					e.printStackTrace();
    				}
            	}
            	if(stmt!=null){
            		try {
    					stmt.close();
    				} catch (SQLException e) {
    					e.printStackTrace();
    				}
            	}
            	if(conn!=null){
            		try {
            			conn.close();
    				} catch (SQLException e) {
    					e.printStackTrace();
    				}
            	}         	
            }
            
            
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
		System.out.println("PostDeleter end...");
	}
}
