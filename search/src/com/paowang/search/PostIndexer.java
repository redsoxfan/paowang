package com.paowang.search;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.wltea.analyzer.lucene.IKAnalyzer;

public class PostIndexer {

	public static void main(String[] args) {
		index();
	}
	
	public synchronized static void index() {
		System.out.println("PostIndexer begin...");
		// TODO:
		IndexWriter writer = null; 		
        try {
        	
//        	writer = new IndexWriter(FSDirectory.getDirectory(INDEX_STORE_PATH, false),
//    				new IKAnalyzer(), false,IndexWriter.MaxFieldLength.UNLIMITED);
        	Directory directory = FSDirectory.getDirectory( PaowangUtil.INDEX_STORE_PATH, false);
        	//IndexWriter.unlock(FSDirectory.getDirectory(PaowangUtil.INDEX_STORE_PATH)); 
        	//writer = new IndexWriter(directory, new IKAnalyzer(), true);
        	try{
        		//追加方式创建索引
        	    writer = new IndexWriter(PaowangUtil.INDEX_STORE_PATH, new IKAnalyzer(), false);
        	}catch(java.io.FileNotFoundException e){
        		//第一次创建索引
        		writer = new IndexWriter(PaowangUtil.INDEX_STORE_PATH, new IKAnalyzer(), true);
        	}
        	
        	
            String url = "jdbc:mysql://localhost:3306/paowang_dev?user=root&password=wangleijxjy";
        	//&useUnicode=true&characterEncoding=utf8
            String user = "root";  
            String password = "paowangpw";   //数据库密码为空  
            String sql = "select p.post_id,user_name author,title,pt.text content,get_forum_name(forum_id) forum_name,FROM_UNIXTIME(p.create_at) publish_date,get_post_pics(p.post_id) pics from pw_post p,pw_post_text pt where p.post_id=pt.post_id  and flag=0 and p.status=1 order by post_id desc limit 0,1000";
            Connection conn = null;
            PreparedStatement pstmt = null;
            Statement stmt = null;
            try {  
                try {  
                	Class.forName("com.mysql.jdbc.Driver").newInstance(); 
                } catch (ClassNotFoundException e) {  
                    e.printStackTrace();  
                }  
                conn = DriverManager.getConnection(url);
                stmt = conn.createStatement();  
                ResultSet rs = stmt.executeQuery(sql);
//    	        File f = new File("d:/cccc.txt");
//    			OutputStreamWriter theOutputStreamWriterModel = new OutputStreamWriter(new FileOutputStream(f), "UTF-8");
//    			BufferedWriter theBufferedWriter = new BufferedWriter(theOutputStreamWriterModel);
                sql = "update pw_post set flag=1 where post_id=?";
                pstmt =conn.prepareStatement(sql);

                String post_id = "";
                String author = "";
                String title = "";
                String content = "";
                String forumName = "";
                String publishDate = "";
                String pics = "";
                
                while (rs.next()) {
                    post_id = rs.getString("post_id");
                    author  = new String(rs.getString("author").getBytes("ISO-8859-1"),"utf-8");
                    title  = new String(rs.getString("title").getBytes("ISO-8859-1"),"utf-8");
                    content  = new String(rs.getString("content").getBytes("ISO-8859-1"),"utf-8");
                    forumName  = new String(rs.getString("forum_name").getBytes("ISO-8859-1"),"utf-8");
                    publishDate = rs.getString("publish_date");
                    pics  = new String(rs.getString("pics").getBytes("ISO-8859-1"),"utf-8");                	
                    //System.out.println(post_id+" "+author+" "+title+" "+content+" "+publishDate+" "+pics);
        			//theBufferedWriter.write((new String(rs.getString("text").getBytes("ISO-8859-1"),"utf-8")));
                    pstmt.setString(1, rs.getString("post_id"));
                    pstmt.executeUpdate();
                    
                	Document document = new Document();
//                	document.add(new Field("post_id", post_id, Store.YES, Index.UN_TOKENIZED));
//                	document.add(new Field("title", title, Store.YES, Index.TOKENIZED));
//                	document.add(new Field("content", content, Store.YES, Index.TOKENIZED));
//                	document.add(new Field("forumName", forumName, Store.YES, Index.UN_TOKENIZED));
//                	document.add(new Field("author", author, Store.YES, Index.UN_TOKENIZED));
//                	document.add(new Field("publishDate", publishDate, Store.YES, Index.UN_TOKENIZED));
//                	document.add(new Field("pics", pics, Store.YES, Index.NO));
                	document.add(new Field("post_id", post_id, Store.YES, Index.UN_TOKENIZED));
                	document.add(new Field("title", title, Store.YES, Index.TOKENIZED));
                	document.add(new Field("content", content, Store.YES, Index.TOKENIZED));
                	document.add(new Field("forumName", forumName, Store.YES, Index.TOKENIZED));
                	document.add(new Field("author", author, Store.YES, Index.TOKENIZED));
                	document.add(new Field("publishDate", publishDate, Store.YES, Index.TOKENIZED));
                	document.add(new Field("pics", pics, Store.YES, Index.NO));                	
                	writer.addDocument(document);
                }  
                rs.close();
//    			theBufferedWriter.flush();
//    			theBufferedWriter.close();            
            } catch (SQLException e) {  
                e.printStackTrace();  
            } finally{
            	if(pstmt!=null){
            		try {
    					pstmt.close();
    				} catch (SQLException e) {
    					e.printStackTrace();
    				}
            	}
            	if(stmt!=null){
            		try {
    					stmt.close();
    				} catch (SQLException e) {
    					e.printStackTrace();
    				}
            	}
            	if(conn!=null){
            		try {
            			conn.close();
    				} catch (SQLException e) {
    					e.printStackTrace();
    				}
            	}         	
            }
            
            writer.optimize();
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
		finally {
			if(writer!=null) {
				
				try {
					writer.close();
				}
				catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		}
		System.out.println("PostIndexer end...");
	}
}
