package com.paowang.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.analysis.Analyzer;  
import org.apache.lucene.document.Document;  
import org.apache.lucene.queryParser.MultiFieldQueryParser;  
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.BooleanClause;  
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;  
import org.apache.lucene.search.Query;  
import org.apache.lucene.search.ScoreDoc;  
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TopDocCollector;  
import org.apache.lucene.search.highlight.Highlighter;  
import org.apache.lucene.search.highlight.QueryScorer;  
import org.apache.lucene.search.highlight.SimpleFragmenter;  
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;   
import org.wltea.analyzer.lucene.IKAnalyzer;
public class PostSearcher {

	public static void main(String[] args) {
		
//		List name =FSDirectory.getField(PaowangUtil.INDEX_STORE_PATH,"title","山西");
//		
//		if(name.size()>0 && name != null){
//		for(int i=0;i<name.size();i++){
//		System.out.println(" 获取查询内容："+name.get(i));
//		}
//		}else{
//			System.out.println(" 获取查询内容为空！！");
//		}
		PostSearcher thePostSearcher = new PostSearcher();
		//thePostSearcher.test("河北",1,2);
        int pageNum = 4;
        int page = 1;
        int start = (page-1)*pageNum;
        String q = "山西";
		System.out.println(thePostSearcher.test(q,start,pageNum));

	}
	
	
	  public synchronized Analyzer getAnalyzer() {  
		    return new IKAnalyzer();  
	  }
	  
	  
	  public HashMap test(String q, int begin, int end) {  
			IndexSearcher searcher;
			List<String> text= new ArrayList<String>();
			HashMap rtn = new HashMap(); 
			try {
				searcher = new IndexSearcher(PaowangUtil.INDEX_STORE_PATH);//建立IndexSearcher对象需要比较长的时间，建议使用单例模式
				//Date date1 = new Date();
				Hits hits = null;
				Query query = null;
				//QueryParser qp = new QueryParser(Lkey,new StandardAnalyzer());//建立需要查询对象
				
				
				BooleanClause.Occur[] clauses = { BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD , BooleanClause.Occur.SHOULD , BooleanClause.Occur.SHOULD };
				query=MultiFieldQueryParser.parse(q,new String[]{"title","content","author","forumName"},clauses,getAnalyzer());
				
				
//				QueryParser qp = new QueryParser("content",new IKAnalyzer());//建立需要查询对象		
//				query = qp.parse(q.toLowerCase());// 建立Query
				
				//hits = searcher.search(query);// 获取查询结果
				hits = searcher.search(query,new Sort(new SortField("post_id", SortField.LONG, true)));// 获取查询结果
				
				  //System.out.println(" 数量："+hits.length());
			      SimpleHTMLFormatter simpleHTMLFormatter = new SimpleHTMLFormatter("<font color='red'>", "</font>");  
			      Highlighter highlighter = new Highlighter(simpleHTMLFormatter, new QueryScorer(query));  
			      highlighter.setTextFragmenter(new SimpleFragmenter(Integer.MAX_VALUE)); 				
				
				  if(end>=hits.length())
					  end=hits.length()-1;
					  List<HashMap<String,String>> list=new ArrayList<HashMap<String,String>>();
					  
					  for(int i=begin;i<=end;i++){
					   Document doc=hits.doc(i);
					   HashMap<String,String> docMap=new HashMap<String,String>();
					   docMap.put("post_id",doc.getField("post_id").stringValue());
					   docMap.put("title",doc.getField("title").stringValue());
					   docMap.put("content",doc.getField("content").stringValue());
					   docMap.put("publishDate",doc.getField("publishDate").stringValue());
					   docMap.put("forumName",doc.getField("forumName").stringValue());
					   docMap.put("author",doc.getField("author").stringValue());
					   docMap.put("pics",doc.getField("pics").stringValue());
					   docMap.put("summary",highlighter.getBestFragment(getAnalyzer(), "title", doc.get("content")+doc.get("title")));
					   list.add(docMap);
					  }
					  
				rtn.put("totalresults", ""+hits.length());
				rtn.put("list", list);
//				System.out.println("查询全部的数量需要： "
//						+ (new Date().getTime() - date1.getTime()) + "ms");
				
				} catch (IOException e) {
					e.printStackTrace();
				}catch (Exception eee) {
					eee.printStackTrace();
				}
				return rtn;
	  }

	  public String searchXml(String q, int begin, int end,int page,int perpagerow) {  
			IndexSearcher searcher;
			StringBuffer rtn = new StringBuffer();
			rtn.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			try {
				searcher = new IndexSearcher(PaowangUtil.INDEX_STORE_PATH);//建立IndexSearcher对象需要比较长的时间，建议使用单例模式
				Date date1 = new Date();
				Hits hits = null;
				BooleanQuery booleanQuery = new BooleanQuery(); 
		        String[] qArray = q.split("\\s+");
//		        System.out.println(qArray.length);
		        
		        for(String qrow : qArray){
					BooleanClause.Occur[] clauses = { BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD , BooleanClause.Occur.SHOULD , BooleanClause.Occur.SHOULD };
					Query query = MultiFieldQueryParser.parse(qrow,new String[]{"title","content","author","forumName"},clauses,getAnalyzer());
					booleanQuery.add(query,BooleanClause.Occur.MUST);
					//System.out.println(qrow);
		        }
				
				
//				QueryParser qp = new QueryParser("content",new IKAnalyzer());//建立需要查询对象		
//				query = qp.parse(q.toLowerCase());// 建立Query
				
				//hits = searcher.search(query);// 获取查询结果
				hits = searcher.search(booleanQuery,new Sort(new SortField("post_id", SortField.LONG, true)));// 获取查询结果
				
				//# new Sort(new SortField("id", SortField.STRING, true))
				//System.out.println(" 数量："+hits.length());
			      SimpleHTMLFormatter simpleHTMLFormatter = new SimpleHTMLFormatter("<font color='red'>", "</font>");  
			      Highlighter highlighter = new Highlighter(simpleHTMLFormatter, new QueryScorer(booleanQuery));  
			      
			      //highlighter.setTextFragmenter(new SimpleFragmenter(Integer.MAX_VALUE));
			      highlighter.setTextFragmenter(new SimpleFragmenter(100));
			      int pages = (int)Math.ceil(hits.length()*1.0/(perpagerow*1.0));
				  rtn.append("<rs total=\""+hits.length()+"\" start=\""+begin+"\" end=\""+end+"\" page=\""+page+"\"  perpage=\""+perpagerow+"\" pages=\""+pages+"\" time=\""+(new Date().getTime() - date1.getTime()) +"\">");
				  if(end>=hits.length())
					  end=hits.length()-1;
					  List<HashMap<String,String>> list=new ArrayList<HashMap<String,String>>();
					  String summary = "";
					  for(int i=begin;i<=end;i++){
					   Document doc=hits.doc(i);
					   HashMap<String,String> docMap=new HashMap<String,String>();
					   rtn.append("<r>");
					   rtn.append("<post_id>"+ doc.getField("post_id").stringValue()+"</post_id>");
					   rtn.append("<title>"+ Util.toHtml(doc.getField("title").stringValue())+"</title>");
					   rtn.append("<content>"+ Util.toHtml(doc.getField("content").stringValue())+"</content>");
					   rtn.append("<publishDate>"+doc.getField("publishDate").stringValue()+"</publishDate>");
					   rtn.append("<forumName>"+doc.getField("forumName").stringValue()+"</forumName>");
					   rtn.append("<author>"+Util.toHtml(doc.getField("author").stringValue())+"</author>");
					   rtn.append("<pics>"+doc.getField("pics").stringValue()+"</pics>");
					   summary = Util.toHtml(highlighter.getBestFragment(getAnalyzer(), "title", doc.get("content")+doc.get("title")));
					   if("".equals(summary)){
						   summary = Util.toHtml(doc.getField("content").stringValue());
					   }
					   rtn.append("<summary>"+summary+"</summary>");
					   //rtn.append("<summary>"+Util.toHtml(highlighter.getBestFragment(getAnalyzer(), "title", doc.get("content")+doc.get("title")))+"</summary>");
					   rtn.append("</r>");
					  }
				rtn.append("</rs>");	  
//				System.out.println("查询全部的数量需要： "
//						+ (new Date().getTime() - date1.getTime()) + "ms");
				
				} catch (IOException e) {
					e.printStackTrace();
					rtn.append("<rs n=\"0\">");
					rtn.append("</rs>");
				}catch (Exception eee) {
					eee.printStackTrace();
					rtn.append("<rs n=\"0\">");
					rtn.append("</rs>");					
				}
				return rtn.toString();
	  }	  

	  public String searchPostIdStr(String q) {  
			IndexSearcher searcher;
			StringBuffer rtn = new StringBuffer("");
			try {
				searcher = new IndexSearcher(PaowangUtil.INDEX_STORE_PATH);//建立IndexSearcher对象需要比较长的时间，建议使用单例模式
				Date date1 = new Date();
				Hits hits = null;
				BooleanQuery booleanQuery = new BooleanQuery(); 
		        String[] qArray = q.split("\\s+");
//		        System.out.println(qArray.length);
		        
		        for(String qrow : qArray){
					BooleanClause.Occur[] clauses = { BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD , BooleanClause.Occur.SHOULD , BooleanClause.Occur.SHOULD };
					Query query = MultiFieldQueryParser.parse(qrow,new String[]{"title","content","author","forumName"},clauses,getAnalyzer());
					booleanQuery.add(query,BooleanClause.Occur.MUST);
					//System.out.println(qrow);
		        }
				
				
//				QueryParser qp = new QueryParser("content",new IKAnalyzer());//建立需要查询对象		
//				query = qp.parse(q.toLowerCase());// 建立Query
				
				//hits = searcher.search(query);// 获取查询结果
				hits = searcher.search(booleanQuery,new Sort(new SortField("post_id", SortField.LONG, true)));// 获取查询结果
				
				//# new Sort(new SortField("id", SortField.STRING, true))
				//System.out.println(" 数量："+hits.length());
			      SimpleHTMLFormatter simpleHTMLFormatter = new SimpleHTMLFormatter("<font color='red'>", "</font>");  
			      Highlighter highlighter = new Highlighter(simpleHTMLFormatter, new QueryScorer(booleanQuery));  
			      
			      //highlighter.setTextFragmenter(new SimpleFragmenter(Integer.MAX_VALUE));
			      highlighter.setTextFragmenter(new SimpleFragmenter(100));
		
					  int end=hits.length()-1;
					  List<HashMap<String,String>> list=new ArrayList<HashMap<String,String>>();
					  for(int i=0;i<=end;i++){
					   Document doc=hits.doc(i);
					   HashMap<String,String> docMap=new HashMap<String,String>();
					   rtn.append(","+ doc.getField("post_id").stringValue());
					 }
				
				} catch (IOException e) {
					e.printStackTrace();
					rtn.append("");

				}catch (Exception eee) {
					eee.printStackTrace();
					rtn.append("");
				}
				return (rtn.toString().length()>0?rtn.toString().substring(1):"");
	  }	  
	  
	/*
	 * 分页
	 */	
	private List<HashMap<String,String>> processHits(Hits hits,int startIndex,int endIndex)throws Exception{
		  if(endIndex>=hits.length())
		   endIndex=hits.length()-1;
		  List<HashMap<String,String>> rtn=new ArrayList<HashMap<String,String>>();
		  
		  for(int i=startIndex;i<=endIndex;i++){
		   Document doc=hits.doc(i);
		   HashMap<String,String> docMap=new HashMap<String,String>();
		   docMap.put("post_id",doc.getField("post_id").stringValue());
		   docMap.put("title",doc.getField("title").stringValue());
		   docMap.put("content",doc.getField("content").stringValue());
		   docMap.put("publishDate",doc.getField("publishDate").stringValue());
		   docMap.put("forumName",doc.getField("forumName").stringValue());
		   docMap.put("author",doc.getField("author").stringValue());
		   docMap.put("pics",doc.getField("pics").stringValue());
		   rtn.add(docMap);
		  }
		  return rtn;
		 }	
	
}
