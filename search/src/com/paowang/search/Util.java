package com.paowang.search;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.*;

public class Util {

	public static String strNotNull(String param) {
		if (param == null)
			return "";
		param = param.trim();
		return param;
	}

	public static String strNotNull(String param, String defaultStr) {
		if (param == null)
			return defaultStr;
		param = param.trim();
		return param;
	}

	public static String strNotBlank(String in) {
		Pattern p = Pattern.compile("\\s*|\t|\r|\n");
		Matcher m = p.matcher(in);
		return m.replaceAll("");
	}

	public static int str2int(String s) {
		return str2int(s, 0);
	}

	public static int str2int(String s, int defu) {
		if (s != null && !(s = s.trim()).equals("")) {
			try {
				defu = Integer.parseInt(s);
			} catch (Exception e) {
				//do nothing
			}
		}
		return defu;
	}

	public static long str2long(String s) {
		return str2long(s, 0);
	}

	public static long str2long(String s, long defu) {
		if (s != null && !(s = s.trim()).equals("")) {
			try {
				defu = Long.parseLong(s);
			} catch (Exception e) {
				//do nothing
			}
		}
		return defu;
	}

	public static float str2float(String s) {
		if (s == null)
			return 0;
		try {
			return Float.parseFloat(s.trim());
		} catch (Exception e) {
			return 0;
		}
	}

	public static double str2double(String s) {
		if (s == null) {
			return 0;
		} else {
			String temp = "";
			for (int i = 0; i < s.length(); i++) {
				if (!s.substring(i, i + 1).equals(",")) {
					temp += s.substring(i, i + 1);
				}
			}
			s = temp;
		}
		try {
			return Double.parseDouble(s.trim());
		} catch (Exception e) {
			return 0;
		}
	}

	public static String double2format(double toFormat) {
		NumberFormat numFormat;
		numFormat = NumberFormat.getInstance();
		((DecimalFormat) numFormat).applyPattern("#,##0.00;(#,##0.00)");
		return ((DecimalFormat) numFormat).format(toFormat);
	}

	public static String double2format(double dNumber, int type) {
		if (type == 0) {
			return double2format(dNumber);
		}

		NumberFormat numFormat;
		numFormat = NumberFormat.getInstance();
		((DecimalFormat) numFormat).applyPattern("###0.00;###0.00");
		return ((DecimalFormat) numFormat).format(dNumber);
	}

	public static java.util.Calendar getNow() {
		return java.util.Calendar.getInstance();
	}

	public static java.util.Calendar getCalendar(String date_str,int day) {
		java.util.Calendar cal = Util.str2calendar(date_str);
		cal.add(Calendar.DATE, day);
		return cal;
	}
	
	
	
	
	public static String getNow19() {
		return Util.calendar2str(Util.getNow(), true);
	}

	public static int calendar2unixtime(Calendar cal) {
		return (int) (cal.getTime().getTime() / 1000L);
	}

	public static Calendar unixtime2calendar(int unixtime) {
		java.util.Calendar ret = java.util.Calendar.getInstance();
		ret.setTime(new java.util.Date(unixtime * 1000L));
		return ret;
	}

	public static String calendar2str(java.util.Calendar t, boolean showTime) {
		String s = "";
		int i;
		if (t == null)
			return s;
		s += (t.get(java.util.Calendar.YEAR) + 0) + "-";
		i = (t.get(java.util.Calendar.MONTH) + 1);
		s += ((i < 10) ? "0" + i : i + "") + "-";
		i = (t.get(java.util.Calendar.DAY_OF_MONTH) + 0);
		s += ((i < 10) ? "0" + i : i + "") + " ";
		if (showTime) {
			i = t.get(java.util.Calendar.HOUR_OF_DAY);
			s += ((i < 10) ? "0" + i : i + "") + ":";
			i = t.get(java.util.Calendar.MINUTE);
			s += ((i < 10) ? "0" + i : i + "") + ":";
			i = t.get(java.util.Calendar.SECOND);
			s += ((i < 10) ? "0" + i : i + "");
		}
		return s.trim();
	}

	public static String calendar2str14(java.util.Calendar t) {
		String s = "";
		int i;
		//if(t==null)t=Util.getNow();
		if (t == null)
			return s;
		s += (t.get(java.util.Calendar.YEAR) + 0) + "";
		i = (t.get(java.util.Calendar.MONTH) + 1);
		s += ((i < 10) ? "0" + i : i + "") + "";
		i = (t.get(java.util.Calendar.DAY_OF_MONTH) + 0);
		s += ((i < 10) ? "0" + i : i + "") + "";
		i = t.get(java.util.Calendar.HOUR_OF_DAY);
		s += ((i < 10) ? "0" + i : i + "") + "";
		i = t.get(java.util.Calendar.MINUTE);
		s += ((i < 10) ? "0" + i : i + "") + "";
		i = t.get(java.util.Calendar.SECOND);
		s += ((i < 10) ? "0" + i : i + "");
		return s.trim();
	}

	public static java.util.Calendar str2calendar(String sData) {
		java.util.Calendar ret = getNow();
		try {
			sData = sData.trim();
			if (sData.length() == 10) {
				sData += " 00:00:00";
			}
			ret = (Calendar.getInstance());
			String temp = sData;
			ret.set(str2int(temp.substring(0, 4)),
					str2int(temp.substring(5, 7)) - 1, str2int(temp.substring(
							8, 10)), str2int(temp.substring(11, 13)),
					str2int(temp.substring(14, 16)),
					str2int(temp.substring(17)));
		} catch (Exception e) {
			ret = null;
		}
		return ret;
	}



	public static String getUrlContent(String s, String s_ch, String t_ch)
			throws Exception {
		java.net.URL _url = new java.net.URL(s);
		java.net.HttpURLConnection huc = (java.net.HttpURLConnection) _url
				.openConnection();
		huc.setReadTimeout(300000);
		huc.setConnectTimeout(300000);
		return getInputStreamContent(huc.getInputStream(), s_ch, t_ch);
	}

	public static String getInputStreamContent(InputStream in, String s_ch,
			String t_ch) throws Exception {
		BufferedReader BR = null;
		try {
			BR = new BufferedReader(new InputStreamReader(in, s_ch));
			char[] c = new char[20 << 10];
			StringBuffer sb_in = new StringBuffer();
			int len = 0;
			while ((len = BR.read(c)) > 0) {
				sb_in.append(c, 0, len);
			}
			String reText = sb_in.toString();
			if (t_ch != null && t_ch.length() != 0) {
				try {
					reText = new String(reText.getBytes(s_ch), t_ch);
				} catch (Exception e) {
				}
			}
			return reText;
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (BR != null)
					BR.close();
			} catch (Exception e) {

			}
		}
	}

	public static String ymd2dmy(String sData) {
		String rtn = "";
		try {
			if ("".equals(sData)) {
				rtn = "";
			} else if (sData.length() < 10) {
				rtn = "";
			} else {
				rtn = sData.substring(8, 10) + "/" + sData.substring(5, 7)
						+ "/" + sData.substring(0, 4);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return rtn;
	}

    /**
     * 把字符串转换成符合xml语句的字符串
     * @param String s  : 要进行替换操作的字符串
     */
    public static String toHtml(String s) {
      if (s==null) return "";
//	      s = s.replace ('\u00a0',' ');
//	      s = s.replaceAll("\u00a0", "");
	      //s = s.replaceAll("\u001d", "");
	      s = s.replaceAll("&","&amp;");
	      s = s.replaceAll("<","&lt;");
	      s = s.replaceAll(">","&gt;");
	      s = s.replaceAll("\"","&quot;");
	      s = s.replaceAll("\"","&quot;");

	      Pattern p = Pattern.compile("\u00a0|\u2007|\u202F|\\p{javaWhitespace}");
	      Matcher m=p.matcher(s);
	      s = m.replaceAll(" ");
	      
      return s;
    }	

	public static void main(String[] args) throws Exception {
	}

		


}
